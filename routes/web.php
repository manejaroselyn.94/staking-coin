<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('auth/geetest','Auth\AuthController@getGeetest');

Route::get('test', 'TestController@getTest');
Route::get('cache', 'TestController@clearCache');
Route::get('date', 'TestController@getDateExpired')->name('getDate');

Route::get('/', 'System\IndexController@index')->name('getIndex');

Route::get('login', 'Auth\LoginController@getLogin')->name('getLogin');
Route::post('login', 'Auth\LoginController@postLogin')->name('postLogin');

Route::get('register', 'Auth\RegisterController@getRegister')->name('getRegister');
Route::post('/register', 'Auth\RegisterController@postRegister')->name('postRegister');

Route::get('active', 'Auth\RegisterController@getActive')->name('getActiveMail');
Route::post('loginCheckOTP','Auth\LoginController@postLoginCheckOTP')->name('postLoginCheckOTP');
Route::get('forgot-password', 'Auth\ForgotPasswordController@getForgotPassword')->name('getForgotPassword');
Route::post('forgot-password', 'Auth\ForgotPasswordController@postForgotPassword')->name('postForgotPassword');
Route::get('active-forgot-password', 'Auth\ForgotPasswordController@activePass')->name('activePass');
//Logout
Route::get('logout', 'Auth\LoginController@getLogout')->name('getLogout');

Route::group(['prefix' => 'system', 'middleware' => 'login'], function () {
    Route::get('/', 'System\DashboardController@getDashboard')->name('Dashboard');
    //member
    Route::group(['prefix' => 'member'], function () {
        Route::get('add', 'System\UserController@getAdd')->name('system.user.getAdd');
		Route::get('list', 'System\UserController@getList')->name('system.user.getList');
        Route::post('member-add', 'System\UserController@postMemberAdd')->name('system.user.addUserBinary');
        Route::get('tree', 'System\UserController@getTree')->name('system.user.getTree');
        Route::get('ajax-change-side-active', 'System\UserController@changeUserSideActivce')->name('changeSideActive');

        Route::get('profile', 'System\UserController@getProfile')->name('getProfile');
        // Route::post('profile', 'System\UserController@postProfile')->name('postProfile');
        Route::post('profile-v2', 'System\UserController@postProfileV2')->name('postProfileV2');
        Route::post('auth', 'System\UserController@postAuth')->name('postAuth');
        Route::post('change-password', 'Auth\ResetPasswordController@changePassword')->name('postChangePassword');
        Route::post('post-kyc', 'System\UserController@PostKYC')->name('system.user.PostKYC');
    });
    //wallet
    Route::group(['prefix' => 'wallet'], function () {
        Route::get('/', 'System\WalletController@getWallet')->name('system.getWallet');

        Route::get('deposit', 'System\WalletController@getDeposit')->name('system.getDeposit');

        Route::get('withdraw', 'System\WalletController@getWithdraw')->name('system.getWithdraw');
        Route::post('withdraw', 'System\WalletController@postWithdraw')->name('system.postWithdraw');
        Route::post('confirm-withdraw', 'System\WalletController@postWithdraw')->name('system.postConfirmWithdraw');

        Route::get('transfer', 'System\WalletController@getTransfer')->name('system.getTransfer');
        Route::post('transfer', 'System\WalletController@postTransfer')->name('system.postTransfer');
        Route::post('confirm-transfer', 'System\WalletController@postConfirmTranfer')->name('system.postConfirmTranfer');

        Route::get('swap', 'System\WalletController@getSwap')->name('system.getSwap');
        Route::post('swap', 'System\WalletController@postSwap')->name('system.postSwap');
        Route::post('swap-usd-coin', 'System\WalletController@postSwapUSDtoCoin')->name('system.postSwapUSDtoCoin');
        Route::post('swap-coin-usd', 'System\WalletController@postSwapCointoUSD')->name('system.postSwapCointoUSD');

	    Route::get('choose-refund/{id}', 'System\WalletController@getResendMailConfirm')->name('system.getResendMailConfirm');
    });
    Route::group(['prefix' => 'trade'], function () {
        Route::get('/', 'System\TradeController@getTrade')->name('system.getTrade');
    });
    //Invest
    Route::group(['prefix' => 'investment'], function () {
        Route::get('/', 'System\InvestmentController@getInvestment')->name('system.getInvestment');
        Route::post('investment', 'System\InvestmentController@postInvestment')->name('system.postInvestment');
	    Route::post('confirm-refund', 'System\InvestmentController@postReInvest')->name('system.postReInvest');
	    Route::post('confirm-bonus', 'System\InvestmentController@postSendADA')->name('system.postSendADA');
        //choose interest
        Route::post('choose-interest', 'System\InvestmentController@postChooseInterest')->name('system.postChooseInterest');


        Route::get('cancel-investment/{id}', 'System\InvestmentController@getCancelInvestment')->name('system.getCancelInvestment');
        Route::get('get-investment', 'System\InvestmentController@getInvestmentByID')->name('system.getInvestmentByID');
        Route::get('cancel-investment', 'System\InvestmentController@CancelInvestmentByID')->name('system.CancelInvestmentByID');
        //Rufund or Reinvestment

        Route::put('action/refund/{id}', 'System\InvestmentController@postActionRefund')->name('postActionRefund');
        Route::put('action/reinvestment/{id}', 'System\InvestmentController@postActionReinvestment')->name('postActionReinvestment');
        //get info pakage
        Route::get('package/{id?}', 'System\InvestmentController@getInfo_Package')->name('getInfo_Package');

    });
    //Ticket

    Route::group(['prefix' => 'ticket'], function () {
        Route::get('/', 'System\TicketController@getTicket')->name('Ticket');
        Route::post('post-ticket', 'System\TicketController@postTicket')->name('postTicket');
        Route::get('destroy-ticket/{id}', 'System\TicketController@destroyTicket')->name('destroyTicket');
        Route::get('get-ticket-detail/{id}', 'System\TicketController@getTicketDetail')->name('getTicketDetail');
        Route::get('ticket-admin', 'System\TicketController@getTicketAdmin')->name('getTicketAdmin');
        Route::get('update-status/{id}', 'System\TicketController@getStatusTicketAdmin')->name('getStatusTicketAdmin');
    });
    //Json
    Route::group(['prefix'=>'json']	, function (){
		Route::get('getAddress', 'System\CoinbaseController@getAddress')->name('system.json.getAddress');
		Route::get('coinbase', 'System\JsonController@getCoinbase')->name('system.json.getCoinbase');
	});
    Route::group(['prefix'=>'ajax'], function (){
        Route::get('ajax-user', 'System\UserController@getAjaxUser')->name('system.getAjaxUser');
        Route::get('ajax-sale-user', 'System\UserController@getAjaxSaleUser')->name('system.getAjaxSaleUser');
	});
    //History
    Route::group(['prefix' => 'history'], function () {
        Route::get('wallet', 'System\WalletController@getHistoryWallet')->name('system.history.getHistoryWallet');
        Route::get('commission', 'System\CommissionController@getHistoryCommission')->name('system.history.getHistoryCommisson');
        Route::get('investment', 'System\InvestmentController@getHistoryInvestment')->name('system.history.getHistoryInvestment');
        Route::get('interest', 'System\CommissionController@getInterest')->name('system.history.getInterest');

    });

    //Admin
    Route::group(['middleware'=>'check.permission','prefix'=>'admin'], function (){
        //fake data
        Route::get('fake-data', 'System\AdminController@getfakedata')->name('system.getfakedata');
        Route::post('fake-data', 'System\AdminController@postfakedata')->name('system.postfakedata');
        //Member
        Route::get('member', 'System\AdminController@getMemberListAdmin')->name('system.admin.getMemberListAdmin');
        Route::get('login/{id}', 'System\AdminController@getLoginByID')->name('system.admin.getLoginByID');
        Route::get('active-mail/{id}', 'System\AdminController@getActiveMail')->name('system.admin.getActiveMail');
        Route::post('edit-mail', 'System\AdminController@getEditMailByID')->name('system.admin.getEditMailByID');
        Route::get('disable-auth/{id}', 'System\AdminController@getDisableAuth')->name('system.admin.getDisableAuth');
        Route::get('edit-user/{id}', 'System\AdminController@getEditUser')->name('system.admin.getEditUser');
        Route::post('edit-user', 'System\AdminController@postEditUser')->name('system.admin.postEditUser');
        Route::get('edit-level/{id}/{level}', 'System\AdminController@getSetLevelUser')->name('system.admin.getSetLevelUser');

		Route::get('block-interest/{id}', 'System\AdminController@getBlockInterest')->name('system.admin.getBlockInterest');

		Route::get('depress-interest/{id}', 'System\AdminController@getDepressProfit')->name('system.admin.getDepressProfit');
        //Wallet
        Route::get('wallet', 'System\AdminController@getWallet')->name('system.admin.getWallet');
        Route::get('interest', 'System\AdminController@getInterest')->name('system.admin.getInterest');
        Route::post('deposit', 'System\AdminController@postDepositAdmin')->name('system.admin.postDepositAdmin');

		//change price
      	Route::get('price', 'System\AdminController@getPriceToken')->name('system.admin.getPriceToken');
		Route::post('add-price', 'System\AdminController@postAddPriceToken')->name('system.admin.postAddPriceToken');
		Route::post('price', 'System\AdminController@postPriceToken')->name('system.admin.postPriceToken');

        Route::get('wallet/detail/{id}', 'System\AdminController@getWalletDetail')->name('system.admin.getWalletDetail');
		Route::get('check-confirm/{id}', 'System\AdminController@getConfirm')->name('system.admin.getConfirm');
		Route::get('confirm/{id}', 'System\AdminController@getConfirm')->name('system.admin.getConfirm');
        //Invest
        Route::get('investment', 'System\AdminController@getAdminInvestmentList')->name('system.admin.InvestmentList');
        Route::post('investment', 'System\AdminController@postInvestAdmin')->name('system.admin.postInvestAdmin');
		Route::get('percent', 'System\AdminController@getPercent')->name('system.admin.getPercent');
		Route::post('percent', 'System\AdminController@postChangePercent')->name('system.admin.postChangePercent');

		Route::get('percent_depress', 'System\AdminController@getPercentDepress')->name('system.admin.getPercentDepress');
		Route::post('percent_depress', 'System\AdminController@postChangePercentDepress')->name('system.admin.postChangePercentDepress');

        Route::post('post-check-interest-list', 'System\AdminController@postCheckInterestList')->name('system.admin.postCheckInterestList');
        Route::get('edit-investment/{id}', 'System\AdminController@getEditInvestment')->name('system.admin.getEditInvestment');
        Route::post('post-investment', 'System\AdminController@postEditInvestment')->name('system.admin.postEditInvestment');

        //statistical
        Route::get('statistical', 'System\AdminController@getStatistical')->name('system.admin.getStatistical');
        //coinbase
        Route::get('coinbase', 'System\CoinbaseController@getCoinbase')->name('system.admin.getCoinbase');
        //Profile
        Route::get('profile', 'System\AdminController@getProfile')->name('system.admin.getProfile');
        Route::post('confirm-profile', 'System\AdminController@confirmProfile')->name('system.admin.confirmProfile');
        //Log Mail
        Route::get('log-mail', 'System\AdminController@getLogMail')->name('system.admin.getLogMail');
        //Log SOX
        Route::get('log-sox', 'System\AdminController@getLogSOX')->name('system.admin.getLogSOX');
        Route::group(['prefix' => 'manager-wallet'], function(){
            Route::get('fetch-data-money/{id}', 'System\AdminController@getFetchDataMoney')->name('getFetchDataMoney');
            Route::put('edit-data-money/{id}', 'System\AdminController@putEditDataMoney')->name('putEditDataMoney');
        });
        //ITO
        Route::get('trade-ito', 'System\TradeController@getTradeITOAdmin')->name('system.admin.getTradeITOAdmin');


        Route::get('check-balance', 'System\AdminController@getBalance')->name('system.admin.getBalance');

        //update doanh số trái phải
        Route::get('update-sales', 'System\BackupController@updateSalesAll')->name('updateSalesAll');

		Route::get('list-members', 'System\AdminController@getMemberSupport')->name('system.admin.getMemberSupport');
		Route::get('resent-mail-active/{id}', 'System\AdminController@getResentMailActive')->name('system.admin.getResentMailActive');
		Route::get('active-mail-member/{id}', 'System\AdminController@getActiveMailMember')->name('system.admin.getActiveMailMember');
    });
});
Route::group(['prefix'=>'cron'], function () {
    Route::get('check-pay', 'Cron\CronController@checkPayMoney')->name('cron.checkPayMoney');
    Route::get('deposit', 'Cron\CronController@getDeposit')->name('cron.getDeposit');
    Route::get('deposit-token', 'Cron\CronController@getDepositToken')->name('cron.getDepositToken');
    Route::get('deposit-usdt', 'Cron\CronController@getDepositUSDT')->name('cron.getDepositUSDT');
    Route::get('deposit-trx', 'Cron\CronController@getDepositTRX')->name('cron.getDepositTRX');
    Route::get('interest', 'Cron\CronController@getProfits')->name('system.cron.getProfits');
    Route::get('interest-skc', 'Cron\CronController@getProfitsSKC')->name('system.cron.getProfitsSKC');
    Route::get('send-bonus', 'Cron\CronController@cronSendTRX')->name('system.cron.cronSendTRX');
    Route::get('refund', 'Cron\CronController@checkRefundInvest')->name('system.cron.checkRefundInvest');
    Route::get('pay-token', 'Cron\CronController@sendToken')->name('system.cron.sendToken');
    Route::get('week-package-com', 'Cron\CronController@WeekPackageCommission')->name('system.cron.WeekPackageCommission');
    Route::get('week-weak-com', 'Cron\CronController@WeekWeakCommission')->name('system.cron.WeekWeakCommission');

    Route::get('total-sales', 'Cron\CronController@totalSalesMonth')->name('cron.totalSalesMonth');
    Route::get('auto-pay', 'Cron\CronController@AutoPayInterest')->name('cron.AutoPayInterest');
    Route::get('deposit-sox', 'Cron\CronController@getDepositSOX')->name('cron.getDepositSOX');
    Route::get('rate', 'Cron\CronController@getRateCoin')->name('cron.getRateCoin');
    Route::get('get-price', 'Cron\CronController@getPriceCoin')->name('cron.getPriceCoin');
    Route::get('set-price', 'Cron\CronController@setPriceCoin')->name('cron.setPriceCoin');

});
Route::get('checksum', 'Cron\Cron2Controller@checkSuminvestment');
Route::get('test-bot', 'TestController@testTelegramBot');
Route::get('test-mail2', 'TestController@testmail');
