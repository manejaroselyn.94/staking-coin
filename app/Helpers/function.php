<?php

use App\Model\Log;
if (!function_exists('floorp')) {

    function floorp($val, $precision)
    {
        $mult = pow(10, $precision); // Can be cached in lookup table        
        return floor($val * $mult) / $mult;
    }
}


