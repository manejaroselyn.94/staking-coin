<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use App\Model\User;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('user') && session('user')->User_Status == 1) {
            $user = User::find(session('user')->User_ID);
            if($user->User_Block == 1){
                return redirect()->route('getLogout');
            }
            /*if($user->User_Level != 1 && $user->User_Level != 5){
                Session::forget('user');
                return redirect()->route('getLogin')->with(['flash_level'=>'error', 'flash_message'=>'We are updating! Please come back later']);
            }*/
            return $next($request);
        }
        return redirect()->route('getLogin');
    }
}
