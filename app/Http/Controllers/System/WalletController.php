<?php

namespace App\Http\Controllers\System;

use App\Model\Money;
use App\Model\Profile;
use App\Model\User;
use App\Model\Wallet;
use App\Model\Investment;
use App\Model\GoogleAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use vendor\project\StatusTest;
use GuzzleHttp\Client;
use App\Model\Log;
use DB;
use App\Jobs\SendTelegramJobs;
use App\Jobs\SendMailJobs;
class WalletController extends Controller
{
    public $feeWithdraw = 0.05;
    public $feeTransfer = 0.05;
    
    public function getSwap(){
		$RandomToken = Money::RandomToken();
		$user = User::find(Session('user')->User_ID);
		$balance['SKC'] = $user->User_BalanceSKC;
		$balance['USDT'] = $user->User_BalanceDeposit;
		$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
		return view('System.Wallet.Swap', compact('balance', 'RandomToken', 'rate'));
    }
    
    public function postSwap(Request $req){
	    
        $user = User::where('User_ID', Session('user')->User_ID)->first();
      	if($user->User_BlockAction == 1){
          	//return redirect()->back();
        }
        $check_custom = $user->User_Level;
        if($check_custom == 4){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Your account can\'t use this function!"]);
        }
        $this->validate($req, [
            'coin_from' => 'required',
            'amount_from' => 'required|numeric|min:0',
            'coin_to' => 'required'
		]);
        //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        
        $arr_from_wallet = [
            5 => 'USD',
            8 => 'SKC',
        ];
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }

        $google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);
        if(!$valid){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
        $coin_from = $req->coin_from;
        $coin_to = $req->coin_to;
      	if(($coin_from == $coin_to)){
          return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Currency Error!']);
        }
        if((!isset($arr_from_wallet[$coin_from]) == $arr_from_wallet[$coin_to])){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Currency Error!']);
        }
        //đầu tư SKC mới thực hiện đc chức năng
      	if($coin_from == 8){
            $checkInvestSKC = Investment::checkInvestSKC($user->User_ID);
            if(!$checkInvestSKC){
                return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Please invest SKC to use this action!']);
            }
        }
        $symbolFrom = $arr_from_wallet[$coin_from];
        $symbolTo = $arr_from_wallet[$coin_to];
        $amount_from = $req->amount_from;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        //Balance
		$balance = User::getBalance($user->User_ID, $coin_from);
		$fee = $coin_from == 8 ? 0.1 : 0.01;
        if($amount_from > $balance){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Your balance is not enough']);
        }
        
        $amount_to = $amount_from*$rate[$symbolFrom]/$rate[$symbolTo];
        
        $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amount_from,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Swap Coin From '.$symbolFrom.' To '.$symbolTo,
			'Money_MoneyAction' => 12,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rate[$symbolFrom],
            'Money_CurrentAmount' => $amount_from,
			'Money_Currency' => $coin_from
        );
        DB::table('money')->insert($moneyArray);
        $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => $amount_to,
		    'Money_USDTFee' =>$amount_to*$fee,
		    'Money_Time' => time(),
			'Money_Comment' => 'Swap Coin From '.$symbolFrom,
			'Money_MoneyAction' => 12,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rate[$symbolTo],
            'Money_CurrentAmount' => $amount_to - ($amount_to*$fee),
			'Money_Currency' => $coin_to
        );
        DB::table('money')->insert($moneyArray);
        if($coin_from == 8){
            $updateBalance = User::where('User_ID', $user->User_ID)->increment('User_BalanceSKC', -$amount_from);
            $updateBalance = User::updateBalanceDeposit($user->User_ID, ($amount_to - ($amount_to*$fee)));
        }else{
            $updateBalance = User::updateBalanceDeposit($user->User_ID, -$amount_from);
            $updateBalance = User::where('User_ID', $user->User_ID)->increment('User_BalanceSKC', ($amount_to - ($amount_to*$fee)));
        }
		//update Balance
		// $updateblance = User::updateBalance($user->User_ID, $coin_from, -$amount_from);
		//update Balance
		// $updateblance = User::updateBalance($user->User_ID, $coin_to, ($amount_to - ($amount_to*$fee)) );
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Swap From $symbolFrom To $symbolTo Success!"]);
    }

    public function getResendMailConfirm($idMoney){
        $user = User::find(Session('user')->User_ID);
        $money = Money::find($idMoney);
        if(!$money || $money->Money_Confirm != 0){
			return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'This action isn\'t exist or confirmed']);
        }
        if($money->Money_MoneyAction != 17 && $money->Money_MoneyAction != 20){
			return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'This action invalid']);
        }
        $address = Wallet::getCurrentWallet($user->User_ID);
        if(!$address){
	        return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Please update your wallet']);
        }
	    $data = array('ID'=>$user->User_ID, 'amount'=>$money->Money_USDT+0, 'address'=>$address, 'id'=>$money->Money_ID);
		$token = urlencode(encrypt($data));
		$data['token'] = $token;
		return redirect()->route('system.investment', ['token'=>$token, 'id'=>$money->Money_ID]);
		
		
        dispatch(new SendMailJobs($user, $data, 'RefundInvest', 'Refund Investment'));
		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Please check your email to choose payment refund investment!']);
    }
    
    public function getDeposit(){
	    $user = User::find(Session('user')->User_ID);
	    $money = Money::where('Money_User', $user->User_ID)->where('Money_MoneyAction', 1)->get();
		return view('System.Wallet.Deposit', compact('money'));
    }
    public function getTransfer(){
		$RandomToken = Money::RandomToken();
		$balance['Deposit'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceDeposit');
		$balance['TRX'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceToken');
		$balance['SKC'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceSKC');
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();

	    $money = Money::join('currency', 'Money_Currency', 'Currency_ID')->join('moneyaction','MoneyAction_ID', 'Money_MoneyAction')->where('Money_User', Session('user')->User_ID)->where('Money_MoneyAction', 2)->get();
        
        $feeTransfer = $this->feeTransfer;
		return view('System.Wallet.Transfer', compact('balance', 'RandomToken', 'rate', 'feeTransfer', 'money'));
	}
	public function postTransfer(Request $req){
		
        //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }
        $user = User::find(Session('user')->User_ID);
		if($user->User_Block == 1){
			return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Account Error!']);
		}
      	if($user->User_BlockAction == 1){
          	return redirect()->back();
        }
        $this->validate($req, [
            'userID' => 'required',
            'amount' => 'required|numeric|min:0',
            'otp' => 'required',
            'currency' => 'required'
		]);
        //Bảo mật
        $checkProfile = Profile::where('Profile_User', $user->User_ID)->first();
		if(!$checkProfile || $checkProfile->Profile_Status != 1){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your Profile KYC Is Unverify!']);
		}
        $google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);
        if(!$valid){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
		if(!$req->amount || $req->amount <= 0){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Amount USD Invalid']);
        }
        //đầu tư SKC mới thực hiện đc chức năng
        $checkInvestSKC = Investment::checkInvestSKC($user->User_ID);
        if(!$checkInvestSKC){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Please invest SKC to use this action!']);
        }
        //ID người nhận
        $transferUserID  = $req->userID;
        //Check User tồn tại được nhận tiền có tồn tại không???
        $checkUser = User::where('User_ID', $transferUserID)->first();
        if(!$checkUser){
            //ngươi nhận không tồn tại
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'The user is not exist!']);
        }
        
        //Check Array Coin
        $arrCoin = [
            5 => 'USD',
            8 => 'SKC',
        ];
        $arrBalance = [
            5 => 'User_BalanceDeposit',
            8 => 'User_BalanceSKC',
        ];
        if(!isset($arrCoin[$req->currency])){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Invalid currency']);
        }
        //check balance
        $info = User::where('User_ID', Session('user')->User_ID)->first();
        $balanceString = $arrBalance[$req->currency];
        $balance = $info->$balanceString;
        //Fee
        $amountFee = $req->amount* $this->feeTransfer;
        //check m\amount balance
        if($req->amount + $amountFee > $balance){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your balance is not enough!']);
        }
        $symbol = $arrCoin[$req->currency];
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
        // trừ tiền người chuyển
	    $money = new Money();
		$money->Money_User = $user->User_ID;
		$money->Money_USDT = -($req->amount-$amountFee);
		$money->Money_USDTFee = $amountFee;
		$money->Money_Time = time();
		$money->Money_Comment = "Transfer $req->amount $symbol to UserID: $transferUserID";
		$money->Money_MoneyAction = 8;
		$money->Money_MoneyStatus = 1;
        $money->Money_Currency = $req->currency;
        $money->Money_CurrentAmount = $req->amount + $amountFee;
        $money->Money_Rate = $rate; 
        //Save
        $money->save();

        // cộng tiền cho người nhận
		$money = new Money();
		$money->Money_User = $transferUserID;
		$money->Money_USDT = $req->amount - $amountFee;
		$money->Money_USDTFee = 0;
		$money->Money_Time = time();
		$money->Money_Comment = "Give ".($req->amount - $amountFee)." $symbol from UserID: $user->User_ID";
		$money->Money_MoneyAction = 8;
        $money->Money_MoneyStatus = 1;
        $money->Money_Currency = $req->currency;
        $money->Money_CurrentAmount = $req->amount - $amountFee;
        $money->Money_Rate = $rate;
        $money->save();
        if($req->currency == 8){
            //Update Balance (-)
            User::updateBalance($user->User_ID, 'SKC', -$req->amount);
            //Update Balance (- )
            User::updateBalance($transferUserID, 'SKC', $req->amount - $amountFee);
        }else{
            //Update Balance thằng chuyển(-)
            $updateBalance_T = User::updateBalanceDeposit($user->User_ID, -($req->amount));
            //Update Balance thằng nhận(+)
            $updateBalance_R = User::updateBalanceDeposit($transferUserID, $req->amount - $amountFee);
        }

        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Transfer '.$req->amount.' '.$symbol.' To ID: '.$transferUserID.' Success!']);
    }
    public function getHistoryWallet()
    {
	    $user = Session('user');
        $money = DB::table('money')
            ->leftjoin('moneyaction', 'Money_MoneyAction', 'MoneyAction_ID')
            ->leftjoin('currency', 'Money_Currency', 'Currency_ID')
            ->where('Money_MoneyStatus', '<>', -1)
            ->where('Money_User', $user->User_ID)
            ->orderByDesc('Money_ID')
            ->paginate(15);
        
        return view('System.History.Wallet-History', compact('money'));
    }
	
	public function getWithdraw(){
		$RandomToken = Money::RandomToken();
		$balance['Deposit'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceDeposit');
		$balance['TRX'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceToken');
		$balance['SKC'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceSKC');
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();

	    $money = Money::join('currency', 'Money_Currency', 'Currency_ID')->join('moneyaction','MoneyAction_ID', 'Money_MoneyAction')->where('Money_User', Session('user')->User_ID)->where('Money_MoneyAction', 2)->get();
        
        $feeWithdraw = $this->feeWithdraw;
        
		return view('System.Wallet.Withdraw', compact('balance', 'RandomToken', 'rate', 'feeWithdraw', 'money'));
	}
    
    
    
    public function withdrawTRX(Request $req){
	    //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }

        //Validate
        $this->validate($req, [
		    'address' => 'required',
		    'otp' => 'required',
		    'amount' => 'required|numeric|min:0'
	    ]);
		
		$user = Session('user');
		if($user->User_Block == 1){
			return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Account Error!']);
		}
		if($user->User_Level == 4){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your account can\'t use this function!']);
		}
        //Bảo mật
		$checkProfile = Profile::where('Profile_User', $user->User_ID)->first();
		if(!$checkProfile || $checkProfile->Profile_Status != 1){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your Profile KYC Is Unverify!']);
		}
		$google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();

        if(!$AuthUser){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);
        if(!$valid){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
	    
		if(!$req->amount || $req->amount <= 0){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Amount SKC Invalid']);
		} 
		//sỐ TIỀN MUỐN RÚT
        $amount = $req->amount;
        //loại coin mà nó muốn nhận khi rút
        $coin_want_withdraw = 9;
        //Rút về Ví coin nào???
        $arr_coin_want_withdraw = [
            2 => [
				'symbol' => 'ETH'
            ],
			9 => [
				'symbol' => 'TRX'
			]
		];
		if(!isset($arr_coin_want_withdraw[$coin_want_withdraw])){

			return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Currency Invalid']);
        }
        $symbol = $arr_coin_want_withdraw[$coin_want_withdraw]['symbol'];

        //Các loại rate
        $rateCoin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
        
        $info = User::where('User_ID', $user->User_ID)->first();
        //ví usd
		$balance = $info->User_BalanceToken;
		$fee = $amount * $this->feeWithdraw;
		if($amount > $balance){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your Balance Isn\'t Enough!']);
        }
	    // đặt lệnh rút
		$money = new Money();
		$money->Money_User = $user->User_ID;
        $money->Money_USDT = -($amount - $fee);
		$money->Money_USDTFee = $fee;
		$money->Money_Time = time();
        $money->Money_MoneyAction = 21;
		$money->Money_MoneyStatus = 1;
		$money->Money_Comment = "Withdraw $amount $symbol to Address $req->address"; 
		$money->Money_Address = $req->address; 
        $money->Money_Currency = $coin_want_withdraw;
        $money->Money_CurrentAmount = ($amount - $fee);
		$money->Money_Rate = $rateCoin;
        $money->save();
        
        //Update Balance (- )
        User::updateBalance($user->User_ID, 'Token', -$amount);
    
		//Chưa làm
		$coin = $coin_want_withdraw;
		$priceCoin = $rateCoin;
		$message = $user->User_ID. " Withdraw $symbol\n"
						. "<b>User ID: </b>\n"
						. "$user->User_ID\n"
						. "<b>Email: </b>\n"
						. "$user->User_Email\n"
						. "<b>Address: </b>\n"
						. "$req->address\n"
						. "<b>Amount: </b>\n"
						. ($coin != 9 ? ($amount - $fee)/$priceCoin : ($amount - $fee))." $symbol\n"
						. "<b>Send Coin Time: </b>\n"
						. date('d-m-Y H:i:s',time());
				
		// dispatch(new SendTelegramJobs($message, -441418880));
		
        return redirect()->route('system.getWithdraw')->with(['flash_level'=>'success', 'flash_message'=>"Your withdrawal will be sent successfully within 24 hours. If you cannot receive it after 24h, please send feedback via mail support@stakingcoin.co, Thanks!!!"]);
    }
    public function withdrawSKC(Request $req){
	    //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }

        //Validate
        $this->validate($req, [
		    'address' => 'required',
		    'otp' => 'required',
		    'amount' => 'required|numeric|min:0'
	    ]);
		
		$user = Session('user');
		if($user->User_Block == 1){
			return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Account Error!']);
		}
		if($user->User_Level == 4){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your account can\'t use this function!']);
		}
        //Bảo mật
		$checkProfile = Profile::where('Profile_User', $user->User_ID)->first();
		if(!$checkProfile || $checkProfile->Profile_Status != 1){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your Profile KYC Is Unverify!']);
		}
		$google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();

        if(!$AuthUser){
		    return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);
        if(!$valid){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
	    
		if(!$req->amount || $req->amount <= 0){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Amount SKC Invalid']);
		} 
	    
		if($req->amount < 100){
            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Min Amount 100 SKC']);
		} 
		//sỐ TIỀN MUỐN RÚT
        $amount = $req->amount;
        //loại coin mà nó muốn nhận khi rút
        $coin_want_withdraw = 8;
        //Rút về Ví coin nào???
        $arr_coin_want_withdraw = [
            2 => [
				'symbol' => 'ETH'
            ],
			9 => [
				'symbol' => 'TRX'
			],
			8 => [
				'symbol' => 'SKC'
			]
		];
		if(!isset($arr_coin_want_withdraw[$coin_want_withdraw])){

			return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Currency Invalid']);
        }
        $symbol = $arr_coin_want_withdraw[$coin_want_withdraw]['symbol'];

        //Các loại rate
        $rateCoin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
        
        $info = User::where('User_ID', $user->User_ID)->first();
        //ví usd
		$balance = $info->User_BalanceSKC;
		$fee = $amount * $this->feeWithdraw;
		if($amount > $balance){

            return redirect()->route('system.getWithdraw')->with(['flash_level'=>'error', 'flash_message'=>'Your Balance Isn\'t Enough!']);
        }
	    // đặt lệnh rút
		$money = new Money();
		$money->Money_User = $user->User_ID;
        $money->Money_USDT = -($amount - $fee);
		$money->Money_USDTFee = $fee;
		$money->Money_Time = time();
        $money->Money_MoneyAction = 2;
		$money->Money_MoneyStatus = 1;
		$money->Money_Comment = "Withdraw $amount $symbol to Address $req->address"; 
		$money->Money_Address = $req->address; 
        $money->Money_Currency = $coin_want_withdraw;
        $money->Money_CurrentAmount = ($amount - $fee);
		$money->Money_Rate = $rateCoin;
        $money->save();
        
        //Update Balance (- )
        User::updateBalance($user->User_ID, 'SKC', -$amount);
        
		//Chưa làm
		$coin = $coin_want_withdraw;
		$priceCoin = $rateCoin;
		$message = $user->User_ID. " Withdraw $symbol\n"
						. "<b>User ID: </b>\n"
						. "$user->User_ID\n"
						. "<b>Email: </b>\n"
						. "$user->User_Email\n"
						. "<b>Address: </b>\n"
						. "$req->address\n"
						. "<b>Amount: </b>\n"
						. ($coin != 8 ? ($amount - $fee)/$priceCoin : ($amount - $fee))." $symbol\n"
						. "<b>Send Coin Time: </b>\n"
						. date('d-m-Y H:i:s',time());
				
		// dispatch(new SendTelegramJobs($message, -441418880));
		
        return redirect()->route('system.getWithdraw')->with(['flash_level'=>'success', 'flash_message'=>"Your withdrawal will be sent successfully within 24 hours. If you cannot receive it after 24h, please send feedback via mail support@stakingcoin.co, Thanks!!!"]);
    }
    public function postWithdraw(Request $req){
	    
		$user = User::find(Session('user')->User_ID);
      	if($user->User_BlockAction == 1){
          	return redirect()->back();
        }
	    if($req->from_wallet && $req->from_wallet == 9){
		    $this->withdrawTRX($req);
		    return redirect()->back();
	    }
	    if($req->from_wallet && $req->from_wallet == 8){
		    $this->withdrawSKC($req);
		    return redirect()->back();
	    }
	    //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        
        
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }

        //Validate
        $this->validate($req, [
		    'address' => 'required',
		    'otp' => 'required',
		    'amount' => 'required|numeric|min:0'
	    ]);
		if($user->User_Block == 1){
			return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Account Error!']);
		}
		if($user->User_Level == 4 || $user->User_Level == 5){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your account can\'t use this function!']);
		}
        //Bảo mật
		$checkProfile = Profile::where('Profile_User', $user->User_ID)->first();

		if(!$checkProfile || $checkProfile->Profile_Status != 1){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your Profile KYC Is Unverify!']);
		}
		$google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);
        if(!$valid){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
	    
		if(!$req->amount || $req->amount <= 0){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Amount USD Invalid']);
		} 
		if($req->amount < 20){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Min Withdraw 20 USDT']);
		} 
		//sỐ TIỀN MUỐN RÚT
        $amount = $req->amount;
        //loại coin mà nó muốn nhận khi rút
        $coin_want_withdraw = $req->coin_want_withdraw;
        //Rút về Ví coin nào???
        $arr_coin_want_withdraw = [
            2 => [
				'symbol' => 'ETH'
            ],
			9 => [
				'symbol' => 'TRX'
			]
		];
		if(!isset($arr_coin_want_withdraw[$coin_want_withdraw])){

			return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Currency Invalid']);
        }
        

        //Các loại rate
        $rateCoin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        
        $info = User::where('User_ID', $user->User_ID)->first();
        //ví usd
		$balance = $info->User_BalanceDeposit;
		$fee = $amount * $this->feeWithdraw;
		if($amount > $balance){

            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your Balance Isn\'t Enough!']);
        }
        $rate = $rateCoin[$arr_coin_want_withdraw[$coin_want_withdraw]['symbol']]*1.01;
        $currentAmount = ($amount - $fee) / $rate;
	    // đặt lệnh rút
		$money = new Money();
		$money->Money_User = $user->User_ID;
        $money->Money_USDT = -($amount - $fee);
		$money->Money_USDTFee = $fee;
		$money->Money_Time = time();
        $money->Money_MoneyAction = 2;
		$money->Money_MoneyStatus = 1;
		$money->Money_Comment = "Withdraw $amount USD to Address $req->address"; 
		$money->Money_Address = $req->address; 
        $money->Money_Currency = $coin_want_withdraw;
        $money->Money_CurrentAmount = $currentAmount;
		$money->Money_Rate = $rate;
        $money->save();
        
        //Update Balance (-)
        $updateBalance = User::updateBalanceDeposit($user->User_ID, -($amount));
    
		//Chưa làm
		$symbol = $arr_coin_want_withdraw[$coin_want_withdraw]['symbol'];
		$coin = $coin_want_withdraw;
		$priceCoin = $rate;
		$message = $user->User_ID. " Withdraw $symbol\n"
						. "<b>User ID: </b>\n"
						. "$user->User_ID\n"
						. "<b>Email: </b>\n"
						. "$user->User_Email\n"
						. "<b>Address: </b>\n"
						. "$req->address\n"
						. "<b>Amount: </b>\n"
						. ($amount - $fee)/$priceCoin." $symbol\n"
						. "<b>Amount USD: </b>\n"
						. "$".($amount - $fee)."\n"
						. "<b>Rate: </b>\n"
						. "$".$priceCoin."\n"
						. "<b>Send Coin Time: </b>\n"
						. date('d-m-Y H:i:s',time());
				
		dispatch(new SendTelegramJobs($message, -441418880));
		
		$data = ['User_Email'=>$user->User_Email, 'amount'=>$amount-$fee, 'amountCoin'=>$currentAmount, 'symbol'=>$symbol, 'amountFee'=>$fee];
		dispatch(new SendMailJobs('NoticeWithdraw', $data, 'Withdrawal Successful!', $user->User_ID));
		
        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>"Your withdrawal will be sent successfully within 24 hours. If you cannot receive it after 24h, please send feedback via mail support@stakingcoin.co, Thanks!!"]);
    }

    public function getAjaxUser(Request $req){
        //get address
        $userID = $req->userID;
        $result = User::where('User_ID', $userID)->first();
        if($result){
            return [
                'status' => true,
                'message' => 'Transfer to '.$result->User_Email,
                'class' => 'success'
            ];
        }
        else{
            return [
                'status' => true,
                'message' => 'Not found, please check again!',
                'class' => 'error'
            ];
        }

    
    }
}
