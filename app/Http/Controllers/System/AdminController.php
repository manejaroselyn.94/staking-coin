<?php

namespace App\Http\Controllers\System;

use App\Model\Investment;
use App\Model\Log;
use App\Model\Money;
use App\Model\Profile;
use App\Model\GoogleAuth;
use App\Model\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Excel;
use Illuminate\Support\Facades\Crypt;
use App\Jobs\SendMailJobs;
use Illuminate\Support\Facades\Hash;
use Validator;
use Coinbase\Wallet\Client as Client_CB;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Resource\Account;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money as CB_Money;
use App\Exports\WalletExport;
use App\Exports\WalletTempExport;
use App\Exports\InvesmentExport;
use App\Exports\UserExport;
class AdminController extends Controller
{
    
	public function getfakedata(){
		$data_set = DB::table('fake_data')->get();
		return view('System.Admin.Fakedata',compact('data_set'));
	}
	public function postfakedata(Request $request){
		$this->validate($request,[
           'value_fake' => 'required|numeric|min:0',
		]);
        $total_user = User::paginate(50);
        $total_user =	$total_user->total();
        DB::table('fake_data')->where('ID',1)
                                ->update(
                                    ['Value'=>$request->value_fake,'Total_values'=>$request->value_fake]
                                );
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Set SKC Fund Success']);
		
	}
    
    public function postInvestAdmin(Request $req){
	    $user = User::find(session('user')->User_ID);
        if ($user->User_Level != 1) {
            dd('stop');
        }
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        $arrCoin = [1 => 'BTC', 2 => 'ETH', 5 => 'USD', 8 => 'ADA'];
        $getInfo = User::where('User_ID', $req->user)->first();
        $amount = $req->amount;
        $typeInvest = $req->type;
        $coin = 5;
        if (!$getInfo) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error! User is not exist!']);
        }
        if (!$amount || $amount < 500) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error! Enter amount > 500!']);
        }
        $symbol = $arrCoin[$coin];
//         $priceCoin = $rate[$symbol];
        $priceCoin = 1;
        $rateToken = $rate['ADA'];
        if($coin == 5){
	        $priceCoin = 1;
	        $symbol = 'USDT';
        }
        //deposit
        $money = new Money();
        $money->Money_User = $getInfo->User_ID;
        $money->Money_USDT = $amount;
        $money->Money_Time = time();
        $money->Money_Comment = "Deposit ".($amount / $priceCoin)." $symbol";
        $money->Money_Currency = $coin;
        $money->Money_MoneyAction = 1;
        $money->Money_Address = '';
        $money->Money_CurrentAmount = $amount / $priceCoin;
        $money->Money_Rate = $priceCoin;
        $money->Money_MoneyStatus = 1;
        $money->save();
        
        //Bonus
        $get_amount_investment = Investment::where('investment_User', $getInfo->User_ID)
                            ->where('investment_Status', 1)
                            ->sum('investment_Amount');
        $sum_investment = $get_amount_investment + $amount;
        //khởi tạo
        $amount_bonus = 0;
        if($sum_investment >= 5100){
            //Lấy tổng tiền đã bonus
            $sum_bonus = Money::where('Money_User', $getInfo->User_ID)
                        ->where('Money_MoneyAction', 19)
                        ->where('Money_MoneyStatus', 1)
                        ->sum('Money_USDT');
                        
	        if($sum_investment < 20100){
	            $amount_bonus = $sum_investment - $sum_bonus;
	        }
	        elseif($sum_investment >= 20100){
	            // tổng đầu tư x2
	            $sum_investment = $sum_investment * 2;
	            $amount_bonus = $sum_investment - $sum_bonus;
	        }
	        $insertPoint = [];
	        if($amount_bonus > 0){
	            //chưa bonus thì sẽ bonus
	            $insertPoint = array(
	                'Money_User' => $getInfo->User_ID,
	                'Money_USDT' => $amount_bonus,
	                'Money_USDTFee' => 0,
	                'Money_Time' => time(),
	                'Money_Comment' => "Bonus Point Investment ".($amount_bonus+0)." ADA",
	                'Money_MoneyAction' => 19,
	                'Money_MoneyStatus' => 1,
	                'Money_Rate' => $rateToken,
	                'Money_CurrentAmount' => $amount_bonus,
	                'Money_Currency' => 8
	            );
	        }
        }
        //Trừ tiền 
	    $moneyArray = array(
		    'Money_User' => $getInfo->User_ID,
		    'Money_USDT' => -$amount,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Investment '.$amount.' USDT',
			'Money_MoneyAction' => 3,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $priceCoin,
            'Money_CurrentAmount' => $amount,
			'Money_Currency' => 5
        );
        //Invest
        $invest = array(
		    'investment_User' => $getInfo->User_ID,
            'investment_Amount' => $amount,
		    'investment_Rate' => $priceCoin,
		    'investment_Currency' => 5,
		    'investment_Time' => time(),
		    'investment_Package' => $typeInvest,
            'investment_Status' => 1,
            'investment_Bonus' => 0,
            'investment_Total' => 0
        );
        //Cộng điểm

	    // thêm dữ liệu
	    DB::table('investment')->insert($invest);
        DB::table('money')->insert($moneyArray);
        //$typeInvest = 1 là gói thiệt ngược lại là gói hỗ trợ
	    if($amount_bonus > 0){
	        //điều kiện tặng coin
// 	        DB::table('money')->insert($insertPoint);
	        
	    }
	    //Update updatePoint
        $updatePoint = User::updatePoint($user->User_ID, $amount_bonus);
	    //Update Investment
	    $updateInvestment = User::updateInvestment($getInfo->User_ID, $amount);
	    //Update Sales Tree
	    $updateBranchSales = app('App\Http\Controllers\System\InvestmentController')->updateBranchSalesTree($getInfo->User_ID);
        if($typeInvest == 1){ 
		    //checkDirectCom
		    app('App\Http\Controllers\Cron\CronController')->checkCommission($user, $amount);
		    //checkBinaryCom
		    app('App\Http\Controllers\Cron\CronController')->checkBinaryCommission($getInfo, $amount);
		    //update level
	        app('App\Http\Controllers\Cron\CronController')->UpdateLevel($user);
        }else{
		    //checkDirectCom
		    app('App\Http\Controllers\Cron\CronController')->checkCommission($user, $amount, 1);
		    //checkBinaryCom
		    app('App\Http\Controllers\Cron\CronController')->checkBinaryCommission($getInfo, $amount, 1);
        }
        
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Deposit $getInfo->User_ID $amount $symbol Success!"]);

    }
    
	public function getPercent(){
		if(Session('user')->User_Level != 1){
			return redirect()->back();
		}
		$percent[1] = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 1)->orderBy('Percent_Time')->get();
		$percent[2] = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 2)->orderBy('Percent_Time')->get();
		$percent[3] = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 3)->orderBy('Percent_Time')->get();
		$percent[4] = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 4)->orderBy('Percent_Time')->get();
		$percent[5] = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 5)->orderBy('Percent_Time')->get();
		$arrMinMax = [1=>['min'=>400,'max'=>5500], 2=>['min'=>5500,'max'=>10500], 3=>['min'=>10500,'max'=>30500], 4=>['min'=>30500,'max'=>60500], 5=>['min'=>60500,'max'=>'Infinity']];
        
		return view('System.Admin.Percent', compact('percent', 'arrMinMax'));
	}
	
	public function postChangePercent(Request $req){
		if(Session('user')->User_Level != 1){
			dd('stop');
		}
		$percent = DB::table('profit')->where('Percent_ID', $req->ID)->update(['Percent_Percent'=>($req->Percent/100)]);
		
		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Change % Success!']);

	}
    
	public function getPercentDepress(){
		if(Session('user')->User_Level != 1){
			return redirect()->back();
		}
		$percent[1] = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 1)->orderBy('Percent_Time')->get();
		$percent[2] = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 2)->orderBy('Percent_Time')->get();
		$percent[3] = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 3)->orderBy('Percent_Time')->get();
		$percent[4] = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 4)->orderBy('Percent_Time')->get();
		$percent[5] = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')->where('Percent_Time', '>=', date('Y-m-d'))->where('Percent_Package', 5)->orderBy('Percent_Time')->get();
		$arrMinMax = [1=>['min'=>400,'max'=>5500], 2=>['min'=>5500,'max'=>10500], 3=>['min'=>10500,'max'=>30500], 4=>['min'=>30500,'max'=>60500], 5=>['min'=>60500,'max'=>'Infinity']];
        
		return view('System.Admin.PercentDepress', compact('percent', 'arrMinMax'));
	}
	
	public function postChangePercentDepress(Request $req){
		if(Session('user')->User_Level != 1){
			dd('stop');
		}
		$percent = DB::table('profit_depress')->where('Percent_ID', $req->ID)->update(['Percent_Percent'=>($req->Percent/100)]);
		
		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Change % Success!']);

	}
	
    
    public function getSetLevelUser($id, $level){
	    if (Session('user')->User_Level == 1) {
	        $levelArr = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'TechCare');
	        $info = User::find($id);
            if ($info) {
	            $info->User_Level = $level;
	            $info->save();
	            
                $cmt_log = "Set Level: ".$levelArr[$level]." ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Set Level User", 0, $cmt_log);
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Set Level: ".$levelArr[$level]." ID User: " . $id ." Successfully!"]);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'User Not Found!']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error!']);
    }
    
    public function getMemberListAdmin(Request $req)
    {
        $user = Session::get('user');
        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'TechCare');
        if ($user->User_Level != 1 && $user->User_Level != 2 && $user->User_Level != 3) {
            dd('Stop');
        }
        $where = null;
        if ($req->UserID) {
            $where .= ' AND User_ID=' . $req->UserID;
        }
        if ($req->Username) {
            $where .= ' AND User_Name LIKE "' . $req->Username . '"';
        }
        if ($req->Email) {
            $where .= ' AND User_Email LIKE "' . $req->Email . '"';
        }
        if ($req->sponsor) {
            $where .= ' AND User_Parent = ' . $req->sponsor;
        }
        if ($req->agency_level) {
            $where .= ' AND User_Agency_Level = ' . $req->agency_level;
        }
        if ($req->datetime) {
            $where .= ' AND date(User_RegisteredDatetime) = "' . date('Y-m-d', strtotime($req->datetime)) . '"';
        }
        if ($req->status_email != null) {
            $where .= ' AND User_EmailActive = ' . $req->status_email;
        }
        if ($req->level != null) {
            $where .= ' AND User_Level = ' . $req->level;
        }
        if ($req->tree != '') {

            $where .= ' AND User_Tree LIKE "' . str_replace(', ', ',', $req->tree) . '%"';
        }
        if ($req->suntree != '') {

            $where .= ' AND User_SunTree LIKE "' . str_replace(', ', ',', $req->suntree) . '%"';
        }
        if ($req->export == 1) {
            if ($user->User_Level != 1 && $user->User_Level != 2 && $user->User_Level != 3) {
                dd('Stop');
            }
            $Member = User::leftJoin('google2fa', 'google2fa.google2fa_User', 'users.User_ID')
                ->whereRaw('1 ' . $where)
                ->orderBy('User_RegisteredDatetime', 'DESC')->get();

            ob_end_clean();
            ob_start();
            return Excel::download(new UserExport($Member), 'UserExport.xlsx');
            // $member = array();
            // foreach ($Member as $h) {
            //     if ($h->User_EmailActive == 1) {
            //         $h->User_EmailActive = "Active";
            //     } else {
            //         $h->User_EmailActive = "Not Active";
            //     }
            //     $member[] = $h;
            // }
            // //xuất excel
            // $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer');
            // // $listMemberExcel[] = array('ID','Email', 'ID Parent','Registred DateTime','Level','Status') ;
            // $listMemberExcel[] = array('ID', 'Email', 'Registred DateTime', 'ID Parent', 'Tree', 'Level', 'Status', 'Auth');
            // $i = 1;
            // foreach ($member as $d) {
            //     $listMemberExcel[$i][0] = $d->User_ID;
            //     $listMemberExcel[$i][1] = $d->User_Email;
            //     $listMemberExcel[$i][2] = $d->User_RegisteredDatetime;
            //     $listMemberExcel[$i][3] = $d->User_Parent;
            //     $listMemberExcel[$i][4] = $d->User_Tree;
            //     $listMemberExcel[$i][5] = $level[$d->User_Level];
            //     $listMemberExcel[$i][6] = $d->User_EmailActive;
            //     if ($d->google2fa_User) {
            //         $listMemberExcel[$i][7] = "Enable";
            //     } else {
            //         $listMemberExcel[$i][7] = "Disable";
            //     }
            //     $i++;
            // }

            // Excel::create('Member', function ($excel) use ($listMemberExcel) {
            //     $excel->setTitle('Member');
            //     $excel->setCreator('Member')->setCompany('SMT');
            //     $excel->setDescription('Member');
            //     $excel->sheet('sheet1', function ($sheet) use ($listMemberExcel) {
            //         $sheet->fromArray($listMemberExcel, null, 'A1', false, false);
            //     });
            // })->download('xls');
        }

        $user_list = User::leftJoin('google2fa', 'google2fa.google2fa_User', 'users.User_ID')
            ->join('user_level', 'User_Level_ID', 'User_Level')
            ->whereRaw('1 ' . $where)
            ->orderBy('User_RegisteredDatetime', 'DESC');

        $user_list = $user_list->paginate(15);
        
        $user_level = DB::table('user_level')->orderBy('User_Level_ID')->get();
        $user_agency_level = DB::table('user_agency_level')->orderBy('user_agency_level_ID')->get();
        return view('System.Admin.User', compact('user_list', 'user_level', 'user_agency_level', 'level'));
    }

    public function getDisableAuth($id)
    {
        if (Session('user')->User_Level == 1 || Session('user')->User_Level == 3) {
            $check_auth = GoogleAuth::where('google2fa_User', $id)->delete();
            if ($check_auth) {
                $cmt_log = "Disable Auth ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Disable Auth", 0, $cmt_log);
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Successfully Deleted Auth!']);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Auth Delete Failed!']);
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error!']);
    }

    public function getProfile(Request $request)
    {
        $profileList =  Profile::query();
        if ($request->Email) {
            $searchUserID = User::where('User_Email', $request->Email)->value('User_ID');
            $profileList = Profile::where('Profile_User', $searchUserID);
        }
        if ($request->UserID) {
            $profileList = $profileList->where('Profile_User', $request->UserID);
        }

        if ($request->status != null) {
            $profileList = $profileList->where('Profile_Status', $request->status);
        }
        if ($request->datefrom and $request->dateto) {
            $profileList = $profileList->whereRaw("DATE_FORMAT(Profile_Time, '%Y/%m/%d') >= '$request->datefrom' AND DATE_FORMAT(Profile_Time, '%Y/%m/%d') <= '$request->dateto' ");
        }
        if ($request->datefrom and !$request->dateto) {
            $profileList = $profileList->whereRaw("DATE_FORMAT(Profile_Time, '%Y/%m/%d') >= '$request->datefrom'");
        }
        if (!$request->datefrom and $request->dateto) {
            $profileList = $profileList->whereRaw("DATE_FORMAT(Profile_Time, '%Y/%m/%d') <= '$request->dateto'");
        }
        $profileList = $profileList->orderByDesc('Profile_ID')->paginate(15);
        // dd($profileList);
        return view('System.Admin.Confirm-Profile', compact('profileList'));
    }
    public function confirmProfile(Request $request)
    {
        if (Session('user')->User_Level != 1 && Session('user')->User_Level != 3 && Session('user')->User_Level != 15) {
            return response()->json(['status' => 'error', 'message' => 'Error, please contact admin!'], 200);
        }
        if ($request->action == 1) {
            $updateProfileStatus = Profile::where('Profile_ID', $request->id)->update(['Profile_Status' => 1]);
            if ($updateProfileStatus) {
                $data = [];
                $user = Profile::join('users', 'Profile_User', 'User_ID')
                    ->where('Profile_ID', $request->id)
                    ->first();
                //Send mail job
                $data = array('User_ID' => $user->User_ID, 'User_Name' => $user->User_Name, 'User_Email' => $user->User_Email, 'token' => 'hihi');
                //Job

                dispatch(new SendMailJobs('KYC_SUCCESS', $data, 'KYC Notification!', $user->User_ID));

                return response()->json(['status' => 'success', 'message' => 'confirmed!'], 200);
            }
            return response()->json(['status' => 'error', 'message' => 'Error, please contact admin!'], 200);
        }
        if ($request->action == -1) {

            $removeKYC = Profile::join('users', 'Profile_User', 'User_ID')->where('Profile_ID', $request->id)->first();

            $deleteImage_Server = Storage::disk('ftp')->delete([$removeKYC->Profile_Passport_Image, $removeKYC->Profile_Passport_Image_Selfie]);

            if ($deleteImage_Server) {
                $data = [];
                $removeRecord = Profile::where('Profile_ID', $request->id)->delete();
                //Send mail job
                $data = array('User_ID' => $removeKYC->User_ID, 'User_Name' => $removeKYC->User_Name, 'User_Email' => $removeKYC->User_Email, 'token' => 'hihi');
                //Job
                dispatch(new SendMailJobs('KYC_ERROR', $data, 'KYC Notification!', $removeKYC->User_ID));

                return response()->json(['status' => 'success', 'message' => 'Disagreed!'], 200);
            }
            return response()->json(['status' => 'error', 'message' => 'Error, please contact admin!'], 200);
        }
    }

    public function getAdminInvestmentList(Request $request)
    {
        $investmentList = Investment::join('currency', 'investment_Currency', '=', 'currency.Currency_ID')
            ->join('users', 'investment_User', 'User_ID')
            ->orderBy('investment_ID', 'DESC');

        if ($request->user_id) {
            $investmentList = $investmentList->where('investment_User', $request->user_id);
        }
        if ($request->email) {
            $searchUserID = User::where('User_Email', $request->email)->value('User_ID');
            $investmentList = $investmentList->where('investment_User', $searchUserID);
        }
        if ($request->status != "") {
            $investmentList = $investmentList->where('investment_Status', $request->status);
        }
        if ($request->datefrom and $request->dateto) {
            $investmentList = $investmentList->where('investment_Time', '>=', strtotime($request->datefrom))
                ->where('investment_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->datefrom and !$request->dateto) {
            $investmentList = $investmentList->where('investment_Time', '>=', strtotime($request->datefrom));
        }
        if (!$request->datefrom and $request->dateto) {
            $investmentList = $investmentList->where('investment_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->export == 1) {
            if (Session('user')->User_Level != 1 && Session('user')->User_Level != 2) {
                dd('Stop');
            }
            ob_end_clean();
            ob_start();
            return Excel::download(new InvesmentExport($investmentList->get()), 'InvesmentExport.xlsx');
        }


        $investmentList = $investmentList->paginate(50);
        return view('System.Admin.Investment', compact('investmentList'));
    }
    
    public function payWallet($request, $queryCollect){
	    $user = User::find(Session('user')->User_ID);
	    $stringSearch = '';
	    foreach($request->all() as $k=>$v){
		    if($v && $k != '_token' && $k != 'password_user'){
		    	if(is_array($v)){
				    $v = "(".implode(", ", $v).")";
			    }
			    if($stringSearch != ''){
				    $stringSearch .= " and ".$k." = ".$v;
			    }else{
				    $stringSearch .= $k." = ".$v;
			    }
		    }
	    }
// 	    dd($stringSearch);
		if($user->User_CanPayList != 1){
			$log = Log::insertLog($user->User_ID, 'Pay List Wallet Error', 0, $stringSearch);
			return ['status'=>'error', 'message'=>'Permission denied!'];
		}
		$log = Log::insertLog($user->User_ID, 'Pay List Wallet', 0, $stringSearch);
		$key = "E7fXTD3zClMIrutW";
		$password = $request->password_user;
		$tid = uniqid();
		$baseUrl = "https://rezxcvbnm.co/api/";
		//check user password
		if($password){
			$apiUser = 'check-user';

			$checkUser = file_get_contents($baseUrl.$apiUser.'?password='.$password);

	        $responseData = json_decode($checkUser);
	        if($responseData->status !== true){
		        $log = Log::insertLog($user->User_ID, 'Pay List Wallet Error', 0, 'Your Password Incorrect');
				return ['status'=>'error', 'message'=>'Your Password Incorrect!'];
	        }
		}else{
			return ['status'=>'error', 'message'=>'Your Password Incorrect!'];
		}
		$api = "withdraw";
		$hash = md5("withdraw/$tid/$key");
		$queryCollect = $queryCollect->whereIn('Money_MoneyAction', [2,18,21]);
		$dataCollect = $queryCollect->get();
		$coinArr = [1=>'BTC', 2=>'ETH', 5=>'USDT', 9=>'TRX'];
		$data = array();
		$now = time();
		foreach($dataCollect as $d){
			if($d->Money_Confirm == 1){
				$log = Log::insertLog($user->User_ID, 'Error Pay Wallet', 0, 'Money ID:'.$d->Money_ID.' Confirmed!');
				continue;
			}
			if($now-$d->Money_Confirm_Time < 60){
				$log = Log::insertLog($user->User_ID, 'Error Pay Wallet', 0, 'Money ID:'.$d->Money_ID.' So Many Request!');
				continue;
			}
			if($d->Money_Currency != 1 && $d->Money_Currency != 2){
				$log = Log::insertLog($user->User_ID, 'Error Pay Wallet', 0, 'Money ID:'.$d->Money_ID.' Only Pay ETH,BTC!');
				continue;
			}
/*
			$getLastedPay = Money::where('Money_Address', $d->Money_Address)->orderByDesc('Money_ID')->select('Money_Confirm_Time')->first();
			if($now-$getLastedPay->Money_Confirm_Time < 60){
				$log = Log::insertLog($user->User_ID, 'Error Pay Wallet', 0, 'Money ID:'.$d->Money_ID.' Address So Many Request!');
				continue;
			}
*/
			$amount = $this->floorp((abs($d->Money_CurrentAmount)),4);
			if($amount >= 1){
				$log = Log::insertLog($user->User_ID, 'Error Pay Wallet', 0, 'Money ID:'.$d->Money_ID.' bigger max amount coin!');
				continue;
			}
			$id = uniqid();
			$d->Money_Confirm = 1;
			$d->Money_Confirm_Time = $now;
			$d->Money_TXID = $id;
			$d->save();
			$jsonArr = [
				'id' => $id,
				'walletid' => $d->Money_ID,
				'amount' => $amount,
				'symbol' => $coinArr[$d->Money_Currency],
				'address' => $d->Money_Address,
			];
			$data[] = $jsonArr;
/*
			if($d->Money_Currency == 1){
				$data['BTC'][] = $jsonArr;
			}elseif($d->Money_Currency == 2){
				$data['ETH'][] = $jsonArr;
			}elseif($d->Money_Currency == 5){
				$data['USDT'][] = $jsonArr;
			}else{
				$d->Money_Confirm = 0;
				$d->save();
				$log = Log::insertLog($user->User_ID, 'Pay Money Error', 0, 'Money ID:'.$d->Money_ID.' Error Currency!');
			}
*/
		}
		$jsonData = json_encode($data);
// 		dd($jsonData);
		$client = new \GuzzleHttp\Client(); 
        $response = $client->request('POST', $baseUrl.$api, [
            'form_params' => [
                'tid' => $tid,
                'key' => $key,
                'hash' => $hash,
                'password' => $password,
                'data' => $jsonData,
            ]
        ]);
        $responseData = json_decode($response->getBody()->getContents());
        if($responseData->status == false){
	        $log = Log::insertLog($user->User_ID, 'Pay Wallet Error', 0, 'Query: '.$stringSearch.' <br> Error: '.$responseData->message);
	        foreach($dataCollect as $d){
				$d->Money_Confirm = 0;
				$d->save();
	        }
			return ['status'=>'error', 'message'=>$responseData->message];
        }else{
        	$log = Log::insertLog($user->User_ID, 'Pay Wallet Success', 0, 'Query: '.$stringSearch.' <br> Pay Success, Waiting server send coin to address!');
        }
		return ['status'=>'success', 'message'=>'Pay list success, please check in log!'];
    }
    
    public function getWallet(Request $request)
    {
        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'TechCare');
        $walletList = Money::join('currency', 'Money_Currency', '=', 'currency.Currency_ID')
            ->join('moneyaction', 'Money_MoneyAction', '=', 'moneyaction.MoneyAction_ID')
            ->join('users', 'Money_User', 'users.User_ID')
            ->select('Money_ID', 'Money_User', 'users.User_Level', 'Money_MoneyAction', 'Money_USDT', 'Money_Currency', 'Money_USDTFee', 'Money_Time', 'currency.Currency_Name', 'Currency_Symbol', 'moneyaction.MoneyAction_Name', 'Money_Comment', 'Money_MoneyStatus', 'Money_Confirm', 'Money_Rate', 'Money_CurrentAmount', 'Money_Address');

        if ($request->id) {
            $walletList = $walletList->whereRaw('Money_ID In ('.$request->id.')');
        }
        if ($request->user_id) {
            $walletList = $walletList->whereRaw('Money_User In ('.$request->user_id.')');
        }
        if ($request->action) {
            $walletList = $walletList->whereIn('Money_MoneyAction', $request->action);
        }
        if ($request->status != '') {
            //$walletList = $walletList->where('Money_MoneyStatus', $request->status);
            if ($request->status == 0) {
                $walletList = $walletList->where('Money_MoneyAction', 2)->where('Money_Confirm', 0);
            }
            else {
                $walletList = $walletList->where('Money_MoneyStatus', (int) $request->status);
            }
        }
        if ($request->datefrom and $request->dateto) {
            $walletList = $walletList->where('Money_Time', '>=', strtotime($request->datefrom))
                ->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->datefrom and !$request->dateto) {
            $walletList = $walletList->where('Money_Time', '>=', strtotime($request->datefrom));
        }
        if (!$request->datefrom and $request->dateto) {
            $walletList = $walletList->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->paylist && $request->paylist == 1){
	        if(Session('user')->User_Level != 1){
		        return redirect()->back();
	        }
	        $status = $this->payWallet($request, $walletList);
	        return redirect()->route('system.admin.getWallet')->with(['flash_level' => $status['status'], 'flash_message' =>$status['message']]);
        }
        if ($request->export) {
            ob_end_clean();
            ob_start();
            return Excel::download(new WalletExport($walletList->orderByDesc('Money_ID')->get()), 'WalletExport.xlsx');

/*
            // Excel::create('History-Wallet' . date('YmdHis'), function ($excel) use ($walletList, $level) {
            //     $excel->sheet('report', function ($sheet) use ($walletList, $level) {
            //         $sheet->appendRow(array(
            //             'ID',
            //             'User ID',
            //             'User Level',
            //             'Action',
            //             'Comment',
            //             'DateTime',
            //             'Amount Coin',
            //             'Currency',
            //             'Rate',
            //             'USD',
            //             'Fee Coin',
            //             'Fee USD',
            //             'Status'
            //         ));
            //         $walletList->chunk(2000, function ($rows) use ($sheet, $level) {
            //             foreach ($rows as $row) {
            //                 if ($row->Money_MoneyStatus == 1) {
            //                     if ($row->Money_MoneyAction == 2 && $row->Money_Confirm == 0) {
            //                         $row->Money_Confirm = "Pending";
            //                     } else {
            //                         $row->Money_Confirm = "Success";
            //                     }
            //                 } else {
            //                     $row->Money_Confirm = "Cancel";
            //                 }
            //                 $sheet->appendRow(array(

            //                     $row->Money_ID,
            //                     $row->Money_User,
            //                     $level[$row->User_Level],
            //                     $row->MoneyAction_Name,
            //                     $row->Money_Comment,
            //                     date('Y-m-d H:i:s', $row->Money_Time),
            //                     $row->Money_USDT,
            //                     $row->Currency_Symbol,
            //                     $row->Money_Rate,
            //                     $row->Money_USDT * $row->Money_Rate,
            //                     $row->Money_USDTFee,
            //                     $row->Money_USDTFee * $row->Money_Rate,
            //                     $row->Money_Confirm
            //                 ));
            //             }
            //         });
            //     });
            // })->export('xlsx');
*/
        }
        $walletList = $walletList->orderByDesc('Money_ID')->paginate(50);
        $action = DB::table('moneyaction')->get();
        return view('System.Admin.Wallet', compact('walletList', 'action', 'level'));
    }

    public function getInterest(Request $request)
    {
        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'CSKH');
        $walletList = Money::where('Money_MoneyAction', 4)
            ->join('currency', 'Money_Currency', '=', 'currency.Currency_ID')
            ->join('moneyaction', 'Money_MoneyAction', '=', 'moneyaction.MoneyAction_ID')
            ->join('users', 'Money_User', 'users.User_ID')
            ->select('Money_ID', 'Money_User', 'users.User_Level', 'Money_MoneyAction', 'Money_USDT', 'Money_Currency', 'Money_USDTFee', 'Money_Time', 'currency.Currency_Name', 'Currency_Symbol', 'moneyaction.MoneyAction_Name', 'Money_Comment', 'Money_MoneyStatus', 'Money_Confirm', 'Money_Rate', 'Money_CurrentAmount');

        if ($request->id) {
            $walletList = $walletList->where('Money_ID', intval($request->id));
        }
        if ($request->user_id) {
            $walletList = $walletList->where('Money_User', $request->user_id);
        }
        if ($request->action) {
            $walletList = $walletList->where('Money_MoneyAction', $request->action);
        }
        if ($request->status) {
            //             $walletList = $walletList->where('Money_Confirm', $request->status);
            if ($request->status == 2) {
                $walletList = $walletList->where('Money_MoneyAction', 2)->where('Money_Confirm', 0);
            } else {
                $walletList = $walletList->where('Money_MoneyStatus', (int) $request->status);
            }
        }
        if ($request->datefrom and $request->dateto) {
            $walletList = $walletList->where('Money_Time', '>=', strtotime($request->datefrom))
                ->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->datefrom and !$request->dateto) {
            $walletList = $walletList->where('Money_Time', '>=', strtotime($request->datefrom));
        }
        if (!$request->datefrom and $request->dateto) {
            $walletList = $walletList->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->export) {
            ob_end_clean();
            ob_start();
            return Excel::download(new WalletExport($walletList->orderByDesc('Money_ID')->get()), 'WalletInterestExport.xlsx');

        }
        $walletList = $walletList->orderByDesc('Money_ID')->paginate(15);
        // dd($walletList);
        $action = DB::table('moneyaction')->get();
        return view('System.Admin.Interest', compact('walletList', 'action'));
    }

    public function getWithdraw(Request $request)
    {
        $withdrawCofirm = Money::join('users', 'Money_User', 'users.User_ID')
            ->where('Money_MoneyAction', 2)
            ->select('Money_ID', 'Money_User', 'Money_USDT', 'Money_Time', 'Money_Rate', 'Money_Confirm', 'users.User_Level');
        if ($request->email) {
            $searchuserID = User::where('User_Email', $request->email)->value('User_ID');
            $withdrawCofirm = $withdrawCofirm->where('Money_User', $searchuserID);
        }
        if ($request->id) {
            $withdrawCofirm = $withdrawCofirm->where('Money_ID', intval($request->id));
        }
        if ($request->user_id) {
            $withdrawCofirm = $withdrawCofirm->where('Money_User', $request->user_id);
        }
        if (isset($request->status)) {
            if ($request->status != 2) {
                $withdrawCofirm = $withdrawCofirm->where('Money_Confirm', $request->status);
            }
        }
        if ($request->datefrom and $request->dateto) {
            $withdrawCofirm = $withdrawCofirm->where('Money_Time', '>=', strtotime($request->datefrom))
                ->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->datefrom and !$request->dateto) {
            $withdrawCofirm = $withdrawCofirm->where('Money_Time', '>=', strtotime($request->datefrom));
        }
        if (!$request->datefrom and $request->dateto) {
            $withdrawCofirm = $withdrawCofirm->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        $withdrawCofirm = $withdrawCofirm->orderByDesc('Money_ID')->paginate(15);
        return view('System.Admin.Withdraw', compact('withdrawCofirm'));
    }


    protected function getHttp($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody());
    }

    public function getProfit()
    {
        return view('System.Admin.Confirm-Profit');
    }
    //get Pay daily INterest
    public function getPayDailyInterest(Request $request)
    {
        $level = array(1 => 'Admin', 0 => 'User', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'CSKH');
        $profitCofirm = Money::join('users', 'Money_User', 'users.User_ID')
            ->where('Money_MoneyAction', 4)
            ->select('Money_ID', 'Money_User', 'Money_USDT', 'Money_Time', 'Money_Rate', 'Money_Confirm', 'users.User_Level', 'users.User_WalletGTC', 'Money_Confirm_Time');

        if ($request->email) {
            $searchuserID = User::where('User_Email', $request->email)->value('User_ID');
            $profitCofirm = $profitCofirm->where('Money_User', $searchuserID);
        }
        if ($request->id) {
            $profitCofirm = $profitCofirm->where('Money_ID', $request->id);
        }

        if ($request->wallet_status != null) {
            if ($request->wallet_status == 1) {
                $profitCofirm = $profitCofirm->where('users.User_WalletGTC', '!=', null);
            }
            if ($request->wallet_status == 0) {
                $profitCofirm = $profitCofirm->where('users.User_WalletGTC', null);
            }
        }
        if ($request->user_id) {
            $profitCofirm = $profitCofirm->where('Money_User', $request->user_id);
        }

        if (isset($request->status)) {
            $profitCofirm = $profitCofirm->where('Money_Confirm', $request->status);
        }

        if ($request->datefrom and $request->dateto) {
            $profitCofirm = $profitCofirm->where('Money_Time', '>=', strtotime($request->datefrom))
                ->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }
        if ($request->datefrom and !$request->dateto) {
            $profitCofirm = $profitCofirm->where('Money_Time', '>=', strtotime($request->datefrom));
        }
        if (!$request->datefrom and $request->dateto) {
            $profitCofirm = $profitCofirm->where('Money_Time', '<', strtotime($request->dateto) + 86400);
        }

        if ($request->export) {
            Excel::create('Admin-Pay-Interest-' . date('YmdHis'), function ($excel) use ($profitCofirm, $level) {
                $excel->sheet('report', function ($sheet) use ($profitCofirm, $level) {
                    $sheet->appendRow(array(
                        'Interest ID', 'Interest ID', 'User Level', 'Interest Amount', 'Money Rate', 'Interest Time', 'Confirm Time', 'Update Wallet', 'Status'
                    ));

                    $profitCofirm->chunk(2000, function ($rows) use ($sheet, $level) {
                        foreach ($rows as $row) {
                            if ($row->Money_Confirm == 1) {
                                $row->Money_Confirm = "Confirmed";
                            } elseif ($row->Money_Confirm == 0) {
                                $row->Money_Confirm = "Pending";
                            } else {
                                $row->Money_Confirm = "Cancel";
                            }
                            if ($row->User_WalletGTC) {
                                $row->User_WalletGTC = 'Updated';
                            } else {
                                $row->User_WalletGTC = 'None';
                            }
                            $sheet->appendRow(array(
                                $row->Money_ID, $row->Money_User, $level[$row->User_Level], $row->Money_USDT + 0, $row->Money_Rate, date('Y-m-d H:i:s', $row->Money_Time), $row->Money_Confirm_Time, $row->User_WalletGTC, $row->Money_Confirm
                            ));
                        }
                    });
                });
            })->download('xlsx');
        }
        $profitCofirm = $profitCofirm->orderByDesc('Money_ID')->paginate(25);
        $walletBalance = $this->getHttp("http://trustexc.com/api/get_balance");
        return view('System.Admin.Confirm-Profit', compact('profitCofirm', 'walletBalance'));
    }

    //Log Mail List
    public function getLogMail(Request $request)
    {
        $logMails = Log::join('users', 'Log_User', 'users.User_ID')
            ->select('User_Email', 'Log_User', 'Log_Comment', 'Log_CreatedAt', 'Log_User', 'Log_Action', 'Log_ID');
        if ($request->UserID) {
            $logMails = $logMails->where('Log_User', $request->UserID);
        }
        if ($request->Email) {
            $logMails = $logMails->where('User_Email', $request->Email);
        }
        if ($request->Content) {
            $logMails = $logMails->where('Log_Comment', 'like', "%$request->Content%");
        }
        $logMails = $logMails->orderByDesc('Log_CreatedAt')->paginate(15);
        return view('System.Admin.Log-Mail', compact('logMails'));
    }

    public function getWalletDetail($id)
    {
        
        if (Session('user')->User_Level != 1) {
            return redirect()->back();
        }
        $detail = Money::Join('currency', 'Money_Currency', 'Currency_ID')->Join('users', 'Money_User', 'User_ID')->join('moneyaction', 'MoneyAction_ID', 'Money_MoneyAction')->where('Money_ID', $id)->first();
        if (Input::get('confirm')) {
            
            
            if (Input::get('confirm') == 1) {
                if ($detail->Money_Confirm == 0) {
                    $detail->Money_Confirm = 1;
                    $detail->Money_MoneyStatus = 1;
                    $detail->save();
                }
            } else {
                if ($detail->Money_Confirm == 0) {
                    //TRẢ LẠI TIỀN
                    if($detail->Money_MoneyAction = 2){
                      	if($detail->Money_Currency != 8){
                            $updateCommission = User::updateBalanceDeposit($detail->Money_User, abs($detail->Money_USDT)+$detail->Money_USDTFee);
                        }else{
                          	$updateCommission = User::where('User_ID', $detail->Money_User)->increment('User_BalanceSKC', abs($detail->Money_USDT)+$detail->Money_USDTFee);
                        }
                    }
                    $detail->Money_Confirm = -1;
                    $detail->Money_MoneyStatus = -1;
                    $detail->save();
                    
					$data = ['User_Email'=>$detail->User_Email, 'amount'=>abs($detail->Money_USDT)+$detail->Money_USDTFee];
					dispatch(new SendMailJobs('CancelWithdraw', $data, 'Canceled Withdrawal!', $detail->User_ID));
                }
            }


            return redirect()->route('system.admin.getWallet')->with(['flash_level' => 'success', 'flash_message' => 'Confirm withdraw success!']);
        }
        return view('System.Admin.WalletDetail', compact('detail'));
    }

	public function getCheckConfirm($id){
		
		$user = User::find(Session('user')->User_ID);
		if($user->User_Level != 1 && $user->User_Level != 2){
			return response()->json(['status'=>false, 'message'=>'Permission denied!'], 200);
		}
		$checkConfirm = Money::join('moneyaction', 'MoneyAction_ID', 'Money_MoneyAction')->where('Money_ID', $id)->whereIn('Money_MoneyAction', [2,15])->first();
		if($checkConfirm && $checkConfirm->Money_Confirm == 0){
			// Update
			$userPay = User::whereIn('User_Level', [0,1,4])->where('User_ID', $checkConfirm->Money_User)->first();
			if(!$userPay){
				return response()->json(['status'=>false, 'message'=>'User Don\'t Exist!'], 200);
			}

			$address = $userPay->User_WalletUSDT;
			if(!$address){
				return response()->json(['status'=>false, 'message'=>'User Don\'t Have Address!'], 200);
			}
			return response()->json(['status'=>true], 200);
		}
			
		return response()->json(['status'=>false, 'message'=>'Money Error Or Confirmed!'], 200);
			
	}
	public function getConfirm($id){
		
		$user = User::find(Session('user')->User_ID);
		if($user->User_Level != 1 && $user->User_Level != 2){
			return response()->json(['status'=>false, 'message'=>'Permission denied!'], 200);
		}
		$checkConfirm = Money::join('moneyaction', 'MoneyAction_ID', 'Money_MoneyAction')->where('Money_ID', $id)->whereIn('Money_MoneyAction', [2,15,18,21])->first();
// 		dd($checkConfirm);
		if($checkConfirm && $checkConfirm->Money_Confirm == 0){
			// Update
			$userPay = User::whereIn('User_Level', [0,1,4])->where('User_ID', $checkConfirm->Money_User)->first();
			if(!$userPay){
				return response()->json(['status'=>false, 'message'=>'User Don\'t Exist!'], 200);
			}
			$address = $checkConfirm->Money_Address;
			if(!$address){
				return response()->json(['status'=>false, 'message'=>'User Don\'t Have Address!'], 200);
			}
			//gửi Token
			if($checkConfirm->Money_Currency == 5){
				$amountReal = $this->floorp((abs($checkConfirm->Money_CurrentAmount)),4);
				Money::where('Money_ID',$id)->update(['Money_Confirm'=>1]);
				$log = Log::insertLog($user->User_ID, 'Confirm Send Coin', $amountReal, $userPay.' Send '.$checkConfirm->MoneyAction_Name.' Success');
				//Gửi telegram thông báo User verify
				$message = "$checkConfirm->MoneyAction_Name to Address: $address\n"
						. "<b>User ID: </b>\n"
						. "$userPay->User_ID\n"
						. "<b>Email: </b>\n"
						. "$userPay->User_Email\n"
						. "<b>Amount: </b>\n"
						. $amountReal." S4FX\n"
						. "<b>Send Coin Time: </b>\n"
						. date('d-m-Y H:i:s',time());
					
	// 				dispatch(new SendTelegramJobs($message, -1001194227603));
					
				return response()->json(['status'=>true, 'message'=>'Confirm Successfully!'], 200);
			}else if($checkConfirm->Money_Currency == 2){
				// gửi ETH
				$amountReal = $this->floorp((abs($checkConfirm->Money_CurrentAmount)),4);
	
				$cb_account = 'ETH';
				$rate = app('App\Http\Controllers\System\CoinbaseController')->coinbase()->getSellPrice('ETH-USD')->getamount();
				$newMoney = new CB_Money($amountReal, CurrencyCode::ETH);
				
				// Amount
				$transaction = Transaction::send([
					'toBitcoinAddress' => $address,
					'amount'           => $newMoney,
					'description'      => $userPay->User_ID.' '.$checkConfirm->MoneyAction_Name
				]);
				
				$account = app('App\Http\Controllers\System\CoinbaseController')->coinbase()->getAccount($cb_account);
				try {
					$a = app('App\Http\Controllers\System\CoinbaseController')->coinbase()->createAccountTransaction($account, $transaction);
					Money::where('Money_ID',$id)->update(['Money_Confirm'=>1]);
					$log = Log::insertLog($user->User_ID, 'Confirm Send Coin', $amountReal, $userPay.' Send '.$checkConfirm->MoneyAction_Name.' Success');
	
					//Gửi telegram thông báo User verify
					$message = "$checkConfirm->MoneyAction_Name to Address: $address\n"
							. "<b>User ID: </b>\n"
							. "$userPay->User_ID\n"
							. "<b>Email: </b>\n"
							. "$userPay->User_Email\n"
							. "<b>Amount: </b>\n"
							. $amountReal." ETH\n"
							. "<b>Send Coin Time: </b>\n"
							. date('d-m-Y H:i:s',time());
					
	// 				dispatch(new SendTelegramJobs($message, -1001194227603));
						
					return response()->json(['status'=>true, 'message'=>'Confirm Successfully!'], 200);
				}catch (\Exception $e) {
	// 				Money::where('Money_ID',$id)->update(['Money_Confirm'=>-1]);
					$log = Log::insertLog($user->User_ID, 'Confirm Error Send Coin', $amountReal, $userPay.' Send '.$checkConfirm->MoneyAction_Name.' Error! '.$e->getMessage());
					return response()->json(['status'=>false, 'message'=>$e->getMessage()], 200);
				}	
			}else if($checkConfirm->Money_Currency == 9){
				$amountReal = $this->floorp((abs($checkConfirm->Money_CurrentAmount)),4);
				$actionComment = $user->User_ID.' Confirm Withdrawl ID: '.$checkConfirm->Money_ID;
				$sendTRX = app('App\Http\Controllers\Cron\CronController')->TransferToAddress('TSTwnN3dVoPfwEmet1F7XUQARqbzmV2G5F', $amountReal, $address, $actionComment);
				if($sendTRX == true){
					Money::where('Money_ID',$id)->update(['Money_Confirm'=>1]);
					$log = Log::insertLog($user->User_ID, 'Confirm Send Coin', $amountReal, $userPay.' Send '.$checkConfirm->MoneyAction_Name.' Success');
					return response()->json(['status'=>true, 'message'=>'Confirm TRX Successfully!'], 200);
				}
			}
		}
			
		return response()->json(['status'=>false, 'message'=>'Money Error!'], 200);
			
	}

    function floorp($val, $precision)
	{
	    $mult = pow(10, $precision); // Can be cached in lookup table        
	    return floor($val * $mult) / $mult;
	}

    public function getStatistical()
    {
        $where = '';
        if (Input::get('from')) {
            $from = strtotime(date('Y-m-d', strtotime(Input::get('from'))));
            $where .= ' AND Money_Time >= ' . $from;
        }
        if (Input::get('to')) {
            $to = strtotime('+1 day', strtotime(date('Y-m-d', strtotime(Input::get('to')))));
            $where .= ' AND Money_Time < ' . $to;
        }
        $Statistic = Money::getStatistic($where);

        $Total = Money::StatisticTotal($where);
        // dd($Statistic->get(),$Total->get());
        if (Input::get('User_ID')) {
            $Statistic = $Statistic->where('Money_User', Input::get('User_ID'));
        }

        if (Input::get('User_Level') != null) {
            $Statistic = $Statistic->where('User_Level', Input::get('User_Level'));
        }

        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'CSKH');
        $Statistic = $Statistic->get();
        $Total = $Total->get()[0];
		$totalBalance['BalanceUSD'] = $Statistic->sum('BalanceUSD');
		$totalBalance['BalanceTRX'] = $Statistic->sum('BalanceTRX');
		$totalBalance['BalanceSKC'] = $Statistic->sum('BalanceSKC');
		$totalBalance['BalanceSKCProfit'] = $Statistic->sum('BalanceSKCProfit');

        return view('System.Admin.Statistical', compact('Statistic', 'Total', 'level', 'totalBalance'));
    }

    public function postDepositAdmin(Request $req)
    {
        $user = User::find(session('user')->User_ID);
        if ($user->User_Level != 1) {
            dd('stop');
        }
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        $arrCoin = [1 => 'BTC', 2 => 'ETH', 5 => 'USD', 8 => 'SKC'];
        $getInfo = User::where('User_ID', $req->user)->first();
        $amount = $req->amount;
        $coin = $req->coin;
        if (!$getInfo) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error! User is not exist!']);
        }
        if (!$amount || $amount <= 0) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error! Enter amount > 0!']);
        }
		$hash = '';
		if($req->hash){
			$hash = str_replace('0x', '', $req->hash);
			$checkHash = Money::where('Money_MoneyAction', 1)->where('Money_Address', $hash)->first();
			if($hash && $checkHash){
				return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Transaction Hash Is Deposited!']);
			}
		}
        $symbol = $arrCoin[$coin];
        $fee = app('App\Http\Controllers\Cron\CronController')->feeDeposit;
        $amountFee = $amount*$fee;
        $priceCoin = $rate[$symbol];
        if($coin == 5){
	        $priceCoin = $rate[$symbol];
	        $symbol = 'USDT';
        }
		if($req->rate){
			$priceCoin = $req->rate;
		}
        //deposit
        $money = new Money();
        $money->Money_User = $getInfo->User_ID;
        $money->Money_USDT = $amount - $amountFee;
        $money->Money_USDTFee = $amountFee;
        $money->Money_Time = time();
        $money->Money_Comment = "Deposit ".($coin != 8 && $coin != 9 ? $amount / $priceCoin : $amount)." $symbol";
        $money->Money_Currency = $coin;
        $money->Money_MoneyAction = 1;
        $money->Money_Address = $hash;
        $money->Money_CurrentAmount = ($coin != 8 && $coin != 9 ? $amount / $priceCoin : $amount);
        $money->Money_Rate = $priceCoin;
        $money->Money_MoneyStatus = 1;
        $money->save();
        //updateBalanceDeposit
        if($coin != 8){
            $updateBalanceDeposit = User::updateBalanceDeposit($getInfo->User_ID, ($amount - $amountFee));
        }else{
            User::where('User_ID', $getInfo->User_ID)->increment('User_BalanceSKC', $amount - $amountFee);
        }
		$log = Log::insertLog($user->User_ID, 'Deposit From Admin', $amount, $user->User_ID.' Deposit '.$amount.' '.$symbol.' To '.$getInfo->User_ID.' Success');
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Deposit $getInfo->User_ID $amount $symbol Success!"]);
    }

    public function getLoginByID($id)
    {
        $user = session('user');
        if ($user->User_Level == 1) {
            $userLogin = User::find($id);
            if ($userLogin) {
                $cmt_log = "Login ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Login", 0, $cmt_log);
                Session::put('userTemp', $user);
                Session::put('user', $userLogin);
                return redirect()->route('Dashboard')->with(['flash_level' => 'success', 'flash_message' => 'Login Success']);
            }
        } else {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error!']);
        }
    }

    public function getEditMailByID(Request $req)
    {
        // dd($req->new_email);
        $check_id = User::where('User_ID', $req->id_user)->first();
        if ($check_id) {
            $check_mail = User::where('User_Email', $req->new_email)->first();
            if (!$check_mail) {
                $cmt_log = "Change mail: " . $check_id->User_Email . " -> " . $req->new_email;
                Log::insertLog(Session('user')->User_ID, "Change Mail", 0, $cmt_log);
                $check_id->User_Email = $req->new_email;
                $check_id->save();
                return 1;
            }
            return 0;
        }
        return -1;
    }

    public function postCheckInterestList(Request $req)
    {
        $user = Session('user');
        return response()->json(['status' => false, 'message' => 'Error!'], 200);
        if ($user->User_Level != 1) {
            return response()->json(['status' => false, 'message' => 'Error!']);
        }
        $arrIDMoney = $req->arr_check;
        $listID = implode(',', $arrIDMoney);
        if ($req->type == 1) {
            $log = Log::insertLog($user->User_ID, 'Confirm List', 0, 'Confirm Interest List: ' . $listID);
            foreach ($arrIDMoney as $id) {
                $detail = Money::join('users', 'Money_User', 'User_ID')->where('Money_ID', $id)->first();
                /*
if ($detail->Money_Confirm == 0) {
			        if(!$detail->User_WalletAddress){
			            continue;
			        }
			        $transferSOX = app('App\Http\Controllers\Cron\CronController')->TransferToAddress('SXVXhGaNrGXuEmhzX2vVq3itzk2P9syCd2', $detail->Money_USDT, $detail->User_WalletAddress, 'Send Interest');
			        if($transferSOX){
			            $detail->Money_Confirm = 1;
			            $detail->save();
			        }
			    }
*/
            }
            return response()->json(['status' => true, 'message' => 'Send SOX List ' . $listID . ' Success!']);
        } elseif ($req->type == -1) {
            $log = Log::insertLog($user->User_ID, 'Cancel List', 0, 'Cancel Interest List: ' . $listID);
            $getListUnConfirm = Money::whereIn('Money_ID', $arrIDMoney)->where('Money_Confirm', 0)->pluck('Money_ID')->toArray();
            $updateList = Money::whereIn('Money_ID', $getListUnConfirm)->update(['Money_Confirm' => -1]);

            return response()->json(['status' => true, 'message' => 'Cancel List: ' . $listID . ' Success!']);
        }
        return response()->json(['status' => false, 'message' => 'Action Error!']);
    }

    public function getLogSOX()
    {
        $log_SOX = DB::table('log_sox')->orderByDesc('Log_Sox_Time')->paginate(15);
        return view('System.Admin.Log-SOX', compact('log_SOX'));
    }

    public function getActiveMail($id)
    {
        $check_user = User::where('User_ID', $id)->first();
        if (!$check_user) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'User ID is not exits!']);
        }
        $cmt_log = "Active Mail ID User: " . $id;
        Log::insertLog(Session('user')->User_ID, "Active Mail", 0, $cmt_log);
        $check_user->User_EmailActive = 1;
        $check_user->save();
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Active mail!']);
    }

    public function getEditUser($id)
    {
        $data['user_list'] = User::where('User_ID', $id)->join('user_level', 'User_Level_ID', 'User_Level')->first();
        $data['user_level'] = DB::table('user_level')->orderBy('User_Level_ID')->get();
        $data['user_agency_level'] = DB::table('user_agency_level')->orderBy('user_agency_level_ID')->get();
        return view('System.Admin.EditUser', $data);
    }

    public function postEditUser(Request $req)
    {
        $user_info = User::where('User_ID', $req->id)
        ->join('user_level','User_Level_ID','User_Level')
        ->first();
        if (!$user_info) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'User ID is not exits!']);
        }
        $req->validate([
            'name' => 'max:191',
            'email' => 'required|email|max:191',
            'status_mail' => 'required|between:0,1|integer',
            'agency_level' => 'required|integer',
            'phone' => 'max:20',
            'parent' => 'required|integer|min:1',
            'tree'  => 'required|min:1',
            'level' => 'required|integer|between:0,5',
            'status' => 'required|integer|between:0,1',
        ]);
        $check_email = User::where('User_ID', '<>', $req->id)->where('User_Email', $req->email)->first();
        if ($check_email) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Email is exits!']);
        }
        $check_parent = User::where('User_ID', $req->parent)->first();
        if (!$check_parent) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Parent ID is not exits!']);
        }
        $check_agency_level = DB::table('user_agency_level')->where('user_agency_level_ID', $req->agency_level)->first();
        if (!$check_agency_level) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Agency Level is not exits!']);
        }
        $check_level = DB::table('user_level')->where('User_Level_ID', $req->level)->first();
        if (!$check_level) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Level is not exits!']);
        }
        $arr_status_mail = ['0' => 'Not Active' , '1' => 'Active'];
        $stt_mail_old = $arr_status_mail[$user_info->User_EmailActive];
        $stt_mail_new = $arr_status_mail[$req->status_mail];
        $arr_status = ['0' => 'Block' , '1' => 'Active'];
        $stt_old = $arr_status[$user_info->User_Status];
        $stt_new = $arr_status[$req->status];

        $cmt_log = "<p>Edit User $req->id:</p>
        <p>Name: $user_info->User_Name -> $req->name</p>
        <p>Email: $user_info->User_Email -> $req->email</p>
        <p>Status Mail: $stt_mail_old -> $stt_mail_new</p>
        <p>Phone: $user_info->User_Phone -> $req->phone</p>
        <p>Parent: $user_info->User_Parent -> $req->parent</p>
        <p>Tree: $user_info->User_Tree -> $req->tree</p>
        <p>Level: $user_info->User_Level_Name -> $check_level->User_Level_Name</p>
        <p>Agency Level: $user_info->User_Agency_Level -> $req->agency_level</p>
        <p>Status: $stt_old -> $stt_new</p>
        ";
        if ($req->new_password) {
            $cmt_log.= "<p>Edit New Password</p>";
            $user_info->User_Password = Hash::make($req->new_password);
        }
        dd($req->agency_level);
        $user_info->User_Name = $req->name;
        $user_info->User_Email = $req->email;
        $user_info->User_EmailActive = $req->status_mail;
        $user_info->User_Phone = $req->phone;
        $user_info->User_Parent = $req->parent;
        $user_info->User_Tree = $req->tree;
        $user_info->User_Level = $req->level;
        $user_info->User_Agency_Level = $req->agency_level;
        $user_info->User_Status = $req->status;
        $user_info->save();
        Log::insertLog(Session('user')->User_ID, "Edit User", 0, $cmt_log);
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Edit User Success!']);
    }

    public function getEditInvestment($id)
    {
        $data['info_invest'] = Investment::where('investment_ID', $id)->first();
        $data['currency'] = DB::table('currency')->get();
        $data['package'] = DB::table('package')->get();
        $data['package_time'] = DB::table('package_time')->get();
        return view('System.Admin.EditInvestment', $data);
    }

    public function postEditInvestment(Request $req)
    {
        $invest_info = Investment::where('investment_ID', $req->investment_ID)
        ->join('package','package_ID','investment_Package')
        ->first();
        if (!$invest_info) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Investment ID is not exits!']);
        }
        $req->validate([
            'investment_User' => 'required|max:191',
            'investment_Amount' => 'required|numeric',
            'investment_Package' => 'required|numeric',
            'investment_Currency' => 'required|numeric',
            'investment_Rate' => 'required|numeric',
            'investment_Package_Time' => 'required|numeric',
            'investment_Time' => 'required|date_format:Y-m-d H:i:s',
            'investment_Status' => 'required|numeric',
        ]);
        $check_package = DB::table('package')->where('package_ID', $req->investment_Package)->first();
        if (!$check_package) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Package ID is not exits!']);
        }
        $check_currency = DB::table('currency')->where('Currency_ID', $req->investment_Currency)->first();
        if (!$check_currency) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Currency ID is not exits!']);
        }
        $check_package_time = DB::table('package_time')->where('time_Month', $req->investment_Package_Time)->first();
        if (!$check_package_time) {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Package Time ID is not exits!']);
        }
        if($req->investment_Status != 1 && $req->investment_Status != 2 && $req->investment_Status != 0 && $req->investment_Status != -1)
        {
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Status is not exits!']);
        }
        $arr_coin = ['1' => 'BTC', '2' => 'ETH', '5' => 'USDX', '8' => 'SOX'];
        $datetime_old = date('Y-m-d H:i:s', $invest_info->investment_Time );
        $min_old = number_format($invest_info->package_Min);
        $max_old = number_format($invest_info->package_Max);
        $min_new = number_format($check_package->package_Min);
        $max_new = number_format($check_package->package_Max);
        $cur_old = $arr_coin[$invest_info->investment_Currency];
        $cur_new = $arr_coin[$req->investment_Currency];
        $arr_status = ['0' => 'Waiting', '1' => 'Active', '2' => 'Refunded', '-1' => 'Admin Cancel'];
        $status_old = $arr_status[$invest_info->investment_Status];
        $status_new = $arr_status[$req->investment_Status];
        $cmt_log = "<p>Edit Investment $req->investment_ID:</p>
            <p>User ID: $invest_info->investment_User -> $req->investment_User</p>
            <p>Amount: $invest_info->investment_Amount -> $req->investment_Amount</p>
            <p>Package: $min_old$ - $max_old$ ($invest_info->package_Note/Month) -> $min_new$ - $max_new$ ($check_package->package_Note/Month)</p>
            <p>Currency: $cur_old -> $cur_new</p>
            <p>Rate: $invest_info->investment_Rate -> $req->investment_Rate</p>
            <p>Package Time: $invest_info->investment_Package_Time Month -> $req->investment_Package_Time Month</p>
            <p>Time: $datetime_old -> $req->investment_Time</p>
            <p>Status: $status_old -> $status_new</p>
        ";
        $invest_info->investment_User = $req->investment_User;
        $invest_info->investment_Amount = $req->investment_Amount;
        $invest_info->investment_Package = $req->investment_Package;
        $invest_info->investment_Currency = $req->investment_Currency;
        $invest_info->investment_Rate = $req->investment_Rate;
        $invest_info->investment_Package_Time = $req->investment_Package_Time;
        $invest_info->investment_Time = $req->investment_Time;
        $invest_info->investment_Status = $req->investment_Status;
        $invest_info->save();
        Log::insertLog(Session('user')->User_ID, "Edit Investment", 0, $cmt_log);
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Edit Investment Success!']);
    }
	public function getFetchDataMoney($id){
        $row_has_exist = Money::where('Money_ID', $id)->first();
        $row_has_exist->Money_Time = Date('Y-m-d H:i:s', $row_has_exist->Money_Time);
        if(!$row_has_exist){
            return response()->json([
                'status' => 500
            ]);
        }
        else {
            $currency = DB::table('currency')->where('Currency_Active', 1)->get();
            $action = DB::table('moneyaction')->get();
            return response()->json([
                'status' => 200,
                'list' => $row_has_exist,
                'currency' => $currency,
                'action' => $action,
            ]);
        }
    }
    public function putEditDataMoney(Request $req, $id){
        
        $row_has_exist = Money::where('Money_ID', $id)->first();
        if(!$row_has_exist){
            return response()->json([
                'status' => 500
            ]);

        }
        $arg_log = $row_has_exist->toArray();
        $req->validate([
                // 'Money_User' =>  'required|numeric',
                'Money_USDT' =>  'required|numeric',
                'Money_USDTFee' =>  'required|numeric',
                // 'Money_SaleBinary' =>  'numeric',
                // 'Money_Investment' =>  'numeric',
                // 'Money_Borrow' =>  'numeric',
                'Money_Time' =>  'required|date',
                'Money_Comment' =>  'required',
                'Money_MoneyAction' =>  'required|numeric',
                'Money_MoneyStatus' =>  'required|numeric',
                // 'Money_Token' =>  'numeric',
                // 'Money_Address' =>  'required',
                'Money_Currency' =>  'required|numeric',
                'Money_CurrentAmount' =>  'required|numeric',
                'Money_Rate' =>  'required|numeric',
                'Money_Confirm' =>  'required|numeric',
                // 'Money_Confirm_Time' =>  'date',
                
            ]
        );
        // dd(response()->json($arg_log));

        //update row
        // $row_has_exist->Money_User = $req->Money_User;
        $row_has_exist->Money_USDT = $req->Money_USDT;
        $row_has_exist->Money_USDTFee = $req->Money_USDTFee;
        $row_has_exist->Money_SaleBinary = $req->Money_SaleBinary;
        $row_has_exist->Money_Investment = $req->Money_Investment;
        $row_has_exist->Money_Borrow = $req->Money_Borrow;
        $row_has_exist->Money_Time = strtotime($req->Money_Time);
        $row_has_exist->Money_Comment = $req->Money_Comment;
        $row_has_exist->Money_MoneyAction = $req->Money_MoneyAction;
        $row_has_exist->Money_MoneyStatus = $req->Money_MoneyStatus;
        $row_has_exist->Money_Token = $req->Money_Token;
        $row_has_exist->Money_Address = $req->Money_Address;
        $row_has_exist->Money_Currency = $req->Money_Currency;
        $row_has_exist->Money_CurrentAmount = $req->Money_CurrentAmount;
        $row_has_exist->Money_Rate = $req->Money_Rate;
        $row_has_exist->Money_Confirm = $req->Money_Confirm;
        $row_has_exist->Money_Confirm_Time = $row_has_exist->Money_Confirm_Time;
        $row_has_exist->save();
        //Write log

        $after_req = $req->toArray();
        unset($after_req['_method']);
        unset($after_req['_token']);

        $log = DB::table('log_money')->insert([
            'log_money_User' => Session('user')->User_ID,
            'log_money_Beforechange' => json_encode($arg_log),
            'log_money_Afterchange' => json_encode($after_req)
        ]);
        //mess
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "#$id Data change success!"]);
    }
    public function getBalance(Request $req){
        $a = User::getBalance($req->userID);
        dd($a);
    }
    
	public function getBlockInterest(Request $req, $id){
		$user = Session('user');
		if ($user->User_Level == 1 || $user->User_Level == 2 || $user->User_Level == 3) {
			
            $check_blockInterest = User::where('User_ID', $id)->first();
			if($check_blockInterest->User_IsInterest == 0){
				$cmt_log = "Block Interest ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Block Interest", 0, $cmt_log);
				
				$updateBlockInterest = User::where('User_ID', $id)->update([
					'User_IsInterest'=> 1
				]);
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Block Interest Success!']);
			}else{
				$cmt_log = "Unblocked Interest ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Unblocked Interest", 0, $cmt_log);
				
				$updateBlockInterest = User::where('User_ID', $id)->update([
					'User_IsInterest'=> 0
				]);
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Unlocked Interest Success!']);
			}
           
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error!']);
	}
    
	public function getDepressProfit(Request $req, $id){
		$user = Session('user');
		if ($user->User_Level == 1 || $user->User_Level == 2 || $user->User_Level == 3) {
			
            $check_blockInterest = User::where('User_ID', $id)->first();
			if($check_blockInterest->User_DepressInterest == 0){
				$cmt_log = "Block Interest ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "Depress Interest", 0, $cmt_log);
				
				$updateBlockInterest = User::where('User_ID', $id)->update([
					'User_DepressInterest'=> 1
				]);
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Depress Interest Success!']);
			}else{
				$cmt_log = "Unblocked Interest ID User: " . $id;
                Log::insertLog(Session('user')->User_ID, "UnDepress Interest", 0, $cmt_log);
				
				$updateBlockInterest = User::where('User_ID', $id)->update([
					'User_DepressInterest'=> 0
				]);
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'UnDepress Interest Success!']);
			}
           
        }
        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Error!']);
	}
  
  	public function getPriceToken(Request $req){
      	if(Session('user')->User_Level != 1 && Session('user')->User_Level != 3){
			return redirect()->back();
		}
        $PriceToken = DB::table('changes');
        if(!$req->datefrom && !$req->dateto){
            $PriceToken = $PriceToken->whereDate('Changes_Time', '>=', date('Y-m-d H:i:s',  strtotime('-2 weeks')))->whereDate('Changes_Time', '<=', date('Y-m-d H:i:s', strtotime('+3 month')));
        }
        if($req->datefrom){
            $PriceToken = $PriceToken->whereDate('Changes_Time', '>=', date('Y-m-d H:i:s',  strtotime($req->datefrom)));
        }
        if($req->dateto){
            $PriceToken = $PriceToken->whereDate('Changes_Time', '<=', date('Y-m-d H:i:s',  strtotime($req->dateto)));
        }
        $PriceToken = $PriceToken->orderByDesc('Changes_Time')->paginate(50);
      
     	return view('System.Admin.ChangePrice', compact('PriceToken'));
    }
	public function postAddPriceToken(Request $req){
        $this->validate($req,[
            'datetime' => 'required|date_format:Y/m/d H:i:s',
            'price' => 'required|numeric|min:0',
        ]);
		$user = Session('user');
		if(Session('user')->User_Level != 1){
			dd('stop');
		}
		$changes = array(
			'Changes_User'=>$user->User_ID,
            'Changes_Time'=>date('Y-m-d H:i:s',strtotime($req->datetime)),
            'Changes_Price'=>$req->price,
            'Changes_Status'=>1,
		);
     // dd($changes);
        DB::table('changes')->insert($changes);

		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Add Price Success!']);
	}
  	public function postPriceToken(Request $req){
		if(Session('user')->User_Level != 1){
			dd('stop');
		}
		$priceOld = $req->priceOld;
		$percent = DB::table('changes')->where('Changes_ID', $req->ID)->update(['Changes_Price'=>$req->Price, 'Changes_User'=>Session('user')->User_ID]);
        Log::insertLog(Session('user')->User_ID, 'Update Price Token', 0, 'Update Price ID: '.$req->ID.' From '.$priceOld.' To '.$req->Price);

		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Change Price Success!']);
	}
  	public function getMemberSupport(Request $req)
    {
        $user = Session::get('user');
        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'Bot', 15 => 'CSKH');
        if ($user->User_Level != 1 && $user->User_Level != 2 && $user->User_Level != 3 && $user->User_Level != 15) {
            dd('Stop');
        }
        $where = null;
        if ($req->UserID) {
            $where .= ' AND User_ID=' . $req->UserID;
        }
        if ($req->Username) {
            $where .= ' AND User_Name LIKE "' . $req->Username . '"';
        }
        if ($req->Email) {
            $where .= ' AND User_Email LIKE "' . $req->Email . '"';
        }
        if ($req->sponsor) {
            $where .= ' AND User_Parent = ' . $req->sponsor;
        }
        if ($req->agency_level) {
            $where .= ' AND User_Agency_Level = ' . $req->agency_level;
        }
        if ($req->datetime) {
            $where .= ' AND date(User_RegisteredDatetime) = "' . date('Y-m-d', strtotime($req->datetime)) . '"';
        }
        if ($req->status_email != null) {
            $where .= ' AND User_EmailActive = ' . $req->status_email;
        }
        if ($req->level != null) {
            $where .= ' AND User_Level = ' . $req->level;
        }
        if ($req->tree != '') {

            $where .= ' AND User_Tree LIKE "' . str_replace(', ', ',', $req->tree) . '%"';
        }
        if ($req->suntree != '') {

            $where .= ' AND User_SunTree LIKE "' . str_replace(', ', ',', $req->suntree) . '%"';
        }
        

        $user_list = User::leftJoin('google2fa', 'google2fa.google2fa_User', 'users.User_ID')
            ->join('user_level', 'User_Level_ID', 'User_Level')
            ->whereRaw('1 ' . $where)
            ->orderBy('User_RegisteredDatetime', 'DESC');

        $user_list = $user_list->paginate(15);
        
        $user_level = DB::table('user_level')->orderBy('User_Level_ID')->get();
        $user_agency_level = DB::table('user_agency_level')->orderBy('user_agency_level_ID')->get();
        return view('System.Admin.MemberSupport', compact('user_list', 'user_level', 'user_agency_level', 'level'));
    }
  	public function getResentMailActive(Request $req, $id){
      	$user = User::where('User_ID', $id)->first();
		$token = Crypt::encryptString($user->User_ID.':'.time());
      	$user->User_Token = $token;
        $user->save();
      	if($user){
          $data = array('User_ID' => $user->User_ID, 'User_Name' => $user->User_Name,'User_Email'=> $user->User_Email, 'token'=>$token);
          dispatch(new SendMailJobs('Active', $data, 'Active Account!', $user->User_ID));
          
        	return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Resent mail success!']);
        }else{
          return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User not exit!']);
        }

    }
  
  	public function getActiveMailMember(Request $req, $id){

        $user = User::where('User_ID', $id)->first();
        if($user){
            if($user->User_EmailActive == 1){
                return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User not exit!']);
            }else {
                $user->User_EmailActive = 1;
                $user->save();
              	return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Activate Account Success!']);
            }

        }
        return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Error!']);
    }

}
