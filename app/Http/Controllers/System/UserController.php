<?php

namespace App\Http\Controllers\System;

use App\Http\Requests\PersonalInfo;
use App\Http\Requests\Register;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use PragmaRX\Google2FA\Google2FA;

use App\Model\GoogleAuth;
use App\Model\User;
use App\Model\Profile;
use App\Model\Log;
use App\Model\Wallet;
use App\Jobs\SendTelegramJobs;
use App\Model\Investment;
use Crypt;
use App\Jobs\SendMailJobs;
class UserController extends Controller
{
    public function getProfile()
    {
        
        $user = User::find(session('user')->User_ID);
        $trustWalletAddress = DB::table('address')
            ->where('Address_User', $user->User_ID)
            ->where('Address_Currency', 1)
            ->value('Address_Address');

        $google2fa = app('pragmarx.google2fa');
        //kiểm tra member có secret chưa?
        $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

        $Enable = false;
        if($auth == null){
            $secret = $google2fa->generateSecretKey();
            Session::put('Auth', $secret);
        }else{
            $Enable = true;
            $secret = $auth->User_Auth;
        }
        $google2fa->setAllowInsecureCallToGoogleApis(true);

        $inlineUrl = $google2fa->getQRCodeUrl(
            trans("message.name_auth"),
            $user->User_Email,
            $secret
        );
        $kycProfile = Profile::where('Profile_User', $user->User_ID)->first();
        
        return view('System.Members.Profile', compact('user', 'trustWalletAddress','inlineUrl', 'secret', 'Enable', 'kycProfile'));
    }

    public function postAuth(Request $req){
        
        $user = Session('user');
        $google2fa = app('pragmarx.google2fa');
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        $authCode = null;
        if(Session('Auth')){
            $authCode =  Session('Auth');
        }else{
            $authCode = $AuthUser->google2fa_Secret;
        }
        if(!$req->verifyCode){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Please Enter Verification Code']);
        }
        $valid = $google2fa->verifyKey($authCode, $req->verifyCode);

        if($valid){
            //kiểm tra member có secret chưa?
            $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

            if($auth){
                // xoá
                GoogleAuth::where('google2fa_User',$user->User_ID)->delete();
                return redirect()->route('getProfile')->with(['flash_level'=>'success', 'flash_message'=>'Disable Authenticator']);
            }else{
                // Insert bảng google2fa
                $r = new GoogleAuth();
                $r->google2fa_User = $user->User_ID;
                $r->google2fa_Secret = Session('Auth');
                $r->save();
                return redirect()->route('getProfile')->with(['flash_level'=>'success', 'flash_message'=>'Enable Authenticator']);
            }

        }else{
            return redirect()->route('getProfile')->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
    }

    public function postProfileV2(Request $req){
        $google2fa = app('pragmarx.google2fa');
	    $this->validate($req, [
		    'otp' => 'required'
	    ]);

	    $user = User::find(Session('user')->User_ID);
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);

        if(!$valid){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
        $data['User_Email'] = $user->User_Email;
        if($req->addressToken != $user->User_WalletETH ){
	        $data['old_address'] = $user->User_WalletETH;
	        $data['new_address'] = $req->addressToken;
	        $user->User_WalletETH = $req->addressToken;
			dispatch(new SendMailJobs('ChangeAddress', $data, 'Update Wallet Address ETH!', $user->User_ID));
        }
        if($req->addressTRX != $user->User_WalletTRX){
	        $data['old_address'] = $user->User_WalletTRX;
	        $data['new_address'] = $req->addressTRX;
	        $user->User_WalletTRX = $req->addressTRX;
			dispatch(new SendMailJobs('ChangeAddress', $data, 'Update Wallet Address TRX!', $user->User_ID));
        }
        if($req->addressSKC != $user->User_WalletSKC){
	        $data['old_address'] = $user->User_WalletSKC;
	        $data['new_address'] = $req->addressSKC;
	        $user->User_WalletSKC = $req->addressSKC;
			dispatch(new SendMailJobs('ChangeAddress', $data, 'Update Wallet Address SKC!', $user->User_ID));
        }
        
        if($req->coin == 2 || $req->coin == 9){
            $user->User_TypePay = $req->coin;
        }
	    $user->save();
		Session::put('user', $user);
        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Change Information Success!']);
    }
    public function postProfile(Request $req)
    {
        exit();
        //do not use

        $google2fa = app('pragmarx.google2fa');
	    $this->validate($req, [
		    'address' => 'required',
		    'otp' => 'required'
	    ]);

	    $user = User::find(Session('user')->User_ID);
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
		    return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'User Unable Authenticator']);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, $req->otp);

        if(!$valid){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Wrong code']);
        }
        
	    $user->User_WalletAddress = $req->address;
	    // $user->User_CoinCommission = $req->coin;
	    $user->save();
		Session::put('user', $user);
        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Change Information Success!']);
    }

    

    public function PostKYC(Request $request)
    {
// 		dd($request);
        $request->validate([
            'passport' => 'required|unique:profile,Profile_Passport_ID',
            'passport_image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif',
            'passport_image_selfie' =>'required|image|mimes:jpeg,jpg,bmp,png,gif'
        ]);

        $user = session('user');
        
        $checkExist = Profile::where('Profile_User', $user->User_ID)->whereIn('Profile_Status', [0,1] )->first();
        // dd($checkExist);
        if ($checkExist) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "submitted successfully!"]);
        }
        $passportID = $request->passport;
        //get file extension
        $passportImageExtension = $request->file('passport_image')->getClientOriginalExtension();
        $passportImageSelfieExtension = $request->file('passport_image_selfie')->getClientOriginalExtension();
        
        //set folder and file name
        $randomNumber = uniqid();
        $passportImageStore = "users/".$user->User_ID."/profile/passport_image_".$user->User_ID."_".$randomNumber.".".$passportImageExtension;
        $passportImageSelfieStore = "users/".$user->User_ID."/profile/passport_image_selfie_".$user->User_ID."_".$randomNumber.".".$passportImageSelfieExtension;
        //send to Image server
        $passportImageStatus =Storage::disk('ftp')->put($passportImageStore, fopen($request->file('passport_image'), 'r+'));
        $passportImageSelfieStatus =Storage::disk('ftp')->put($passportImageSelfieStore, fopen($request->file('passport_image_selfie'), 'r+'));
        // if($user->User_ID == 2967580843){
        //     dd($passportImageStatus, $passportImageStore);
        // } 
        if ($passportImageStatus and $passportImageSelfieStatus) {
            $insertProfileData = [
                'Profile_User' => $user->User_ID,
                'Profile_Passport_ID' => $passportID,
                'Profile_Passport_Image' => $passportImageStore,
                'Profile_Passport_Image_Selfie' => $passportImageSelfieStore,
                'Profile_Time' => date('Y-m-d H:i:s')
            ];
            $inserStatus = Profile::create($insertProfileData);
            if ($inserStatus) {
				//Gửi telegram thông báo lệh hoa hồng
				// $message = $user->User_ID. " Post KYC\n"
				// 				. "<b>User ID: </b>\n"
				// 				. "$user->User_ID\n"
				// 				. "<b>Email: </b>\n"
				// 				. "$user->User_Email\n"
				// 				. "<b>POST KYC Time: </b>\n"
				// 				. date('d-m-Y H:i:s',time());

				// dispatch(new SendTelegramJobs($message, -364563312));
                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Update profile Noted"]);
            }
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Please contact admin!"]);

        }

        return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Update profile error"]);

    }

    public function getList(Request $req)
    {
        $where = '';
        if (Input::get('from')) {
            $from = strtotime(date('Y-m-d', strtotime(Input::get('from'))));
            $where .= ' AND investment_Time >= ' . $from;
        }
        if (Input::get('to')) {
            $to = strtotime('+1 day', strtotime(date('Y-m-d', strtotime(Input::get('to')))));
            $where .= ' AND investment_Time < ' . $to;
        }
        
	    $user = User::where('User_ID', Session::get('user')->User_ID)->first();
	    $user_list = User::select('User_Tree', 'User_TotalInvestment', 'User_BalanceCommission', 'User_DoubleProfit', 'User_BalanceDeposit', 'Profile_Status','User_ID', 'User_Email', 'User_RegisteredDatetime', 'User_Parent', DB::raw("(CHAR_LENGTH(User_SunTree)-CHAR_LENGTH(REPLACE(User_SunTree, ',', '')))-" . substr_count($user->User_SunTree, ',') . " AS f, User_Agency_Level, User_SunTree"))
                        ->leftJoin('profile', 'Profile_User', 'User_ID')
                        ->whereRaw('User_SunTree LIKE "'.$user->User_SunTree.'%"')
						->where('User_ID','<>',$user->User_ID)
						->orderBy('User_RegisteredDatetime','DESC')
                        ->get();
                        
        foreach($user_list as $v){
            $v->aaa = Investment::totalInvest($v->User_ID, 5)->sum('investment_Amount');
            $v->invest_skc = Investment::totalInvest($v->User_ID, 8)->sum('investment_Amount');
            $v->total_invest_branch = User::where('User_Tree', 'LIKE', $v->User_Tree.'%')
                                        ->sum(DB::raw('User_TotalInvestment'));
        }
        $total_invest_root = User::join('investment', 'investment_User', 'User_ID')->where('User_Tree', 'LIKE', $user->User_Tree.'%')->sum(DB::raw('investment_Amount * investment_Rate'));
	    return view('System.Members.Members-List', compact('user_list', 'total_invest_root'));
    }

    public function getTree(Request $req){

        $user = Session('user');
        $list = array(
			'id' => $user->User_ID,
            'name' => '',
            'title' => $user->User_ID,
            'children' => $this->buildTree($user->User_ID),
            'className' => 'node-tree '.strtoupper($user->User_Name),
        );
        $list = json_encode($list);
        return view('System.Members.Members-Tree',compact('list'));
    }

    function buildTree($idparent, $idRootTemp = null, $barnch = null) {

        $build = User::select('User_Email', 'User_Name','User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($idparent,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $idparent, ',' ,User_ID) )")->orderBy('User_IsRight')->GET();
        $child = array();
        if(count($build) > 0){
            for($i=0;$i<2;$i++){
                if(isset($build[$i])){
                    if($build[$i]->User_IsRight == 0){
                        
                        $child[] = array(
                            'id' => $build[$i]->User_ID,
                            'name' => '',
                            'title' => $build[$i]->User_ID,
                            'className' => 'node-tree '.strtoupper($build[$i]->User_Name),
                            'children' => $this->buildTree($build[$i]->User_ID, $build[$i]->User_ID, 0),
                        );
                        if(count($build) <2){
                            $child[] = array(
                                'id' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'name' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'title' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'className' => 'node-empty right'
                            );
                        }
                        
                    }
                    if($build[$i]->User_IsRight == 1){
                        if(count($build) <2){
                            $child[] = array(
                                'id' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'name' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'title' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'className' => 'node-empty left'
                            );
                        }
    
                        $child[] = array(
                            'id' => $build[$i]->User_ID,
                            'name' => '',
                            'title' => $build[$i]->User_ID,
                            'className' => 'node-tree '.strtoupper($build[$i]->User_Name),
                            'children' => $this->buildTree($build[$i]->User_ID, $build[$i]->User_ID),
                        );
                        
                        
    
                    }
                    
                }
                
    
            }
        }
        else{
            $child[] = array(
                'id' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'name' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'title' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'className' => 'node-empty left'
            );
            $child[] = array(
                'id' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'name' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'title' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'className' => 'node-empty right'
            );
        }
        return $child;
    }
    public function getAjaxUser(Request $req){
        //get address
        $userID = $req->User_ID;
        $result = User::where('User_ID', $userID)->select('User_ID', 'User_Email', 'User_Tree', 'User_Parent')->first();
        if($result){
            return [
                'status' => true,
                'message' => $result->User_Email,
                'class' => 'success'
            ];
        }
        else{
            return [
                'status' => true,
                'message' => 'Not found, please check again!',
                'class' => 'error'
            ];
        }

    
    }
    public function getAjaxSaleUser(Request $req){
        $branch_trade = [];
        $count_children = [];
        $branch_trade['leftTrade'] = 0;
        $branch_trade['rightTrade'] = 0;
        
        $userID = $req->User_ID;
        $result = User::where('User_ID', $userID)->select('User_ID', 'User_Email', 'User_Tree', 'User_Parent')->firstorfail();
        
        //lấy thòi gian đầu tư khi có User đã đầu tư min 50$ thì bắt đầu cộng doanh số nhánh
		$getInvestFirst = Investment::where('investment_User', $userID)->where('investment_Amount', '>=', 50)->orderBy('investment_ID', 'ASC')->first();
        $timeBranch = time();
        if($getInvestFirst){
			$timeBranch = $getInvestFirst->investment_Time;
        }
        
		$branch  = User::select('User_Name','User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($userID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $userID, ',' ,User_ID) )")->orderBy('User_IsRight')->GET();
		if(count($branch) > 0){

            
            for($i=0;$i<2;$i++){
                if(isset($branch[$i])){
                    if($branch[$i]->User_IsRight == 0){
                        
                        $total_sales_children = Investment::join('users', 'User_ID', 'investment_User')->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->where('investment_Time', '>=', $timeBranch)->where('investment_Status', 1)->sum('investment_Amount');
                        $branch_trade['leftTrade'] = number_format($total_sales_children, 2);
                        $count_children['children_left'] = User::where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->count();

                    }
                    if($branch[$i]->User_IsRight == 1){
                        
						$total_sales_children = Investment::join('users', 'User_ID', 'investment_User')->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->where('investment_Time', '>=', $timeBranch)->where('investment_Status', 1)->sum('investment_Amount');
						$branch_trade['rightTrade'] = number_format($total_sales_children, 2);
                        $count_children['children_right'] = User::where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->count();
                    }
                    
                }
                
    
            }
        }
        

		if($result){
			return response()->json([
				'status' => 200,
				'infor' => $result,
                'trade' => $branch_trade,
                'count_children' => $count_children,
			], 200);
        }
    }
    public function postMemberAdd(Request $request){
        
        $request->validate([
            'Email' => 'required|email|max:100|unique:users,User_Email',
            'parent' => 'required',
            'brother' => 'required',
            'node_side' => 'required',
            // 'Password' => 'required|max:40',
            // 'Re-Password' => 'required|same:Password'
        ]);
        if (!filter_var($request->Email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Email format is wrong!']);
		}
        include(app_path() . '/functions/xxtea.php');

        $userID_Parent = $request->parent;

        //Kiểm tra ID Parent đó có tồn tại không????
        $show_ID_Parent = User::where('User_ID', $userID_Parent)->value('User_ID');
        if(!$show_ID_Parent){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Please enter Parent!']);
        }
        //BiẾN tạm LƯU  ID Presenter 
        $userID_Presenter = $request->brother;
        
        if(!$userID_Presenter){
            //nếu cố tình sửa presenter

            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Please enter presenter!']);
            //IF Có nhập người chỉ định
            
        }
        //Add vào left or right do người chỉ định
        $check_location_branch = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("(User_Tree LIKE CONCAT($userID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $userID_Presenter, ',' ,User_ID))")->where('User_IsRight', $request->node_side)->first();
        if($check_location_branch){
            //nêu vị trí nhánh đso đã có người vào thì lỗi
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'This branch already has subscribers!']);
        }
        //Add branch

        if($request->node_side == 0){
            $user_side = 0;
        }
        if($request->node_side == 1){
            $user_side = 1;

        }
        //Tạo random ID user
        $user_id = $this->RandomIDUser();
        // get tree người chỉ định

        $user_Presenter_Tree = User::where('User_ID', $userID_Presenter)->value('User_Tree');


        //xử lý tree
        $user_tree = !$user_Presenter_Tree ? $userID_Presenter . "," . $user_id : $user_Presenter_Tree . ',' . $user_id;

        //nhập parent và presenter khác nhánh
        if( strpos($user_tree, "$show_ID_Parent") === false){
        	return redirect()->route('getRegister')->with(['flash_level' => 'error', 'flash_message' => 'Ponser and Presenter another branch, please check again!']);
        }

        //Tạo token cho mail
        $token = Crypt::encryptString($request->Email.':'.time());
        //Xử lý
        $create_Password = rand(100000, 999999);
        
        //lấy sun tree parent
        $sunTree = (User::where('User_ID', $show_ID_Parent)->value('User_SunTree')).','.$user_id;

        //nhánh show
        $level = 0;
        if(strpos($user_tree, '510619') !== false){
	        $level = 5;
        }

        $Register = [
            'User_ID' => $user_id,
            'User_Email' => $request->Email,
            'User_Parent' => $show_ID_Parent,
            'User_IsRight' => $user_side,
            'User_Tree' => $user_tree,
            'User_SunTree' => $sunTree,
            'User_EmailActive' => 0,
            // 'User_Password' => bcrypt($request->Password),
            'User_Password' => bcrypt($create_Password),
            'User_RegisteredDatetime' => date('Y-m-d H:i:s'),
            'User_Level' => $level,
            'User_Status' => 1,
            'User_Token' => $token,

        ];

        User::insert($Register);
        
        
        //dữ liệu gửi sang mailtemplate
        $data = array('password' => $create_Password,'User_ID' => $user_id, 'User_Email'=> $request->Email, 'token'=>$token);
        //Job
        // gửi mail thông báo
        dispatch(new SendMailJobs('ADD_BINARY', $data, 'Active Account!', $user_id));

        //return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Add Binary Member Success! Login with user name and password!']);
        return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Register Account Success, Please check your email to Activate Account!']);
    }
    public function changeUserSideActivce(Request $req){
        $user = User::find(Session('user')->User_ID);
		$side_active = $req->side_active;
		if($user->User_Side_Active == $side_active){
			return '';
        }
		$user->User_Side_Active = $side_active;
		$user->save();

		return response()->json(['status'=>'success', 'message'=>'Change Node Success!']);
    }
    public function RandomIDUser()
    {
        $id = rand(100000, 999999);
        //TẠO RA ID RANĐOM
        $user = User::where('User_ID', $id)->first();
        //KIỂM TRA ID RANDOM ĐÃ CÓ TRONG USER CHƯA
        if (!$user) {
            return $id;
        } else {
            return $this->RandomIDUser();
        }
    }

    
}
