<?php

namespace App\Http\Controllers\System;

use App\Model\Investment;
use App\Model\Money;
use App\Model\Log;
use App\Model\User;
use GuzzleHttp\Client;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Model\Invesment;
class InvestmentController extends Controller
{
    
    public function investmentToken(Request $req){
        $user = Session('user');
        //Rate Token
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        $rateToken = $rate['TRX'];
        $rateSKC = $rate['SKC'];
        $amount = $req->amount;
        $amountToken = $amount/$rateSKC;
        //Min đầu tư
        if($amountToken < 1000){
            return ['status'=>'error', 'message'=>'Min invest 1000 SKC!'];
        }
        //RATE USD
        $rateUSD = 1;
        //Balance
        $balance = User::where('User_ID', Session('user')->User_ID)->first();
        if($amountToken > $balance->User_BalanceSKC){
            return ['status'=>'error', 'message'=>'Your balance is not enough!'];
        }
        //Bonus
        $getPackage = User::getPackageSKC($user->User_ID, $amountToken);
        //khởi tạo
        $amount_bonus = 0;
        //Update Balance
        $updateBalance = User::where('User_ID', $user->User_ID)->increment('User_BalanceSKC', -$amountToken);
		$totalInvest = Investment::totalInvest($user->User_ID, 8);
		$totalInvest = $totalInvest->sum('investment_Amount');
        //Trừ tiền 
	    $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amountToken,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Invested '.$amountToken.' SKC ~ '.$amount.' USDT',
			'Money_MoneyAction' => 3,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rateSKC,
            'Money_CurrentAmount' => $amountToken,
			'Money_Currency' => 8,
        );
        //Invest
        $invest = array(
		    'investment_User' => $user->User_ID,
            'investment_Amount' => $amountToken,
		    'investment_Rate' => $rateSKC,
		    'investment_Currency' => 8,
		    'investment_Time' => time(),
            'investment_Status' => 1,
            'investment_Bonus' => 0,
            'investment_Total' => $totalInvest+$amountToken
        );
        //Cộng điểm

	    // thêm dữ liệu
	    DB::table('investment')->insert($invest);
        DB::table('money')->insert($moneyArray);
        if($amount_bonus > 0){
            //điều kiện tặng coin
            DB::table('money')->insert($insertPoint);
        }
        //Update updatePoint
        $updatePoint = User::updatePoint($user->User_ID, $amount_bonus);
        //Update Investment
        $updateInvestment = User::updateInvestment($user->User_ID, $amount);
        //Update Sales Tree
        $updateBranchSales = $this->updateBranchSalesTree($user->User_ID);
        //checkDirectCom
        $this->checkCommissionSKC($user, $amountToken);
        //checkBinaryCom
        $this->checkBinaryCommissionSKC($user, $amountToken);
        //update level
        // $this->UpdateLevel($user);
        //check lệnh hoa hồng pending
        $this->checkMaxoutCommissionSKC($user->User_ID, $amountToken);
        $this->BonusBinarySKC($user);
        return ['status'=>'success', 'message'=>'Investment complete'];
        return redirect()->route('system.getInvestment')->with(['flash_level'=>'success','flash_message'=>'Investment complete']);  
    }
	// function cộng tiền hoa hồng cho user
    public function checkCommissionSKC($user, $amount, $support = 0){
        // hoa hồng trực tiếp 
        $percentArr = [1=>0.08, 2=>0.09, 3=>0.1, 4=>0.12, 5=>0.15];
        $currency = 8;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('SKC');
        $getMaxPackage = Money::where('Money_User', $user->User_ID)->where('Money_MoneyAction', 9)->where('Money_Currency', $currency)->where('Money_MoneyStatus', 1)->max('Money_USDT');
        if($getMaxPackage && $getMaxPackage > 0){
	        $getInvestActive = (Investment::where('investment_User', $user->User_ID)->where('investment_Currency', $currency)->where('investment_Status', 1)->sum('investment_Amount'));
	        if($getInvestActive < $getMaxPackage){
		        return false;
	        }
	        if(($getInvestActive - $amount) < $getMaxPackage){
		        $amount = $getInvestActive - $getMaxPackage;
		        if($amount <= 0){
			        return false;
		        }
	        }
		}
        $user = User::find($user->User_ID);
        // if($user->User_Level == 4 || $user->User_Level == 5){
	    //     return false;
        // }
        $parent = User::where('User_ID', $user->User_Parent)->first();
        if(!$parent || $parent->User_Block == 1){
            return false;
        }
        $getPackage = User::getPackageSKC($parent->User_ID);
        if(!$getPackage){
	        return false;
        }
        $percent = $percentArr[$getPackage->package_ID];
        $amountCom = $amount * $percent;
        $checkMaxOut = $this->checkMaxOutSKC($parent->User_ID);
        if($checkMaxOut < $amountCom){
	        if($checkMaxOut > 0){
		        $amountComPending = $amountCom - $checkMaxOut;
		        $money = new Money();
		        $money->Money_User = $parent->User_ID;
		        $money->Money_USDT = $amountComPending;
		        $money->Money_Time = time();
		        $money->Money_Comment = 'Direct Commission From User ID: '.$user->User_ID.' (Pending)';
		        $money->Money_Currency = $currency;
		        $money->Money_MoneyAction = 7;
		        $money->Money_Address = '';
		        $money->Money_Rate = $rate;
		        $money->Money_MoneyStatus = 1;
		        $money->Money_Confirm = 0;
		        $money->save();
		        
		        $amountCom = $checkMaxOut;
	        }else{
		        $amountComPending = $amountCom;
		        $money = new Money();
		        $money->Money_User = $parent->User_ID;
		        $money->Money_USDT = $amountComPending;
		        $money->Money_Time = time();
		        $money->Money_Comment = 'Direct Commission From User ID: '.$user->User_ID.' (Pending)';
		        $money->Money_Currency = $currency;
		        $money->Money_MoneyAction = 7;
		        $money->Money_Address = '';
		        $money->Money_Rate = $rate;
		        $money->Money_MoneyStatus = 1;
		        $money->Money_Confirm = 0;
		        $money->save();
		        return false;
	        }
        }
        $money = new Money();
        $money->Money_User = $parent->User_ID;
        $money->Money_USDT = $amountCom;
        $money->Money_Time = time();
        $money->Money_Comment = 'Direct Commission (F1 '.($percent*100).'%) From User ID: '.$user->User_ID;
        $money->Money_Currency = $currency;
        $money->Money_MoneyAction = 5;
        $money->Money_Address = '';
        $money->Money_Rate = $rate;
        $money->Money_MoneyStatus = 1;
        $money->save();
        //gói hỗ trợ ko cộng balance
        if($support == 0){
	        //Update Balance
            $updateBalance = User::where('User_ID', $parent->User_ID)->increment('User_BalanceSKC', $amountCom);
	        // $this->AffiliateCommission($parent, $amountCom);
        }
    }
    
    public function checkBinaryCommissionSKC($user, $amount, $support = 0){
	    
        $percentArr = [1=>0.05, 2=>0.05, 3=>0.06, 4=>0.08, 5=>0.1];
        $percentArrAffiliate = [1=>0.01, 2=>0.014, 3=>0.016, 4=>0.02];
        $currency = 8;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('SKC');
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        // dd($arrParent);
		// $percentBinary = 0.05;
		//lấy level của user hiện tại
		$levelStarCurrent = $user->User_Agency_Level;
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            if(!$parentTree || $parentTree->User_Block == 1){
                continue;
            }
	        $getPackage = User::getPackageSKC($parentTree->User_ID);
	        if(!$getPackage){
		        continue;
	        }
	        $percentBinary = $percentArr[$getPackage->package_ID];
            //lấy doanh số 2 nhánh
            $branch_sales = $this->branchSalesSKC($parentTree->User_ID);
            $smallBranch = min($branch_sales);
	        // lấy doanh số binary đã trả
	        $getLastCom = Money::where('Money_User', $parentTree->User_ID)->where('Money_Currency', $currency)->where('Money_MoneyStatus', 1)->where('Money_MoneyAction', 13)->orderByDesc('Money_ID')->first();
	        $saleBinary = 0;
	        if($getLastCom){
	            $saleBinary = $getLastCom->Money_SaleBinary;
	        }
	        // tính doanh số còn lại được nhận
	        $saleCom = $smallBranch - $saleBinary;
	        if($saleCom <= 0){
	            continue;
	        }
	        
	        $amountComBinary = $saleCom * $percentBinary;
	        $checkMaxOut = $this->checkMaxOutSKC($parentTree->User_ID);
	        if($checkMaxOut < $amountComBinary){
		        if($checkMaxOut > 0){
			        $amountComBinary = $checkMaxOut;
		        }else{
			        continue;
		        }
	        }
	        $amountComBinary = round($amountComBinary,8);
	        if($amountComBinary <= 0){
		        continue;
	        }
	        // cộng tiền binary tree commission
	        $money = new Money();
            $money->Money_User = $parentTree->User_ID;
            $money->Money_USDT = $amountComBinary;
            $money->Money_Time = time();
            $money->Money_Comment = 'Binary Tree Commission (F1 '.($percentBinary*100).'%) From User ID: '.$user->User_ID;
            $money->Money_Currency = $currency;
            $money->Money_SaleBinary = $smallBranch;
            $money->Money_MoneyAction = 13;
            $money->Money_Address = '';
            $money->Money_Rate = $rate;
            $money->Money_MoneyStatus = 1;
            $money->save();
            //gói hỗ trợ ko cộng balance
            if($support == 0){
		        //Update Balance
                $updateBalance = User::where('User_ID', $parentTree->User_ID)->increment('User_BalanceSKC', $amountComBinary);
		        // $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountComBinary);
		        // $this->AffiliateCommission($parentTree, $amountComBinary);
            }
	    }
	    
	    // $this->sendToken(15);
    }

	public function branchSalesSKC($user_ID){
		$branch_sales = [];
		$branch_sales['left'] = 0;
		$branch_sales['right'] = 0;
		//lấy ra thông tin thằng cha
		$result = User::where('User_ID', $user_ID)->first();
		if(!$result){
			return $branch_sales;
		}
		//lấy thòi gian đầu tư khi có User đã đầu tư min 50$ thì bắt đầu cộng doanh số nhánh
		$getInvestFirst = Investment::where('investment_User', $user_ID)->where('investment_Amount', '>=', 1000)->where('investment_Currency', 8)->orderBy('investment_ID', 'ASC')->first();
		$timeBranch = time();
		if($getInvestFirst){
			$timeBranch = $getInvestFirst->investment_Time;
		}
		else{
			//nếu chưa đầu tư thì doanh số == 0
			return $branch_sales;
		}
		$branch  = User::select('User_Email','User_ID', 'User_Tree', 'User_IsRight', 'User_TotalInvestment')->whereRaw("( User_Tree LIKE CONCAT($user_ID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID, ',' ,User_ID) )")->orderBy('User_IsRight')->get();
		if(count($branch) > 0){
            for($i=0;$i<2;$i++){
                if(isset($branch[$i])){
                    if($branch[$i]->User_IsRight == 0){
						
						$left = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')
								->where('investment_Time', '>=', $timeBranch)
								->where('investment_Currency', 8)
								->where('investment_Status', 1)
								->sum(DB::raw('`investment_Amount`'));
						
						$branch_sales['left'] = $left;
                        
                    }
                    if($branch[$i]->User_IsRight == 1){
						
						$right = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')
								->where('investment_Time', '>=', $timeBranch)
								->where('investment_Currency', 8)
								->where('investment_Status', 1)
								->sum(DB::raw('`investment_Amount`'));
						$branch_sales['right'] = $right;

                    }
                }
            }
		}
		return $branch_sales;

	}
    
    public function BonusBinarySKC($user){
        $currency = 5;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('USD');
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        //         dd($arrParent);
        // 		$percentBinary = 0.05;
		$action = 23;
		$arrBonus = [1000000=>2500, 2000000=>6000, 5000000=>15000, 15000000=>60000 ];
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            if(!$parentTree){
                continue;
            }
            $getFirstInvest = Investment::where('investment_User', $parentTree->User_ID)->where('investment_Currency', 8)->where('investment_Status', 1)->orderBy('investment_ID')->first();
            if(!$getFirstInvest){
	            continue;
            }
            
            // if(time() > strtotime('+30 days', $getFirstInvest->investment_Time)){
	        //     continue;
            // }
            
	        $getPackage = User::getPackageSKC($parentTree->User_ID);
	        if(!$getPackage){
		        return false;
	        }
            //lấy doanh số 2 nhánh
            $branch_sales = $this->branchSalesSKC($parentTree->User_ID);
            $smallBranch = min($branch_sales);
            if($smallBranch >= 1000000){
				$checkBonus = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyAction', $action)->where('Money_MoneyStatus', 1)->groupBy('Money_User')->selectRaw('SUM(`Money_USDT`) as Money_USDT')->first();
	            $amountBonus = $arrBonus[1000000];
                $commentSales = 1000000;
                $level = 1;
	            if($smallBranch >= 2000000){
	            	$amountBonus = $arrBonus[2000000];
	            	$commentSales = 2000000;
                    $level = 2;
		        }
	            if($smallBranch >= 5000000){
		            $amountBonus = $arrBonus[5000000];
	            	$commentSales = 5000000;
                    $level = 3;
		        }
	            if($smallBranch >= 15000000){
		            $amountBonus = $arrBonus[15000000];
	            	$commentSales = 15000000;
                    $level = 4;
		        }
		        $amountBonusReceived = 0;
		        if($checkBonus){
			    	$amountBonusReceived = $checkBonus->Money_USDT;
		        }
		        $amountBonus = $amountBonus - $amountBonusReceived;
		        
				if($amountBonus > 0){
			        $money = new Money();
			        $money->Money_User = $parentTree->User_ID;
			        $money->Money_USDT = $amountBonus;
			        $money->Money_Time = time();
			        $money->Money_Comment = 'Bonus Binary Sales '.$commentSales.' SKC';
			        $money->Money_Currency = $currency;
			        $money->Money_MoneyAction = $action;
			        $money->Money_Address = '';
			        $money->Money_Rate = $rate;
			        $money->Money_MoneyStatus = 1;
				    $money->save();
			        //Update Balance
                    // $updateBalance = User::where('User_ID', $parentTree->User_ID)->increment('User_BalanceSKC', $amountBonus);
			        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountBonus);
                }
                if($level>=1 && $level > $parentTree->User_Agency_LevelSKC){
                    User::where('User_ID', $parentTree->User_ID)->update(['User_Agency_LevelSKC'=>$level]);
                }
            }
        }
    }
    
    public function checkMaxoutCommissionSKC($userID, $amountInvest){
	    //lấy những lệnh action pending và chưa duyệt
	    $getPendingCom = Money::where('Money_User', $userID)
    						->where('Money_MoneyAction', 7)
    						->where('Money_Confirm', 0)
    						->where('Money_Currency', 8)
    						->whereRaw(time().' - Money_Time < 86400')
    						->get();
    	$arrayPaid = [];
    	$maxoutNew = $amountInvest*4;
	    foreach($getPendingCom as $money){
		    $amount = $money->Money_USDT;
		    if($amount > $maxoutNew){
			    //cập nhật số tiền pending lại
			    $money->Money_USDT = $amount - $maxoutNew;
		        $money->save();
			    $amount = $maxoutNew;
			    //tạo lệnh mới vào bảng money
			    $confirmNew = $money->replicate();
			    //sửa comment và số tiền còn lại
			    $confirmNew->Money_USDT = $maxoutNew;
			    $comment = $confirmNew->Money_Comment;
			    $comment = str_replace('(Pending)', "", $comment);
			    if(strpos($comment, "Direct") !== false){
				    $action = 5;
			    }else{
				    $action = 13;
			    }
			    $confirmNew->Money_MoneyAction = $action;
			    $confirmNew->Money_Comment = $comment;
			    $confirmNew->save();
                $updateBalance = User::where('User_ID', $userID)->increment('User_BalanceSKC', $amount);
		        // $updateBalance = User::updateBalanceDeposit($userID, $amount);
		        $arrayPaid[] = $money->Money_ID;
		    }else{
			    $confirmNew = $money->replicate();
			    $comment = $confirmNew->Money_Comment;
			    $comment = str_replace('(Pending)', "", $comment);
			    if(strpos($comment, "Direct") !== false){
				    $action = 5;
			    }else{
				    $action = 13;
			    }
			    $confirmNew->Money_MoneyAction = $action;
			    $confirmNew->Money_Comment = $comment;
			    $confirmNew->save();
                $updateBalance = User::where('User_ID', $userID)->increment('User_BalanceSKC', $amount);
		        // $updateBalance = User::updateBalanceDeposit($userID, $amount);
		        //confirm lệnh 
		        $money->Money_USDT = 0;
		        $money->Money_Confirm = 1;
		        $money->save();
		        $arrayPaid[] = $money->Money_ID;
		    }
	    }
	    
	    $getPendingCom = Money::where('Money_User', $userID)
	    					->whereNotIn('Money_ID', $arrayPaid)
    						->where('Money_MoneyAction', 7)
    						->where('Money_Currency', 8)
    						->where('Money_Confirm', 0)
    						->update(['Money_Confirm'=>-1]);
    	return true;
    }

	public function checkMaxOutSKC($user_ID){
		$checkMaxout = User::where('User_ID', $user_ID)->value('User_UnMaxout');
		if($checkMaxout == 1){
			return 999999999;
		}
		$total_invest = Investment::where('investment_User', $user_ID)->where('investment_Status', 1)->where('investment_Currency', 8)->sum(DB::raw('investment_Amount'));
		//400% maxout 
		$percent_maxout = 4;

		//SUM COMMISSION And Interest
		$total_com = Money::where('Money_User', $user_ID)
							->whereIN('Money_MoneyAction', [4,5,6,9,13,14])
							->where('Money_Currency', 8)
							->where('Money_MoneyStatus', 1)
							->sum('Money_USDT');
		
		$total_invest = $total_invest * $percent_maxout;
		$amountMaxout = $total_invest - $total_com;
		//báo sắp maxout
		if($total_com >= ($total_invest*3.8)){
	        //gửi mail thông báo gần maxout
	        $userInfo = User::find($user_ID);
			$data = array('ID'=>$userInfo->User_ID);
			// dispatch(new SendMailJobs($userInfo, $data, 'NoticeRefund', 'Notice MaxOut Investment'));
		}
		return $amountMaxout;
	}

    public function postReInvest(Request $req){
	    $user = User::find(Session('user')->User_ID);
	    if($user->User_Level != 0){
			// return response()->json(['status'=>false, 'message'=>'Only Member Can Use!'], 200);
	    }
	    //lấy action reinvest hoặc thu về ví
	    $action = $req->action;
	    if(!$action){
			return response()->json(['status'=>false, 'message'=>'Action invalid!'], 200);
	    }
	    //lấy lệnh trả gửi tiền về ví của user
	    $getRefund = Money::where('Money_User', $user->User_ID)
	    					->where('Money_ID', $req->id)
	    					->where('Money_MoneyAction', 10)
	    					->where('Money_MoneyStatus', 1)
	    					->where('Money_Confirm', 0)->first();
	    if(!$getRefund || $getRefund->Money_Confirm != 0){
			return response()->json(['status'=>false, 'message'=>'This Action Confirmed!'], 200);
	    }
	    
		$getRefund->Money_Confirm = 1;
		$getRefund->save();
		
        $amount = $getRefund->Money_USDT;
	    if($action == 3 && $user->User_Level == 0){
	        //Invest
	        $invest = array(
			    'investment_User' => $user->User_ID,
	            'investment_Amount' => $amount,
			    'investment_Rate' => 1,
			    'investment_Currency' => 5,
			    'investment_Time' => time(),
	            'investment_Status' => 1,
	            'investment_Bonus' => 0
	        );
	        //Cộng điểm
		    // thêm dữ liệu
		    DB::table('investment')->insert($invest);
			return response()->json(['status'=>true, 'message'=>'Reinvest success!'], 200);
	    }else{
          	$currency = 8;
		    $symbol = "SKC";
			//Refund 
			$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
			$amountCoin = app('App\Http\Controllers\Cron\CronController')->floorp(($amount / $rate), 6);
            $money = new Money();
            $money->Money_User = $user->User_ID;
            $money->Money_USDT = $amountCoin;
            $money->Money_Time = time();
            $money->Money_Comment = 'Refund Investment $'.($amount*1);
            $money->Money_Currency = $currency;
			$money->Money_MoneyAction = 17;
            $money->Money_Address = '';
            $money->Money_Rate = $rate;
            $money->Money_MoneyStatus = 1;
            $money->Money_Confirm = 1;
            $money->save();
            //gói hỗ trợ ko cộng balance
            //Update Balance
            $updateBalanceToken = User::where('User_ID', $user->User_ID)->increment('User_BalanceSKCProfit', $amountCoin);
          	/*
		    $address = Wallet::getCurrentWallet($user->User_ID);
		    if(!$address){
				$getRefund->Money_Confirm = 0;
				$getRefund->save();
		        return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Please update your wallet']);
		    }
          	$currency = 8;
          	$currency = $$user->User_TypePay;
		    $symbol = "SKC";
			//Refund 
			$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateSell($symbol);
			$amountCoin = app('App\Http\Controllers\Cron\CronController')->floorp(($amount / $rate), 6);
	    	// trừ balance
			$row = new Money();
			$row->Money_User = $user->User_ID;
			$row->Money_USDT = -$amount;
			$row->Money_Time = time();
			$row->Money_Comment = 'Send: '.$amountCoin.' '.$symbol.' To Address: '.$address;
			$row->Money_MoneyAction = 17;
			$row->Money_MoneyStatus = 1;
	        $row->Money_Address = $address;
			$row->Money_Currency = $user->User_TypePay;
			$row->Money_CurrentAmount = $amountCoin;
			$row->Money_Rate = $rate;
			if($user->User_Level != 0){
				$row->Money_Confirm = 1;
			}
			$row->save();
            */
			/*
				//Rèund ETH
			$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateSell('ETH');
			$amountETH = app('App\Http\Controllers\Cron\CronController')->floorp(($amount / $rate * (0.97)), 6);
	    	// trừ balance
			$row = new Money();
			$row->Money_User = $user->User_ID;
			$row->Money_USDT = -$amount;
			$row->Money_Time = time();
			$row->Money_Comment = 'Send: '.$amountETH.' ETH To Address: '.$user->User_WalletETH;
			$row->Money_MoneyAction = 13;
			$row->Money_MoneyStatus = 1;
			$row->Money_Currency = 2;
			$row->Money_CurrentAmount = $amountETH;
			$row->Money_Rate = $rate;
			if($user->User_Level != 0){
				$row->Money_Confirm = 1;
				$row->Money_IsPaid = 1;
			}
			$row->save();
            */
			
			//Gửi telegram thông báo lệh hoa hồng
			/*
        $message = $user->User_ID. " Send Refund Invest\n"
							. "<b>User ID: </b>\n"
							. "$user->User_ID\n"
							. "<b>Email: </b>\n"
							. "$user->User_Email\n"
							. "<b>Address: </b>\n"
							. "$user->User_WalletETH\n"
							. "<b>Amount: </b>\n"
							. $amountETH." ETH\n"
							. "<b>Send Coin Time: </b>\n"
							. date('d-m-Y H:i:s',time());
            */
			
			// dispatch(new SendTelegramJobs($message, -297921617));

			return response()->json(['status'=>true, 'message'=>'Success! Please waiting refund coin to your wallet!'], 200);
	    }
	    
    }
	
	public function postSendTRX(Request $req){
		
	    $user = User::find(Session('user')->User_ID);
	    if($user->User_Level != 0){
			// return response()->json(['status'=>false, 'message'=>'Only Member Can Use!'], 200);
	    }
	    //lấy action reinvest hoặc thu về ví
	    $action = $req->action;
	    if(!$action){
			return response()->json(['status'=>false, 'message'=>'Action invalid!'], 200);
	    }
	    //lấy lệnh trả gửi tiền về ví của user
	    $getRefund = Money::where('Money_User', $user->User_ID)
	    					->where('Money_ID', $req->id)
	    					->where('Money_MoneyAction', 10)
	    					->where('Money_MoneyStatus', 1)
	    					->where('Money_Confirm', 0)->first();
	    if(!$getRefund || $getRefund->Money_Confirm != 0){
			return response()->json(['status'=>false, 'message'=>'This Action Confirmed!'], 200);
	    }
	    
		$getRefund->Money_Confirm = 1;
		$getRefund->save();
		
	    if($action == 3 && $user->User_Level == 0){
		    $amount = $getRefund->Money_USDT;
	        //Invest
	        $invest = array(
			    'investment_User' => $user->User_ID,
	            'investment_Amount' => $amount,
			    'investment_Rate' => 1,
			    'investment_Currency' => 5,
			    'investment_Time' => time(),
	            'investment_Status' => 1,
	            'investment_Bonus' => 0
	        );
	        //Cộng điểm
		    // thêm dữ liệu
		    DB::table('investment')->insert($invest);
			return response()->json(['status'=>true, 'message'=>'Success! Please Reinvest!'], 200);
	    }else{
		    $amount = $getRefund->Money_USDT;
		    
		    $address = Wallet::getCurrentWallet($user->User_ID);
		    if(!$address){
				$getRefund->Money_Confirm = 0;
				$getRefund->save();
		        return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Please update your wallet']);
		    }
			//Refund 
			$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
			$amountCoin = app('App\Http\Controllers\Cron\CronController')->floorp(($amount / $rate), 6);
	    	// trừ balance
			$row = new Money();
			$row->Money_User = $user->User_ID;
			$row->Money_USDT = -$amount;
			$row->Money_Time = time();
			$row->Money_Comment = 'Send: '.$amountCoin.' '.$symbol.' To Address: '.$address;
			$row->Money_MoneyAction = 20;
			$row->Money_MoneyStatus = 1;
	        $row->Money_Address = $address;
			$row->Money_Currency = $user->User_TypePay;
			$row->Money_CurrentAmount = $amountCoin;
			$row->Money_Rate = $rate;
			if($user->User_Level != 0){
				$row->Money_Confirm = 1;
			}
			$row->save();
			
			//Gửi telegram thông báo lệh hoa hồng
			/*
            $message = $user->User_ID. " Send Refund Invest\n"
							. "<b>User ID: </b>\n"
							. "$user->User_ID\n"
							. "<b>Email: </b>\n"
							. "$user->User_Email\n"
							. "<b>Address: </b>\n"
							. "$user->User_WalletETH\n"
							. "<b>Amount: </b>\n"
							. $amountETH." ETH\n"
							. "<b>Send Coin Time: </b>\n"
							. date('d-m-Y H:i:s',time());
            */
			
			// dispatch(new SendTelegramJobs($message, -297921617));

			return response()->json(['status'=>true, 'message'=>'Success! Please waiting send bonus coin to your wallet!'], 200);
	    }
	}
	
    public function getInvestment(){
        $RandomToken = Money::RandomToken();
        $user = session('user');
        $balance = User::where('User_ID', Session('user')->User_ID)->first();
        $history_invest = Investment::totalInvest($user->User_ID, 5);
		$totalInvest = $history_invest->sum('investment_Amount');
        $interest_current = DB::table('interest_day')->where('interest_User', $user->User_ID)->first();
		$packagePlan['USDT'] = DB::table('package')
    				->whereRaw($balance->User_TotalInvestment." BETWEEN package_Min AND package_Max")
    				->select('package_Interest', 'package_Date', 'package_Name')
                    ->first();

        $history_invest_skc = Investment::totalInvest($user->User_ID, 8);
        $totalInvestSKC = $history_invest_skc->sum('investment_Amount');
        $packagePlan['SKC'] = DB::table('package_skc')
                    ->whereRaw($totalInvestSKC." BETWEEN package_Min AND package_Max")
                    ->select('package_Interest', 'package_Date', 'package_Name')
                    ->first();
        $totalBonus = Investment::where('investment_User', $user->User_ID)->sum('investment_Bonus');
        $rate['SKC'] = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('SKC');
        // dd($rate);
        return view('System.Investment.Index', compact('interest_current','balance', 'RandomToken', 'history_invest', 'history_invest_skc', 'totalInvest', 'packagePlan', 'totalBonus', 'rate', 'totalInvestSKC'));
    }

    public function getInfo_Package($id){
        $package_info = DB::table('package')->where('package_ID', $id)->first();
        if(!$package_info){
            exit();
        }
        return response()->json($package_info);
    }
    public function postInvestment(Request $req){
        //check spam
        $checkSpam = DB::table('string_token')->where('User', Session('user')->User_ID)->where('Token', $req->CodeSpam)->first();
        if($checkSpam == null){
            //khoong toonf taij
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Misconduct!']);
        }
        else{
            DB::table('string_token')->where('User', Session('user')->User_ID)->delete();
        }
        $user = Session('user');
        if($user->User_Level == 5){
            // return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'You can not investment!']); 
        }
        $req->validate([
            'amount' => 'required|numeric|min:0',
        ]);
        //Check amount
        $amount = $req->amount;
        if($amount <= 0){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Investment amount invalid']);
        }
        if($req->currency == 8){
            $investToken = $this->investmentToken($req);
            return redirect()->back()->with(['flash_level' => $investToken['status'], 'flash_message' => $investToken['message']]);
        }
        //Min đầu tư
        if($amount < 400){
            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => 'Min invest $400!']);
        }
        //RATE USD
        $rateUSD = 1;
        //Rate Token
        $rateToken = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('TRX');
        //Balance
        $balance = User::where('User_ID', Session('user')->User_ID)->first();
        if($amount > $balance->User_BalanceDeposit){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Your balance is not enough!']);
        }
        //Bonus
        $getPackage = User::getPackage($user->User_ID, $amount);
        //khởi tạo
        $amount_bonus = 0;
        // if($getPackage){
        //     //Lấy tổng tiền đã bonus
        //     $sum_bonus = Money::where('Money_User', $user->User_ID)
        //                 ->where('Money_MoneyAction', 19)
        //                 ->where('Money_MoneyStatus', 1)
        //                 ->sum('Money_USDT');
        //     $amount_bonus = $getPackage->package_Point - $sum_bonus;
        //     $amount_bonus = 0;
	    //     $insertPoint = [];
	    //     if($amount_bonus > 0){
	    //         //chưa bonus thì sẽ bonus
	    //         $insertPoint = array(
	    //             'Money_User' => $user->User_ID,
	    //             'Money_USDT' => $amount_bonus,
	    //             'Money_USDTFee' => 0,
	    //             'Money_Time' => time(),
	    //             'Money_Comment' => "Bonus Investment ".($amount_bonus+0)." TRX",
	    //             'Money_MoneyAction' => 19,
	    //             'Money_MoneyStatus' => 1,
	    //             'Money_Rate' => $rateToken,
	    //             'Money_CurrentAmount' => $amount_bonus,
	    //             'Money_Currency' => 9
	    //         );
	    //     }
        // }
        //Trừ tiền 
	    $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amount,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Invested '.$amount.' USDT',
			'Money_MoneyAction' => 3,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rateUSD,
            'Money_CurrentAmount' => $amount,
			'Money_Currency' => 5
        );
        //Invest
        $invest = array(
		    'investment_User' => $user->User_ID,
            'investment_Amount' => $amount,
		    'investment_Rate' => $rateUSD,
		    'investment_Currency' => 5,
		    'investment_Time' => time(),
            'investment_Status' => 1,
            'investment_Bonus' => $amount_bonus,
            'investment_Total' => $amount+$balance->User_TotalInvestment
        );
        //Cộng điểm

	    // thêm dữ liệu
	    DB::table('investment')->insert($invest);
        DB::table('money')->insert($moneyArray);
        if($amount_bonus > 0){
            //điều kiện tặng coin
            DB::table('money')->insert($insertPoint);
            
        }
        //Update Balance
        $updateBalance = User::updateBalanceDeposit($user->User_ID, -$amount);
        //Update updatePoint
        $updatePoint = User::updatePoint($user->User_ID, $amount_bonus);
        //Update Investment
        $updateInvestment = User::updateInvestment($user->User_ID, $amount);
        //Update Sales Tree
        $updateBranchSales = $this->updateBranchSalesTree($user->User_ID);
        //checkDirectCom
        app('App\Http\Controllers\Cron\CronController')->checkCommission($user, $amount);
        //checkBinaryCom
        app('App\Http\Controllers\Cron\CronController')->checkBinaryCommission($user, $amount);
        //update level
        app('App\Http\Controllers\Cron\CronController')->UpdateLevel($user);
        
        //check lệnh hoa hồng pending
        $this->checkMaxoutCommission($user->User_ID, $amount);
        
        $this->BonusBinary($user);

        return redirect()->route('system.getInvestment')->with(['flash_level'=>'success','flash_message'=>'Investment complete']);  
    }
    
    public function BonusBinary($user){
        $currency = 5;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('USD');
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        //         dd($arrParent);
        // 		$percentBinary = 0.05;
		$action = 22;
		$arrBonus = [20000=>500, 30000=>1000, 50000=>2000];
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            if(!$parentTree || $parentTree->User_TotalInvestment < 500){
                continue;
            }
            $getFirstInvest = Investment::where('investment_User', $parentTree->User_ID)->where('investment_Status', 1)->orderBy('investment_ID')->first();
            if(!$getFirstInvest){
	            continue;
            }
            
            if(time() > strtotime('+30 days', $getFirstInvest->investment_Time)){
	            continue;
            }
            
	        $getPackage = User::getPackage($parentTree->User_ID);
	        if(!$getPackage){
		        return false;
	        }
            //lấy doanh số 2 nhánh
            $branch_sales = app('App\Http\Controllers\Cron\CronController')->branchSales($parentTree->User_ID);
            $smallBranch = min($branch_sales);
            if($smallBranch >= 20000){
				$checkBonus = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyAction', $action)->where('Money_MoneyStatus', 1)->groupBy('Money_User')->selectRaw('SUM(`Money_USDT`) as Money_USDT')->first();
	            $amountBonus = $arrBonus[20000];
            	$commentSales = 20000;
	            if($smallBranch >= 30000){
	            	$amountBonus = $arrBonus[30000];
	            	$commentSales = 30000;
		        }
	            if($smallBranch >= 50000){
		            $amountBonus = $arrBonus[50000];
	            	$commentSales = 50000;
		        }
		        $amountBonusReceived = 0;
		        if($checkBonus){
			    	$amountBonusReceived = $checkBonus->Money_USDT;
		        }
		        $amountBonus = $amountBonus - $amountBonusReceived;
		        
				if($amountBonus > 0){
			        $money = new Money();
			        $money->Money_User = $parentTree->User_ID;
			        $money->Money_USDT = $amountBonus;
			        $money->Money_Time = time();
			        $money->Money_Comment = 'Bonus Binary Sales $'.$commentSales;
			        $money->Money_Currency = $currency;
			        $money->Money_MoneyAction = $action;
			        $money->Money_Address = '';
			        $money->Money_Rate = $rate;
			        $money->Money_MoneyStatus = 1;
				    $money->save();
			        //Update Balance
			        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountBonus);
				}
            }
            /*
            if($smallBranch >= 30000){
	            $amountBonus = $arrBonus[30000];
				$checkBonus = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyAction', $action)->where('Money_USDT', $amountBonus)->where('Money_MoneyStatus', 1)->first();
				if(!$checkBonus){
			        $money = new Money();
			        $money->Money_User = $parentTree->User_ID;
			        $money->Money_USDT = $amountBonus;
			        $money->Money_Time = time();
			        $money->Money_Comment = 'Bonus Binary Sales $30000';
			        $money->Money_Currency = $currency;
			        $money->Money_MoneyAction = $action;
			        $money->Money_Address = '';
			        $money->Money_Rate = $rate;
			        $money->Money_MoneyStatus = 1;
				    $money->save();
			        //Update Balance
			        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountBonus);
				}
            }
            if($smallBranch >= 50000){
	            $amountBonus = $arrBonus[50000];
				$checkBonus = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyAction', $action)->where('Money_USDT', $amountBonus)->where('Money_MoneyStatus', 1)->first();
				if(!$checkBonus){
			        $money = new Money();
			        $money->Money_User = $parentTree->User_ID;
			        $money->Money_USDT = $amountBonus;
			        $money->Money_Time = time();
			        $money->Money_Comment = 'Bonus Binary Sales $50000';
			        $money->Money_Currency = $currency;
			        $money->Money_MoneyAction = $action;
			        $money->Money_Address = '';
			        $money->Money_Rate = $rate;
			        $money->Money_MoneyStatus = 1;
				    $money->save();
			        //Update Balance
			        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountBonus);
				}
            }
            */
        }
    }
    
    public function checkMaxoutCommission($userID, $amountInvest){
	    //lấy những lệnh action pending và chưa duyệt
	    $getPendingCom = Money::where('Money_User', $userID)
    						->where('Money_MoneyAction', 7)
    						->where('Money_Confirm', 0)
    						->where('Money_Currency', 5)
    						->whereRaw(time().' - Money_Time < 86400')
    						->get();
    	$arrayPaid = [];
    	$maxoutNew = $amountInvest*4;
	    foreach($getPendingCom as $money){
		    $amount = $money->Money_USDT;
		    if($amount > $maxoutNew){
			    //cập nhật số tiền pending lại
			    $money->Money_USDT = $amount - $maxoutNew;
		        $money->save();
			    $amount = $maxoutNew;
			    //tạo lệnh mới vào bảng money
			    $confirmNew = $money->replicate();
			    //sửa comment và số tiền còn lại
			    $confirmNew->Money_USDT = $maxoutNew;
			    $comment = $confirmNew->Money_Comment;
			    $comment = str_replace('(Pending)', "", $comment);
			    if(strpos($comment, "Direct") !== false){
				    $action = 5;
			    }else{
				    $action = 13;
			    }
			    $confirmNew->Money_MoneyAction = $action;
			    $confirmNew->Money_Comment = $comment;
			    $confirmNew->save();
		        $updateBalance = User::updateBalanceDeposit($userID, $amount);
		        $arrayPaid[] = $money->Money_ID;
		    }else{
			    $confirmNew = $money->replicate();
			    $comment = $confirmNew->Money_Comment;
			    $comment = str_replace('(Pending)', "", $comment);
			    if(strpos($comment, "Direct") !== false){
				    $action = 5;
			    }else{
				    $action = 13;
			    }
			    $confirmNew->Money_MoneyAction = $action;
			    $confirmNew->Money_Comment = $comment;
			    $confirmNew->save();
		        $updateBalance = User::updateBalanceDeposit($userID, $amount);
		        //confirm lệnh 
		        $money->Money_USDT = 0;
		        $money->Money_Confirm = 1;
		        $money->save();
		        $arrayPaid[] = $money->Money_ID;
		    }
	    }
	    
	    $getPendingCom = Money::where('Money_User', $userID)
	    					->whereNotIn('Money_ID', $arrayPaid)
    						->where('Money_MoneyAction', 7)
    						->where('Money_Confirm', 0)
    						->update(['Money_Confirm'=>-1]);
    	return true;
    }
    
    public function postChooseInterest(Request $req){
        $req->validate([
            'selectMonth' => 'required'
        ],[
            'selectMonth.required' => 'Please select interest!'
        ]);
        $user = Session('user');
        //check đã đầu tư hay chưa
        $check_invest = User::where('User_ID', Session('user')->User_ID)->value('User_TotalInvestment');
        if(!$check_invest){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'You have not invested']);
        }
        //check dup
        $check_dup = DB::table('interest_day')->where('interest_User', $user->User_ID)->where('interest_Status', 1)->first();
        if($check_dup){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'You cannot change']);
        }
        $comment = '';
        $text = '';
        $num = 0;
        if($req->selectMonth == '1w'){
            $text = '1 Week';
            $num = 7;
        }
        elseif($req->selectMonth == '1m'){

            $text = '1 Month';
            $num = 30;
        }elseif($req->selectMonth == '3m'){
            $num = 90;
            $text = '3 Months';
        }
        elseif($req->selectMonth == '6m'){
            $num = 180;
            $text = '6 Months';
        }
        elseif($req->selectMonth == '12m'){
            $num = 360;
            $text = '12 Months';
        }
        if($req->selectMonth != 0){
            //Save
            DB::table('interest_day')->insert([
                'interest_User' => $user->User_ID,
                'interest_ReviceDay' => $req->selectMonth,
                'interest_Number' => $num,
                'interest_EDAY' => Date('Y-m-d H:i:s', strtotime('+'.$text)),
            ]);
        }
        return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Choose to receive compound interest successfully']);
    }
    public function checkDirectCom($user, $amount, $rateTGT){
        $bonus = $amount * 0.08;
        //get parent
        $direct = User::where('User_ID', $user->User_Parent)->first();
        if(!$direct){
	        return false;
        }
        $total_invest = $direct->User_TotalInvestment + $direct->User_DoubleProfit;
        //Tìm max out

        //callback funtion max out
        $amount_maxout = app('App\Http\Controllers\Cron\CronController')->checkMaxOut($direct->User_ID, $total_invest);
        if($bonus <= $amount_maxout){
            //max out chắc chắn lớn > 0
            $amount_maxout = $bonus;
        }
        elseif($amount_maxout <=0){
            //trường hợp max out == 0;
            return false;
        }

        //Cộng tiền
	    $moneyArray = array(
		    'Money_User' => $direct->User_ID,
		    'Money_USDT' => $amount_maxout,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Direct Commission From '.$user->User_ID,
			'Money_MoneyAction' => 5,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rateTGT,
            'Money_CurrentAmount' => $amount_maxout,
			'Money_Currency' => 8
        );
        
	    // thêm dữ liệu
        DB::table('money')->insert($moneyArray);
        //Update Commission
        $updateCommission = User::updateCommission($direct->User_ID, $amount_maxout);
    }
    public function checkBinaryCom($user, $amount, $rateTGT){
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            
            if(!$parentTree){
                continue;
            }
            $branchSales = app('App\Http\Controllers\Cron\CronController')->branchSales($parentTree->User_ID);
            
            $min_branchSales = min($branchSales);
            
            //lấy hoa hồng nhánh đã trả
            $getLastCom = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyStatus', 1)->where('Money_MoneyAction', 13)->orderByDesc('Money_ID')->first();
            $saleBinary = 0;
	        if($getLastCom){
	            $saleBinary = $getLastCom->Money_SaleBinary;
	        }
            // tính doanh số còn lại được nhận
	        $saleCom = $min_branchSales - $saleBinary;
	        if($saleCom <= 0){
	            continue;
            }

            //Lấy tổng đầu tư
            $total_invest = $parentTree->User_TotalInvestment;
            
            $bonus = 0;
            $percent = 0;
            if($total_invest <= 0){
                continue;
            }
            if($total_invest >= 100 && $total_invest < 10000){
                $bonus = $saleCom * 0.06;
                $percent = 0.06 * 100;
            }
            elseif($total_invest >= 10000 && $total_invest < 100000){
                $bonus = $saleCom * 0.07;
                $percent = 0.07 * 100;
            }elseif($total_invest >= 100000){
                $bonus = $saleCom * 0.08;
                $percent = 0.08 * 100;
            }
            
            //Tìm max out
            //callback funtion max out
            $amount_maxout = app('App\Http\Controllers\Cron\CronController')->checkMaxOut($parentTree->User_ID, $total_invest);

            if($bonus <= $amount_maxout){
                //max out chắc chắn lớn > 0
                $amount_maxout = $bonus;
            }
            elseif($amount_maxout <=0){
                //trường hợp max out == 0;
                return false;
            }
            
            //Trừ tiền 
            $moneyArray = array(
                'Money_User' => $parentTree->User_ID,
                'Money_USDT' => $amount_maxout,
                'Money_USDTFee' => 0,
                'Money_Time' => time(),
                'Money_Comment' => 'Binary Commission From '.$user->User_ID.' ('.$percent.'%)',
                'Money_MoneyAction' => 13,
                'Money_SaleBinary' => $min_branchSales,
                'Money_MoneyStatus' => 1,
                'Money_Rate' => $rateTGT,
                'Money_CurrentAmount' => $amount_maxout,
                'Money_Currency' => 8
            );
            
            
            // thêm dữ liệu
            DB::table('money')->insert($moneyArray);
            //Update Balance
            $updateCommission = User::updateCommission($parentTree->User_ID, $amount_maxout);
        }

        
        
    }
    public function checkF1Com($user, $amount, $rateTGT){
        return false;
        //check active
        $check_active = false;
        $userTree = User::where('User_ID', $user->User_ID)->value('User_Tree');
        $usersArray = explode(",", $userTree);
        $usersArray = array_reverse($usersArray);
        
        for ($i=1; $i <= 8 ; $i++) { 
            if(!isset($usersArray[$i])){
                continue;
            }
            
            //Info User 
            $info_user = User::where('User_ID', $usersArray[$i])->first();
            if(!$info_user){
                continue;
            }
            
            //check total >= 100
            $check_invest = $info_user->User_TotalInvestment + $info_user->User_DoubleProfit;
            
            if($check_invest < 100){
                continue;
            }
            //find num F1
            
            $count_F1 = User::where('User_Parent', $usersArray[$i])
                        ->whereRaw('User_TotalInvestment + User_DoubleProfit >= 100')
                        ->get()
                        ->count();

                        
            // $count_F1 = User::join('investment', 'investment_User', 'User_ID')
            //                 ->where('User_Parent', $usersArray[$i])
            //                 ->where('investment_Status', 1)
            //                 ->selectRaw('Sum(investment_Amount) as total')
            //                 ->groupBy('investment_User')
            //                 ->get()->where('total', '>=', 100)->count();
            if($i == 1){
                if($count_F1 >= 2){
                    $check_active = true;
                }
            }else{
                if($count_F1 >= $i){
                    $check_active = true;
                }
            }
            
            if($check_active){
                //total bonus
                $amount_bonus = $amount * ($i < 6 ? 0.08 : 0.05);

                //Tìm max out
                //callback funtion max out

                $amount_maxout = app('App\Http\Controllers\Cron\CronController')->checkMaxOut($usersArray[$i], $check_invest);

                if($amount_bonus <= $amount_maxout){
                    //max out chắc chắn lớn > 0
                    $amount_maxout = $amount_bonus;
                }
                elseif($amount_maxout <=0){
                    //trường hợp max out == 0;
                    continue;
                }
                //cộng tiền
                $moneyArray = array(
                    'Money_User' => $usersArray[$i],
                    'Money_USDT' => $amount_maxout,
                    'Money_USDTFee' => 0,
                    'Money_Time' => time(),
                    'Money_Comment' => 'Indirect Commission From '.$usersArray[$i].' (F'.$i.')',
                    'Money_MoneyAction' => 10,
                    'Money_MoneyStatus' => 1,
                    'Money_Rate' => $rateTGT,
                    'Money_CurrentAmount' => $amount_maxout,
                    'Money_Currency' => 8
                );
                
                
                // thêm dữ liệu
                DB::table('money')->insert($moneyArray);
                //Update Balance
                $updateCommission = User::updateCommission($usersArray[$i], $amount_maxout);
            }
        }
    }
    public function updateBranchSalesTree($userID){

        $tree = User::where('User_ID', $userID)->value('User_Tree');
        if(!$tree){
            return 0;
        }
        $usersArray = explode(",", $tree);
        $usersArray = array_reverse($usersArray);
        for ($i= 0; $i < count($usersArray) ; $i++) {
            

            $updateBranchSales = User::updateBranchSales($usersArray[$i]);
        }
        
    }
    public function getHistoryInvestment()
    {
        $user = session('user');
        $history_invest = Investment::join('currency', 'Currency_ID' ,'investment_Currency')->where('investment_User', $user->User_ID )->where('investment_Status','<>', -1)->orderBy('investment_ID', 'DESC')->paginate(15);
        $sum =  0;
        foreach($history_invest as $v){
	        $sum += $v->investment_Amount;
        }
        return view('System.History.Investment-History', compact('history_invest', 'sum'));
    }

    protected function getHttp($url)
    {
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody());
    }

    

    
    public function getTreeInvest($userID, $timeCheck) {
        $userTree = User::where('User_ID', $userID)->value('User_Tree');
        $amount = Investment::join('users', 'Investment_User', 'users.User_ID')
            ->where('users.User_Tree', 'like', "$userTree,%")
            ->where('investment_Time', '>=', $timeCheck)
            ->where('investment_Status', 1)
            ->selectRaw('investment_Amount*investment_Rate as a')->get()->sum('a');
        return $amount;

    }
    public function postCancelInvestment(Request $req){
        $user = User::where('User_ID', Session('user')->User_ID)->where('User_Status', 1)->first();
        if(!$user){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Error User']);
        }
        
        //Lấy ngày đầu đầu tư
        $date_investment = Investment::where('investment_User', $user->User_ID)->where('investment_Status', 1)->orderBy('investment_ID', 'ASC')->first();
        
        //dd($date_investment->investment_Time, time(), Date('Y-m-d H:i:s', $date_investment->investment_Time), Date('Y-m-d H:i:s', strtotime('+3 month', $date_investment->investment_Time)), strtotime('+3 month', $date_investment->investment_Time));
        if(!$date_investment){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'Error Cancel Investment']);
        }
        $now = time();
        //check gói đầu tư đã đủ 3 tháng chưa
        
        if($now < strtotime('+3 month', $date_investment->investment_Time)){
	        // return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'You only cancel package after 3 months']);
        }
        //tính số tháng của gói đầu tư (thời gian hiện tại - thời gian đầu tư / cho 1 tháng)
        // $number_month_cancel = (int)(($now - $date_investment->investment_Time) / 2629800);
        $number_month_cancel = 4;
        //lãi và hoa hồng không vượt quá tổng đầu tư
        $totalCom = $user->User_BalanceCommission + $user->User_BalanceInterest;
        $totalInvest = $user->User_TotalInvestment + $user->User_DoubleProfit;
        if($totalCom >= ($totalInvest * 0.7)){
            return redirect()->back()->with(['flash_level'=>'error','flash_message'=>'You are not eligible to cancel the investment package']);
        }
        //Cancel All Investment
        $update_cancel = DB::table('investment')->where('investment_User', $user->User_ID)->where('investment_Status', 1)->update([
            'investment_Status' => -1
        ]);
        //Cancel Nhận lãi kép
        $update = DB::table('interest_day')->where('interest_User', $user->User_ID)->update([
            'interest_Status' => -1
        ]);
        //tuần nhận được tiền từ đầu tư
        if($number_month_cancel >= 3 && $number_month_cancel < 6){
            //phí hủy 10%
            $feeCancel = 0.1;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 5;
            //số tièn nhận được mỗi tuần (5 tuần)
            $result_amount_cancel = $amount_cancel / $num_week;
            
        }elseif($number_month_cancel >= 6 && $number_month_cancel < 12){
            //phí hủy 5%
            $feeCancel = 0.05;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 2;
            //số tièn nhận được mỗi tuần (2 tuần)
            $result_amount_cancel = $amount_cancel / $num_week;
        }elseif($number_month_cancel == 12){
            //phí hủy 0%
            $feeCancel = 0;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 1;
            //số tièn nhận được
            $result_amount_cancel = $amount_cancel / $num_week;
        }
        if($number_month_cancel < 12){
            for ($i=0; $i < $num_week ; $i++) {
                $insert_cancel = DB::table('cancel_investment')->insert([
                    'cancel_User' => $user->User_ID,
                    'cancel_Amount' => $result_amount_cancel,
                    'cancel_TotalInvest' => $totalInvest,
                    'cancel_ReceivedDay' => Date('Y-m-d 00:00:00', strtotime("+".($i + 1)." week")),
                    'cancel_TimeCancel' => Date('Y-m-d H:i:s'),
                    'cancel_Status' => 1,
                    'cancel_Log' => "$number_month_cancel Months - $num_week week - Fee $feeCancel - Total Invest $totalInvest",
                ]);
            }
        } else {
            $insert_cancel = DB::table('cancel_investment')->insert([
                'cancel_User' => $user->User_ID,
                'cancel_Amount' => $totalInvest,
                'cancel_TotalInvest' => $totalInvest,
                'cancel_ReceivedDay' => Date('Y-m-d 00:00:00', strtotime("+1 day")),
                'cancel_TimeCancel' => Date('Y-m-d H:i:s'),
                'cancel_Status' => 1,
                'cancel_Log' => "$number_month_cancel Months - 24h - Fee $feeCancel - Total Invest $totalInvest",
            ]);
        }
        //Update updateInvestment(-)
        $updateInvestment = User::updateInvestment($user->User_ID, -$user->User_TotalInvestment);
        //Update updateDoubleProfit(-)
        $updateDoubleProfit = User::updateDoubleProfit($user->User_ID, -$user->User_DoubleProfit);
        //updateBranchSales
        $updateBranchSales = $this->updateBranchSalesTree($user->User_ID);
        //update updateBalanceDeposit(+)
        //chưa cộng balance liền, mỗi ngày chạy cron 1 lần vào khung giờ chính 
        // $updateBalanceDeposit = User::updateBalanceDeposit($user->User_ID, $totalInvest);
		return redirect()->back()->with(['flash_level'=>'success','flash_message'=>'Cancel Success']);
    }
    
    public function investmentTokenBackup(Request $req){
        $user = Session('user');
        //Rate Token
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        $rateToken = $rate['TRX'];
        $rateSKC = $rate['SKC'];
        $amount = $req->amount;
        $amountToken = $amount/$rateSKC;
        //Min đầu tư
        if($amount < 400){
            return ['status'=>'error', 'message'=>'Min invest $400!'];
        }
        //RATE USD
        $rateUSD = 1;
        //Balance
        $balance = User::where('User_ID', Session('user')->User_ID)->first();
        if($amountToken > $balance->User_BalanceSKC){
            return ['status'=>'error', 'message'=>'Your balance is not enough!'];
        }
        //Bonus
        $getPackage = User::getPackage($user->User_ID, $amount);
        //khởi tạo
        $amount_bonus = 0;
        //Trừ tiền 
	    $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amountToken,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Invested '.$amount.' USDT',
			'Money_MoneyAction' => 3,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rateSKC,
            'Money_CurrentAmount' => $amountToken,
			'Money_Currency' => 8,
        );
        //Invest
        $invest = array(
		    'investment_User' => $user->User_ID,
            'investment_Amount' => $amount,
		    'investment_Rate' => $rateUSD,
		    'investment_Currency' => 5,
		    'investment_Time' => time(),
            'investment_Status' => 1,
            'investment_Bonus' => $amount_bonus,
            'investment_Total' => $amount+$balance->User_TotalInvestment
        );
        //Cộng điểm

	    // thêm dữ liệu
	    DB::table('investment')->insert($invest);
        DB::table('money')->insert($moneyArray);
        if($amount_bonus > 0){
            //điều kiện tặng coin
            DB::table('money')->insert($insertPoint);
            
        }
        //Update Balance
        $updateBalance = User::where('User_ID', $user->User_ID)->increment('User_BalanceSKC', -$amountToken);
        //Update updatePoint
        $updatePoint = User::updatePoint($user->User_ID, $amount_bonus);
        //Update Investment
        $updateInvestment = User::updateInvestment($user->User_ID, $amount);
        //Update Sales Tree
        $updateBranchSales = $this->updateBranchSalesTree($user->User_ID);
        //checkDirectCom
        app('App\Http\Controllers\Cron\CronController')->checkCommission($user, $amount);
        //checkBinaryCom
        app('App\Http\Controllers\Cron\CronController')->checkBinaryCommission($user, $amount);
        //update level
        app('App\Http\Controllers\Cron\CronController')->UpdateLevel($user);
        
        //check lệnh hoa hồng pending
        $this->checkMaxoutCommission($user->User_ID, $amount);
        
        $this->BonusBinary($user);

        return ['status'=>'success', 'message'=>'Investment complete'];
        return redirect()->route('system.getInvestment')->with(['flash_level'=>'success','flash_message'=>'Investment complete']);  
    }
    
}