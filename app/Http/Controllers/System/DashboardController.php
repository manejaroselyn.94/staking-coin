<?php

namespace App\Http\Controllers\System;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Money;
use App\Model\Investment;
use GuzzleHttp\Client;
use Session;
use App\Model\User;
use DB;
class DashboardController extends Controller
{
	

    public function getDashboard()
    {

        $user = Session('user');
        // thông tin user
		$info = User::find($user->User_ID);

        $balance['Deposit'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceDeposit');
        $balance['Point'] = User::where('User_ID', Session('user')->User_ID)->value('User_PointStock');
        $balance['Bonus'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceToken');
        $balance['Token'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceSKC');
        $balance['TokenProfit'] = User::where('User_ID', Session('user')->User_ID)->value('User_BalanceSKCProfit');
        $bonusTRX = Money::where('Money_User', $user->User_ID)
	        						->where('Money_MoneyAction', 20)
	        						->where('Money_MoneyStatus', 1)
	        						->sum('Money_USDT');
		$branch = app('App\Http\Controllers\Cron\CronController')->branchSales($user->User_ID);
        $total['Deposit'] = Money::where('Money_User', $user->User_ID)->where('Money_MoneyStatus', 1)->where('Money_MoneyAction', 1)->sum(DB::raw('Money_USDT'));
        $totalCommission = Money::where('Money_User', $user->User_ID)->where('Money_MoneyStatus', 1)->whereIn('Money_MoneyAction', [5,6, 9,13,16,17])->select('Money_USDT', 'Money_Currency')->get();
        $totalProfit = Money::where('Money_User', $user->User_ID)->where('Money_MoneyStatus', 1)->whereIn('Money_MoneyAction', [4,14])->select('Money_USDT', 'Money_Currency')->get();
        $total['Commission'] = $totalCommission->where('Money_Currency', 5)->sum('Money_USDT');
        $total['Profit'] = $totalProfit->where('Money_Currency', 5)->sum('Money_USDT');
        $total['Bonus'] = Money::where('Money_User', $user->User_ID)->where('Money_MoneyStatus', 1)->whereIn('Money_MoneyAction', [19])->sum(DB::raw('Money_USDT'));
        //giảm tỉ giá xuống 0
        $percentDeposit = 1;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        $history_invest = Investment::totalInvest($user->User_ID, 5);
		$total_invest = $history_invest->sum('investment_Amount');
        //skc
        $history_invest_skc = Investment::totalInvest($user->User_ID, 8);
        $totalInvestSKC = $history_invest_skc->sum('investment_Amount');
		$branchSKC = app('App\Http\Controllers\System\InvestmentController')->branchSalesSKC($user->User_ID);
        $total['CommissionSKC'] = $totalCommission->where('Money_Currency', 8)->sum('Money_USDT');
        $total['ProfitSKC'] = $totalProfit->where('Money_Currency', 8)->sum('Money_USDT');
        //Maxout
        // $total_invest = User::where('User_ID', Session('user')->User_ID)->value(DB::raw('User_TotalInvestment'));
        // $total_invest = Investment::where('investment_User', Session('user')->User_ID)->where('investment_Status', 1)->sum('investment_Amount');
        $sum_invest = $total_invest;
		$percent_maxout = 4;
        $total_invest = $total_invest * $percent_maxout;
		$maxout = $total_invest;
        $resultmaxout = $total_invest - $total['Commission'] - $total['Profit'];
		$chartMaxout =[
							['value'=>$total['Profit'], 'name'=>'Profit'], 
							['value'=>$total['Commission'], 'name'=>'Commission'], 
							['value'=>$resultmaxout, 'name'=>'Max Out'],
                        ];
        $resultmaxoutSKC = app('App\Http\Controllers\System\InvestmentController')->checkMaxOutSKC($user->User_ID);
		$chartMaxoutSKC =[
            ['value'=>$total['ProfitSKC'], 'name'=>'Profit'], 
            ['value'=>$total['CommissionSKC'], 'name'=>'Commission'], 
            ['value'=>$resultmaxoutSKC, 'name'=>'Max Out'],
        ];
		$data_set = DB::table('fake_data')->where('ID', 1)->first();
        // dd($data_set, $totalProfit, $chartMaxoutSKC);
        return view('System.Dashboard.Index', compact('data_set', 'sum_invest', 'history_invest_skc', 'totalInvestSKC', 'branchSKC', 'balance', 'total', 'history_invest', 'rate', 'chartMaxout', 'chartMaxoutSKC', 'total_invest', 'percentDeposit', 'branch', 'sum_invest', 'bonusTRX'));
    }
}
