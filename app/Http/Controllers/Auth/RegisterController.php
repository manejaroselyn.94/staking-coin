<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Register;
use App\Model\Log;
use App\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Jobs\SendMailJobs;
class RegisterController extends Controller
{


    public function getRegister(Request $request)
    {
	    $parent = 859707;
        $leftnode = $this->leftNode($parent, 0);
        return view('Auth.Register', compact('leftnode', 'parent'));
    }

    public function postRegister(Request $request)
    {   
        
        $request->validate([
            // 'UserName' => 'max:15|unique:users,User_Name|regex:/^[A-Za-z][A-Za-z0-9]*$/',
            'Email' => 'required|email|max:34|unique:users,User_Email',
            'Password' => 'required|max:40',
            'Re-Password' => 'required|same:Password'
        ]);
        if (!filter_var($request->Email, FILTER_VALIDATE_EMAIL)) {
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Email format is wrong!']);
		}
        include(app_path() . '/functions/xxtea.php');
        
        $userID_Parent = '859707';
        if($request->parent){
		    $userID_Parent = $request->parent;
        }
        //Kiểm tra ID Parent đó có tồn tại không????
        $check_parent_ex = User::find($userID_Parent);
        if(!$check_parent_ex){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Sponser ID not found!']);
        }
        $show_ID_Parent = User::where('User_ID', $userID_Parent)->value('User_ID');
        $get_Side_Active = User::where('User_ID', $userID_Parent)->value('User_Side_Active');
        //BiẾN tạm LƯU  ID Presenter 
        $userID_Presenter = $request->presenter;
        if($userID_Presenter){
            //IF Có nhập người chỉ định
            // $node = User::where('User_ID', $userID_Presenter)->value('user_Node_Active');
            // $user_ID_Presenter = $userID_Presenter;
            // $user_Presenter_Tree = User::where('User_ID', $userID_Presenter)->value('User_Tree');

            $info_Presenter = User::find($userID_Presenter);
            if(!$info_Presenter){
            return redirect()->back()->with(['flash_level'=>'error', 'flash_message'=>'Presenter ID not found!']);
            }

            $node = $info_Presenter->User_Side_Active;

            $prevNodeID = $this->leftNode($info_Presenter->User_ID, $info_Presenter->User_Side_Active);

            $user_ID_Presenter = $prevNodeID;
            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
            
        }
        else{
            //Còn ko nhập thì lấy Paren mặc định và thêm vào vị trí trái bên cùng
            // $node = User::where('User_ID', $userID_Parent)->value('user_Node_Active');
            
            $node = $get_Side_Active;

            $prevNodeID = $this->leftNode($show_ID_Parent, $get_Side_Active);
            $user_ID_Presenter = $prevNodeID;
            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
        }
		
        $childrenNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->get();
        // dd($user_ID_Presenter,$childrenNode);

        if(count($childrenNode) > 2){
            //Nhánh đã đủ 2 chân rồi
            return redirect()->route('getRegister')->with(['flash_level' => 'error', 'flash_message' => 'This person has enough 2 children, please choose other one!']);
        }else{
            if(count($childrenNode) == 0){
                //tức không nhập người chỉ địh hoặc có nhập mà người chỉ định chưa có chân nào,
                //cho nên sẽ vào nhánh trái và cuối cùng
                $user_side = $node;
            }
            else{
                //khi nào đucợ chạy vào đây
                // dk khi có nhập người chỉ định
                if($childrenNode[0]->User_IsRight == 1){
                    $user_side = 0;
                }
                else{
                    $user_side = 1;
                }
            }
        }
        //đăng ký chỉ định vào chân nào
        $node_register = $request->node;
        $user_ID_Presenter = $request->presenter;
        if(!is_null($node_register)){

	        $getChilNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->where('User_IsRight', $node_register)->first();
			
	        if($getChilNode){
		        if($node_register == 1){
			        $note_string = 'right';
		        }else{
			        $note_string = 'left';
		        }

            	return redirect()->route('getRegister')->with(['flash_level' => 'error', 'flash_message' => 'This person have branch '.$note_string.', please choose other branch!']);
	        }
            $node = $node_register;

            $prevNodeID = $this->leftNode($info_Presenter->User_ID, $node);

            $user_ID_Presenter = $prevNodeID;
            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
	        $user_side = $node_register;
        }

        $user_id = $this->RandomIDUser();

        $user_tree = !$user_Presenter_Tree ? $userID_Presenter . "," . $user_id : $user_Presenter_Tree . ',' . $user_id;

        //nhập parent và presenter khác nhánh
        if( strpos($user_tree, "$show_ID_Parent") === false){
        	return redirect()->route('getRegister')->with(['flash_level' => 'error', 'flash_message' => 'Ponser and Presenter another branch, please check again!']);
        }
        //lấy sun tree parent
        $sunTree = (User::where('User_ID', $show_ID_Parent)->value('User_SunTree')).','.$user_id;
        //$sunTree = (User::where('User_ID', $userID_Presenter)->value('User_SunTree')).','.$user_id;

        //Tạo token cho mail
        $token = Crypt::encryptString($request->Email.':'.time());
        //nhánh show
        $level = 0;
        if(strpos($user_tree, '510619') !== false){
	        $level = 5;
        }
        //Xử lý
        $Register = [
            'User_ID' => $user_id,
            // 'User_Name' => $request->UserName,
            'User_Email' => $request->Email,
            'User_Parent' => $show_ID_Parent,
            'User_IsRight' => $user_side,
            'User_Tree' => $user_tree,
            'User_SunTree' => $sunTree,
            'User_EmailActive' => 0,
            'User_Password' => bcrypt($request->Password),
            'User_RegisteredDatetime' => date('Y-m-d H:i:s'),
            'User_Level' => $level,
            'User_Status' => 1,
            'User_Token' => $token,

        ];

        User::insert($Register);
        
        //dữ liệu gửi sang mailtemplate
        $data = array('User_ID' => $user_id, 'User_Name' => $request->Email,'User_Email'=> $request->Email, 'token'=>$token);
        //Job
        dispatch(new SendMailJobs('Active', $data, 'Active Account!', $user_id));

        return redirect()->route('getLogin')->with(['flash_level'=>'success', 'flash_message'=>'Register Account Success, Please check your email to Activate Account!']);

    }

    public function leftNode($id, $node = 0)
    {
        $childnode = User::whereRaw("User_IsRight = $node AND ( User_Tree LIKE CONCAT($id,',',User_ID) OR User_Tree LIKE CONCAT('%,',$id,',',User_ID) ) ")->first();
        
        if (!$childnode) {
	        
            return $id;
        } else {
            return $this->leftNode($childnode->User_ID, $node);
        }
    }

    public function getActive(Request $req){

        $user = User::where('User_Token', $req->token)->first();
        if($user){
            if($user->User_EmailActive == 1){
                return redirect()->route('getLogin');
            }else {
                $user->User_EmailActive = 1;
                $user->save();
                return redirect()->route('getLogin')->with(['flash_level'=>'success', 'flash_message'=>'Activate Account Success!']);
            }

        }
        return redirect()->route('getLogin')->with(['flash_level'=>'error', 'flash_message'=>'Error!']);
    }
    
    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function RandomIDUser()
    {
        $id = rand(100000, 999999);
        //TẠO RA ID RANĐOM
        $user = User::where('User_ID', $id)->first();
        //KIỂM TRA ID RANDOM ĐÃ CÓ TRONG USER CHƯA
        if (!$user) {
            return $id;
        } else {
            return $this->RandomIDUser();
        }
    }
}
