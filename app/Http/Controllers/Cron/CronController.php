<?php

namespace App\Http\Controllers\Cron;

use App\Model\Money;
use App\Model\User;
use App\Model\Wallet;
use App\Model\Investment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use DB;

use PayusAPI\Http\Client as PayusClient;
use PayusAPI\Resources\Payus;
use App\Jobs\SendTelegramJobs;
use App\Jobs\SendMailJobs;
class CronController extends Controller
{
	//giảm 2%
    public $percentDeposit = 1;
    public $feeDeposit = 0;
    

// check số tiền nhận đã đủ 200% số tiền invest mỗi gói để gửi gốc về
    public static function checkRefundInvest(Request $req){
      	
	    $now = time();
	    $getInvest = Investment::where('investment_Status', 1)->where('investment_Currency', '!=', 8)->groupBy('investment_User')->selectRaw('SUM(`investment_Amount` * investment_Rate) as total, MAX(investment_Time) as investment_Time, investment_ID, investment_User, investment_Rate, investment_Bonus')->get();
		$arrCoinSend = [1=>'BTC', 2=>'ETH', 5=>'USD', 9=>'TRX'];
		$arrWallet = [1=>'User_WalletBTC', 2=>'User_WalletETH', 5=>'User_WalletUSDT', 9=>'User_WalletTRX'];
        $currency = 8;
        $symbol = "SKC";
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
	    foreach($getInvest as $i){
		    
		    $user = User::find($i->investment_User);
		    if(!$user){
			    continue;
		    }
		    $amount = $i->total*$i->investment_Rate;
		    $getPackage = DB::table('package')
		    				->whereRaw($amount." BETWEEN package_Min AND package_Max")
		    				->select('package_Interest', 'package_Date')
		    				->orderByDesc('package_ID')
		    				->first();
		    if($now < strtotime('+'.((int)$getPackage->package_Date).' days', $i->investment_Time)){
			    continue;
		    }
			$amountCoin = $amount / $rate;
	        $money = new Money();
	        $money->Money_User = $i->investment_User;
	        $money->Money_USDT = $amountCoin;
	        $money->Money_Time = $now;
	        $money->Money_Comment = 'Refund Investment';
	        // refund = ETH
	        $money->Money_Investment = $i->investment_ID;
	        $money->Money_Currency = 5;
	        $money->Money_MoneyAction = 10;
	        $money->Money_Rate = $rate;
	        $money->Money_Confirm = 0;
	        $money->Money_MoneyStatus = 1;
          	echo $i->investment_User." Refund Investment ".$amountCoin." SKC <br>";
          	if($req->pay == 1){
                $update = Investment::where('investment_User', $user->User_ID)->where('investment_Status', 1)->update(['investment_Status'=>2]);
                $money->save();
                $updateBalanceToken = User::where('User_ID', $i->investment_User)->increment('User_BalanceSKCProfit', $amountCoin);
            }
	        // trả Bonus về Balance trước 
	        /*
            $amountTRX = $i->investment_Bonus;
	        $money = new Money();
	        $money->Money_User = $i->investment_User;
	        $money->Money_USDT = $amountTRX;
	        $money->Money_Time = $now;
	        $money->Money_Comment = 'Refund Bonus Investment';
	        // refund = ETH
	        $money->Money_Investment = $i->investment_ID;
	        $money->Money_Currency = 9;
	        $money->Money_CurrentAmount = $amountTRX;
	        $money->Money_MoneyAction = 16;
	        $money->Money_Rate = $rate;
	        $money->Money_Confirm = 0;
	        $money->Money_MoneyStatus = 1;
	        $money->save();
	        */
		    $data = array('ID'=>$user->User_ID, 'amount'=>$amount, 'address'=>$user->User_WalletETH, 'id'=>$money->Money_ID);
			$token = urlencode(encrypt($data));
			$data['token'] = $token;
	//         dispatch(new SendMailJobs($user, $data, 'RefundInvest', 'Refund Investment'));
	    }
	    
    }
  
	public function checkPayMoney(){
		$timeCheck = strtotime('-180 minutes');
		$getMoneyConfirm = Money::whereIn('Money_MoneyAction', [2,18,21])
							->where('Money_Confirm_Time', '>=', $timeCheck)
							->whereNotNull('Money_TXID')
							->where('Money_Confirm', 1)
							->where('Money_IsPaid', 0)->get();
		$api = 'https://rezxcvbnm.co/api/check-transaction/';
		foreach($getMoneyConfirm as $money){
			$tid = $money->Money_TXID;
			$checkTrans = file_get_contents($api.$tid);
			$data = json_decode($checkTrans);
			if($data->status == true){
				if($data->transaction_status == 1){
					$money->Money_IsPaid = 1;
					$money->save();
				}elseif($data->transaction_status == 2){
					$money->Money_Confirm = -1;
					$money->Money_Message = $data->message;
					$money->save();
				}
			}
		}
		dd('check pay success');
	}
	
	public function cronSendTRX(){
		//Lấy danh
		$timeCheck = strtotime('today');
		$percent = 0.1;
		$action = 20;
		$coin = 9;
		$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('TRX');
		$getUsers = User::where('User_PointStock', '>', 0)->get();
// 		dd($getUsers);
		foreach($getUsers as $user){
			
			$totalPoint = $user->User_PointStock;
			$amountBonus = $totalPoint * $percent;
			
	        $totalTRX = User::where('User_ID', $user->User_ID)->value('User_PointStock');
	        $bonusTRX = Money::where('Money_User', $user->User_ID)
	        						->where('Money_MoneyAction', $action)
	        						->where('Money_MoneyStatus', 1)
	        						->sum('Money_USDT');

			if($amountBonus > ($totalTRX - $bonusTRX)){
				$amountBonus = $totalTRX - $bonusTRX;
				if($amountBonus <= 0){
					continue;
				}
			}
			$getLastedInvest = Investment::where('investment_User', $user->User_ID)->orderByDesc('investment_Time')->first();
			if(!$getLastedInvest || time() - $getLastedInvest->investment_Time < 2592000){
				continue;
			}
			$checkPaid = Money::where('Money_User', $user->User_ID)
							->where('Money_MoneyAction', $action)
							->orderByDesc('Money_Time')
							->first();
			if($checkPaid){
				if(time() - $checkPaid->Money_Time < 2592000){
					continue;
				}
			}
			echo $user->User_ID.' - Monthly Send '.$amountBonus.' TRX Bonus Investment totalTRX : '.$totalTRX.' - bonusTRX : '.$bonusTRX.'<br>';
// 			continue;
            //cộng tiền lãi
			$row = new Money();
			$row->Money_User = $user->User_ID;
			$row->Money_USDT = $amountBonus;
			$row->Money_Time = time();
			$row->Money_Comment = 'Monthly Send '.$amountBonus.' TRX Bonus Investment';
			$row->Money_MoneyAction = $action;
			$row->Money_MoneyStatus = 1;
			$row->Money_Currency = $coin;
			$row->Money_Rate = $rate;
			$row->save();
			
			User::updateBalance($user->User_ID, 'Token', $amountBonus);
		}
		
		dd('check refund bonus success');
	}
	
    public function depositTRXWithHash($transactionHash){
	    $client = new Client();
	    $response = $client->request('GET', 'https://apilist.tronscan.org/api/transaction-info?hash='.$transactionHash, [
	    ])->getBody()->getContents();
	    $v = json_decode($response)->contractData;
	    $hash = Money::where('Money_Address', $transactionHash)->first();
		if(!$hash){
			$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('TRX');
			$priceCoin = $rate;
			$address = $v->to_address;
			$infoAddress = Wallet::join('users', 'users.User_ID', 'Address_User')->select('Address_User','User_Email')->where('Address_Address', $address)->first();

			if($infoAddress){
                $amount = $v->amount/1000000;
				$amountUSD =$amount * $priceCoin;
				$amountFee = $amountUSD * $this->feeDeposit;
				
				$money = new Money();
				$money->Money_User = $infoAddress->Address_User;
				$money->Money_USDT = $amountUSD - $amountFee;
				$money->Money_USDTFee = $amountFee;
				$money->Money_Time = time();
				$money->Money_Comment = 'Deposit '.($amount+0).' TRX';
				$money->Money_Currency = 9;
				$money->Money_MoneyAction = 1;
				$money->Money_Address = $transactionHash;
				$money->Money_CurrentAmount = $amount;
				$money->Money_Rate = $priceCoin;
				$money->Money_MoneyStatus = 1;
                $money->save();	
                //deposit -354905750
				//update Balance
				$updateblance = User::updateBalanceDeposit($infoAddress->Address_User, $amountUSD - $amountFee);
                
			    $tranfer = $this->TransferToAddress($address);
				// 	Gửi telegram thông báo User verify
				$message = "$infoAddress->User_Email Deposit $amount TRX\n"
						. "<b>User ID: </b> "
						. "$infoAddress->Address_User\n"
						. "<b>Email: </b> "
						. "$infoAddress->User_Email\n"
						. "<b>Amount: </b> "
						. $amount." TRX\n"
						. "<b>Rate: </b> "
						. "$ $priceCoin \n"
						. "<b>Submit Deposit Time: </b>\n"
						. date('d-m-Y H:i:s',time());
							
		        dispatch(new SendTelegramJobs($message, -411888101));

// 				    $this->TransferToAddress($address);
			}
			
		} 
	    return true;
	    
    }
    
    public function getDepositTRXWithAddress(Request $req){
	    if($req->hash){
		    $depositWithHash = $this->depositTRXWithHash($req->hash);
		    dd('check success');
	    }
  		$client = new Client();
	    $response = $client->request('GET', 'https://apilist.tronscan.org/api/transfer?sort=-timestamp&count=true&limit=40&start=0', [
	    ])->getBody()->getContents();
	    $transactions = json_decode($response)->data;
	    $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('TRX');
	    $priceCoin = $rate;
		foreach($transactions as $v){
			$transactionHash = $v->transactionHash;
			$hash = Money::where('Money_Address', $transactionHash)->first();
			if(!$hash){
				$address = $v->transferToAddress;
				$infoAddress = Wallet::join('users', 'users.User_ID', 'Address_User')->select('Address_User','User_Email')->where('Address_Address', $address)->first();

				if($infoAddress){
                    $amount = $v->amount/1000000;
					$amountUSD =$amount * $priceCoin;
					$amountFee = $amountUSD * $this->feeDeposit;
					$money = new Money();
					$money->Money_User = $infoAddress->Address_User;
					$money->Money_USDT = $amountUSD - $amountFee;
					$money->Money_USDTFee = $amountFee;
					$money->Money_Time = time();
					$money->Money_Comment = 'Deposit '.($amount+0).' TRX';
					$money->Money_Currency = 9;
					$money->Money_MoneyAction = 1;
					$money->Money_Address = $transactionHash;
					$money->Money_CurrentAmount = $amount;
					$money->Money_Rate = $priceCoin;
					$money->Money_MoneyStatus = 1;
                    $money->save();	
                    //deposit -354905750
					//update Balance
					$updateblance = User::updateBalanceDeposit($infoAddress->Address_User, ($amountUSD - $amountFee));
                    
				    $tranfer = $this->TransferToAddress($address);
					// 	Gửi telegram thông báo User verify
					$message = "$infoAddress->User_Email Deposit $amount TRX\n"
							. "<b>User ID: </b> "
							. "$infoAddress->Address_User\n"
							. "<b>Email: </b> "
							. "$infoAddress->User_Email\n"
							. "<b>Amount: </b> "
							. $amount." TRX\n"
							. "<b>Rate: </b> "
							. "$ $priceCoin \n"
							. "<b>Submit Deposit Time: </b>\n"
							. date('d-m-Y H:i:s',time());
								
			        dispatch(new SendTelegramJobs($message, -411888101));

// 				    $this->TransferToAddress($address);
				}
			} 
		}
		echo 'check deposit success';exit;
    }
    
    public function getDepositTRX(Request $req){
	    if($req->hash){
		    $this->depositTRXWithHash($req->hash);
	    }
  		$client = new Client();
	    $response = $client->request('GET', 'https://apilist.tronscan.org/api/transfer?sort=-timestamp&count=true&limit=40&start=0', [
	    ])->getBody()->getContents();
	    $transactions = json_decode($response)->data;
	    $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('TRX');
	    $priceCoin = $rate;
		foreach($transactions as $v){
			$transactionHash = $v->transactionHash;
			$hash = Money::where('Money_Address', $transactionHash)->first();
			if(!$hash){
				$address = $v->transferToAddress;
				$infoAddress = Wallet::join('users', 'users.User_ID', 'Address_User')->select('Address_User','User_Email')->where('Address_Address', $address)->first();

				if($infoAddress){
                    $amount = $v->amount/1000000;
					$amountUSD =$amount * $priceCoin;
					$amountFee = $amountUSD * $this->feeDeposit;
					
					$money = new Money();
					$money->Money_User = $infoAddress->Address_User;
					$money->Money_USDT = $amountUSD - $amountFee;
					$money->Money_USDTFee = $amountFee;
					$money->Money_Time = time();
					$money->Money_Comment = 'Deposit '.($amount+0).' TRX';
					$money->Money_Currency = 9;
					$money->Money_MoneyAction = 1;
					$money->Money_Address = $transactionHash;
					$money->Money_CurrentAmount = $amount;
					$money->Money_Rate = $priceCoin;
					$money->Money_MoneyStatus = 1;
                    $money->save();	
                    //deposit -354905750
					//update Balance
					$updateblance = User::updateBalanceDeposit($infoAddress->Address_User, $amountUSD - $amountFee);
                    
				    $tranfer = $this->TransferToAddress($address);
					// 	Gửi telegram thông báo User verify
					$message = "$infoAddress->User_Email Deposit $amount TRX\n"
							. "<b>User ID: </b> "
							. "$infoAddress->Address_User\n"
							. "<b>Email: </b> "
							. "$infoAddress->User_Email\n"
							. "<b>Amount: </b> "
							. $amount." TRX\n"
							. "<b>Rate: </b> "
							. "$ $priceCoin \n"
							. "<b>Submit Deposit Time: </b>\n"
							. date('d-m-Y H:i:s',time());
								
			        dispatch(new SendTelegramJobs($message, -411888101));

// 				    $this->TransferToAddress($address);
				}
				
			} 
		}
		echo 'check deposit success';exit;
    }
    
    public function TransferToAddress($from, $amount = 0, $to = 'TWqj4pb54ehVgFe3hB2AFJ7cF4PZWhsepB', $action = 'Send To Big Address'){
	    $checkAddressTo = Wallet::where('Address_Address', $to)->where('Address_Currency', 8)->first();
	    if($checkAddressTo){
		    $hexAddress = $checkAddressTo->Address_HexAddress;
	    }else{
		    $hexAddress = Wallet::base58check2HexString($to);
		    if(!$hexAddress){
				$dataLog = [
					'Log_TRX_From' => $from,
					'Log_TRX_To' => $to,
					'Log_TRX_Amount' => 0,
					'Log_TRX_Action' => $action,
					'Log_TRX_Comment' => 'Transfer From '.$from.' To '.$to,
					'Log_TRX_Error' => 'Error Hex Address',
					'Log_TRX_Time' => date('Y-m-d H:i:s'),
					'Log_TRX_Status' => 1
				];
				DB::table('log_TRX')->insert($dataLog);
			    return 'Don\'t find hex address';
		    }
	    }
	    $checkAddress = Wallet::where('Address_Address', $from)->first();
		
	    if(!$checkAddress){
			$dataLog = [
				'Log_TRX_From' => $from,
				'Log_TRX_To' => $to,
				'Log_TRX_Amount' => 0,
				'Log_TRX_Action' => $action,
				'Log_TRX_Comment' => 'Transfer From '.$from.' To '.$to,
				'Log_TRX_Error' => 'Address TRX Not found in Database',
				'Log_TRX_Time' => date('Y-m-d H:i:s'),
				'Log_TRX_Status' => 1
			];
			DB::table('log_TRX')->insert($dataLog);
		    return 'Don\'t find address';
	    }
  		$client = new Client();
	    $response = $client->request('POST', 'https://api.trongrid.io/wallet/getaccount', [
		    'json'    => ['address' => $checkAddress->Address_HexAddress],
	    ])->getBody()->getContents();

	    $data = json_decode($response);
	    if(!isset($data->balance) || $data->balance <= 0){
			$dataLog = [
				'Log_TRX_From' => $from,
				'Log_TRX_To' => $to,
				'Log_TRX_Amount' => 0,
				'Log_TRX_Action' => $action,
				'Log_TRX_Comment' => 'Transfer From '.$from.' To '.$to,
				'Log_TRX_Error' => 'Account Not Found Data',
				'Log_TRX_Time' => date('Y-m-d H:i:s'),
				'Log_TRX_Status' => 1
			];
			DB::table('log_TRX')->insert($dataLog);
		    return 'Account Not Found Data';
	    }
		$balance = round($amount > 0 ? $amount*1000000 : $data->balance);
		if($balance > $data->balance){
			$dataLog = [
				'Log_TRX_From' => $from,
				'Log_TRX_To' => $to,
				'Log_TRX_Amount' => $amount,
				'Log_TRX_Action' => $action,
				'Log_TRX_Comment' => 'Transfer From '.$from.' To '.$to,
				'Log_TRX_Error' => 'Balance Is Not Enough',
				'Log_TRX_Time' => date('Y-m-d H:i:s'),
				'Log_TRX_Status' => 1
			];
			DB::table('log_TRX')->insert($dataLog);
			return 'Balance Is Not Enough';
		}

  		$client = new Client();
	    $response = $client->request('POST', 'https://api.trongrid.io/wallet/easytransferbyprivate', [
		    'json'    => ['privateKey' => $checkAddress->Address_PrivateKey, 
		    			  'toAddress' => $hexAddress,
		    			  'amount' => $balance
		    			 ],
	    ])->getBody()->getContents();
	    $dataSend = json_decode($response);
	    if(isset($dataSend->result->result) && $dataSend->result->result == true){
			$dataLog = [
				'Log_TRX_From' => $checkAddress->Address_HexAddress,
				'Log_TRX_To' => $hexAddress,
				'Log_TRX_Amount' => $balance/1000000,
				'Log_TRX_Action' => $action,
				'Log_TRX_Hash' => $dataSend->transaction->txID,
				'Log_TRX_Comment' => 'Transfer '.($balance/1000000).' TRX From '.$from.' To '.$to,
				'Log_TRX_Time' => date('Y-m-d H:i:s'),
				'Log_TRX_Status' => 1
			];
			DB::table('log_TRX')->insert($dataLog);
			return true;
	    }
		$message = hex2bin($dataSend->result->message);
		$dataLog = [
			'Log_TRX_From' => $checkAddress->Address_HexAddress,
			'Log_TRX_To' => $hexAddress,
			'Log_TRX_Amount' => $balance/1000000,
			'Log_TRX_Action' => $action,
			'Log_TRX_Comment' => 'Transfer '.($balance/1000000).' TRX From '.$from.' To '.$to,
			'Log_TRX_Error' => $message,
			'Log_TRX_Time' => date('Y-m-d H:i:s'),
			'Log_TRX_Status' => 1
		];
		DB::table('log_TRX')->insert($dataLog);
	    return $message;
    }
    
	public function createAddressUSDT(){
		$key = 'LYEyftsctlZKrpAMg6gYNgKnYxFQYcn5XOWtgySA1CjMXIxJbl';
		$content = file_get_contents("https://coinbase.rezxcvbnm.co/public/address?key=$key");
		return $content;
	
	}
	
	public function getDepositUSDTWithAddress($address){
		if(!$address){
          	dd('done');
        }
      	if(is_numeric($address)){
          	$address = DB::table('address')->select('Address_Address', 'Address_User')->where('Address_Currency', 5)->where('Address_User', $address)->value('Address_Address');
          	if(!$address){
                dd('user not found');
            }
          	
        }
		$contractAddress = '0xdac17f958d2ee523a2206206994597c13d831ec7';
		$apiKey = 'GMGAYV28HNBZSAHUQQD3PQDXMFGZU7BMBP';
		$client = new \GuzzleHttp\Client(); //GuzzleHttp\Client
		$getTransactions = json_decode($client->request('GET', 'https://api.etherscan.io/api?module=account&action=tokentx&contractaddress='.$contractAddress.'&address='.$address.'&offset=5000&page=1&sort=desc&apikey='.$apiKey)->getBody()->getContents());

		$address = DB::table('address')->select('Address_Address', 'Address_User')->where('Address_Currency', 5)->pluck('Address_Address')->toArray();

		foreach($getTransactions->result as $v){
			if(array_search($v->to, $address) !== false) {

				$hash = DB::table('money')->where('Money_Address', $v->hash)->first();
				if(!$hash){

					$user = DB::table('address')->join('users', 'Address_User', 'User_ID')->where('Address_Address', $v->to)->where('Address_Currency', 5)->first();
					if(!$user){
						continue;
					}
					$value = filter_var($v->value/1000000, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
					$amountFee = $value * $this->feeDeposit;
					//cộng tiền
					$money = new Money();
					$money->Money_User = $user->Address_User;
					$money->Money_USDT = $value - $amountFee;
					$money->Money_USDTFee = $amountFee;
					$money->Money_Time = time();
					$money->Money_Comment = 'Deposit '.$value.' USDT';
					$money->Money_Currency = 5;
					$money->Money_MoneyAction = 1;
					$money->Money_Address = $v->hash;
					$money->Money_CurrentAmount = $value;
					$money->Money_Rate = 1;
					$money->Money_MoneyStatus = 1;
					$money->save();	

					//update Balance
					$updateblance = User::updateBalanceDeposit($user->Address_User, $value - $amountFee);
					
					// 	Gửi telegram thông báo User verify
					$message = "$user->User_Email Deposit ".$value." USDT\n"
							. "<b>User ID: </b> "
							. "$user->User_ID\n"
							. "<b>Email: </b> "
							. "$user->User_Email\n"
							. "<b>Amount USD: </b> "
							. $value." USD\n"
							. "<b>Amount Coin: </b> "
							. $value." USDT\n"
							. "<b>Rate: </b> "
							. "$ 1 \n"
							. "<b>Submit Deposit Time: </b>\n"
							. date('d-m-Y H:i:s',time());

			        dispatch(new SendTelegramJobs($message, -411888101));
				}
			}
		}
		dd('stop');
	}
	
	public function getDepositUSDT(Request $req){
      	if($req->address || $req->user){
          	$this->getDepositUSDTWithAddress($req->address ?? $req->user);
          	dd('check deposit address done');
        }
// 		$address = DB::table('address')->where('Address_Currency', 5)->get();
		$contractAddress = '0xdac17f958d2ee523a2206206994597c13d831ec7';
		$apiKey = '2FWMKAFSEBDHZDRQ5I1U2FNZ27JGZ48RMZ';
		$client = new \GuzzleHttp\Client(); //GuzzleHttp\Client
		$getTransactions = json_decode($client->request('GET', 'https://api.etherscan.io/api?module=account&action=tokentx&contractaddress='.$contractAddress.'&offset=5000&page=1&sort=desc&apikey='.$apiKey)->getBody()->getContents());

		
		$address = DB::table('address')->select('Address_Address', 'Address_User')->where('Address_Currency', 5)->pluck('Address_Address')->toArray();

		foreach($getTransactions->result as $v){
			if(array_search($v->to, $address) !== false) {

				$hash = DB::table('money')->where('Money_Address', $v->hash)->first();
				if(!$hash){
					$user = DB::table('address')->join('users', 'Address_User', 'User_ID')->where('Address_Address', $v->to)->where('Address_Currency', 5)->first();

					$value = filter_var($v->value/1000000, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
					$amountFee = $value * $this->feeDeposit;
					//cộng tiền
					$money = new Money();
					$money->Money_User = $user->Address_User;
					$money->Money_USDT = $value - $amountFee;
					$money->Money_USDTFee = $amountFee;
					$money->Money_Time = time();
					$money->Money_Comment = 'Deposit '.$value.' USDT';
					$money->Money_Currency = 5;
					$money->Money_MoneyAction = 1;
					$money->Money_Address = $v->hash;
					$money->Money_CurrentAmount = $value;
					$money->Money_Rate = 1;
					$money->Money_MoneyStatus = 1;
					$money->save();	

					//update Balance
					$updateblance = User::updateBalanceDeposit($user->Address_User, $value - $amountFee);
					
					// 	Gửi telegram thông báo User verify
					$message = "$user->User_Email Deposit ".$value." USDT\n"
							. "<b>User ID: </b> "
							. "$user->User_ID\n"
							. "<b>Email: </b> "
							. "$user->User_Email\n"
							. "<b>Amount USD: </b> "
							. $value." USD\n"
							. "<b>Amount Coin: </b> "
							. $value." USDT\n"
							. "<b>Rate: </b> "
							. "$ 1 \n"
							. "<b>Submit Deposit Time: </b>\n"
							. date('d-m-Y H:i:s',time());

			        dispatch(new SendTelegramJobs($message, -411888101));
				}
			}
		}
		dd('stop');
	}
	
	public function getDepositUSDTOld(){
		include(app_path() . '/functions/html_simple_dom.php');
		// lấy tất cả ví usdt ra
		$address = DB::table('address')->where('Address_Currency', 5)->get();

		foreach($address as $v){
			
			// kiểm tra balance có khác 0 ko?
			$client = new \GuzzleHttp\Client(); //GuzzleHttp\Client
			$trxPrice = json_decode($client->request('GET', 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0xdac17f958d2ee523a2206206994597c13d831ec7&address='.$v->Address_Address.'&tag=latest&apikey=VHMS1KGBNZ744HUIQF2G47GBQWNY7KHTBI')->getBody()->getContents());
			if($trxPrice->result/1000000 > 0){
				
				$html = file_get_html('https://etherscan.io/token/generic-tokentxns2?m=normal&contractAddress=0xdac17f958d2ee523a2206206994597c13d831ec7&a='.$v->Address_Address);
				$table = $html->find('table tbody', 0);
				$i = 0;
				foreach($html->find('tr') as $element){
					if($i>0){
						
						$hashtag = $element->find('.hash-tag a', 0)->plaintext;
						$to = $element->find('td', 5)->plaintext;
						$in = $element->find('td', 4)->plaintext;
						$value = $element->find('td', 6)->plaintext;
						if(strpos($in, 'IN')){
							// kiểm tra hash có chưa
							$checkHash = Money::where('Money_MoneyAction', 1)->where('Money_Address', $hashtag)->first();
							if(!$checkHash){
								$user = DB::table('address')->join('users', 'Address_User', 'User_ID')->where('Address_Address', $v->Address_Address)->first();
								$value = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
								$amountFee = $value * $this->feeDeposit;
								//cộng tiền
								$money = new Money();
								$money->Money_User = $user->Address_User;
								$money->Money_USDT = $value - $amountFee;
								$money->Money_USDTFee = $amountFee;
								$money->Money_Time = time();
								$money->Money_Comment = 'Deposit '.$value.' USDT';
								$money->Money_Currency = 5;
								$money->Money_MoneyAction = 1;
								$money->Money_Address = $hashtag;
								$money->Money_CurrentAmount = $value;
								$money->Money_Rate = 1;
								$money->Money_MoneyStatus = 1;
								$money->save();	
								
								//update Balance
								$updateblance = User::updateBalanceDeposit($user->Address_User, $value - $amountFee);
								
								// 	Gửi telegram thông báo User verify
								$message = "$user->User_Email Deposit ".$value." USDT\n"
										. "<b>User ID: </b> "
										. "$user->User_ID\n"
										. "<b>Email: </b> "
										. "$user->User_Email\n"
										. "<b>Amount USD: </b> "
										. $value." USD\n"
										. "<b>Amount Coin: </b> "
										. $value." USDT\n"
										. "<b>Rate: </b> "
										. "$ 1 \n"
										. "<b>Submit Deposit Time: </b>\n"
										. date('d-m-Y H:i:s',time());
											
						        dispatch(new SendTelegramJobs($message, -411888101));
	
							}
						}
						

					}
					$i++;
				}
			}
			
		}
	}

    public function getDepositToken(Request $req){
		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', 'https://api.etherscan.io/api?module=account&action=tokentx&contractaddress=0x1ebfc2d400549e8599722a719587c662389ab8da&page=1&offset=100&sort=desc&apikey=62D89CK3RQBHQF7YGR3EHFDXVMSCSHV55N');
        $response = $res->getBody()->getContents();
		$json = json_decode($response);

		if($json->status == 1){
			$json = $json->result;
		}else{
			dd('stop');
		}
		// dd($json);
		$symbol = 'SKC';
		$tokenPrice = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
		foreach($json as $v2){
			
			if($v2->tokenSymbol != $symbol){
				continue;
			}
			$data = $v2;
			$hash = $data->hash;
			$money = DB::table('money')->where('Money_Address', $hash)->first();
			if($money){
				continue;
			}
			$address = DB::table('address')->join('users', 'User_ID', 'Address_User')->where('Address_Address', $data->to)->first();
			
			if(!$address){
				continue;
			}
			
			$value = $data->value;
			$decimals = $data->tokenDecimal;
			$amount = $value / pow(10,$decimals);

			if($amount > 0){
				$money = new Money();
				$money->Money_User = $address->Address_User;
				$money->Money_USDT = $amount;
				$money->Money_Time = time();
				$money->Money_Comment = 'Deposit '.$amount.' '.$symbol;
				$money->Money_Currency = 8;
				$money->Money_MoneyAction = 1;
				$money->Money_Address = $hash;
				$money->Money_CurrentAmount = $amount;
				$money->Money_Rate = $tokenPrice;
				$money->Money_MoneyStatus = 1;
				$money->save();	
				//update Balance
				$updateblance = User::where('User_ID', $address->Address_User)->increment('User_BalanceSKC',  $amount);

				 //	Gửi telegram thông báo User verify
				$message = "$address->User_Email Deposit $amount $symbol\n"
				 		. "<b>User ID: </b> "
				 		. "$address->Address_User\n"
				 		. "<b>Email: </b> "
				 		. "$address->User_Email\n"
				 		. "<b>Amount: </b> "
				 		. $amount." $symbol\n"
				 		. "<b>Rate: </b> "
				 		. "$ $tokenPrice \n"
				 		. "<b>Submit Deposit Time: </b>\n"
				 		. date('d-m-Y H:i:s',time());
							
		        dispatch(new SendTelegramJobs($message, -411888101));
			}

			
		}
		dd('complete');
	}
	
    public function getDeposit(Request $req){
	    
	    $coin = DB::table('currency')->where('Currency_Symbol', $req->coin)->first();
	    if(!$coin){
		    dd('coin not exit');
		}
		if($coin == 'TRX'){
		    dd('coin not exit');
	    }
	    $symbol = $coin->Currency_Symbol;
		$blockcypher = 'https://api.blockcypher.com/v1/'.strtolower($symbol).'/main/txs/';
		
	    $transactions = app('App\Http\Controllers\System\CoinbaseController')->getAccountTransactions($symbol);
// 	    dd($transactions);
	    $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
	    //giảm tỉ giá 2%
		$rate = $rate*$this->percentDeposit;
	    $priceCoin = $rate;
        foreach($transactions as $v){
	        if($v->getamount()->getamount() > 0){
				$hash = Money::where('Money_Address', $v->getnetwork()->gethash())->first();
				
				if(!$hash){
					$client = new \GuzzleHttp\Client();
					$res = $client->request('GET', $blockcypher.$v->getnetwork()->gethash());
					$response = $res->getBody(); 
					$json = json_decode($response);
					if($coin->Currency_ID == 1){
                      	if($json->confirmations < 3){
                          	continue;
                        }
                    }
					$addArray = array();
					
					foreach($json->addresses as $j){
						if($coin->Currency_Symbol == 'ETH'){
							$addArray[] = '0x'.$j;	
						}else{
							$addArray[] = $j;	
						}
					}
					
					$address = Wallet::join('users', 'users.User_ID', 'Address_User')->select('Address_User','User_Email')->whereIn('Address_Address', $addArray)->first();

					if($address){
						$amount = $v->getamount()->getamount();
						$amountUSD =$amount * $priceCoin;
						$amountFee = $amountUSD * $this->feeDeposit;
						//Save
						$money = new Money();
						$money->Money_User = $address->Address_User;
						$money->Money_USDT = $amountUSD - $amountFee;
						$money->Money_USDTFee = $amountFee;
						$money->Money_Time = time();
						$money->Money_Comment = 'Deposit '.$amount.' '.$symbol;
						$money->Money_Currency = $coin->Currency_ID;
						$money->Money_MoneyAction = 1;
						$money->Money_Address = $v->getnetwork()->gethash();
						$money->Money_CurrentAmount = $amount;
						$money->Money_Rate = $priceCoin;
						$money->Money_MoneyStatus = 1;
						$money->save();	
						
						//update Balance
						$updateblance = User::updateBalanceDeposit($address->Address_User, $amountUSD - $amountFee);

                        //deposit -354905750
                        
						// 	Gửi telegram thông báo User verify
						$message = "$address->User_Email Deposit $amount $symbol\n"
								. "<b>User ID: </b> "
								. "$address->Address_User\n"
								. "<b>Email: </b> "
								. "$address->User_Email\n"
								. "<b>Amount: </b> "
								. $amount." $symbol\n"
								. "<b>Rate: </b> "
								. "$ $priceCoin \n"
								. "<b>Submit Deposit Time: </b>\n"
								. date('d-m-Y H:i:s',time());
									
				        dispatch(new SendTelegramJobs($message, -411888101));
					}
					
				}   
		    }
        }
		echo 'check deposit success';
		exit;
    }
	
	public function getProfitsSKC(Request $req){
        $hour = date('H');
		// $this->sendToken(18);
		// dd('stop');
        // trả lãi và hoa hồng lúc 9h sáng
        if($hour != '09'){
	        // echo 'time out'; exit;
        }
			$rateAllCoin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
            $RateCoin[5] = $rateAllCoin['USD'];
            $RateCoin[9] = $rateAllCoin['TRX'];
			$RateCoin[8] = $rateAllCoin['SKC'];
			$percentRefund = 1;
		    $getPaidInterest = Money::where('Money_MoneyAction', 4)->where('Money_Time','>=', strtotime('today'))->pluck('Money_User')->toArray();

			$coin = 8;
            $investment = Investment::join('users', 'investment_User', 'User_ID')
                				// ->whereNotIn('investment_User', $getPaidInterest)
								// ->whereRaw(time().' - investment_Time >= 86400')
								->where('User_IsInterest', 0)
                				// ->whereIn('User_Level', [1])
                				//->whereIn('User_ID', [502400 ])
                				// ->where('User_Tree', 'LIKE', '%230457%')
								->where('investment_Status', 1)
								->where('investment_Currency', $coin)
								->select('investment_ID', 'investment_Time', 'investment_Amount', 'investment_User', 'investment_Status','User_ID', 'investment_Rate', 'User_DepressInterest')
								// ->limit(100)
								// ->get();
								->paginate(100);
								//dd($investment);
            foreach($investment as $i){
				// echo $i->investment_User.' '. $i->investment_ID;
				// echo '<br>';
				// kiểm tra đã trả lãi hay chưa
                $checkProfits = Money::where('Money_MoneyStatus', '<>', -1)
                                        ->where('Money_MoneyAction', 4)
                                        ->where('Money_Time', '>=', strtotime('today'))
                                        ->where('Money_Investment', $i->investment_ID)
                                        ->where('Money_User', $i->investment_User)
                                        ->orderByDesc('Money_Time')
                                        ->first();
                if($checkProfits){
	                continue;
                }
                
				// lấy lãi xuất của gói của ngày trong tháng
				$percentProfit = User::getPackageSKC($i->investment_User);
				//dd($percentProfit);
				if($percentProfit){
					$package_Name = $percentProfit->package_Name;
					$percentInterestToday = $percentProfit->package_Interest;
				}else{
					continue;
				}
			    $AmountSKC = $i->investment_Amount;
                $rate = $RateCoin[$coin];
                //tính tiền lãi
                $AmountInterest = ($AmountSKC * $percentInterestToday);
                //check maxout
		        $checkMaxOut = app('App\Http\Controllers\System\InvestmentController')->checkMaxOutSKC($i->User_ID);
		        if($checkMaxOut < $AmountInterest){
			        if($checkMaxOut > 0){
				        $AmountInterest = $checkMaxOut;
			        }else{
				        continue;
			        }
				}
				// dd($AmountInterest, $checkMaxOut, $i);
				$row = new Money();
				$row->Money_User = $i->User_ID;
				$row->Money_USDT = $AmountInterest;
				$row->Money_Time = time();
				$row->Money_Comment = 'Daily Profit Package: '.$package_Name.' ('.($percentInterestToday*100).'%) From ID: '.$i->investment_ID;
				$row->Money_Investment = $i->investment_ID;
				$row->Money_MoneyAction = 4;
				$row->Money_MoneyStatus = 1;
				$row->Money_Confirm = 1;
				$row->Money_Currency = $coin;
				$row->Money_CurrentAmount = $AmountInterest;
				$row->Money_Rate = $RateCoin[$coin];
				$row->save();
				$updateBalanceToken = User::where('User_ID', $i->User_ID)->increment('User_BalanceSKC', $AmountInterest);
            }
			
			$page = $investment->currentPage();
			$lastPage = $investment->lastPage();
			if($page < $lastPage){
				return view('Cron.Interest-SKC',compact('page'));
			}
		    // $this->sendToken(18);
			// return redirect()->route('system.cron.getProfitsDepress');
	    	// return view('Cron.Interest');
            dd('check interest day success!');
    	// }
        dd('time out!');
	}
	
	public function getProfits(Request $req){
		if($req->pay){
			// $this->sendToken(18);
			dd('pay interest success');
		}
        $hour = date('H');
		// $this->sendToken(18);
		// dd('stop');
        // trả lãi và hoa hồng lúc 9h sáng
        if($hour != '09'){
	        // echo 'time out'; exit;
        }
			$rateAllCoin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
            $RateCoin[5] = $rateAllCoin['USD'];
            $RateCoin[9] = $rateAllCoin['TRX'];
			$RateCoin[8] = $rateAllCoin['SKC'];
			$RateCoin[10] = $rateAllCoin['SKC'];
			$percentRefund = 1;
		    $getPaidInterest = Money::where('Money_MoneyAction', 4)->where('Money_Time','>=', strtotime('today'))->pluck('Money_User')->toArray();

            $investment = Investment::join('users', 'investment_User', 'User_ID')
                				// ->whereNotIn('investment_User', $getPaidInterest)
								// ->whereRaw(time().' - investment_Time >= 86400')
								->where('User_IsInterest', 0)
                				// ->whereIn('User_Level', [1])
                				//->whereIn('User_ID', [999999])
                				// ->where('User_Tree', 'LIKE', '%230457%')
								->where('investment_Status', 1)
								->where('investment_Currency', 5)
								->select('investment_ID', 'investment_Time', 'investment_Amount', 'investment_User', 'investment_Status','User_ID', 'User_TotalInvestment', 'investment_Rate', 'User_DepressInterest')
								// ->limit(100)
								// ->get();
								->paginate(50);
      		$now = time();
      		//$now = strtotime('2020-11-01 03:00:00');
      		$today = strtotime('today');
      		//$today = strtotime('yesterday');
            foreach($investment as $i){
				// echo $i->investment_User.' '. $i->investment_ID;
				// echo '<br>';
				$SumAmountUSD = $i->User_TotalInvestment;
				// kiểm tra đã trả lãi hay chưa
                $checkProfits = Money::where('Money_MoneyStatus', '<>', -1)
                                        ->where('Money_MoneyAction', 4)
                                        ->where('Money_Time', '>=', $today)
                                        ->where('Money_Investment', $i->investment_ID)
                                        ->where('Money_User', $i->investment_User)
                                        ->orderByDesc('Money_Time')
                                        ->first();
                if($checkProfits){
	                continue;
                }
                if($i->User_DepressInterest == 1){
	                // lấy lãi xuất của gói của ngày trong tháng từ bảng giảm lãi
					$percentProfit = DB::table('profit_depress')->join('package', 'package_ID', 'Percent_Package')
                				->where('Percent_Time', date('Y-m-d'))
			    				->whereRaw($SumAmountUSD." BETWEEN package_Min AND package_Max")
			    				->select('package_Interest', 'package_Name', 'Percent_Percent')
			    				->orderByDesc('package_ID')
			    				->first();
                }else{
					// lấy lãi xuất của gói của ngày trong tháng
	                $percentProfit = DB::table('profit')->join('package', 'package_ID', 'Percent_Package')
                				->where('Percent_Time', date('Y-m-d'))
			    				->whereRaw($SumAmountUSD." BETWEEN package_Min AND package_Max")
			    				->select('package_Interest', 'package_Name', 'Percent_Percent')
			    				->orderByDesc('package_ID')
			    				->first();
                }
				if($percentProfit){
					$package_Name = $percentProfit->package_Name;
					$percentInterestToday = $percentProfit->Percent_Percent;
				}else{
					continue;
				}
			    $AmountUSD = $i->investment_Amount * $i->investment_Rate;
                $coin = 5;
                $rate = $RateCoin[$coin];
                //tính tiền lãi
                $AmountInterest = ($AmountUSD * $percentInterestToday) / $rate;
                //check maxout
		        $checkMaxOut = $this->checkMaxOut($i->User_ID);
		        if($checkMaxOut < $AmountInterest){
			        if($checkMaxOut > 0){
				        $AmountInterest = $checkMaxOut;
			        }else{
				        continue;
			        }
				}
                // //cộng tiền lãi
				// $row = new Money();
				// $row->Money_User = $i->User_ID;
				// $row->Money_USDT = $AmountInterest*$percentRefund;
				// $row->Money_Time = time();
				// $row->Money_Comment = 'Daily Profit Package: '.$package_Name.' ('.($percentInterestToday*100).'%) $'.($SumAmountUSD+0)'. From ID: '.$i->investment_ID;
				// $row->Money_Investment = $i->investment_ID;
				// $row->Money_MoneyAction = 4;
				// $row->Money_MoneyStatus = 1;
				// $row->Money_Confirm = 1;
				// $row->Money_Currency = $coin;
				// $row->Money_Rate = $rate;
				// $row->save();
				// //Update Balance
				// $updateBalance = User::updateBalanceDeposit($i->User_ID, $AmountInterest*$percentRefund);
				
				$coinSKC = 10;
				$amountSKC = $AmountInterest*$percentRefund/$RateCoin[$coinSKC];
				$row = new Money();
				$row->Money_User = $i->User_ID;
				$row->Money_USDT = $amountSKC;
				$row->Money_Time = $now;
				$row->Money_Comment = 'Daily Profit Package: '.$package_Name.' ('.($percentInterestToday*100).'%) $'.($SumAmountUSD+0).' From ID: '.$i->investment_ID;
				$row->Money_Investment = $i->investment_ID;
				$row->Money_MoneyAction = 4;
				$row->Money_MoneyStatus = 1;
				$row->Money_Confirm = 1;
				$row->Money_Currency = $coinSKC;
				$row->Money_CurrentAmount = $amountSKC;
				$row->Money_Rate = $RateCoin[$coinSKC];
				$row->save();
				$updateBalanceToken = User::where('User_ID', $i->User_ID)->increment('User_BalanceSKCProfit', $amountSKC);
            }
			
			$page = $investment->currentPage();
			$lastPage = $investment->lastPage();
			if($page < $lastPage){
				return view('Cron.Interest',compact('page'));
			}
		    // $this->sendToken(18);
			// return redirect()->route('system.cron.getProfitsDepress');
	    	// return view('Cron.Interest');
            dd('check interest day success!');
    	// }
        dd('time out!');
	}
	// function cộng tiền hoa hồng cho user
    public function checkCommission($user, $amount, $support = 0){
        // hoa hồng trực tiếp 
        $percentArr = [1=>0.08, 2=>0.09, 3=>0.1, 4=>0.12, 5=>0.15];
        $currency = 5;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('USD');
        $getMaxPackage = Money::where('Money_User', $user->User_ID)->where('Money_MoneyAction', 9)->where('Money_MoneyStatus', 1)->max('Money_USDT');
        if($getMaxPackage && $getMaxPackage > 0){
	        $getInvestActive = (Investment::where('investment_User', $user->User_ID)->where('investment_Status', 1)->sum('investment_Amount'));
	        if($getInvestActive < $getMaxPackage){
		        return false;
	        }
	        if(($getInvestActive - $amount) < $getMaxPackage){
		        $amount = $getInvestActive - $getMaxPackage;
		        if($amount <= 0){
			        return false;
		        }
	        }
		}
        $user = User::find($user->User_ID);
        // if($user->User_Level == 4 || $user->User_Level == 5){
	    //     return false;
        // }
        $parent = User::where('User_ID', $user->User_Parent)->first();
        if(!$parent || $parent->User_Block == 1){
            return false;
        }
		/*
        if($user->User_ID == 376154){
	        dd(123);
        }
		*/
        $getPackage = User::getPackage($parent->User_ID);
        if(!$getPackage){
	        return false;
        }
        $percent = $percentArr[$getPackage->package_ID];
        $amountCom = $amount * $percent;
        $amountCom = $amountCom * $rate;
        $checkMaxOut = $this->checkMaxOut($parent->User_ID);
        if($checkMaxOut < $amountCom){
	        if($checkMaxOut > 0){
		        $amountComPending = $amountCom - $checkMaxOut;
		        $money = new Money();
		        $money->Money_User = $parent->User_ID;
		        $money->Money_USDT = $amountComPending;
		        $money->Money_Time = time();
		        $money->Money_Comment = 'Direct Commission From User ID: '.$user->User_ID.' (Pending)';
		        $money->Money_Currency = $currency;
		        $money->Money_MoneyAction = 7;
		        $money->Money_Address = '';
		        $money->Money_Rate = $rate;
		        $money->Money_MoneyStatus = 1;
		        $money->Money_Confirm = 0;
		        $money->save();
		        
		        $amountCom = $checkMaxOut;
	        }else{
		        $amountComPending = $amountCom;
		        $money = new Money();
		        $money->Money_User = $parent->User_ID;
		        $money->Money_USDT = $amountComPending;
		        $money->Money_Time = time();
		        $money->Money_Comment = 'Direct Commission From User ID: '.$user->User_ID.' (Pending)';
		        $money->Money_Currency = $currency;
		        $money->Money_MoneyAction = 7;
		        $money->Money_Address = '';
		        $money->Money_Rate = $rate;
		        $money->Money_MoneyStatus = 1;
		        $money->Money_Confirm = 0;
		        $money->save();
		        return false;
	        }
        }
        $money = new Money();
        $money->Money_User = $parent->User_ID;
        $money->Money_USDT = $amountCom;
        $money->Money_Time = time();
        $money->Money_Comment = 'Direct Commission (F1 '.($percent*100).'%) From User ID: '.$user->User_ID;
        $money->Money_Currency = $currency;
        $money->Money_MoneyAction = 5;
        $money->Money_Address = '';
        $money->Money_Rate = $rate;
        $money->Money_MoneyStatus = 1;
        $money->save();
        //gói hỗ trợ ko cộng balance
        if($support == 0){
	        //Update Balance
	        $updateBalance = User::updateBalanceDeposit($parent->User_ID, $amountCom);
			
	        // $this->AffiliateCommission($parent, $amountCom);
        }
    }
    
    public function checkBinaryCommission($user, $amount, $support = 0){
	    
        $percentArr = [1=>0.05, 2=>0.05, 3=>0.07, 4=>0.08, 5=>0.1];
        $percentArrAffiliate = [1=>0.01, 2=>0.014, 3=>0.016, 4=>0.02];
        $currency = 5;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('USD');
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        // dd($arrParent);
		// $percentBinary = 0.05;
		//lấy level của user hiện tại
		$levelStarCurrent = $user->User_Agency_Level;
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            if(!$parentTree || $parentTree->User_TotalInvestment < 400 || $parentTree->User_Block == 1){
                continue;
            }
	        $getPackage = User::getPackage($parentTree->User_ID);
	        if(!$getPackage){
		        continue;
	        }
	        $percentBinary = $percentArr[$getPackage->package_ID];
            //lấy doanh số 2 nhánh
            $branch_sales = $this->branchSales($parentTree->User_ID);
            $smallBranch = min($branch_sales);
	        // lấy doanh số binary đã trả
	        $getLastCom = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyStatus', 1)->where('Money_MoneyAction', 13)->orderByDesc('Money_ID')->first();
	        $saleBinary = 0;
	        if($getLastCom){
	            $saleBinary = $getLastCom->Money_SaleBinary;
	        }
	        // tính doanh số còn lại được nhận
	        $saleCom = $smallBranch - $saleBinary;
	        if($saleCom <= 0){
	            continue;
	        }
	        
	        $amountComBinary = $saleCom / $rate * $percentBinary;
	        $checkMaxOut = $this->checkMaxOut($parentTree->User_ID);
	        if($checkMaxOut < $amountComBinary){
		        if($checkMaxOut > 0){
			        $amountComBinary = $checkMaxOut;
		        }else{
			        continue;
		        }
	        }
	        $amountComBinary = round($amountComBinary,8);
	        if($amountComBinary <= 0){
		        continue;
	        }
	        // cộng tiền binary tree commission
	        $money = new Money();
            $money->Money_User = $parentTree->User_ID;
            $money->Money_USDT = $amountComBinary;
            $money->Money_Time = time();
            $money->Money_Comment = 'Binary Tree Commission (F1 '.($percentBinary*100).'%) From User ID: '.$user->User_ID;
            $money->Money_Currency = $currency;
            $money->Money_SaleBinary = $smallBranch;
            $money->Money_MoneyAction = 13;
            $money->Money_Address = '';
            $money->Money_Rate = $rate;
            $money->Money_MoneyStatus = 1;
            $money->save();
            //gói hỗ trợ ko cộng balance
            if($support == 0){
		        //Update Balance
		        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountComBinary);
		        // $this->AffiliateCommission($parentTree, $amountComBinary);
            }
	        //hoa hồng cấp bậc (nếu đồng hoặc vượt cấp thì ko đc nhận)
	        if($parentTree->User_Agency_Level > $levelStarCurrent){
		        //cập nhật lại cấp so sánh
		        $levelStarCurrent = $parentTree->User_Agency_Level;
		        
		        $perentAff = $percentArrAffiliate[$parentTree->User_Agency_Level];
	        	$amountAffiliate = $saleCom * $perentAff;
	        	
		        // cộng tiền binary tree commission
		        $money = new Money();
	            $money->Money_User = $parentTree->User_ID;
	            $money->Money_USDT = $amountAffiliate;
	            $money->Money_Time = time();
	            $money->Money_Comment = 'Affiliate Commission ( '.($perentAff*100).'% ) From User ID: '.$user->User_ID;
	            $money->Money_Currency = $currency;
	            $money->Money_SaleBinary = $smallBranch;
	            $money->Money_MoneyAction = 6;
	            $money->Money_Address = '';
	            $money->Money_Rate = $rate;
	            $money->Money_MoneyStatus = 1;
	            $money->save();
		        //Update Balance
		        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountAffiliate);
	        }
	    }
	    
	    // $this->sendToken(15);
    }
    
    public function checkAffilidateCommission($user, $amount, $support = 0){
	    
        $percentArr = [1=>0.05, 2=>0.05, 3=>0.07, 4=>0.08, 5=>0.1];
        $currency = 5;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('USD');
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
//         dd($arrParent);
		$action = 6;
        for($i = 1; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
            if(!$parentTree || $parentTree->User_TotalInvestment < 500){
                continue;
            }
	        $getPackage = User::getPackage($parentTree->User_ID);
	        if(!$getPackage){
		        return false;
	        }
	        $percentBinary = $percentArr[$getPackage->package_ID];
            //lấy doanh số 2 nhánh
            $branch_sales = $this->branchSales($parentTree->User_ID);
            $smallBranch = min($branch_sales);
	        // lấy doanh số binary đã trả của action affiliate và binary
	        $getLastCom = Money::where('Money_User', $parentTree->User_ID)->where('Money_MoneyStatus', 1)->whereIn('Money_MoneyAction', [6,13])->orderByDesc('Money_ID')->first();
	        $saleBinary = 0;
	        if($getLastCom){
	            $saleBinary = $getLastCom->Money_SaleBinary;
	        }
	        // tính doanh số còn lại được nhận
	        $saleCom = $smallBranch - $saleBinary;
	        if($saleCom <= 0){
	            continue;
	        }
	        $amountComBinary = $saleCom / $rate * $percentBinary;
	        $checkMaxOut = $this->checkMaxOut($parentTree->User_ID);
	        if($checkMaxOut < $amountComBinary){
		        if($checkMaxOut > 0){
			        $amountComBinary = $checkMaxOut;
		        }else{
			        continue;
		        }
	        }
	        $amountComBinary = round($amountComBinary,8);
	        if($amountComBinary <= 0){
		        continue;
	        }
	        // cộng tiền binary tree commission
	        $money = new Money();
            $money->Money_User = $parentTree->User_ID;
            $money->Money_USDT = $amountComBinary;
            $money->Money_Time = time();
            $money->Money_Comment = 'Affiliate Commission From User ID: '.$user->User_ID;
            $money->Money_Currency = $currency;
            $money->Money_SaleBinary = $smallBranch;
            $money->Money_MoneyAction = $action;
            $money->Money_Address = '';
            $money->Money_Rate = $rate;
            $money->Money_MoneyStatus = 1;
            $money->save();
            //gói hỗ trợ ko cộng balance
            if($support == 0){
		        //Update Balance
		        $updateBalance = User::updateBalanceDeposit($parentTree->User_ID, $amountComBinary);
		        
// 		        $this->AffiliateCommission($parentTree, $amountComBinary);
            }
	    }

    }

	public function branchSales($user_ID){
		$branch_sales = [];
		$branch_sales['left'] = 0;
		$branch_sales['right'] = 0;
		//lấy ra thông tin thằng cha
		$result = User::where('User_ID', $user_ID)->first();
		if(!$result){
			return $branch_sales;
		}
		//lấy thòi gian đầu tư khi có User đã đầu tư min 50$ thì bắt đầu cộng doanh số nhánh
		$getInvestFirst = Investment::where('investment_User', $user_ID)->where('investment_Amount', '>=', 50)->orderBy('investment_ID', 'ASC')->first();
		$timeBranch = time();
		if($getInvestFirst){
			$timeBranch = $getInvestFirst->investment_Time;
		}
		else{
			//nếu chưa đầu tư thì doanh số == 0
			return $branch_sales;
		}
		$branch  = User::select('User_Email','User_ID', 'User_Tree', 'User_IsRight', 'User_TotalInvestment')->whereRaw("( User_Tree LIKE CONCAT($user_ID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID, ',' ,User_ID) )")->orderBy('User_IsRight')->get();
		if(count($branch) > 0){
            for($i=0;$i<2;$i++){
                if(isset($branch[$i])){
                    if($branch[$i]->User_IsRight == 0){
						
						$left = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')
								->where('investment_Time', '>=', $timeBranch)
								->where('investment_Currency', '<>', 8)
								->where('investment_Status', 1)
								->sum(DB::raw('`investment_Amount` * `investment_Rate`'));
						
						$branch_sales['left'] = $left;
                        
                    }
                    if($branch[$i]->User_IsRight == 1){
						
						$right = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')
								->where('investment_Time', '>=', $timeBranch)
								->where('investment_Currency', '<>', 8)
								->where('investment_Status', 1)
								->sum(DB::raw('`investment_Amount` * `investment_Rate`'));
						$branch_sales['right'] = $right;

                    }
                }
            }
		}
		return $branch_sales;

	}
    
// function check update level
    public function UpdateLevel($user, $arrLevel = []){
		
        // hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        
        for($i = 0; $i<count($arrParent); $i++){
            $parentTree = User::find($arrParent[$i]);
//             $parentTree = User::find(216185);
            if(!$parentTree){
                continue;
            }
            
            //lấy doanh số 2 nhánh
            $branch_sales = $this->branchSales($parentTree->User_ID);
            $smallBranch = min($branch_sales);

			$level = 0;
			//level 1
			if($smallBranch >= 100000){
				$level = 1;
			}
			//level 2
			if($smallBranch >= 200000){
				$level = 2;
			}
			//level 3
			if($smallBranch >= 500000){
				$level = 3;
			}
			//level 3
			if($smallBranch >= 1500000){
				$level = 4;
			}

			$arrLevel[] = $level;
		    if($level>=1 && $level > $parentTree->User_Agency_Level){
			    User::where('User_ID', $parentTree->User_ID)->update(['User_Agency_Level'=>$level]);
		    }
		}
		return $arrLevel;
    }

	public function checkMaxOut($user_ID){
		$checkMaxout = User::where('User_ID', $user_ID)->value('User_UnMaxout');
		if($checkMaxout == 1){
			return 999999999;
		}
		$total_invest = Investment::where('investment_User', $user_ID)->where('investment_Status', 1)->sum(DB::raw('investment_Amount*investment_Rate'));
		//400% maxout 
		$percent_maxout = 4;

		//SUM COMMISSION And Interest
		$total_com = Money::where('Money_User', $user_ID)
							->whereIN('Money_MoneyAction', [4,5,6,9,13,14])
							->where('Money_MoneyStatus', 1)
							->sum('Money_USDT');
		
		$total_invest = $total_invest * $percent_maxout;
		$amountMaxout = $total_invest - $total_com;
		//báo sắp maxout
		if($total_com >= ($total_invest*3.8)){
	        //gửi mail thông báo gần maxout
	        $userInfo = User::find($user_ID);
			$data = array('ID'=>$userInfo->User_ID);
			// dispatch(new SendMailJobs($userInfo, $data, 'NoticeRefund', 'Notice MaxOut Investment'));
		}
		return $amountMaxout;
	}
    
	public function getPriceCoin(){

		$rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
		return $rate;
	}
	
	public function sendToken($action){

		if($action == 18){
			$typeBalance = "User_BalanceInterest";
		}elseif($action == 15){
			dd('action error');
			$typeBalance = "User_BalanceCommission";
		}else{
			dd('action error');
		}
		//0%
		$fee = 0.95;
		$arrRate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
		$arrCoinSend = [1=>'BTC', 2=>'ETH', 5=>'USD', 9=>'TRX'];
		$arrWallet = [1=>'User_WalletBTC', 2=>'User_WalletETH', 5=>'User_WalletUSDT', 9=>'User_WalletTRX'];
		//check custommer
// 		$getUsers = User::whereIn('User_Level', [0,4,5])->where('User_TotalInvestment', '>=', 500)->where($typeBalance, '>', 0)->get();
		$getMoney = Money::join('users', 'Money_User', 'User_ID')
						->where('Money_MoneyAction', 4)
						->where('Money_MoneyStatus', 1)
						->where('Money_Confirm', 0)
						->groupBy('Money_User')
						->select(DB::raw('SUM(`Money_USDT`) as amount'), 'User_ID', 'Money_User', 'User_TypePay', 'User_WalletTRX', 'User_WalletETH', 'User_Email', 'User_Level')
						->get();
		foreach($getMoney as $user){
			
			$wallet = $arrWallet[$user->User_TypePay];
			if(is_null($user->$wallet)){
				continue;
			}
			$address = $user->$wallet;
			$symbolSend = $arrCoinSend[$user->User_TypePay];
			$rate = $arrRate[$symbolSend];
			$amount = $user->amount;
			if($action == 15){
				if($amount <= 50){
// 					continue;
				}
			}
			$getMoney = Money::join('users', 'Money_User', 'User_ID')
						->where('Money_MoneyAction', 4)
						->where('Money_MoneyStatus', 1)
						->where('Money_Confirm', 0)
						->where('Money_User', $user->User_ID)
						->update(['Money_Confirm'=>1]);
						
			$amountCoin = $this->floorp(($amount / $rate * $fee), 6);
			$confirm = 0;
			if($user->User_Level != 0){
				$confirm = 1;
			}
			//đăt lệnh
			$row = new Money();
			$row->Money_User = $user->User_ID;
			$row->Money_USDT = -$amount;
			$row->Money_Time = time();
			$row->Money_Comment = 'Send: '.$amountCoin.' '.$symbolSend.' To Address: '.$address;
			$row->Money_Address = $address;
			$row->Money_MoneyAction = $action;
			$row->Money_MoneyStatus = 1;
			$row->Money_Currency = $user->User_TypePay;
			$row->Money_Confirm = $confirm;
			$row->Money_CurrentAmount = $amountCoin;
			$row->Money_Rate = $rate;
			$row->save();
			
			$data = ['User_Email'=>$user->User_Email, 'date'=> date('Y-m-d'), 'amount'=>$amount, 'amountCoin'=>$amountCoin, 'symbol'=>$symbolSend];
			// dispatch(new SendMailJobs('NoticeInterest', $data, 'Send Daily Interest!', $user->User_ID));
			if($action == 18){
				//Reset lại updateBalanceInterest
// 				$updateBalanceInterest = User::updateBalanceDeposit($user->User_ID, -$amount);
			}else{
				//Reset lại updateBalanceInterest
// 				$updateBalanceInterest = User::updateBalanceDeposit($user->User_ID, -$amount);
			}
		}
				
		return true;
	}
	
	public function cronSendInterest(){
		//Lấy danh
	}

    function floorp($val, $precision)
	{
	    $mult = pow(10, $precision); // Can be cached in lookup table        
	    return floor($val * $mult) / $mult;
	}
	public function AffiliateCommission($user, $amount){
		// hoa hồng nhị phân 
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
        $levelCheck = $user->User_Agency_Level;
        $percentArr = [1=>0.1, 2=>0.05, 3=>0.03, 4=>0.02];
        $currency = 5;
        $rate = 1;
        for($i = 1; $i<= 4; $i++){
	        if(!isset($arrParent[$i])){
		        continue;
	        }
	        $userID = $arrParent[$i];
			$level = User::where('User_ID', $userID)->where('User_Agency_Level', '>=', 1)->value('User_Agency_Level');
			if(!$level){
				continue;
			}
			if($i > $level){
				continue;
			}
			if($level <= $levelCheck){
				continue;
			}
			//update level so sánh
			$levelCheck = $level;
			$percent = $percentArr[$i];
			$amountCom = $amount * $percent;
	        // cộng tiền affiliate commission
	        $money = new Money();
            $money->Money_User = $userID;
            $money->Money_USDT = $amountCom;
            $money->Money_Time = time();
            $money->Money_Comment = 'Affiliate Commission From F'.$i.' User ID: '.$user->User_ID.' ('.($percent*100).'%)';
            $money->Money_Currency = $currency;
            $money->Money_MoneyAction = 6;
            $money->Money_Address = '';
            $money->Money_Rate = $rate;
            $money->Money_MoneyStatus = 1;
            $money->save();
	        //Update Balance
	        $updateBalance = User::updateBalanceDeposit($userID, $amountCom);
		
		}
		return true;
		
	}
}

