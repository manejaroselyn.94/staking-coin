<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Http\Controllers\System\CoinbaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Resource\Account;

use Image;
use PragmaRX\Google2FA\Google2FA;

use Sop\CryptoTypes\Asymmetric\EC\ECPublicKey;
use Sop\CryptoTypes\Asymmetric\EC\ECPrivateKey;
use Sop\CryptoEncoding\PEM;
use kornrunner\Keccak;

use App\Model\GoogleAuth;
use App\Model\User;
use App\Model\Log;
use App\Model\Money;
use App\Model\Profile;
use App\Model\Wallet;
use App\Model\Investment;
use DB;
use Mail;

use App\Jobs\SendTelegramJobs;
use App\Jobs\SendMailJobs;
class UserController extends Controller{

	
	public $successStatus = 200;
	public $keyHash	 = 'We_@Stake!123';
    
    public function postChangeNode(Request $req){
		include(app_path() . '/functions/xxtea.php');
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
// 		var_dump($data);
// 		exit;
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
	    if($data->node != 1 && $data->node != 0){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Node')),$this->keyHash)), 200);
	    }
	    $node = $data->node;
	    $user = User::find(Auth::user()->User_ID);
	    $getNodeCurrent = $user->User_Side_Active;
	    if($getNodeCurrent != $node){
		    $user->User_Side_Active = $node;
		    $user->save();
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Change Node Default Success!')),$this->keyHash)), 200);
	    }else{
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Can\'t change node same node current')),$this->keyHash)), 200);
	    }
    }
    
    public function postProfile(Request $req){
		include(app_path() . '/functions/xxtea.php');
        $google2fa = app('pragmarx.google2fa');
        
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		if(!$data->CoinReceive){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Type Pay')),$this->keyHash)), 200);
		}
	    $user = User::find(Auth::user()->User_ID);
        $AuthUser = GoogleAuth::select('google2fa_Secret')->where('google2fa_User', $user->User_ID)->first();
        if(!$AuthUser){
	    	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'User Unable Authenticator!')),$this->keyHash)), 200);
        }
        $valid = $google2fa->verifyKey($AuthUser->google2fa_Secret, "$data->otp");
        if(!$valid){
	    	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Wrong code!')),$this->keyHash)), 200);
        }
        
        $arr_Coin = [
            1 => 'BTC',
            2 => 'ETH',
            8 => 'ADA',
            5 => 'USD'
        ];
        
        $dataMail['User_Email'] = $user->User_Email;
        if(isset($data->addressETH) && $data->addressETH != $user->User_WalletETH ){
	        $dataMail['old_address'] = $user->User_WalletETH;
	        $dataMail['new_address'] = $data->addressETH;
	        $user->User_WalletETH = $data->addressETH;
			dispatch(new SendMailJobs('ChangeAddress', $dataMail, 'Update Wallet Address ETH!', $user->User_ID));
        }
        if(isset($data->addressADA) && $data->addressADA != $user->User_WalletADA){
	        $dataMail['old_address'] = $user->User_WalletADA;
	        $dataMail['new_address'] = $data->addressADA;
	        $user->User_WalletADA = $data->addressADA;
			dispatch(new SendMailJobs('ChangeAddress', $dataMail, 'Update Wallet Address ADA!', $user->User_ID));
        }
        if($user->User_TypePay != $data->CoinReceive){
            Log::insertLog($user->User_ID, "Change Coin Receive Interest", 0, 'Change Coin Receive Interest From '.$arr_Coin[$user->User_TypePay].' To '.$arr_Coin[$data->CoinReceive]);
	        $user->User_TypePay = $data->CoinReceive;
        }
	    $user->save();
	    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Change Information Success!')),$this->keyHash)), 200);
    }
	
    public function PostKYC(Request $request)
    {

		include(app_path() . '/functions/xxtea.php');
        $google2fa = app('pragmarx.google2fa');
        
		$data = json_decode(xxtea_decrypt(base64_decode($request->data),$this->keyHash));
		
		if(!$request->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		/*
if(!$data->passport){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Type Pay')),$this->keyHash)), 200);
		}
		if(!$data->passport_image){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Type Pay')),$this->keyHash)), 200);
		}
		if(!$data->passport_image_selfie){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Type Pay')),$this->keyHash)), 200);
		}
*/

        $validator = Validator::make((array)$data,[
            'passport' => 'required|unique:profile,Profile_Passport_ID',
            'passport_image' => 'required|unique:profile,Profile_Passport_Image',
            'passport_image_selfie' =>'required|unique:profile,Profile_Passport_Image_Selfie'
        ]);
		if ($validator->fails()) {
			return response()->json(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>$validator->errors())),$this->keyHash)), 200);
        }

        $user = Auth::user();
        
        $checkExist = Profile::where('Profile_User', $user->User_ID)->whereIn('Profile_Status', [0,1] )->first();
        // dd($checkExist);
        if ($checkExist) {
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please waiting admin verify!')),$this->keyHash)), 200);
//             return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => "Submitted successfully!"]);
        }
        $passportID = $data->passport;
        $passportImageStore = $data->passport_image;
        $passportImageSelfieStore = $data->passport_image_selfie;
        //get file extension
/*
        $passportImageExtension = $request->file('passport_image')->getClientOriginalExtension();
        $passportImageSelfieExtension = $request->file('passport_image_selfie')->getClientOriginalExtension();
        
        //set folder and file name
        $randomNumber = uniqid();
        $passportImageStore = "users/".$user->User_ID."/profile/passport_image_".$user->User_ID."_".$randomNumber.".".$passportImageExtension;
        $passportImageSelfieStore = "users/".$user->User_ID."/profile/passport_image_selfie_".$user->User_ID."_".$randomNumber.".".$passportImageSelfieExtension;
        //send to Image server

        $passportImageStatus =Storage::disk('ftp')->put($passportImageStore, fopen($request->file('passport_image'), 'r+'));
        $passportImageSelfieStatus =Storage::disk('ftp')->put($passportImageSelfieStore, fopen($request->file('passport_image_selfie'), 'r+'));
*/
        // if($user->User_ID == 2967580843){
        //     dd($passportImageStatus, $passportImageStore);
        // } 
//         if ($passportImageStatus and $passportImageSelfieStatus) {
            $insertProfileData = [
                'Profile_User' => $user->User_ID,
                'Profile_Passport_ID' => $passportID,
                'Profile_Passport_Image' => $passportImageStore,
                'Profile_Passport_Image_Selfie' => $passportImageSelfieStore,
                'Profile_Time' => date('Y-m-d H:i:s')
            ];
            $inserStatus = Profile::create($insertProfileData);
            if ($inserStatus) {
				//Gửi telegram thông báo lệh hoa hồng
				// $message = $user->User_ID. " Post KYC\n"
				// 				. "<b>User ID: </b>\n"
				// 				. "$user->User_ID\n"
				// 				. "<b>Email: </b>\n"
				// 				. "$user->User_Email\n"
				// 				. "<b>POST KYC Time: </b>\n"
				// 				. date('d-m-Y H:i:s',time());

				// dispatch(new SendTelegramJobs($message, -364563312));
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Upload profile successfully! Please waiting admin verify!')),$this->keyHash)), 200);
            }else{
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please try again!')),$this->keyHash)), 200);
// 	            return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Please contact admin!"]);
            }

//         }

		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please try again!')),$this->keyHash)), 200);
//         return redirect()->back()->with(['flash_level' => 'error', 'flash_message' => "Update profile error"]);

    }
    
	public function postAuth(Request $req){
		include(app_path() . '/functions/xxtea.php');
		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('isEnable'=>0)),$this->keyHash)));exit;
		$user = Auth::user();
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}

		if(property_exists($data, 'isEnable')){
			$Enable = $data->isEnable;
			if($Enable != 1 && $Enable != 0){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Enable wrong')),$this->keyHash)), 200);
			}
		}else{
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss IsEnable')),$this->keyHash)), 200);
		}
		
		// kiểm tra member có Auth chưa
		
		$userInfo = User::select('User_Auth', 'User_AuthStatus')->where('User_ID', $user->User_ID)->first();
		
		$google2fa = app('pragmarx.google2fa');
		
		if($userInfo->User_AuthStatus == 0){
			
			//bất auth
			if($Enable==0){
				return response()->json(array('status'=>false), 200);
			}
			
			$secret = $google2fa->generateSecretKey();
			
			$google2fa->setAllowInsecureCallToGoogleApis(true);
        
			$qrCodeUrl = $google2fa->getQRCodeUrl(
			    "GPG Token",
			    $user->User_Email,
			    $secret
			);

			User::where('User_ID', $user->User_ID)->update(['User_Auth'=>$secret]);
			$data = array('secret'=>$secret, 'QrCode'=>$qrCodeUrl);
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'data'=>$data)),$this->keyHash)), 200);
			
		}else{
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please contact admin!')),$this->keyHash)), 200);
		}
		
	}
	
	public function getAuth(){
		include(app_path() . '/functions/xxtea.php');
		$user = Auth::user();
		$google2fa = app('pragmarx.google2fa');
        //kiểm tra member có secret chưa?
        $auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

        $Enable = false;
        if($auth == null){
            $secret = $google2fa->generateSecretKey();
        }else{
            $Enable = true;
            $secret = $auth->User_Auth;
        }
        $google2fa->setAllowInsecureCallToGoogleApis(true);

        $inlineUrl = $google2fa->getQRCodeUrl(
            "WeStake",
            $user->User_Email,
            $secret
		);
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'Enable' => $Enable, 'secret' => $secret , 'inlineUrl' => $inlineUrl)),$this->keyHash)), 200);
	}

	public function postConfirmAuth(Request $req){
		
		$user = Auth::user();
		include(app_path() . '/functions/xxtea.php');
		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('secret'=>"TEKFDG3MNTPJJRYT",'authCode'=>"TEKFDG3MNTPJJRYT")),$this->keyHash)));exit;

		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		// print_r ($data);
		// exit;
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}

		if(!$data->authCode){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Auth Code')),$this->keyHash)), 200);
		}
		
		$google2fa = app('pragmarx.google2fa');
		
		$auth = GoogleAuth::where('google2fa_User',$user->User_ID)->first();

		$authCode = $data->authCode."";

        if($auth == null){
			if(!$data->secret){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Secret')),$this->keyHash)), 200);
			}
			$valid = $google2fa->verifyKey($data->secret, $authCode);
        }else{
            $valid = $google2fa->verifyKey($auth->google2fa_Secret, $authCode);
		}
		
		if($valid){
			if($auth){
                // xoá
				GoogleAuth::where('google2fa_User',$user->User_ID)->delete();
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Disable Authenticator')),$this->keyHash)), 200);
            }else{
                // Insert bảng google2fa
                $r = new GoogleAuth();
                $r->google2fa_User = $user->User_ID;
                $r->google2fa_Secret = $data->secret;
                $r->save();
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Enable Authenticator')),$this->keyHash)), 200);
            }
		}
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Code wrong')),$this->keyHash)), 200);
	}
	
	public function getCoin(){
		include(app_path() . '/functions/xxtea.php');
		$user = Auth::user();

	    $coin = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
		$balance = User::find($user->User_ID);
		$coinArray = array(
					'ADA'=>array(
						'id'=>8,
						'name'=>'Cardano',
						'balance'=> $balance->User_PointStock+0,
						'wallet' => $balance->User_WalletADA,
						'Price'=> $coin['ADA']+0,
						'PecentPlus'=> 0,
						'FeeWithdraw' => 0,
						'FeeSwap' => [],
						'Deposit'=>0,
						'Withdraw'=>[],
						'Transfer'=>0,
						'Invest'=>0
					),
					'BTC'=>array(
						'id'=>1,
						'name'=>'BitCoin',
						'balance'=> 0,
						'wallet' => '',
						'Price'=> $coin['BTC']+0,
						'PecentPlus'=> 0,
						'FeeWithdraw' => 0,
						'FeeSwap' => [],
						'Deposit'=>1,
						'Withdraw'=>[],
						'Transfer'=>0,
						'Invest'=>0
					),
					'ETH'=>array(
						'id'=>2,
						'name'=>'Ethereum',
						'balance'=> 0,
						'wallet' => $balance->User_WalletETH,
						'Price'=> $coin['ETH']+0,
						'PecentPlus'=> 0,
						'FeeWithdraw' => 0,
						'FeeSwap' => [],
						'Deposit'=>1,
						'Withdraw'=>[],
						'Transfer'=>0,
						'Invest'=>0
					),
					'USDT'=>array(
						'id'=>5,
						'name'=>'Tether',
						'balance'=> $balance->User_BalanceDeposit+0,
						'wallet' => '',
						'Price'=> $coin['USD']+0,
						'PecentPlus'=> 0,
						'Withdraw'=>[2,8],
						'FeeWithdraw' => 0.02,
						'FeeSwap' => [],
						'Deposit'=>1,
						'Transfer'=>0,
						'Invest'=>1
					)
		);
		
		$arrayReturn = array('status'=>true, 'data'=>$coinArray);
		return response(base64_encode(xxtea_encrypt(json_encode($arrayReturn),$this->keyHash)), 200);
	}

	public function getDeposit($id){
		include(app_path() . '/functions/xxtea.php');
		$user = Auth::user();
		$Coinbase = new CoinbaseController();
		$req = new Request;
		$req->coin = $id;
		$check = $Coinbase->getAddress($req);
		$arrayReturn = array('status'=>true, 'data'=>$check);
		return response(base64_encode(xxtea_encrypt(json_encode($arrayReturn),$this->keyHash)), 200);
	}
	
	public function getBalance(){
		
		include(app_path() . '/functions/xxtea.php');
		$user = Auth::user();
		// lấy giá của coin
		$balance = User::find($user->User_ID);
		$balanceArray = array(
					'ADA'=> $balance->User_PointStock,
					'BTC'=> 0,
					'ETH'=> 0,
					'USDT'=> $balance->User_BalanceDeposit+0
		);
		
		$arrayReturn = array('status'=>true, 'data'=>$balanceArray);
		return response(base64_encode(xxtea_encrypt(json_encode($arrayReturn),$this->keyHash)), 200);
	}
	
	public function getInfo(){
		include(app_path() . '/functions/xxtea.php');
		$user = User::find(Auth::user()->User_ID);
		$wallet = 1;
		if($user->User_WalletAddress == null){
			$wallet = 0;
		}
		$check_auth = DB::table('users')->where('User_ID',$user->User_ID)->join('google2fa','google2fa.google2fa_User','users.User_ID')->first();
		$status_auth = 0;
		if($check_auth){
			$status_auth = 1;
		}
        $arr_Coin = [
            1 => 'BTC',
            2 => 'ETH',
            8 => 'ADA',
            5 => 'USD'
        ];
        //check KYC
        $checkProfile = Profile::where('Profile_User', $user->User_ID)->first();
		$kyc = 0;
		if($checkProfile && $checkProfile->Profile_Status == 1){
			$kyc = 1;
		}
		$info = array(
					'ID'=>$user->User_ID,
					'Email'=>$user->User_Email,
					'Phone'=>$user->User_Phone,
					'RegisteredDatetime'=>$user->User_RegisteredDatetime,
					'Parent'=>$user->User_Parent,
					'KYC'=>$kyc,
					'Wallet'=>$wallet,
					'CoinReceive' => $arr_Coin[$user->User_TypePay],
					'Node_Active'=>$user->User_Side_Active == 1 ? 'Right' : 'Left',
					'Auth'=> $status_auth
						);
		$info['Level'] = $user->User_Agency_Level;
		$info['LevelName'] = ($user->User_Agency_Level == 0) ? "Member" : "Star ".$user->User_Agency_Level;
		$info['LevelImage'] = 'https://WeStake.co/assets/images/level/LEVEL_'.$user->User_Agency_Level.'.png';
		$getInvestFirst = Investment::where('investment_User', $user->User_ID)->where('investment_Status', 1)->orderBy('investment_ID')->first();
		$sales = 0;
		if($getInvestFirst){
			$sales = Investment::selectRaw('Sum(`investment_Amount`*`investment_Rate`) as SumInvest')
		    									->whereRaw('investment_User IN (SELECT User_ID FROM users WHERE User_Tree LIKE "'.$user->User_Tree.'%")')
		    									// ->where('investment_Time', '>=', $getInvestFirst->investment_Time)
		    									->where('investment_User', '<>', $user->User_ID)
		    									->where('investment_Status', 1)
		    									->first()->SumInvest;
		}
		$arrayReturn = array('status'=>true, 'data'=>array('info'=>$info, 'totalSales'=>number_format($sales, 2)));
		return response(base64_encode(xxtea_encrypt(json_encode($arrayReturn),$this->keyHash)), 200);

	}
	
	public function postMemberList(Request $req){ 
		
		include(app_path() . '/functions/xxtea.php');
		$limit = 30;
		$page = 1;
		$sort = 'asc';
		if($req->data){
			$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
			if(isset($data->limit)){
				$limit = $data->limit;
				if($limit<1){
					$limit = 30;
				}
			}
			
			
			if(isset($data->page)){
				$page = $data->page;
				if($page<1){
					$page = 1;
				}
			}
			if(property_exists($data, 'sort')){
				if($data->sort == 'desc'){
					$sort = 'desc';
				}
			}
		}
		
		
		$user = Auth::user();
		$dataReturn = array('status'=>true, 'link'=>'https://westake.co/register?ref='.$user->User_ID, 'data'=>'', 'total'=>1, 'page'=>$page, 'limit'=>$limit, 'sort'=>$sort);
		
		$total = User::select('User_ID')
						->whereRaw('User_Tree LIKE "'.$user->User_Tree.',%"')
						->count();
						
		$dataReturn['total'] = round($total/$limit) == 0 ? 1 : round($total/$limit);
		
		$user_list = User::join('user_agency_level', 'user_agency_level_ID', 'User_Agency_Level')
						->select('User_ID', 'User_Email', 'User_RegisteredDatetime', 'User_Parent', DB::raw("(CHAR_LENGTH(User_Tree)-CHAR_LENGTH(REPLACE(User_Tree, ',', '')))-" . substr_count($user->User_Tree, ',') . " AS f, User_Agency_Level, User_Tree, user_agency_level_Name"))
						->whereRaw('User_Tree LIKE "'.$user->User_Tree.',%"')
                        ->orderBy('f', $sort);
		// lấy tổng doanh số
		$totalSales = DB::table('investment')->join('users', 'investment_User', 'User_ID')
											->whereRaw('User_Tree LIKE "'.$user->User_Tree.'%" AND User_ID != '.$user->User_ID)
											->where('investment_Status', 1)
											->selectRaw('sum(investment_Amount*investment_Rate) as SumTotalSales');
											
		if($req->data){
			$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
			if(isset($data->user)){
				$getDataUser = User::where('User_ID', $data->user)->orWhere('User_Email', $data->user)->first();
				if($getDataUser){
					$totalSales = $totalSales->whereRaw('User_Tree LIKE "'.$getDataUser->User_Tree.'%"');
				}
			}
		}	
		Paginator::currentPageResolver(function () use ($page) {
	        return $page;
	    });
		$user_list = $user_list->paginate($limit);	
		$totalSales = $totalSales->first();				
		$arrayReturn = array();
		foreach($user_list as $v){
			
			$TotalInvest = DB::table('investment')
								->where('investment_User', $v->User_ID)
								->select(DB::raw('SUM(investment_Amount * investment_Rate) as Sales'))->first()->Sales + 0;
								
			// lấy tổng doanh số
			$SalesMember = DB::table('investment')->join('users', 'investment_User', 'User_ID')
											->whereRaw('User_Tree LIKE "'.$v->User_Tree.'%" AND User_ID != '.$v->User_ID)
											->where('investment_Status', 1)
											->selectRaw('sum(investment_Amount*investment_Rate) as SumTotalSales')->first()->SumTotalSales + 0;
			$level = $v->user_agency_level_Name;
			
			$arrayReturn[] = array(
				'ID' => $v->User_ID,
				'Email' => $v->User_Email,
				'RegisteredDatetime' => $v->User_RegisteredDatetime,
				'Parent' => $v->User_Parent,
				'F' => $v->f,
				'TotalInvest' => $v->User_TotalInvestment,
				'TotalSales' => number_format($this->getSaleUser($v->User_ID, true), 4),
				'level' => $level
			);
		}				
		$dataReturn['totalSales'] = $totalSales->SumTotalSales == null ? 0 : round($totalSales->SumTotalSales, 2);
		$dataReturn['data'] = $arrayReturn;
		return response(base64_encode(xxtea_encrypt(json_encode($dataReturn),$this->keyHash)), 200);

	}
	
    public function getSaleUser($userID, $root = null){
        $branch_trade = [];
        $count_children = [];
        $branch_trade['leftTrade'] = 0;
        $branch_trade['rightTrade'] = 0;

        $result = User::where('User_ID', $userID)->select('User_TotalInvestment','User_ID', 'User_Email', 'User_Tree', 'User_Parent')->first();
        if(!$result){
            return $branch_trade['leftTrade'] + $branch_trade['rightTrade'];
        }

        //lấy thòi gian đầu tư khi có User đã đầu tư min 50$ thì bắt đầu cộng doanh số nhánh
		$getInvestFirst = Investment::where('investment_User', $userID)->orderBy('investment_ID', 'ASC')->first();
		$timeBranch = time();
		if($getInvestFirst){
			$timeBranch = $getInvestFirst->investment_Time;
		}
		else{
			//nếu chưa đầu tư thì doanh số == 0
			return $branch_trade['leftTrade'] + $branch_trade['rightTrade'];
        }
        
		$branch  = User::select('User_Name','User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($userID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $userID, ',' ,User_ID) )")->orderBy('User_IsRight')->GET();
		if(count($branch) > 0){

            
            for($i=0;$i<2;$i++){
                if(isset($branch[$i])){
                    if($branch[$i]->User_IsRight == 0){
                        
                        $total_sales_children = Investment::join('users', 'User_ID', 'investment_User')->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->where('investment_Time', '>=', $timeBranch)->where('investment_Status', 1)->sum('investment_Amount');
                        $branch_trade['leftTrade'] = $total_sales_children;
                        // $count_children['children_left'] = User::where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->count();

                    }
                    if($branch[$i]->User_IsRight == 1){
                        
						$total_sales_children = Investment::join('users', 'User_ID', 'investment_User')->where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->where('investment_Time', '>=', $timeBranch)->where('investment_Status', 1)->sum('investment_Amount');
						$branch_trade['rightTrade'] = $total_sales_children;
                        // $count_children['children_right'] = User::where('User_Tree', 'LIKE', $branch[$i]->User_Tree.'%')->count();
                    }
                    
                }
                
    
            }
        }
        if($root == true){
            return $branch_trade['leftTrade'] + $branch_trade['rightTrade'];
        }
        else{
            return $branch_trade['leftTrade'] + $branch_trade['rightTrade'] + $result->User_TotalInvestment;
        }
        
    }
    
	public function postMemberTree(Request $req){
		include(app_path() . '/functions/xxtea.php');
		$user = Auth::user();
		$user_ID = $user->User_ID;
		if($req->data){
			$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
			if($data && property_exists($data, 'user')){
				$user_ID = $data->user;
			}
		}
		$dataReturn = array('status'=>true, 'link'=>'https://WeStake.co/register?ref='.$user->User_ID, 'data'=>'');
		$user_list = User::select('User_ID', 'User_Email', 'User_RegisteredDatetime', 'User_Parent', DB::raw("(CHAR_LENGTH(User_Tree)-CHAR_LENGTH(REPLACE(User_Tree, ',', '')))-" . substr_count($user->User_Tree, ',') . " AS f, User_Agency_Level, User_Tree, User_TotalInvestment, User_IsRight"))
						->whereRaw("( User_Tree LIKE CONCAT($user_ID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID, ',' ,User_ID) )")
						->orWhere('User_ID', $user_ID)
						->orderBy('f')
						->orderBy('User_IsRight', 'DESC')
// 						->whereRaw('User_Tree LIKE "'.$user->User_Tree.',%"')
						->get();			
		$arrayReturn = array();
		foreach($user_list as $v){
			
			$TotalInvest = $v->User_TotalInvestment;
			// lấy tổng doanh số
			$SalesMember = User::where('User_Tree', 'LIKE', $v->User_Tree.'%')->sum(DB::raw('User_TotalInvestment'));
			
			$level = $v->User_Agency_Level == 0 ? "Member" : "TGT ".$v->User_Agency_Level;
			
	        $arrParent = explode(',', $v->User_Tree);
	        $arrParent = array_reverse($arrParent);
	        $node = $v->User_IsRight == 1 ? 'Right' : 'Left';
			$arrayReturn[] = array(
				'ID' => $v->User_ID,
				'Email' => $v->User_Email,
				'RegisteredDatetime' => $v->User_RegisteredDatetime,
				'Parent' => $v->User_Parent,
				'Level' => $level,
				'Presenter' => isset($arrParent[1]) ? $arrParent[1] : $v->User_Parent,
				'Node' => $node,
				'TotalInvest' => $v->User_TotalInvestment,
				'TotalSales' => number_format($this->getSaleUser($v->User_ID, true), 4),
			);
		}
		$dataReturn['data'] = $arrayReturn;
		return response(base64_encode(xxtea_encrypt(json_encode($dataReturn),$this->keyHash)), 200);
	}
	
    function buildTree($idparent, $idRootTemp = null, $barnch = null) {

        $build = User::select('User_Email', 'User_Name','User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($idparent,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $idparent, ',' ,User_ID) )")->orderBy('User_IsRight')->GET();
        $child = array();
        if(count($build) > 0){
            for($i=0;$i<2;$i++){
                if(isset($build[$i])){
                    if($build[$i]->User_IsRight == 0){
                        
                        $child[] = array(
                            'id' => $build[$i]->User_ID,
                            'name' => '',
                            'title' => $build[$i]->User_ID,
                            'className' => 'node-tree '.strtoupper($build[$i]->User_Name),
                            'sales' => number_format(app('App\Http\Controllers\System\UserController')->getSaleUser($build[$i]->User_ID), 4),
                            'children' => $this->buildTree($build[$i]->User_ID, $build[$i]->User_ID, 0),
                        );
                        if(count($build) <2){
                            $child[] = array(
                                'id' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'name' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'title' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'sales' => '',
                                'className' => 'node-empty right'
                            );
                        }
                        
                    }
                    if($build[$i]->User_IsRight == 1){
                        if(count($build) <2){
                            $child[] = array(
                                'id' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'name' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'title' => 'a'.(int)$build[$i]->User_ID.''.rand(1,99),
                                'sales' => '',
                                'className' => 'node-empty left'
                            );
                        }
    
                        $child[] = array(
                            'id' => $build[$i]->User_ID,
                            'name' => '',
                            'title' => $build[$i]->User_ID,
                            'sales' => number_format($this->getSaleUser($build[$i]->User_ID), 4),
                            'className' => 'node-tree '.strtoupper($build[$i]->User_Name),
                            'children' => $this->buildTree($build[$i]->User_ID, $build[$i]->User_ID),
                        );
                        
                        
    
                    }
                    
                }
                
    
            }
        }
        else{
            $child[] = array(
                'id' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'name' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'title' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'className' => 'node-empty left'
            );
            $child[] = array(
                'id' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'name' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'title' => 'a'.(int)$idRootTemp.''.rand(1,99),
                'className' => 'node-empty right'
            );
        }
        return $child;
    }
	
	public function postHistory(Request $req){
		include(app_path() . '/functions/xxtea.php');
		
		$user = Auth::user();
		$limit = 30;
		$page = 1;
		$sort = 'asc';
		//echo base64_encode(xxtea_encrypt(json_encode(array('page'=>1, 'limit'=>20,'action'=>9)),$this->keyHash));exit;
		$where = null;
		
		if($req->data){
			$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
			if(isset($data->limit)){
				$limit = $data->limit;
				if($limit<1){
					$limit = 30;
				}
			}
			
			if(isset($data->page)){
				$page = $data->page;
				if($page<1){
					$page = 1;
				}
			}
			if(property_exists($data, 'sort')){
				if($data->sort == 'desc'){
					$sort = 'desc';
				}
			}
			if(property_exists($data, 'action')){
				if($data->action == 9 || $data->action == 10){
					$where = ' AND (Money_MoneyAction = 9 OR Money_MoneyAction = 10)';
					
				}else{
					$where = ' AND Money_MoneyAction = '.$data->action;
				}
			}
		}
		
		
		
		
		Paginator::currentPageResolver(function () use ($page) {
	        return $page;
	    });
		$history = DB::table('money')->select('Money_ID', 'Money_USDT', 'Money_USDTFee', 'Money_Time', 'Money_Comment', 'MoneyAction_Name', 'Currency_Name', 'Currency_Symbol', 'Money_MoneyStatus', 'Money_MoneyAction', 'Money_Confirm', 'Money_Rate')
					->join('moneyaction', 'Money_MoneyAction', 'MoneyAction_ID')
					->join('currency', 'Currency_ID', 'Money_Currency')
					->where('Money_User', $user->User_ID)
					->whereRaw('1 '.$where)
// 					->skip(($page-1)*$limit)->take($limit)
					->orderBy('Money_ID', $sort)->paginate($limit);
		$dataReturn = array('status'=>true, 'data'=>'', 'total'=>$history->lastPage(), 'page'=>$page, 'limit'=>$limit, 'sort'=>$sort, 'Direct'=>0, 'Indirect'=>0, 'Affiliate'=>0);
		$historyData = array();
		foreach($history as $v){
			
			if($v->Money_MoneyStatus == 1){
				if($v->Money_MoneyAction == 2 && $v->Money_Confirm == 0){
					$v->Money_MoneyStatus = "Processing";
				}else{
					$v->Money_MoneyStatus = "Success";
				}
			}else{
				$v->Money_MoneyStatus = "Cancel";
			}
			
			$historyData[] = array(
				'ID' => $v->Money_ID,
				'Amount' => $v->Money_USDT+0,
				'Fee' => $v->Money_USDTFee+0,
				'Time' => date('Y-m-d H:i:s', $v->Money_Time),
				'Comment' => $v->Money_Comment,
				'Rate' => $v->Money_Rate,
				'ActionName' => $v->MoneyAction_Name,
				'Currency' => $v->Currency_Name,
				'Symbol' => $v->Currency_Symbol,
				'Status' => $v->Money_MoneyStatus,
			);
		}
		$dataReturn['data'] = $historyData;
		
		$affiliate = 0;
		//bị ngược action
		$commission = DB::table('money')->where('Money_User', $user->User_ID)->whereIn('Money_MoneyAction', [4,6,7])->where('Money_MoneyStatus', 1)->selectRaw('Sum(Money_USDT) as amountUSD')->first();
		$Interest = DB::table('money')->where('Money_User', $user->User_ID)->where('Money_MoneyAction', 5)->where('Money_MoneyStatus', 1)->selectRaw('Sum(Money_USDT) as amountUSD')->first();
		$dataCom = 0;
		if($commission){
			$dataCom = $commission->amountUSD;
		}
		$dataInterest = 0;
		if($Interest){
			$dataInterest = $Interest->amountUSD;
		}
		$dataReturn['Commission'] = round($dataCom, 2);
		$dataReturn['Interest'] = round($dataInterest, 2);
		$dataReturn['Affiliate'] = round($affiliate, 2);

		return response(base64_encode(xxtea_encrypt(json_encode($dataReturn),$this->keyHash)), 200);
		
	}
	
	public function postEmail(Request $req){

		include(app_path() . '/functions/xxtea.php');
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);

		}

		if(!$data->email){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);

		}
		
		// kiểm tra email có tồn tại hay không
		$email = DB::table('users')->where('User_Email', $data->email)->first();
		if(!$email){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Email not exits')),$this->keyHash)), 200);
		}
		
		// tạo token 
		$otpNumber = rand(10000000, 99999999);
		
		$otp = DB::table('users')->where('User_ID', $email->User_ID)->update(['User_OTP'=>$otpNumber]);
		
		// gửi token về email
		//dữ liệu gửi sang mailtemplate
        $data = array('User_ID'=>$email->User_ID, 'otp'=>$otpNumber);

        // gửi mail thông báo
        $data = array('User_ID' => $userID, 'User_Email'=> $request->email, 'token'=>$token);
        //Job

        dispatch(new SendMailJobs('Active', $data, 'Active Account!', $userID));
        
        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please check your email to Login')),$this->keyHash)), 200);
        
	}
    
	
	public function postLogin(Request $req){
		include(app_path() . '/functions/xxtea.php');
//         echo base64_encode(xxtea_encrypt(json_encode(array('email'=>'ndthinh2035@gmail.com', 'password'=>123123)),$this->keyHash));exit;
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}

		if(!$data->email){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);
		}
		
		if(!$data->password){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Password')),$this->keyHash)), 200);
		}
		$random = rand(1,100);
		$now = time()+$random;
        $user = User::where('User_Email', $data->email)->first();
        if(!$user){
            return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Email is\'nt exist!')),$this->keyHash)), 200);
        }
        if($user->User_EmailActive != 1){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please check your email and active this account!')),$this->keyHash)), 200);
		}
		if($user){
			$user->User_Log = $now;
			$user->save();
        }
		if(Auth::attempt(['User_Email' => $data->email, 'password' => $data->password])){ 
			
            $user = Auth::user(); 
            
            $token = $user->createToken('TGT')->accessToken;
            
            $arrReturn = array('status'=>true, 'token'=>$token);
        
			return response(base64_encode(xxtea_encrypt(json_encode($arrReturn),$this->keyHash)), 200);
        }else{ 
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false,'message' => 'Login information is incorrect')),$this->keyHash)), 200);

        }
    }
    
    public function postForgetPassword(Request $req){
		
		include(app_path() . '/functions/xxtea.php');
		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('email'=>'skipro982301@gmail.com')),$this->keyHash)));exit;
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		
		if(!$data->email){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);
		}	
		
		$user = User::where('User_Email', $data->email)->first();

		if(!$user){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Email not exits')),$this->keyHash)), 200);

		}
		
		// gửi mail pass mới
		$passwordRan = $this->RandomString();
		$user->User_Password = Hash::make($passwordRan);
		$user->save();

		$token = Crypt::encryptString($user->User_ID.':'.time().':'.$passwordRan);

		$data = array('User_Email'=>$data->email, 'pass'=>$passwordRan,'token'=>$token);
        
        // gửi mail thông báo
        
		dispatch(new SendMailJobs('Forgot', $data, 'New Password!', $user->User_ID));
		
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please check your email.')),$this->keyHash)), 200);
		
    }
    
    
    public function postRegister(Request $req){

	    include(app_path() . '/functions/xxtea.php');
	    
		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('email'=>'skipro982301@gmail.com', 'password'=>'123456', 'password_confirm'=>'123456' ,'ponser'=>'')),$this->keyHash)));exit;
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
// 		var_dump($data);
// 		exit;
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		
	   	if(!$data->email){
		   	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);
		}else{
			$email = $data->email;
			if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'wrong email format')),$this->keyHash)), 200);        
			}
			$checkEmail = User::where('User_Email', $email)->first();
			if($checkEmail){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Email exist')),$this->keyHash)), 200);
			}
		}
		
		if(!$data->password || !$data->password_confirm){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss password and password confirm')),$this->keyHash)), 200);
		}
		elseif($data->password != $data->password_confirm){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Password confirm incorrect')),$this->keyHash)), 200);
		}
		$password = $data->password;
		///////////////
		
        $userID_Parent = 924176;
		if(property_exists($data, 'ponser')){
			if($data->ponser){
				$userID_Parent = $data->ponser;
			} 
        }
        //Kiểm tra ID Parent đó có tồn tại không????
        $check_parent_ex = User::find($userID_Parent);
        if(!$check_parent_ex){
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Sponsor is not exist')),$this->keyHash)), 200);
        }
        $show_ID_Parent = User::where('User_ID', $userID_Parent)->value('User_ID');
        $get_Side_Active = User::where('User_ID', $userID_Parent)->value('User_Side_Active');
        //BiẾN tạm LƯU  ID Presenter
        if(property_exists($data, 'presenter') && $data->presenter){
	        $userID_Presenter = $data->presenter;
	        if($userID_Presenter){
	            //IF Có nhập người chỉ định
	            // $node = User::where('User_ID', $userID_Presenter)->value('user_Node_Active');
	            // $user_ID_Presenter = $userID_Presenter;
	            // $user_Presenter_Tree = User::where('User_ID', $userID_Presenter)->value('User_Tree');
	
	            $info_Presenter = User::find($userID_Presenter);
		        if(!$info_Presenter){
			        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Presenter is not exist')),$this->keyHash)), 200);
		        }
	
	            $node = $info_Presenter->User_Side_Active;
	
	            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($info_Presenter->User_ID, $info_Presenter->User_Side_Active);
	
	            $user_ID_Presenter = $prevNodeID;
	            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
	        }
	    }
        else{
            //Còn ko nhập thì lấy Paren mặc định và thêm vào vị trí trái bên cùng
            // $node = User::where('User_ID', $userID_Parent)->value('user_Node_Active');
            
            $node = $get_Side_Active;

            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($show_ID_Parent, $get_Side_Active);
            $user_ID_Presenter = $prevNodeID;
            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
        }

        $childrenNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->get();
        // dd($user_ID_Presenter,$childrenNode);

        if(count($childrenNode) > 2){
            //Nhánh đã đủ 2 chân rồi
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'This person has enough 2 children, please choose other one')),$this->keyHash)), 200);
        }else{
            if(count($childrenNode) == 0){
                //tức không nhập người chỉ địh hoặc có nhập mà người chỉ định chưa có chân nào,
                //cho nên sẽ vào nhánh trái và cuối cùng
                $user_side = $node;
            }
            else{
                //khi nào đucợ chạy vào đây
                // dk khi có nhập người chỉ định
                if($childrenNode[0]->User_IsRight == 1){
                    $user_side = 0;
                }
                else{
                    $user_side = 1;
                }
            }

        }
        
        //đăng ký chỉ định vào chân nào
        if(property_exists($data, 'node') && !is_null($data->node)){
	        $node_register = $data->node;
	        if($node_register){
		        $getChilNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->where('User_IsRight', $node_register)->first();
		        if($getChilNode){
			        if($node_register == 1){
				        $note_string = 'right';
			        }else{
				        $note_string = 'left';
			        }
					return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'This person have branch '.$note_string.', please choose other branch!')),$this->keyHash)), 200);
		        }
		        
	            $node = $node_register;
	
	            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($info_Presenter->User_ID, $node);
	
	            $user_ID_Presenter = $prevNodeID;
	            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
		        $user_side = $node_register;
	        }
		}
        $user_id = app('App\Http\Controllers\Auth\RegisterController')-> RandomIDUser();
        $user_tree = !$user_Presenter_Tree ? $userID_Presenter . "," . $user_id : $user_Presenter_Tree . ',' . $user_id;
        //lấy sun tree parent
        $sunTree = (User::where('User_ID', $show_ID_Parent)->value('User_SunTree')).','.$user_id;
        //Tạo token cho mail
        $token = Crypt::encryptString($email.':'.time());
        $level = 0;
        if( strpos($user_tree, '373468') !== false || strpos($user_tree, '731439') !== false){
	        $level = 5;
        }
        //Xử lý
        $Register = [
            'User_ID' => $user_id,
            // 'User_Name' => $request->UserName,
            'User_Email' => $email,
            'User_Parent' => $show_ID_Parent,
            'User_IsRight' => $user_side,
            'User_Tree' => $user_tree,
            'User_SunTree' => $sunTree,
            'User_EmailActive' => 0,
            'User_Password' => bcrypt($password),
            'User_RegisteredDatetime' => date('Y-m-d H:i:s'),
            'User_Level' => $level,
            'User_Status' => 1,
            'User_Token' => $token,

        ];

        $user = User::insert($Register);
        
        //dữ liệu gửi sang mailtemplate
        $data = array('User_ID' => $user_id, 'User_Name' => $email,'User_Email'=> $email, 'token'=>$token);
        //Job
        dispatch(new SendMailJobs('Active', $data, 'Active Account!', $user_id));
		
		////////////////
        if($user){
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Register complete. Please check email')),$this->keyHash)), 200);
        }
        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Register false. Please contact admin')),$this->keyHash)), 200);        
    }
    
    public function postChangePassword(Request $req){

		include(app_path() . '/functions/xxtea.php');
		
		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('password_old'=>'654321', 'password_new'=>'123456', 'password_new_confirm'=>'123456')),$this->keyHash)));exit;
	    $user = Auth::user();

		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		// print_r($req->data);
		// exit;
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		
	   	if(!isset($data->password_old)){
		   	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss old password')),$this->keyHash)), 200);
		}
		
		if(!isset($data->password_new)){
		   	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss new password')),$this->keyHash)), 200);
		}

		if(!isset($data->password_new_confirm)){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss new password confirm')),$this->keyHash)), 200);
		}
		
		if($data->password_new != $data->password_new_confirm){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'New password confirm incorect')),$this->keyHash)), 200);
		}


		if(Hash::check($data->password_old, $user->User_Password)){
			
			$password = Hash::make($data->password_new); 

			$update = User::where('User_ID', $user->User_ID)->update(['User_Password'=>$password]);
			if($update){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Update new password complete!')),$this->keyHash)), 200);  
			}else{
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Update new password faile!')),$this->keyHash)), 200); 
			}
			
		}else{
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Password is incorrect')),$this->keyHash)), 200);
		}
			
    }
    
    public function postAddMember(Request $req){
	    
	    include(app_path() . '/functions/xxtea.php');
	    $user = Auth::user();
// 		echo base64_encode(xxtea_encrypt(json_encode(array('email'=>'jgjgjgjgj')),$this->keyHash));exit;
		
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		
	   	if(!$data->email){
		   	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);
		}else{
			$email = $data->email;
			if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'wrong email format')),$this->keyHash)), 200);        
			}
			$checkEmail = User::where('User_Email', $email)->first();
			if($checkEmail){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Email exits')),$this->keyHash)), 200);
			}
		}
		
		if(!$data->password || !$data->password_confirm){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss password and password confirm')),$this->keyHash)), 200);
		}
		elseif($data->password != $data->password_confirm){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Password confirm incorect')),$this->keyHash)), 200);
		}
		$password = $data->password;
		///////////////
		
        $userID_Parent = $user->User_ID;
        
        //Kiểm tra ID Parent đó có tồn tại không????
        $check_parent_ex = User::find($userID_Parent);
        if(!$check_parent_ex){
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Ponser not exits')),$this->keyHash)), 200);
        }
        $show_ID_Parent = User::where('User_ID', $userID_Parent)->value('User_ID');
        $get_Side_Active = User::where('User_ID', $userID_Parent)->value('User_Side_Active');
        //BiẾN tạm LƯU  ID Presenter
        if(property_exists($data, 'presenter') && $data->presenter){
	        $userID_Presenter = $data->presenter;
	        if($userID_Presenter){
	            //IF Có nhập người chỉ định
	            // $node = User::where('User_ID', $userID_Presenter)->value('user_Node_Active');
	            // $user_ID_Presenter = $userID_Presenter;
	            // $user_Presenter_Tree = User::where('User_ID', $userID_Presenter)->value('User_Tree');
	
	            $info_Presenter = User::find($userID_Presenter);
		        if(!$info_Presenter){
			        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Presenter not exits')),$this->keyHash)), 200);
		        }
	
	            $node = $info_Presenter->User_Side_Active;
	
	            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($info_Presenter->User_ID, $info_Presenter->User_Side_Active);
	
	            $user_ID_Presenter = $prevNodeID;
	            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
	        }
	    }
        else{
            //Còn ko nhập thì lấy Paren mặc định và thêm vào vị trí trái bên cùng
            // $node = User::where('User_ID', $userID_Parent)->value('user_Node_Active');
            
            $node = $get_Side_Active;

            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($show_ID_Parent, $get_Side_Active);
            $user_ID_Presenter = $prevNodeID;
            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
        }

        $childrenNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->get();
        // dd($user_ID_Presenter,$childrenNode);

        if(count($childrenNode) > 2){
            //Nhánh đã đủ 2 chân rồi
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'This person has enough 2 children, please choose other one')),$this->keyHash)), 200);
        }else{
            if(count($childrenNode) == 0){
                //tức không nhập người chỉ địh hoặc có nhập mà người chỉ định chưa có chân nào,
                //cho nên sẽ vào nhánh trái và cuối cùng
                $user_side = $node;
            }
            else{
                //khi nào đucợ chạy vào đây
                // dk khi có nhập người chỉ định
                if($childrenNode[0]->User_IsRight == 1){
                    $user_side = 0;
                }
                else{
                    $user_side = 1;
                }
            }

        }
        
        //đăng ký chỉ định vào chân nào
        if(property_exists($data, 'node') && $data->node){
	        $node_register = $data->node;
	        if($node_register){
		        $getChilNode = User::select('User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID_Presenter,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID_Presenter, ',' ,User_ID) )")->where('User_IsRight', $node_register)->first();
		        if($getChilNode){
			        if($node_register == 1){
				        $note_string = 'right';
			        }else{
				        $note_string = 'left';
			        }
					return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'This person have branch '.$note_string.', please choose other branch!')),$this->keyHash)), 200);
		        }
		        
	            $node = $node_register;
	
	            $prevNodeID = app('App\Http\Controllers\Auth\RegisterController')->leftNode($info_Presenter->User_ID, $node);
	
	            $user_ID_Presenter = $prevNodeID;
	            $user_Presenter_Tree = User::where('User_ID', $prevNodeID)->value('User_Tree');
		        $user_side = $node_register;
	        }
		}
        $user_id = app('App\Http\Controllers\Auth\RegisterController')-> RandomIDUser();
        $user_tree = !$user_Presenter_Tree ? $userID_Presenter . "," . $user_id : $user_Presenter_Tree . ',' . $user_id;
        //lấy sun tree parent
        $sunTree = (User::where('User_ID', $show_ID_Parent)->value('User_SunTree')).','.$user_id;
        //Tạo token cho mail
        $token = Crypt::encryptString($email.':'.time());
        $level = 0;
        if( strpos($user_tree, '373468') !== false || strpos($user_tree, '731439') !== false){
	        $level = 5;
        }
        //Xử lý
        $Register = [
            'User_ID' => $user_id,
            // 'User_Name' => $request->UserName,
            'User_Email' => $email,
            'User_Parent' => $show_ID_Parent,
            'User_IsRight' => $user_side,
            'User_Tree' => $user_tree,
            'User_SunTree' => $sunTree,
            'User_EmailActive' => 0,
            'User_Password' => bcrypt($password),
            'User_RegisteredDatetime' => date('Y-m-d H:i:s'),
            'User_Level' => $level,
            'User_Status' => 1,
            'User_Token' => $token,

        ];

        $user = User::insert($Register);
        
        //dữ liệu gửi sang mailtemplate
        $data = array('User_ID' => $user_id, 'User_Name' => $email,'User_Email'=> $email, 'token'=>$token);
        //Job
        dispatch(new SendMailJobs('Active', $data, 'Active Account!', $user_id));
		        
        if($user){
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'data'=>$user_id, 'message'=>'Register complete. Please check email')),$this->keyHash)), 200);
        }
        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Register false. Please contact admin')),$this->keyHash)), 200);        
    }
    
    public function getLogout(){
	    include(app_path() . '/functions/xxtea.php');
	    if(Auth::check()) {

	       	$accessToken = Auth::user()->token();
	       	
	        DB::table('oauth_refresh_tokens')
	            ->where('access_token_id', $accessToken->id)
	            ->update([
	                'revoked' => true
	            ]);
			
	        $accessToken->revoke();
			
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Logout complete')),$this->keyHash)), 200);        
		   	
	    }else{
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Logout faile')),$this->keyHash)), 200);
	    }
    }
    
    public function getStrinhCode(){
		include(app_path() . '/functions/xxtea.php');
	    $user = Auth::user();

	    if($user->User_WalletAddress == ''){
		    $string = DB::table('string_code')->inRandomOrder()->limit(12)->get();
		    $aa = '';
		    $arrayExactly = array();
		    foreach($string as $v){
			   $aa .= $v->string_code_String.'.';
			   $arrayExactly[] = $v->string_code_String;
		    }
		    $aa = substr($aa ,0,-1);
		    
		    User::where('User_ID', $user->User_ID)->update(['User_StringCode'=>$aa]);
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'data'=>$arrayExactly)),$this->keyHash)), 200);
	    }
	    
	    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false)),$this->keyHash)), 200);
	       
    }
    
    public function getCreateWallet(Request $req){

	    include(app_path() . '/functions/xxtea.php');
	    $user = Auth::user();
// 		echo base64_encode(xxtea_encrypt(json_encode(array('stringCode'=>'these.deposit.reinvest.group.in.many.brought.we.however.below.understand.back')),$this->keyHash));exit;
		
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		
	   	if(!$data->stringCode){
		   	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss email')),$this->keyHash)), 200);
		}

	   

	    $config = [
		    'private_key_type' => OPENSSL_KEYTYPE_EC,
		    'curve_name' => 'secp256k1'
		];
		$res = openssl_pkey_new($config);
		if (!$res) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'ERROR: Fail to generate private key.')),$this->keyHash)), 200);
		}
		// Generate Private Key
		openssl_pkey_export($res, $priv_key);
		// Get The Public Key
		$key_detail = openssl_pkey_get_details($res);
		$pub_key = $key_detail["key"];
		$priv_pem = PEM::fromString($priv_key);
		// Convert to Elliptic Curve Private Key Format
		$ec_priv_key = ECPrivateKey::fromPEM($priv_pem);
		// Then convert it to ASN1 Structure
		$ec_priv_seq = $ec_priv_key->toASN1();
		// Private Key & Public Key in HEX
		$priv_key_hex = bin2hex($ec_priv_seq->at(1)->asOctetString()->string());
		$priv_key_len = strlen($priv_key_hex) / 2;
		$pub_key_hex = bin2hex($ec_priv_seq->at(3)->asTagged()->asExplicit()->asBitString()->string());
		$pub_key_len = strlen($pub_key_hex) / 2;
		// Derive the Ethereum Address from public key
		// Every EC public key will always start with 0x04,
		// we need to remove the leading 0x04 in order to hash it correctly
		$pub_key_hex_2 = substr($pub_key_hex, 2);
		$pub_key_len_2 = strlen($pub_key_hex_2) / 2;
		// Hash time

		$hash = Keccak::hash(hex2bin($pub_key_hex_2), 256);
		// Ethereum address has 20 bytes length. (40 hex characters long)
		// We only need the last 20 bytes as Ethereum address
		$wallet_address = '0x' . substr($hash, -40);
		$wallet_private_key = '0x' . $priv_key_hex;
		
		// cập nhật ví
		User::where('User_ID', $user->User_ID)->update(['User_PrivateKey'=>$wallet_private_key, 'User_WalletAddress'=>$wallet_address]);
		
	    $return = array('PrivateKey'=>$wallet_private_key,'Address'=>$wallet_address);
	    
	    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true,'data'=>$return)),$this->keyHash)), 200);
	       
    }
    
    public function RandonIDUser(){
	    
	    $id = rand(1000000000, 9999999999);
        //TẠO RA ID RANĐOM
        $user = User::where('User_ID', $id)->first();
        //KIỂM TRA ID RANDOM ĐÃ CÓ TRONG USER CHƯA
        if (!$user) {
            return $id;
        } else {
            return $this->RandonIDUser();
        }
    }
    
    function RandomString(){
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $length = 10;    
		return substr(str_shuffle($characters),1,$length);
	}
}