<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Resource\Account;
use Illuminate\Support\Facades\Session;

use Image;
use PragmaRX\Google2FA\Google2FA;

use App\Model\User;
use App\Model\Money;
use App\Model\Wallet;
use App\Model\Investment;
use App\Model\Profile;
use App\Model\GoogleAuth;

use DB;
use Mail;

use App\Jobs\SendTelegramJobs;

class MoneyController extends Controller{

	
	public $successStatus = 200;
	public $fee = 0.01;
	public $feeSwap = 0.05;
	public $channel = -329987966;
	public $keyHash	 = 'We_@Stake!123'; 
	
	
    public function PostWithdraw(Request $req){
	    include(app_path() . '/functions/xxtea.php');
		//return response(base64_encode(xxtea_encrypt(json_encode(array('coin'=>1, 'address'=>'123123', 'amount'=>0.1)),$this->keyHash)), 200);
	    $user = Auth::user();
	    
		//check spam
        $checkSpam = DB::table('string_token')->where('User', $user->User_ID)->where('Token', $data->CodeSpam)->first();
        
        if($checkSpam == null){
            //khoong toonf taij
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Misconduct!')),$this->keyHash)), 200);
        }
        else{
            DB::table('string_token')->where('User', $user->User_ID)->where('Token', $data->CodeSpam)->delete();
	        $RandomToken = Money::RandomToken($user->User_ID);
        }
        

	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response()->json(array('status'=>false, 'message'=>'Miss Data'), 200);
		}
	    
	    if($user->User_Level == 4 || $user->User_Level == 5){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Please Contact Admin!')),$this->keyHash)), 200);
		}
		
		if(!$data->address){
			return response()->json(array('status'=>false, 'message'=>'Please enter your address'), 200);
		}
	    // check balance
	    $balance = Money::getBalance($user->User_ID);
	    
	    $amountFee = $req->amount*$this->fee;
		$Rate = 0;
		if($data->coin == 2){
	    	$price = $this->coinbase()->getBuyPrice('ETH-USD');
			$Rate = $price->getAmount();
			$balanceCoin = $balance->ETH;
		}
/*
		if($req->coin == 3){
			$result = json_decode(file_get_contents('https://cryptoofyou.com/api/v1/prices/usd'));
			$Rate = $result->ADC;
			$balanceCoin = $balance->ADC;
		}
*/
		if($data->coin == 1){
	    	$price = $this->coinbase()->getBuyPrice('BTC-USD');
			$Rate = $price->getAmount();
			$balanceCoin = $balance->BTC;
	    }
/*
		if($req->coin == 4){
			$Rate = 0.1;
			$balanceCoin = $balance->USDA;
	    }
*/
		if($data->coin == 6){
	    	$price = $this->coinbase()->getBuyPrice('BCH-USD');
			$Rate = $price->getAmount();
			$balanceCoin = $balance->BCH;
	    }
		if($data->coin == 7){
	    	$price = $this->coinbase()->getBuyPrice('LTC-USD');
			$Rate = $price->getAmount();
			$balanceCoin = $balance->LTC;
	    }
		if($data->coin == 9){
			$trxPrice = json_decode(file_get_contents('https://api.binance.com/api/v1/ticker/price?symbol=TRXUSDT'));
			$Rate = $trxPrice->price;
			$balanceCoin = $balance->TRX;
	    }
	    if(($data->amount+$amountFee) > $balanceCoin){
		    return response()->json(array('status'=>false, 'message'=>'Your balance is not enough!'), 200);
	    }


		if($user->User_AuthStatus == 1){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please enter OTP')),$this->keyHash)), 200);
		}else{
			$otp = rand(10000000,99999999);
			//update OTP
			User::where('User_ID', $user->User_ID)->update(['User_OTP'=>$otp]);
			
			
			// gửi mail cho người chơi config
			$data = array('User_ID'=>$user->User_ID,'otp'=>$otp);
			Mail::send('Mail.Withdraw', $data, function($msg) use ($user){
	            $msg->from('do-not-reply@gpgtoken.org','GPG wallet');
	            $msg->to($user->User_Email)->subject('Confirm Withdraw');
	        });
	        
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please check OTP in your Email')),$this->keyHash)), 200);	
		}

    }
    
    public function postConfirmWithdraw(Request $req){
	    include(app_path() . '/functions/xxtea.php');
	    //return response(base64_encode(xxtea_encrypt(json_encode(array('coin'=>1, 'address'=>'123123', 'amount'=>0.1, 'otp'=>'306312')),$this->keyHash)), 200);
	    $user = User::find(Auth::user()->User_ID);
	    
	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
	    
// 		if(!$data->address){
// 			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please enter your address')),$this->keyHash)), 200);
// 		}
	    
		if(!$data->amount || $data->amount < 0){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Amount')),$this->keyHash)), 200);
		}
		if(!isset($data->coin) || !$data->coin){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Coin')),$this->keyHash)), 200);
		}
	    
		if(!$data->otp){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Authenticator Code')),$this->keyHash)), 200);
		}
	    
	    if($user->User_Level == 4 || $user->User_Level == 5){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Please Contact Admin!')),$this->keyHash)), 200);
		}
		// xac thuc KYC		
		$checkProfile = Profile::where('Profile_User', $user->User_ID)->first();

		if(!$checkProfile || $checkProfile->Profile_Status != 1){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Your Profile KYC Is Unverify!')),$this->keyHash)), 200);
		}
		
	    $checkAuth = GoogleAuth::where('google2fa_User', $user->User_ID)->first();
	    if($checkAuth){
			$google2fa = app('pragmarx.google2fa');
			$valid = $google2fa->verifyKey($checkAuth->google2fa_Secret, "$data->otp");
			
			if(!$valid){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'OTP is wrongs')),$this->keyHash)), 200);
			}
		}else{
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please Enable Authenticator')),$this->keyHash)), 200);
		}
        $arr_coin = [
			1 => [
				'symbol' => 'BTC',
				'address' => 'User_WalletBTC'
            ],
            2 => [
				'symbol' => 'ETH',
				'address' => 'User_WalletETH'
            ],
			5 => [
				'symbol' => 'USD',
				'address' => 'User_WalletUSDT'
			],
			8 => [
				'symbol' => 'ADA',
				'address' => 'User_WalletAddress'
			]
		];
		$amount = $data->amount;
		$coin = $data->coin;
        $address = $data->address;
//         $address = $user->User_WalletAddress;
		/*
$addressString = $arr_coin[$coin]['address'];
        $address = $user->$addressString;
        if(!$address){
	    	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please update your wallet in setting profile')),$this->keyHash)), 200);
        }
*/
		//Các loại rate
        $arr_Rate = [
            1 => 'BTC',
            2 => 'ETH',
            8 => 'ADA',
            5 => 'USD'
        ];
		$symbol = $arr_Rate[$coin];
	    // check balance
		$balance = $user->User_BalanceDeposit;
		//phí rút 2%
		$fee = $amount * 0.02;
		if(($amount) > $balance){
	    	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Your Balance Isn\'t Enough')),$this->keyHash)), 200);
        }
	    $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
		$CurrentAmount = ($amount-$fee)/$rate;
	    // đặt lệnh rút
		$money = new Money();
		$money->Money_User = $user->User_ID;
		$money->Money_USDT = -($amount-$fee);
		$money->Money_USDTFee = $fee;
		$money->Money_Time = time();
		$money->Money_MoneyAction = 2;
		$money->Money_MoneyStatus = 1;
		$money->Money_Comment = 'Withdraw to Address '.$address;
		$money->Money_Address = $address;
		$money->Money_CurrentAmount = $CurrentAmount;
		$money->Money_Currency = $coin;
		$money->Money_Rate = $rate;
		$money->save();
		//update balance
		$updateBalanceInterest = User::updateBalanceDeposit($user->User_ID, -($amount));
				
		//Chưa làm
		$message = "<b> $symbol WITHDRAW </b>\n"
				. "ID: <b>$user->User_ID</b>\n"
				. "EMAIL: <b>$user->User_Email</b>\n"
				. "WALLET: <b>$address</b>\n"
				. "RATE: <b>$ $rate</b>\n"
				. "COIN AMOUNT: <b>$CurrentAmount</b>\n"
				. "USD AMOUNT: <b>$ ".($rate*$CurrentAmount)."</b>\n"
				. "<b>Submit Withdraw Time: </b>\n"
				. date('d-m-Y H:i:s',time());
				
// 		dispatch(new SendTelegramJobs($message, $this->channel));
		
	    $newBalance = User::where('User_ID', $user->User_ID)->value('User_BalanceDeposit');

		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Withdraw success!', 'balance'=>$newBalance)),$this->keyHash)), 200);
    }
    
    public function PostTransfer(Request $req){ 
	    include(app_path() . '/functions/xxtea.php');
// 		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please comeback later!')),$this->keyHash)), 200);
	    $user = Auth::user();
	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);

		}
	    
	    if($user->User_Level == 4){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Please Contact Admin!')),$this->keyHash)), 200);
		}
		
		if($user->User_Email == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please update your email in profile')),$this->keyHash)), 200);
		}
	    $userGiveMoney = User::getInfo($data->member);
	    if(!$userGiveMoney){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'User is not exits')),$this->keyHash)), 200);
	    }
	    
	    $checkUser = $this->checkTreeAPI($userGiveMoney->User_ID);
	    if($checkUser === false){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Contact Admin')),$this->keyHash)), 200);
	    }
	    
	    
		$ArrCoin = array(1=>'BTC',2=>'ETH',5=>'USD',6=>'BCH',7=>'LTC',8=>'GPG', 9=>'TRX');
	    // check balance
	    $balance = Money::getBalance($user->User_ID); 
	    $fee = 0;
	    $coin = $ArrCoin[$data->coin];
	    $amountFee = $data->amount*$fee;
	    if(($data->amount+$amountFee) > $balance->$coin){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Your balance is not enough!')),$this->keyHash)), 200);
	    }
	    
		if($user->User_AuthStatus == 1){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please enter OTP')),$this->keyHash)), 200);
		}else{
			$otp = rand(10000000,99999999);
			//update OTP
			User::where('User_ID', $user->User_ID)->update(['User_OTP'=>$otp]);
			// gửi mail cho người chơi config
			$data = array('User_ID'=>$user->User_ID,'otp'=>$otp);
			Mail::send('Mail.Transfer', $data, function($msg) use ($user){
	            $msg->from('do-not-reply@gpgtoken.org','GPG wallet');
	            $msg->to($user->User_Email)->subject('Confirm Transfer');
	        });
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please check OTP in your Email')),$this->keyHash)), 200);
		}
		
		
	    
    }
    
    public function postConfirmTransfer(Request $req){
	    include(app_path() . '/functions/xxtea.php');
// 	    return response(base64_encode(xxtea_encrypt(json_encode(array('coin'=>1, 'member'=>888888, 'amount'=>0.1, 'otp'=>448674)),$this->keyHash)), 200);
	    $user = Auth::user();
	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		if(!$data->member){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss User ID Give Coin')),$this->keyHash)), 200);
		}
		if(!$data->amount || $data->amount <= 0){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Amount')),$this->keyHash)), 200);
		}
	    
	    if($user->User_Level == 4){
// 			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Please Contact Admin!')),$this->keyHash)), 200);
		}
	    
	    $checkAuth = GoogleAuth::where('google2fa_User', $user->User_ID)->first();
	    if($checkAuth){
			$google2fa = app('pragmarx.google2fa');
			$valid = $google2fa->verifyKey($checkAuth->google2fa_Secret, "$data->otp");
			
			if(!$valid){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'OTP is wrongs')),$this->keyHash)), 200);
			}
		}else{
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please Enable Authenticator')),$this->keyHash)), 200);
		}
		
	    $userGiveMoney = User::getInfo($data->member);
	    if(!$userGiveMoney){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'User is not exits')),$this->keyHash)), 200);
	    }
	    //check user nằm trong hệ thống bị chặn hay ko 
	    
	    /*
$checkUser = $this->checkTreeAPI($userGiveMoney->User_ID);
	    if($checkUser === false){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Contact Admin')),$this->keyHash)), 200);
	    }
*/
	    $coin = 8;
	    $amount = $data->amount;
		//Các loại rate
        $arr_Rate = [
            1 => 'BTC',
            2 => 'ETH',
            8 => 'ADA',
            5 => 'USD'
        ];
	    // check balance
	    $symbol = $arr_Rate[$coin];
	    // check balance
		$balance = $user->User_BalanceDeposit;
		//phí rút 1%
		$fee = $amount * 0;
		if(($amount) > $balance){
	    	return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Error! Your Balance Isn\'t Enough')),$this->keyHash)), 200);
        }
	    
	    $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy($symbol);
	    // trừ tiền người chuyển
	    $money = new Money();
		$money->Money_User = $user->User_ID;
		$money->Money_USDT = -($amount-$fee);
		$money->Money_USDTFee = $fee;
		$money->Money_Time = time();
		$money->Money_Comment = 'Transfer to ID:'.$userGiveMoney->User_ID;
		$money->Money_MoneyAction = 4; 
		$money->Money_MoneyStatus = 1;
		$money->Money_Currency = $coin; 
		$money->Money_Rate = $rate; 
		$money->save();
		//update balance
		$updateBalanceInterest = User::updateBalanceDeposit($user->User_ID, -($amount-$fee));
	    
	    // cộng tiền cho người nhận
		$money = new Money();
		$money->Money_User = $userGiveMoney->User_ID;
		$money->Money_USDT = ($amount-$fee);
		$money->Money_USDTFee = 0;
		$money->Money_Time = time();
		$money->Money_Comment = 'Give from ID:'.$user->User_ID;
		$money->Money_MoneyAction = 4; 
		$money->Money_MoneyStatus = 1;
		$money->Money_Currency = $coin; 
		$money->Money_Rate = $rate; 
		$money->save();
		//update balance
		$updateBalanceInterest = User::updateBalanceDeposit($userGiveMoney->User_ID, ($amount-$fee));
		
	    $newBalance = User::where('User_ID', $user->User_ID)->value('User_BalanceDeposit');
	    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'messages'=>'Transfer Success', 'balance'=>$newBalance)),$this->keyHash)), 200);
    
    }

    public function postSwap(Request $req){
	    include(app_path() . '/functions/xxtea.php');
	    $user = Auth::user();
	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'messages'=>'Miss Data')),$this->keyHash)), 200);
		}
	    $coin = $data->coin;
        if($coin == 1){
            $price = $this->coinbase()->getBuyPrice('BTC-USD');
            $rate = $price->getAmount();
		}elseif($coin == 2){
            $price = $this->coinbase()->getBuyPrice('ETH-USD');
            $rate = $price->getAmount();
		}elseif($coin == 9){
			$trxPrice = json_decode(file_get_contents('https://api.binance.com/api/v1/ticker/price?symbol=TRXUSDT'));
			$rate = $trxPrice->price;
		}
		// GPG
	    /*
$rateGPG = DB::table('changes')->where('Changes_Time', date('Y-m-d'))->first()->Changes_Price;
	    
		$priceGPG = $req->amount*$rateGPG / $rate;
		$fee = $priceGPG * 0.02;
		$balance = $priceGPG + $fee;
*/
		// USD
		$rateGPG = 1;
	    
		$priceGPG = $data->amount*$rateGPG / $rate;
		$fee = $priceGPG * 0.02;
		$balance = $priceGPG + $fee;
		$arrayReturn = array('AmountUSD'=>$data->amount, 'CoinNeed'=>$balance);
		
		
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'data'=>$arrayReturn)),$this->keyHash)), 200);
		
    }
    
    public function postConfirmSwap(Request $req){
	    include(app_path() . '/functions/xxtea.php');
// 	    echo base64_encode(xxtea_encrypt(json_encode(array('coin_from'=>8,'coin_to'=>1, 'amount'=>1000)),$this->keyHash));exit;
	    
	    $user = User::find(Auth::user()->User_ID);
	    
	    $data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		if(!$data->amount || $data->amount < 0){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Amount')),$this->keyHash)), 200);
		}
		if(!$data->coin_from){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Coin')),$this->keyHash)), 200);
		}
	    
		if(!$data->otp){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Authenticator Code')),$this->keyHash)), 200);
		}
		
        $arr_from_wallet = [
            1 => 'BTC',
            2 => 'ETH',
            5 => 'USD',
            8 => 'ADA'
        ];
	    $checkAuth = GoogleAuth::where('google2fa_User', $user->User_ID)->first();
	    if($checkAuth){
			$google2fa = app('pragmarx.google2fa');
			$valid = $google2fa->verifyKey($checkAuth->google2fa_Secret, "$data->otp");
			if(!$valid){
				return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'OTP is wrongs')),$this->keyHash)), 200);
			}
		}else{
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Please Enable Authenticator')),$this->keyHash)), 200);
		}
        $coin_from = $data->coin_from;
//         $coin_to = $req->coin_to;
        //chỉ đc 1 chiều từ coin->ADA
        $coin_to = 8;
        $amount_from = $data->amount;
        $rate = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy();
        if(!isset($arr_from_wallet[$coin_from]) || $coin_from == 8){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Currency swap is wrong!')),$this->keyHash)), 200);
        }
        //Balance
        $symbolFrom = $arr_from_wallet[$coin_from];
        $symbolTo = $arr_from_wallet[$coin_to];
        $balanceString = 'User_Balance'.$symbolFrom;
		$balance = $user->$balanceString;
        if($amount_from > $balance){
		    return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Your balance is not enough!')),$this->keyHash)), 200);
        }
        //trừ phí
		$fee = $this->feeSwap;
        $amount_to = $amount_from*$rate[$symbolFrom]/$rate[$symbolTo];
		$amountFee = $amount_to*$fee;
        $moneyArrayFrom = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amount_from,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Swap Coin From '.$symbolFrom.' To '.$symbolTo,
			'Money_MoneyAction' => 12,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rate[$symbolFrom],
            'Money_CurrentAmount' => $amount_from,
			'Money_Currency' => $coin_from
        );
        $moneyArrayTo = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => $amount_to,
		    'Money_USDTFee' =>$amountFee,
		    'Money_Time' => time(),
			'Money_Comment' => 'Swap Coin From '.$symbolFrom,
			'Money_MoneyAction' => 12,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rate[$symbolTo],
            'Money_CurrentAmount' => $amount_to - $amountFee,
			'Money_Currency' => $coin_to
        );
        DB::table('money')->insert($moneyArrayFrom);
        DB::table('money')->insert($moneyArrayTo);
        //update balance Coin
        $updateCoin = User::updateBalanceCoin($user->User_ID, $symbolFrom, -$amount_from);
        $updateADA = User::updateBalanceDeposit($user->User_ID, $amount_to - $amountFee);
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>"Swap From $symbolFrom To $symbolTo Success!")),$this->keyHash)), 200);
        
    }
    
    public static function checkTreeAPI($idCheck = null){
	    $idRoot = 815224391604;
        $user = Auth::user();
        $arrParent = explode(',', $user->User_Tree);
        $arrParent = array_reverse($arrParent);
	    if(array_search($idRoot, $arrParent) !== false){
			$userCheck = User::find($idCheck);
	        $arrParent = explode(',', $userCheck->User_Tree);
	        $arrParent = array_reverse($arrParent);
		    if(array_search($idRoot, $arrParent) !== false){
			    return true;
		    }
	        return false;
	    }
	    return true;
    }
    
}