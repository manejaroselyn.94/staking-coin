<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use DB;

class TicketController extends Controller{


	public $keyHash	 = 'We_@Stake!123';


    public function getTicket(Request $req){
	    include(app_path() . '/functions/xxtea.php');
        $user = Auth::user();
        $tickets = DB::table('ticket')->join('ticket_subject', 'ticket_subject_id', 'ticket_Subject')
            ->where('ticket_User', $user->User_ID)
            ->where('ticket_ReplyID', 0)
            ->orderByDesc('ticket_ID')->get();
        $subject = DB::table('ticket_subject')->get();
        $data = [];
        $dataSubject = [];
        foreach($tickets as $ticket){
	        $data[] = [
	        			'id'=>$ticket->ticket_ID,
				        'email'=>$user->User_Email,
				        'subject'=>$ticket->ticket_subject_name,
				        'content'=>$ticket->ticket_Content,
				        'date'=>$ticket->ticket_Time,
			            'count' => (DB::table('ticket')->where('ticket_ReplyID', $ticket->ticket_ID)->count())+1
					];
        }
        foreach($subject as $s){
	        $dataSubject[] = ['id'=>$s->ticket_subject_id, 'name'=>$s->ticket_subject_name];
        }
		return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'subject'=>$dataSubject, 'data'=>$data)),$this->keyHash)), 200);
    }

    public function postTicket(Request $req)
    {
	    include(app_path() . '/functions/xxtea.php');
        $user = Auth::user();
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if(!$req->data || $data == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Data')),$this->keyHash)), 200);
		}
		if(!isset($data->content) || $data->content == ''){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Content')),$this->keyHash)), 200);
		}
		
        
        $replyID = 0;
        if (isset($data->replyID) && $data->replyID != 0){
            $replyID = $data->replyID;
            $subject = DB::table('ticket')->where('ticket_ID', $replyID)->select('ticket_Subject')->first();
            $subjectID = $subject->ticket_Subject;
        }else{
	        if ($data->subject) {
	            $subjectID = $data->subject;
	        }else{
		        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>false, 'message'=>'Miss Subject')),$this->keyHash)), 200);
	        }
        }

        $addArray = array(
            'ticket_User' => $user->User_ID,
            'ticket_Time' => date('Y-m-d H:i:s'),
            'ticket_Subject' => $subjectID,
            'ticket_Content' => $data->content,
            'ticket_Status' => 0,
            'ticket_ReplyID' => $replyID
        );

        $data = DB::table('ticket')->insert([$addArray]);

        $id = DB::getPdo()->lastInsertId();

        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'message'=>'Please waiting support reply', 'id'=>$id)),$this->keyHash)), 200);
    }
    
    public function getTicketDetail($id){
	    include(app_path() . '/functions/xxtea.php');
        $tickets = DB::table('ticket')->join('users', 'User_ID', 'ticket_User')
			        ->join('ticket_subject', 'ticket_subject_id', 'ticket_Subject')
			        ->where('ticket_ID', $id)
			        ->orWhere('ticket_ReplyID', $id)
			        ->orderBy('ticket_ID')
			        ->get();
        $data = [];
        foreach($tickets as $ticket){
			$data[] = [
			'id'=>$ticket->ticket_ID,
	        'email'=>($ticket->User_Level != 0 ? 'Supporter' : $ticket->User_Email),
	        'subject'=>$ticket->ticket_subject_name,
	        'content'=>$ticket->ticket_Content,
			'date'=>$ticket->ticket_Time
	        ];
        }
        
        return response(base64_encode(xxtea_encrypt(json_encode(array('status'=>true, 'data'=>$data)),$this->keyHash)), 200);
    }
}
