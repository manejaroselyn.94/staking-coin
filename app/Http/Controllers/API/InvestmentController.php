<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Resource\Account;
use Illuminate\Support\Facades\Session;

use App\Model\User;
use App\Model\Money;
use App\Model\Wallet;
use App\Model\Investment;
use DB;
use Mail;
use App\Http\Controllers\System\CoinbaseController;

class InvestmentController extends Controller
{


	public $successStatus = 200;
	public $fee = 0.003;
	public $keyHash	 = 'We_@Stake!123';

	public function postRefundInvest(Request $req)
	{
		include(app_path() . '/functions/xxtea.php');

		$data = json_decode(xxtea_decrypt(base64_decode($req->data), $this->keyHash));

		if (!$req->data || $data == '') {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Data')), $this->keyHash)), 200);
		}

		if (!$data->id) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Amount USD')), $this->keyHash)), 200);
		}

		if (!$data->type) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Coin')), $this->keyHash)), 200);
		}
		$user = Auth::user();

		//refund
		$refund = Investment::where('investment_ID', $data->id)->where('investment_User', $user->User_ID)->where('investment_Status', 0)->first();
		//         var_dump($data, $refund);exit;
		if (!$refund) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Error! Investment ID Not Found Or Confirmed!')), $this->keyHash)), 200);
		}
		// REINVEST
		if ($data->type == 2) {
			$refund->investment_Status = 1;
			$refund->investment_ReInvest = 1;
			$refund->investment_TimeOld = $refund->investment_Time;
			$refund->investment_Time = time();
			$refund->save();
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'ReInvest complete')), $this->keyHash)), 200);
		}
		//REFUND INVESTMENT
		//cập nhật status
		$refund->investment_Status = 2;
		$refund->save();
		//Package Time
		$fee_refund = DB::table('package_time')->where('time_Month', $refund->investment_Package_Time)->value('time_Fee');
		if (!$fee_refund) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Error! Please Contact Admin!')), $this->keyHash)), 200);
		}
		//Rate
		$rateSOX = CoinbaseController::coinRateBuy('SOX');
		//Tiến hành rút gốc
		//Cộng tiền
		$moneyArray = array(
			'Money_User' => $refund->investment_User,
			'Money_USDT' => $refund->investment_Amount,
			'Money_USDTFee' => ($refund->investment_Amount * $fee_refund),
			'Money_Time' => time(),
			'Money_Comment' => 'Refund Investment ' . $refund->investment_Amount . ' SOX ' . $refund->investment_Package_Time . ' Months',
			'Money_MoneyAction' => 8,
			'Money_MoneyStatus' => 1,
			'Money_Rate' => $rateSOX,
			'Money_CurrentAmount' => $refund->investment_Amount,
			'Money_Currency' => $refund->investment_Currency
		);
		DB::table('money')->insert($moneyArray);

		$balance = Money::getBalance($user->User_ID);
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'Refund Investment complete', 'balance' => $balance)), $this->keyHash)), 200);
	}

	public function getPackage(Request $req)
	{
		include(app_path() . '/functions/xxtea.php');

		$user = Auth::user();
		$RandomToken = Money::RandomTokenAPI($user->User_ID);
		$package = DB::table('investment')
			->join('currency', 'Currency_ID','investment_Currency')
			->where('investment_User', $user->User_ID)
			->orderBy('investment_ID', 'DESC')
			->get();
		
		$arrayReturn = array();
		$totalPresent = 0;
		foreach ($package as $v) {
			$amount_USD = $v->investment_Amount;
			if ($v->investment_Status == 1) {
				$totalPresent += $amount_USD;
			}
			$pecent = 0;
			$arrayReturn[] = array(
				'id' => $v->investment_ID,
				'amount' => $v->investment_Amount+0,
				'symbol' => $v->Currency_Symbol,
				'rate' => $v->investment_Rate + 0,
				'time' => date('Y-m-d', $v->investment_Time),
				'status' => $v->investment_Status,
			);
		}
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'data' => $arrayReturn, 'TotalInvest' => $totalPresent)), $this->keyHash)), 200);
	}

	public function postInvestment(Request $req)
	{

		include(app_path() . '/functions/xxtea.php');

		// echo urlencode(base64_encode(xxtea_encrypt(json_encode(array('amount' => 200, 'coin'=>5,'month'=>9)), $this->keyHash)));

		// exit;

		$user = Auth::user();

        if($user->User_Level == 5){
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'You can not investment!')), $this->keyHash)), 200);
        }
        
		$data = json_decode(xxtea_decrypt(base64_decode($req->data), $this->keyHash));

		if (!$req->data || $data == '') {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Data')), $this->keyHash)), 200);
		}

		if (!isset($data->amount) || $data->amount <= 0) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Amount')), $this->keyHash)), 200);
		}
		//Min đầu tư
        if($data->amount < 500){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Min invest $500!')), $this->keyHash)), 200);
        }
		/*
if (!isset($data->coin)) {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Coin')), $this->keyHash)), 200);
		}
*/

		//RATE ADA
        $rateToken = app('App\Http\Controllers\System\CoinbaseController')->coinRateBuy('ADA');
        $rateUSD = 1;
        //Balance
		$balance = User::where('User_ID', $user->User_ID)->first();
		$amount = $data->amount;
        if($amount > $balance->User_BalanceDeposit){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Your balance is not enough!')), $this->keyHash)), 200);
		}
		
        //Bonus
        $get_amount_investment = Investment::where('investment_User', $user->User_ID)
                            ->where('investment_Status', 1)
                            ->sum('investment_Amount');
        $sum_investment = $get_amount_investment + $amount;
        //khởi tạo
        $amount_bonus = 0;
        if($sum_investment >= 5100){
            //Lấy tổng tiền đã bonus
            $sum_bonus = Money::where('Money_User', $user->User_ID)
                        ->where('Money_MoneyAction', 19)
                        ->where('Money_MoneyStatus', 1)
                        ->sum('Money_USDT');
                        
	        if($sum_investment < 20100){
	            $amount_bonus = $sum_investment - $sum_bonus;
	        }
	        elseif($sum_investment >= 20100){
	            // tổng đầu tư x2
	            $sum_investment = $sum_investment * 2;
	            $amount_bonus = $sum_investment - $sum_bonus;
	        }
	        $insertPoint = [];
	        if($amount_bonus > 0){
	            //chưa bonus thì sẽ bonus
	            $insertPoint = array(
	                'Money_User' => $user->User_ID,
	                'Money_USDT' => $amount_bonus,
	                'Money_USDTFee' => 0,
	                'Money_Time' => time(),
	                'Money_Comment' => "Bonus Point Investment ".($amount_bonus+0)." ADA",
	                'Money_MoneyAction' => 19,
	                'Money_MoneyStatus' => 1,
	                'Money_Rate' => $rateToken,
	                'Money_CurrentAmount' => $amount_bonus,
	                'Money_Currency' => 8
	            );
	        }
        }
        //Trừ tiền 
	    $moneyArray = array(
		    'Money_User' => $user->User_ID,
		    'Money_USDT' => -$amount,
		    'Money_USDTFee' => 0,
		    'Money_Time' => time(),
			'Money_Comment' => 'Investment '.$amount.' USDT',
			'Money_MoneyAction' => 3,
			'Money_MoneyStatus' => 1,
            'Money_Rate' => $rateUSD,
            'Money_CurrentAmount' => $amount,
			'Money_Currency' => 5
        );
        //Invest
        $invest = array(
		    'investment_User' => $user->User_ID,
            'investment_Amount' => $amount,
		    'investment_Rate' => $rateUSD,
		    'investment_Currency' => 5,
		    'investment_Time' => time(),
            'investment_Status' => 1,
            'investment_Bonus' => $amount_bonus,
            'investment_Total' => $sum_investment
        );
        //Cộng điểm

	    // thêm dữ liệu
	    DB::table('investment')->insert($invest);
        DB::table('money')->insert($moneyArray);
        if($amount_bonus > 0){
            //điều kiện tặng coin
            DB::table('money')->insert($insertPoint);
        }
        //Update Balance
        $updateBalance = User::updateBalanceDeposit($user->User_ID, -$amount);
        //Update updatePoint
        $updatePoint = User::updatePoint($user->User_ID, $amount_bonus);
        //Update Investment
        $updateInvestment = User::updateInvestment($user->User_ID, $amount);
        //Update Sales Tree
        $updateBranchSales = app('App\Http\Controllers\System\InvestmentController')->updateBranchSalesTree($user->User_ID);
        //checkDirectCom
        app('App\Http\Controllers\Cron\CronController')->checkCommission($user, $amount);
        //checkBinaryCom
        app('App\Http\Controllers\Cron\CronController')->checkBinaryCommission($user, $amount);
        //update level
        app('App\Http\Controllers\Cron\CronController')->UpdateLevel($user);
        
		$balance = User::find($user->User_ID);
		$arrBalance = ['USDT'=>$balance->User_BalanceDeposit+0, 'ADA' => $balance->User_PoitnStock+0];
		
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'Invest $'.$amount.' Success!', 'balance'=>$arrBalance)), $this->keyHash)), 200);
	}

	public function postChooseInterest(Request $req){
		include(app_path() . '/functions/xxtea.php');
	    $user = Auth::user();
		$data = json_decode(xxtea_decrypt(base64_decode($req->data),$this->keyHash));
		if (!$req->data || $data == '') {
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Miss Data')), $this->keyHash)), 200);
		}
		if(!isset($data->date_compound)){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Please select interest!')), $this->keyHash)), 200);
		}
        //check đã đầu tư hay chưa
		$check_invest = User::where('User_ID', $user->User_ID)->value('User_TotalInvestment');
        if(!$check_invest){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'You have not invested')), $this->keyHash)), 200);
        }
        //check dup
        $check_dup = DB::table('interest_day')->where('interest_User', $user->User_ID)->where('interest_Status', 1)->first();
        if($check_dup){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'You cannot change')), $this->keyHash)), 200);
        }
        $comment = '';
        $text = '';
        $num = 0;
        if($data->date_compound == '1w'){
            $text = '1 Week';
            $num = 7;
        }
        elseif($data->date_compound == '1m'){

            $text = '1 Month';
            $num = 30;
        }elseif($data->date_compound == '3m'){
            $num = 90;
            $text = '3 Months';
        }
        elseif($data->date_compound == '6m'){
            $num = 180;
            $text = '6 Months';
        }
        elseif($data->date_compound == '12m'){
            $num = 360;
            $text = '12 Months';
        }else{
	        return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Time Active Compound Interest Error!')), $this->keyHash)), 200);
        }
        if($data->date_compound != 0){
            //Save
            DB::table('interest_day')->insert([
                'interest_User' => $user->User_ID,
                'interest_ReviceDay' => $text,
                'interest_Number' => $num,
                'interest_EDAY' => Date('Y-m-d H:i:s', strtotime('+'.$text)),
            ]);
		}
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'Choose to receive compound interest successfully')), $this->keyHash)), 200);
        
	}
	public function postCancelInvestment(Request $req){

		$auth = Auth::user();

        $user = User::where('User_ID', $auth->User_ID)->where('User_Status', 1)->first();
        if(!$user || $user->User_Level == 4){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Error User')), $this->keyHash)), 200);
        }
        
        //Lấy ngày đầu đầu tư
        $date_investment = Investment::where('investment_User', $user->User_ID)->where('investment_Status', 1)->orderBy('investment_ID', 'ASC')->first();
        
        if(!$date_investment){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'Error Cancel Investment')), $this->keyHash)), 200);
        }
        $now = time();
        //check gói đầu tư đã đủ 3 tháng chưa
        
        if($now < strtotime('+3 month', $date_investment->investment_Time)){

			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'You only cancel package after 3 months')), $this->keyHash)), 200);
        }
        //tính số tháng của gói đầu tư (thời gian hiện tại - thời gian đầu tư / cho 1 tháng)
        $number_month_cancel = (int)(($now - $date_investment->investment_Time) / 2629800);
        //lãi và hoa hồng không vượt quá tổng đầu tư
        $totalCom = $user->User_BalanceCommission + $user->User_BalanceInterest;
        $totalInvest = $user->User_TotalInvestment + $user->User_DoubleProfit;
        if($totalCom >= ($totalInvest * 0.7)){
			return response(base64_encode(xxtea_encrypt(json_encode(array('status' => false, 'message' => 'You are not eligible to cancel the investment package')), $this->keyHash)), 200);
        }
        //Cancel All Investment
        $update_cancel = DB::table('investment')->where('investment_User', $user->User_ID)->where('investment_Status', 1)->update([
            'investment_Status' => -1
        ]);
        //Cancel Nhận lãi kép
        $update = DB::table('interest_day')->where('interest_User', $user->User_ID)->update([
            'interest_Status' => -1
        ]);
        //tuần nhận được tiền từ đầu tư
        if($number_month_cancel >= 3 && $number_month_cancel < 6){
            //phí hủy 10%
            $feeCancel = 0.1;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 5;
            //số tièn nhận được mỗi tuần (5 tuần)
            $result_amount_cancel = $amount_cancel / $num_week;
            
        }elseif($number_month_cancel >= 6 && $number_month_cancel < 12){
            //phí hủy 5%
            $feeCancel = 0.05;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 2;
            //số tièn nhận được mỗi tuần (2 tuần)
            $result_amount_cancel = $amount_cancel / $num_week;
        }elseif($number_month_cancel == 12){
            //phí hủy 0%
            $feeCancel = 0;
            //trừ giảm phí
            $amount_cancel = $totalInvest - ($totalInvest * $feeCancel);
            //Số tuần nnhận
            $num_week = 1;
            //số tièn nhận được
            $result_amount_cancel = $amount_cancel / $num_week;
        }
        if($number_month_cancel < 12){
            for ($i=0; $i < $num_week ; $i++) {
                $insert_cancel = DB::table('cancel_investment')->insert([
                    'cancel_User' => $user->User_ID,
                    'cancel_Amount' => $result_amount_cancel,
                    'cancel_TotalInvest' => $totalInvest,
                    'cancel_ReceivedDay' => Date('Y-m-d 00:00:00', strtotime("+".($i + 1)." week")),
                    'cancel_TimeCancel' => Date('Y-m-d H:i:s'),
                    'cancel_Status' => 1,
                    'cancel_Log' => "$number_month_cancel Months - $num_week week - Fee $feeCancel - Total Invest $totalInvest",
                ]);
            }
        } else {
            $insert_cancel = DB::table('cancel_investment')->insert([
                'cancel_User' => $user->User_ID,
                'cancel_Amount' => $totalInvest,
                'cancel_TotalInvest' => $totalInvest,
                'cancel_ReceivedDay' => Date('Y-m-d 00:00:00', strtotime("+1 day")),
                'cancel_TimeCancel' => Date('Y-m-d H:i:s'),
                'cancel_Status' => 1,
                'cancel_Log' => "$number_month_cancel Months - 24h - Fee $feeCancel - Total Invest $totalInvest",
            ]);
        }
        //Update updateInvestment(-)
        $updateInvestment = User::updateInvestment($user->User_ID, -$user->User_TotalInvestment);
        //Update updateDoubleProfit(-)
        $updateDoubleProfit = User::updateDoubleProfit($user->User_ID, -$user->User_DoubleProfit);
        //updateBranchSales
		$updateBranchSales = $this->updateBranchSalesTree($user->User_ID);
		
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'Cancel Success')), $this->keyHash)), 200);
	}
	public function getHistoryInvestment(){
		$user = Auth::user();
		$history_invest = Investment::join('currency', 'Currency_ID' ,'investment_Currency')
							->where('investment_User', $user->User_ID )
							->where('investment_Status','<>', -1)
							->orderBy('investment_ID', 'DESC')
							->get();
		
		return response(base64_encode(xxtea_encrypt(json_encode(array('status' => true, 'message' => 'Investment complete', 'data' => $history_invest)), $this->keyHash)), 200);
	}
}
