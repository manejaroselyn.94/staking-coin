<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Model\User;
use App\Model\Log;
use DB;

use Mail;

class SendMailJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
 
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $template;
    public $data;
    public $tittle;
    public $userID;
    public $file;

    public function __construct($template, $data = [], $tittle, $userID, $file = null)
    {
        
        $data['url_web'] = 'https://stakingcoin.co/';
        
        $this->template = $template;
        $this->data = $data;
        $this->tittle = $tittle;
        $this->userID = $userID;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $template = $this->template;
        $data = $this->data;
        $tittle = $this->tittle;
        $userID = $this->userID;
        $file = $this->file;


        //prosess
        $templateMail = 'Mail.'.$template;
        Mail::send($templateMail, $data, function($msg) use ($data, $tittle, $file){
            $msg->to($data['User_Email'])->subject($tittle);
      
            $msg->from('no-reply@stakingcoin.co','Staking Coin');
            /*if($file){
	            $msg->attach($data['url_web'].$file);
            }*/
        });

        $amountUSD = 0;
        if(isset($data['amountUSD'])){
            $amountUSD = $data['amountUSD'];
            echo $amountUSD;

        }
        

        $log = Log::insertLog($userID, $tittle, $amountUSD, $tittle);
    }
}
