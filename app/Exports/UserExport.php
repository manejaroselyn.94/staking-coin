<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Model\User;
use DB;
class UserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $temp = '';
    use Exportable;
    public function __construct($query = null){
        $this->temp = $query->toArray();
        
    }
    public function collection()
    {
        $level = array(1 => 'Admin', 0 => 'Member', 2 => 'Finance', 3 => 'Support', 4 => 'Customer', 5 => 'BOT', 15 => 'TechCare');
        $user = $this->temp;
        $result = [];
        foreach ($user as $row) {
            if ($row['google2fa_User']) {
                $row['google2fa_User'] = "Enable";
            } else {
                $row['google2fa_User'] = "Disable";
            }
          	$addressUSDT = DB::table('address')->where('Address_User', $row['User_ID'])->where('Address_Currency', 5)->where('Address_IsUse', 0)->select('Address_Address')->value('Address_Address');
          	$addressSKC = DB::table('address')->where('Address_User', $row['User_ID'])->where('Address_Currency', 8)->where('Address_IsUse', 0)->select('Address_Address')->value('Address_Address');
            $result[] = array(
                '0' => $row['User_ID'],
                '1' => $row['User_Email'],
                '2' => $row['User_RegisteredDatetime'],
                '3' => $row['User_Parent'],
                '4' => $row['User_Tree'],
                '5' => $row['User_SunTree'],
                '6' => $level[$row['User_Level']],
                '7' => ($row['User_EmailActive'] ? 'Active' : 'None'),
                '8' => $row['google2fa_User'],
              	9 => $addressUSDT,
              	10 => $addressSKC,

            );
        }
        return (collect($result));

    }
    public function headings(): array
    {
        
        return [
            'ID', 'Email', 'Registred DateTime', 'ID Parent', 'Binary Tree', 'Sun Tree', 'Level', 'Status', 'Auth', 'Address USDT', 'Address SKC'
        ];
        
    }
}
