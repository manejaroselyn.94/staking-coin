<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Investment extends Model
{
    protected $table = "investment";

    protected $fillable = ['investment_ID','investment_User', 'investment_Amount','investment_Rate', 'investment_Hash', 'investment_Currency', 'investment_Time', 'investment_Status'];

    public $timestamps = false;

    protected $primaryKey = 'investment_ID';

    public static function totalInvest($userID, $currency){
        $totalInvest = Investment::join('currency', 'Currency_ID' ,'investment_Currency')
                                ->where('investment_User', $userID)
                                ->where('investment_Currency', $currency)
                                ->where('investment_Status','<>', -1)
                                ->orderBy('investment_ID', 'DESC')
                                ->get();
        return $totalInvest;
    }

    public static function checkInvestSKC($userID){
        $investment = Investment::where('investment_Status','<>', -1)
                                ->where('investment_User', $userID)
                                ->where('investment_Currency', 8)
                                ->orderBy('investment_Time')
                                ->first();
        return $investment;
    }
}
