<?php

namespace App\Model;

use Laravel\Passport\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'User_Name', 'User_Email', 'User_Password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'User_Password', 'User_Token', 'User_OTP', 'remember_token', 'User_Log'
    ];
    
    protected $primaryKey = 'User_ID';
    
    public $timestamps = false;
    
	public function Invest(){
		return $this->hasMany('App\Model\Investment', 'investment_User')->where('investment_Status', 1);
	}
	
	public function Level(){
		return $this->belongsTo('App\Model\UserLevel', 'User_Agency_Level');
	}
	
	public static function getPackageSKC($userID, $amountInvest = 0){
		$totalInvest = Investment::totalInvest($userID, 8);
		if(!$totalInvest){
			return false;
		}
		$totalInvest = $totalInvest->sum('investment_Amount');
		$totalInvest = $totalInvest + $amountInvest;
		$package = DB::table('package_skc')->whereRaw($totalInvest.' Between package_Min AND package_Max')->orderByDesc('package_ID')->first();
		if(!$package){
			return false;
		}else{
			return $package;
		}
	}
	
	public static function getPackage($userID, $amountInvest = 0){
		$totalInvest = User::where('User_ID', $userID)->value('User_TotalInvestment');
		if(!$totalInvest){
			return false;
		}
		
		$totalInvest = $totalInvest + $amountInvest;
		$package = DB::table('package')->whereRaw($totalInvest.' Between package_Min AND package_Max')->orderByDesc('package_ID')->first();
		if(!$package){
			return false;
		}else{
			return $package;
		}
	}
	
    public function getAuthPassword(){
	    return $this->User_Password;
	}
	
	public static function getInfo($userID){
		$result = DB::table('users')
                        ->where('User_ID',$userID)
                        ->first();
        return $result;
	}
	
	public static function InsertRow($UserID, $username ,$password, $passwordUnHash, $parents = 0, $tree){
		$user = new User();
		
		$user->User_ID = $UserID; 
		$user->User_Name = $username; 
		$user->User_Password = $password; 
		$user->User_PasswordUnHash = $passwordUnHash; 
		$user->User_Parent = $parents; 
		$user->User_RegisteredDatetime = date('Y-m-d H:i:s'); 
		$user->User_Level = 0;
		$user->User_Status = 1;
		$user->User_Tree = $tree.','.$UserID;
		if($user->save()){
			return true;
		}
		return false;
	}
	
	public static function getF1($userID){
		$userList = User::where('User_Parent', $userID)->select('User_ID', 'User_Level','User_Agency_Level')->get();
		return $userList->toArray();
	}
    public static function updateBalance($userID, $coin, $amount){
		$user = User::find($userID);
		if(!$user){
			return false;
		}
		$symbol = 'User_Balance'.$coin;
		$amountBefore = $user->$symbol;
		$amountAfter = $user->$symbol + $amount;
		$user->$symbol = $amountAfter;
		$user->save();
		return $user->$symbol;
	}
	public static function updateInvestment($userID, $amount){
		$user = User::find($userID);
		$amountBefore = $user->User_TotalInvestment;
		$amountAfter = $user->User_TotalInvestment + $amount;
		$user->User_TotalInvestment = $amountAfter;
		$user->save();
		return $user->User_TotalInvestment;
	}
	public static function updateCommission($userID, $amount){
		$user = User::find($userID);
		$amountBefore = $user->User_BalanceCommission;
		$amountAfter = $user->User_BalanceCommission + $amount;
		$user->User_BalanceCommission = $amountAfter;
		$user->save();
		return $user->User_BalanceCommission;
	}
	public static function updateDoubleProfit($userID, $amount){
		$user = User::find($userID);
		$amountBefore = $user->User_DoubleProfit;
		$amountAfter = $user->User_DoubleProfit + $amount;
		$user->User_DoubleProfit = $amountAfter;
		$user->save();
		return $user->User_DoubleProfit;
	}
	public static function updateBalanceDeposit($userID, $amount){
		$user = User::find($userID);
		$amountBefore = $user->User_BalanceDeposit;
		$amountAfter = $user->User_BalanceDeposit + $amount;
		$user->User_BalanceDeposit = $amountAfter;
		$user->save();
		return $user->User_BalanceDeposit;
	}
	public static function updateBalanceInterest($userID, $amount){
		$user = User::find($userID);
		$amountBefore = $user->User_BalanceInterest;
		$amountAfter = $user->User_BalanceInterest + $amount;
		$user->User_BalanceInterest = $amountAfter;
		$user->save();
		return $user->User_BalanceInterest;
	}
	public static function updateBranchSales($userID){
		
		$branchSales = app('App\Http\Controllers\Cron\CronController')->branchSales($userID);
		$user = User::find($userID);
		if($branchSales['left'] != $user->User_TotalLeft){
			$user->User_TotalLeft = $branchSales['left'];
		}
		if($branchSales['right'] != $user->User_TotalRight){
			$user->User_TotalRight = $branchSales['right'];
		}
		$user->save();
		return true;
	}
	public static function updatePoint($userID, $amount){

		$user = User::find($userID);
		$amountBefore = $user->User_PointStock;
		$amountAfter = $user->User_PointStock + $amount;
		$user->User_PointStock = $amountAfter;
		$user->save();
		return true;
	}

    public static function getBalance($userID){
		$result = DB::table('money')
			->where('Money_MoneyStatus', 1)
			->where('Money_User', $userID)
			->selectRaw("
							COALESCE(SUM(IF(`Money_MoneyAction` IN (1), `Money_USDT`-`Money_USDTFee`, 0)), 0) AS User_BalanceDeposit,
							COALESCE(SUM(IF(`Money_MoneyAction` IN (3), `Money_USDT`-`Money_USDTFee`, 0)), 0) AS User_TotalInvestment,
							COALESCE(SUM(IF(`Money_MoneyAction` IN (5,6,9,13,16,17,15), `Money_USDT`-`Money_USDTFee`, 0)), 0) AS User_BalanceCommission,
							COALESCE(SUM(IF(`Money_MoneyAction` IN (14)  AND `Money_Comment` LIKE '(Compound Interest)%', `Money_USDT`-`Money_USDTFee`, 0)), 0) AS User_DoubleProfit")
			->get();
			
		$branchSales = self::branchSales($userID);
		$result[0]->User_TotalLeft = $branchSales['left'];
		$result[0]->User_TotalRight = $branchSales['right'];
		return $result[0];

	}

	public static function branchSales($user_ID){
		$branch_sales = [];
		$branch_sales['left'] = 0;
		$branch_sales['right'] = 0;
		$result = User::where('User_ID', $user_ID)->first();
		if(!$result){
			return $branch_sales;
		}
		$branch  = User::select('User_Email','User_ID', 'User_Tree', 'User_IsRight')->whereRaw("( User_Tree LIKE CONCAT($user_ID,',',User_ID) OR User_Tree LIKE CONCAT('%,' , $user_ID, ',' ,User_ID) )")->orderBy('User_IsRight')->get();

		if(count($branch) > 0){
            for($i=0;$i<2;$i++){
                if(isset($branch[$i])){
                    if($branch[$i]->User_IsRight == 0){
						
						$left = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', $branch[$i]->User_Tree.'%')
								->where('investment_Status', 1)
								->sum('investment_Amount');
						$branch_sales['left'] = $left;
                        
                        
                        
                    }
                    if($branch[$i]->User_IsRight == 1){
						
						$right = Investment::join('users', 'User_ID', 'investment_User')
								->where('User_Tree', $branch[$i]->User_Tree.'%')
								->where('investment_Status', 1)
								->sum('investment_Amount');
						$branch_sales['right'] = $right;

                    }
                    
                }
                
    
            }
		}
		return $branch_sales;

	}



}
