@php
$side_current = App\Model\User::where('User_ID', session('user')->User_ID)->value('User_Side_Active');
@endphp

<div class="form-group row">
    <div class="col-sm-12">
        <div class="input-group">
            <input type="text" id="linkRef" class="form-control" placeholder="Link Ref"
                value="{{route('getRegister')}}?ref={{ Session('user')->User_ID }}">
        </div>
    </div>

    <label class="col-sm-12 text-center">
        <div class="btn-group mt-3 mr-auto">
            <button type="button" id="tooltiptext" class="copytooltip btn btn-stk waves-effect waves-primary"
                onclick="copyToClipboard()">COPY</button>
            <button class="btn btn-{{$side_current == 0 ? 'primary' : 'success'}}" data-value="0"
                id="btn-left-side">Left</button>
            <button class="btn btn-{{$side_current == 0 ? 'success' : 'primary'}}" data-value="1"
                id="btn-right-side">Right</button>
        </div>
    </label>
</div>




<script>
    $('#btn-left-side').click(function () {
        if($(this).hasClass('btn-success')){
            $(this).removeClass('btn-success').addClass('btn-primary');
        }
        if($('#btn-right-side').hasClass('btn-primary')){
            $('#btn-right-side').removeClass('btn-primary').addClass('btn-success');
        }
        side_active = $(this).attr('data-value');
        $.ajax({
            url: '{{ route('changeSideActive')}}',
            type: 'GET',
            data: {'side_active': side_active},
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data['status'] == 'success') {
                    toastr.success(data['message'], 'Success!', {timeOut: 3500})
                }
                else {
                    toastr.error(data['message'], 'Error!', {timeOut: 3500})
                    
                }
            }
        });
    });
    
    $('#btn-right-side').click(function () {
        if($(this).hasClass('btn-success')){
            $(this).removeClass('btn-success').addClass('btn-primary');
        }
        if($('#btn-left-side').hasClass('btn-primary')){
            $('#btn-left-side').removeClass('btn-primary').addClass('btn-success');
        }
        side_active = $(this).attr('data-value');
        $.ajax({
            url: '{{ route('changeSideActive')}}',
            type: 'GET',
            data: {'side_active': side_active},
            dataType: 'json',
            success: function (data) {
                if (data['status'] == 'success') {
                    toastr.success(data['message'], 'Success!', {timeOut: 3500})
                }
                else {
                    toastr.error(data['message'], 'Error!', {timeOut: 3500})
    
                }
    
            }

        });
    });
</script>