<!DOCTYPE html>
<html lang="en">

<head>
	<title>{{ trans('message.title') }} | Sign In</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="{{ trans('message.title') }}">
	<meta name="author" content="{{ trans('message.title') }}">

	<base href="{{asset('/staking').'/'}}">
	<!--favicon-->
	<link rel="icon" href="{{ trans('message.icon') }}" type="image">
	<!-- Bootstrap core CSS-->
	<link href="assets\css\bootstrap.min.css" rel="stylesheet">
	<!-- animate CSS-->
	<link href="assets\css\animate.css" rel="stylesheet" type="text/css">
	<!-- Icons CSS-->
	<link href="assets\css\icons.css" rel="stylesheet" type="text/css">
	<!-- Custom Style-->
	<link href="assets\css\app-style.css?v={{time()}}" rel="stylesheet">

	<link href="notify/toastr.min.css" rel="stylesheet">
	<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="notify/sweetalert2.min.css" />
	<!-- Start Alexa Certify Javascript -->
	<script type="text/javascript">
	_atrk_opts = { atrk_acct:"vaigt1zDGU20kU", domain:"stakingcoin.co",dynamic: true};
	(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
	</script>
	<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=vaigt1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
	<!-- End Alexa Certify Javascript -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161199649-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-161199649-1');
</script>

</head>

<body class="bg-theme bg-theme1">
<div id="wrapper"  class=" flex-center-fixed">

<div class="loader-wrapper">
		<div class="lds-ring">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
		</div>
</div>
	<div class="card card-authentication1 mx-auto ">
		<div class="card-body">
			<div class="card-content p-2">
				<div class="text-center">
					<img src="{{ trans('message.logo') }}" alt="logo icon" height="70" class="logo-img">
				</div>
				<div class="card-title text-uppercase text-center py-3">Sign Up</div>
				<form method="POST" role="form" action="{{route('postRegister')}}">
					@csrf
					<div class="form-group">
						<label class="control-label mb-10 txt-light" for="exampleInputEmail_2">Email
							address</label>
						<input class="form-control" id="exampleInputEmail_2" name="Email" placeholder="Email(*)"
							type="email" value="{{ old('Email')}}">
					</div>
					<div class="form-group">
						<label class="pull-left control-label mb-10 txt-light" for="exampleInputpwd_2">Password</label>
						<input type="password" id="exampleInputpwd_2" class="form-control" placeholder="********"
							name="Password">
					</div>
					<div class="form-group">
						<label class="pull-left control-label mb-10 txt-light" for="exampleInputpwd_3">Confirm
							Password</label>
						<input id="exampleInputpwd_3" type="password" class="form-control" placeholder="********"
							name="Re-Password">
					</div>
					<div class="form-group">
						<label class="pull-left control-label mb-10 txt-light" for="parent">Sponsor</label>
						<input placeholder="Sponser" type="text" class="form-control" id="parent" name="parent"
							value="{{request()->input('ref') ?? $parent}}" autocomplete="off" />
					</div>
					<div class="form-group">
						<label class="pull-left control-label mb-10 txt-light" for="presenter">Presenter</label>
						<input placeholder="Presenter" type="text" name="presenter" class="form-control"
							value="{{!request()->input('ref') && !request()->input('presenter') ? $leftnode : request()->input('presenter')}}" autocomplete="off" />
					</div>
					<input type="hidden" name="node" value="{{request()->input('node')}}">
					<button type="submit" class="btn btnSignIn  btn-block waves-effect waves-light">Sign Up</button>

				</form>
			</div>
		</div>

		<div class="card-footer text-center py-3">
			<p class="text-warning mb-0">Already have an account? <a href="{{ route('getLogin') }}"> Sign In here</a>
			</p>
		</div>
	</div>

	<!--Start Back To Top Button-->
	<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
	<!--End Back To Top Button-->

	<!--end color cwitcher-->

	</div>
	<!--wrapper-->

	<!-- Bootstrap core JavaScript-->
	<script src="assets\js\jquery.min.js"></script>
	<script src="assets\js\popper.min.js"></script>
	<script src="assets\js\bootstrap.min.js"></script>

	<!-- sidebar-menu js -->
	<script src="assets\js\sidebar-menu.js"></script>

	<!-- Custom scripts -->
	<script src="assets\js\app-script.js"></script>
	<script src="notify/toastr.min.js"></script>
	<script src="notify/sweetalert2.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src='https://www.google.com/recaptcha/api.js?hl=us'></script>
	<script type="text/javascript">
		@if(Session::get('flash_level') == 'success')
        toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
        @elseif(Session::get('flash_level') == 'error')
        toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
        @endif

        @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
        toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
        @endforeach
        @endif
	</script>
</body>

</html>