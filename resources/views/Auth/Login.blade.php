<!DOCTYPE html>
<html lang="en">

<head>
	<title>{{ trans('message.title') }} | Sign In</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="{{ trans('message.title') }}">
	<meta name="author" content="{{ trans('message.title') }}">

	<base href="{{asset('/staking').'/'}}">
	<!--favicon-->
	<link rel="icon" href="{{ trans('message.icon') }}" type="image">
	<!-- Bootstrap core CSS-->
	<link href="assets\css\bootstrap.min.css" rel="stylesheet">
	<!-- animate CSS-->
	<link href="assets\css\animate.css" rel="stylesheet" type="text/css">
	<!-- Icons CSS-->
	<link href="assets\css\icons.css" rel="stylesheet" type="text/css">
	<!-- Custom Style-->
	<link href="assets\css\app-style.css?v={{time()}}" rel="stylesheet">

	<link href="notify/toastr.min.css" rel="stylesheet">
	<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="notify/sweetalert2.min.css" />
	<!-- Start Alexa Certify Javascript -->
	<script type="text/javascript">
	_atrk_opts = { atrk_acct:"vaigt1zDGU20kU", domain:"stakingcoin.co",dynamic: true};
	(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
	</script>
	<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=vaigt1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
	<!-- End Alexa Certify Javascript -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161199649-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-161199649-1');
</script>

</head>

<body class="bg-theme bg-theme1">

	<!-- start loader -->
	<div id="pageloader-overlay" class="visible incoming">
		<div class="loader-wrapper-outer">
			<div class="loader-wrapper-inner">
				<div class="loader"></div>
			</div>
		</div>
	</div>
	<!-- end loader -->

	<!-- Start wrapper-->
	<div id="wrapper" class="flex-center-fixed">

		<div class="loader-wrapper">
			<div class="lds-ring">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
		<div class="card card-authentication1 mx-auto">
			<div class="card-body">
				<div class="card-content p-2">
					<div class="text-center">
						<img src="{{ trans('message.logo') }}" alt="logo icon" height="70" class="logo-img">
					</div>
					<div class="card-title text-uppercase text-center py-3">Sign In</div>
					<form method="POST" action="{{route('postLogin')}}">
						@csrf
						<div class="form-group">
							<label class="control-label txt-white mb-10" for="email">Email address</label>
							<div class="position-relative has-icon-right">
								<input class="form-control txt-white" name="email" type="email" id="email" required=""
									placeholder="Email">
								<div class="form-control-position">
									<i class="icon-user"></i>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="pull-left txt-white control-label mb-10" for="password">Password</label>
							<div class="position-relative has-icon-right">
								<input class="form-control txt-white" name="password" id="password" type="password"
									required="" placeholder="Password">
								<div class="form-control-position">
									<i class="icon-lock"></i>
								</div>
							</div>
						</div>
						{{--<div class="form-group">
							<div style="display: flex; justify-content: center;" class="g-recaptcha" data-sitekey="6LdBW9cUAAAAAKCaW-3JsgvqPdtp7SrF19nUJmAO"
								data-callback="enableBtn"></div>
						</div>--}}
						{!! Geetest::render() !!}
						<div class="form-row">
							<div class="form-group col-6">
								<div class="icheck-material-white">
									<input type="checkbox" id="user-checkbox" checked="">
									<label for="user-checkbox">Remember me</label>
								</div>
							</div>
							<div class="form-group col-6 text-right">
								<a href="{{ route('getForgotPassword') }}">Reset Password</a>
							</div>
						</div>

						<button type="submit" class="btnSignIn btn btn-light btn-block">Sign In</button>

					</form>
				</div>
			</div>
			<div class="card-footer text-center py-3">
				<p class="text-warning mb-0">Do not have an account? <a href="{{ route('getRegister') }}"> Sign Up
						here</a></p>
			</div>
		</div>

		<!--Start Back To Top Button-->
		<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
		<!--End Back To Top Button-->

		<!--start color switcher-->
		<div class="right-sidebar">
			<div class="switcher-icon">
				<i class="zmdi zmdi-settings zmdi-hc-spin"></i>
			</div>
			<div class="right-sidebar-content">

				<p class="mb-0">Gaussion Texture</p>
				<hr>

				<ul class="switcher">
					<li id="theme1"></li>
					<li id="theme2"></li>
					<li id="theme3"></li>
					<li id="theme4"></li>
					<li id="theme5"></li>
					<li id="theme6"></li>
				</ul>

				<p class="mb-0">Gradient Background</p>
				<hr>

				<ul class="switcher">
					<li id="theme7"></li>
					<li id="theme8"></li>
					<li id="theme9"></li>
					<li id="theme10"></li>
					<li id="theme11"></li>
					<li id="theme12"></li>
				</ul>

			</div>
		</div>
		<!--end color cwitcher-->

	</div>
	<!--wrapper-->

	<!-- Bootstrap core JavaScript-->
	<script src="assets\js\jquery.min.js"></script>
	<script src="assets\js\popper.min.js"></script>
	<script src="assets\js\bootstrap.min.js"></script>

	<!-- sidebar-menu js -->
	<script src="assets\js\sidebar-menu.js"></script>

	<!-- Custom scripts -->
	<script src="assets\js\app-script.js"></script>
	<script src="notify/toastr.min.js"></script>
	<script src="notify/sweetalert2.min.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src='https://www.google.com/recaptcha/api.js?hl=us'></script>
	<script type="text/javascript">
		$(document).ready(function() {
		@if(Session::has('otp'))
			var CSRF_TOKEN = '{{ csrf_token() }}';
			swal.fire({
				title: 'Enter Authentication',
				text: 'Please enter authentication code.',
				input: 'text',
				type: 'input',
				name: 'txtOTP',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Submit',
				showLoaderOnConfirm: true,
				confirmButtonClass: 'btn btn-confirm',
				cancelButtonClass: 'btn btn-cancel'
				}).then(function (otp) {
					console.log(otp);
					$.ajax({
						url: "{{route('postLoginCheckOTP')}}",
						type: 'POST',
						data: {_token: CSRF_TOKEN, otp:otp.value},
						success: function (data) {
							if(data == 1){
								location.href = "{{route('Dashboard')}}";
							}else{
								Swal.fire({
									title: 'Error',
									text: "Authentication Code Is Wrong",
									type: 'error',
									confirmButtonClass: 'btn btn-confirm',
									allowOutsideClick: false
								}).then(function() {
									location.href = "{{route('getLogin')}}";
								})
							}
						}
					});
				});
		@endif
	});
	@if(Session::get('flash_level') == 'success')
		toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
	@elseif(Session::get('flash_level') == 'error')
		toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
	@endif

	@if (count($errors) > 0)
		@foreach ($errors->all() as $error)
			toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
		@endforeach
	@endif


// 	$('.btnSignIn').attr("disabled", true);
	function enableBtn(){
		console.log(5345);
		$('.btnSignIn').attr("disabled", false);
	}

	$('#sign-in-form').submit(function(e) {
		rcres = grecaptcha.getResponse();
		if (!rcres.length) {
			e.preventDefault();
		}
	});

	</script>
</body>

</html>