<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
    style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Active</title>
    <style type="text/css">
        img {
            max-width: 100%;
        }

        body {

            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6em;
        }

        body {
            background-color: #f6f6f6;
        }

        .bot {
            width: 150px;
            height: auto;
        }

        .main {
            widows: 100%;

        }

        @media only screen and (max-width: 375px) {
            .bot {
                height: 94px;
            }
        }

        @media screen and (max-width: 414px) {
            .bot {
                height: 96px;
            }
        }

        @media screen and (max-width: 320px) {
            .bot {
                height: 77px;
            }
        }

        @media only screen and (max-width: 640px) {
            body {
                padding: 0 !important;
            }

            h1 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h2 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h3 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h4 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                padding: 0 !important;
                width: 100% !important;
            }

            .content {
                padding: 0 !important;
            }

            .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }

            .content-block p {
                margin-left: 18px;
            }
        }
        center >div>div>a{
            color:#e09000!important;
        }
    </style>
</head>

<body
    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;  "
    bgcolor="#f6f6f6">

    <table class="body-wrap"
        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #e6e6e6; margin: 0;"
        bgcolor="#f6f6f6">
        <tr
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
            <td class="container" width="600"
                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
                valign="top">
                <div class="content"
                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; border: 0px #07ab99 solid; background: #002153;border-top-width: 0px;border-bottom-width: 0px;position: relative;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope
                        itemtype="http://schema.org/ConfirmAction"
                        style="background: url('https://stakingcoin.co/public/staking/mail/mailbg3.jpg');background-repeat: no-repeat;background-size: 100% 100%;background-position: top center;">
                        <tr
                            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ">
                            <td class="content-wrap"
                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;border-radius: 7px;">
                                <meta itemprop="name" content="Confirm Email"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" />
                                <div class="bg-op"></div>
                                <table width="100%" cellpadding="0" cellspacing="0"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;background-repeat: no-repeat;background-position: center;background-size: 70% auto;background:rgb(0 0 0 / 0.4)">
                                    <tr>
                                        <td class="bot">
                                            <div style="display: flex;">
                                                <center
                                                    style="margin: 30px 40px 40px 40px;float:left;text-align: right;">
                                                    <img src="https://stakingcoin.co/public/staking/mail/staking_logo.png" height="50"></center>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bot">
                                            <div style="display: flex;">
                                                <center
                                                    style="margin: auto;margin-top: 30px;text-align: center;">
                                                    <h2 style="text-transform: uppercase;color: #fff;">NOTICE ABOUT NEW REWARDS POLICY</h2>
                                                </center>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bot">
                                            <div>
                                                <center style="margin-left: 30px;margin-right: 30px;margin-top: 30px;text-align: left;">
                                                    <p
                                                        style="text-align: left;color: #fff;font-size: 18px;">Dear Investors,
                                                    </p>
                                                </center>
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="bot1">
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 18px; vertical-align: top; margin: 0; padding: 0 0 15px;"
                                                valign="top">
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px;text-align: left;color: #fff;font-size: 18px;">
													To create opportunities for investors' own SKC Token more easily and create a premise of the outstanding Staking Platform's development in the future.   
                                                </p>
												<p style="margin-left: 30px;margin-right: 30px;text-align: left;color: #fff;font-size: 18px;">
													We will apply the new reward policy:
													<br>
													- From September 15th, 2020, all interest on investment packages will be paid 50% by SKC Token.
													<br>
													- On October 1st, 2020, Staking Platform will apply a reward policy by 100% interest with SKC Token.
												</p>
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px;text-align: left;color: #fff;font-size: 18px;">
													Over 6 rounds of ITO, accompanied by a series of Staking Platform's ecosystems that have been continuously launched such as an E-Commerce Platform, Staking Platform, ... SKC Token is a coin which most worth owning in the second half of 2021. 
                                                </p>
                                                
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px;text-align: left;color: #fff;font-size: 18px;">
													The need to own SKC Token is increasing, leading the price of SKC Token increases significantly and will increase even more strongly in a short time.                                                </p>
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px;text-align: left;color: #fff;font-size: 18px;">
													This is a better time for you to own SKC Token to increase profits.
													<br>
													Good luck and tons of best wishes sent to you
                                                </p>
                                            </td>
                                        </tr>
                                        
                                    </div>

                                    <div class="bot1">

                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 15px;float: left;"
                                                valign="top">
                                                <p style="margin-left: 30px;margin-right: 30px; margin-top: 0px;text-align: left;color: #ffff">Best regards,<br>Staking Coin Team</p>
                                            </td>
                                        </tr>

                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 0;float: right;"
                                                valign="top">
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px; margin-top: 0px;text-align: right;color: #fff">
                                                </p>
                                            </td>
                                        </tr>
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;background: #23b5a5;">
                                              <td class="content-block"
                                              style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 0;float: right;"
                                              valign="top">
                                          </td>
                                        </tr>

                                    </div>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
            </td>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
        </tr>
    </table>
</body>

</html>