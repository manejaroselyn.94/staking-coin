<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
    style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Active</title>
    <style type="text/css">
        img {
            max-width: 100%;
        }

        body {

            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6em;
        }

        body {
            background-color: #f6f6f6;
        }

        .bot {
            width: 150px;
            height: auto;
        }

        .main {
            widows: 100%;

        }

        @media only screen and (max-width: 375px) {
            .bot {
                height: 94px;
            }
        }

        @media screen and (max-width: 414px) {
            .bot {
                height: 96px;
            }
        }

        @media screen and (max-width: 320px) {
            .bot {
                height: 77px;
            }
        }

        @media only screen and (max-width: 640px) {
            body {
                padding: 0 !important;
            }

            h1 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h2 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h3 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h4 {
                font-weight: 800 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                padding: 0 !important;
                width: 100% !important;
            }

            .content {
                padding: 0 !important;
            }

            .content-wrap {
                padding: 10px !important;
            }

            .invoice {
                width: 100% !important;
            }

            .content-block p {
                margin-left: 18px;
            }
        }
        center >div>div>a{
            color:#e09000!important;
        }
    </style>
</head>

<body
    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;  "
    bgcolor="#f6f6f6">

    <table class="body-wrap"
        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #e6e6e6; margin: 0;"
        bgcolor="#f6f6f6">
        <tr
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
            <td class="container" width="600"
                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
                valign="top">
                <div class="content"
                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; border: 0px #07ab99 solid; background: #002153;border-top-width: 0px;border-bottom-width: 0px;position: relative;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope
                        itemtype="http://schema.org/ConfirmAction"
                        style="background: url('');background-size: 100% 100%;background-position: top center;background-repeat: no-repeat;">
                        <tr
                            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ">
                            <td class="content-wrap"
                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;border-radius: 7px;background: url('{{ $url_web }}public/staking/mail/mailbg3.jpg');background-repeat: no-repeat;background-size: 100% 100%;background-position: top center;">
                                <meta itemprop="name" content="Confirm Email"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" />
                                <div class="bg-op"></div>
                                <table width="100%" cellpadding="0" cellspacing="0"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;background-repeat: no-repeat;background-position: center;background-size: 70% auto;">
                                    <tr>
                                        <td class="bot">
                                            <div style="display: flex;">
                                                <center
                                                    style="margin: 30px 40px 40px 40px;float:left;text-align: right;">
                                                    <img src="{{ $url_web }}public/staking/mail/staking_logo.png" height="50"></center>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bot">
                                            <div style="display: flex;">
                                                <center
                                                    style="margin: auto;margin-top: 30px;text-align: center;">
                                                    <h2 style="text-transform: uppercase;color: #fff;">NOTICE DAILY INTEREST</h2>
                                                </center>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bot">
                                            <div>
                                                <center style="width: 95%;margin:auto">
                                                    <p
                                                        style="margin: auto;color: #e0e0e0;font-size: 18px;text-align: center;">Dear
                                                        
                                                    </p>
                                                    <div
                                                        style="margin: auto;color: #e0e0e0;font-size: 18px;text-align: center;">
                                                        <div style="
															color: #e09000!important;
															background: #212529;
															padding: 10px 20px;
															border-radius: 100px;
															word-break: break-word;
															border: 1px solid;
															width: 50%;
															margin: 10px auto;
															">  {{ $User_Email}}</div>
                                                    </div>
													
                                                </center>
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="bot1">
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 18px; vertical-align: top; margin: 0; padding: 0 0 15px;"
                                                valign="top">
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px; margin-top: 0px;color: #fff;text-align: center;">
                                                    <span style="color: #fff">The team is pleased to announce that your account on {{$date}}</span>
                                                </p>

                                            </td>
                                        </tr>
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 18px; vertical-align: top; margin: 0; padding: 0 0 5px;"
                                                valign="top">
                                                <p style="margin-left: 30px;margin-right: 30px; margin-top: 0px;color: #fff;text-align: center;">
                                                    <span style="color: #fff">Your daily interest: ${{$amount}} ~ {{$amountCoin}} {{$symbol}}</span>
                                                </p>
                                            </td>
                                        </tr>
                                    </div>

                                    <div class="bot1">
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 18px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 18px; vertical-align: top; margin: 0; padding: 0 0 15px;"
                                                valign="top">
                                                <p
                                                    style="margin: auto;margin-top: 0px;color: #fff;width: 100%;text-align: center;">
                                                    If you need more information, please send us a letter to:<br><br> <span style="
														background: #222323;
														color: #e09000;
														padding: 5px 10px;
														border-radius: 20px;
														border: 1px solid;
														">support@Stakingcoin.co</span></p>
                                            </td>
                                        </tr>

                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 15px;float: right;"
                                                valign="top">
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px; margin-top: 0px;text-align: right;color: #dadada">
                                                    <!-- <img src="dafco/text.png" height="10"> -->
                                                </p>
                                            </td>
                                        </tr>

                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 15px;float: right;"
                                                valign="top">
                                                <p style="margin-left: 30px;margin-right: 30px; margin-top: 0px;text-align: right;color: #dadada">Best regards,<br>Staking Coin Team</p>
                                            </td>
                                        </tr>

                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;">
                                            <td class="content-block"
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; margin: 0; padding: 0 0 0;float: right;"
                                                valign="top">
                                                <p
                                                    style="margin-left: 30px;margin-right: 30px; margin-top: 0px;text-align: right;color: #dadada">
                                                </p>
                                            </td>
                                        </tr>
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; ; 
                                background-size: cover;  
                                background-repeat: no-repeat; 
                                background-position: center;
                                background-size: 100%;background: #23b5a5;">
                                            
                                        </tr>

                                    </div>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
            </td>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
        </tr>
    </table>
</body>

</html>