@extends('System.Layouts.Master')
@section('title', 'Profile')
@section('css')
<link href="dropify/dropify.min.css?v=4322" rel="stylesheet" type="text/css" />
<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Profile</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="card profile-card-2">
            <div class="card-img-block">
                <img class="img-fluid" src="assets\images\profile-wap.png" alt="Card image cap">
            </div>
            <div class="card-body pt-5">
                <div class="profile-image">
                    <img src="assets\images\avatars\avatar.png" alt="profile-image" class="profile">
                </div>
                <h5 class="card-title text-center">ID: {{ Session('user')->User_ID}}</h5>
                <h5 class="card-title text-center">Email: {{ Session('user')->User_Email}}</h5>
                <button type="submit" class="btn btn-stk btn-block mt-3" data-toggle="modal"
                    data-target="#change-password">
                    Change Password</button>
            </div>
            <div class="card-body border-top border-light">
                <div class="media align-items-center">
                    <div>
                        <img src="assets\images\avatars\authentic.png" class="skill-img" alt="skill img">
                    </div>
                    <div class="media-body text-left ml-3">
                        <div class="progress-wrapper">
                            <h6 class=" text-white">GOOGLE AUTHENTICATION USED FOR WITHDRAWALS AND SECURITY MODIFICATIONS</h6>
                        </div>

                    </div>
                </div>
                <button type="submit" class="text-white btn btn-{{ ($Enable) ? 'stk' : 'light' }} btn-block mt-3 "
                    data-toggle="modal" data-target="#ggAuth">
                    {{ ($Enable) ? 'Disable' : 'Enable' }} Auth</button>
                <hr>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-tabs-primary top-icon nav-justified">
                    <li class="nav-item">
                        <a href="javascript:void();" data-target="#INFORMATION" data-toggle="pill"
                            class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">USER
                                INFORMATION</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="javascript:void();" data-target="#VERIFICATION" data-toggle="pill" class="nav-link"><i
                                class="icon-envelope-open"></i> <span class="hidden-xs">VERIFICATION</span></a>
                    </li>
                </ul>
                <div class="tab-content p-3">
                    <div class="tab-pane active" id="INFORMATION">
                        <h5 class="mb-3">USER INFORMATION</h5>
                        <form method="post" action="{{route('postProfileV2')}}" data-toggle="validator" role="form"
                            novalidate="true"  id="post_address">
                            @csrf
                            <div class="form-body overflow-hide">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Wallet ETH</label>
                                    <div class="input-group">
                                        <input type="text" id="address_eth" name="addressToken" class="form-control"
                                            placeholder="ETH Wallet Address"
                                            value="{{$user->User_WalletETH}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Wallet TRX</label>
                                    <div class="input-group">
                                        <input type="text" id="address_TRX" name="addressTRX" class="form-control"
                                            placeholder="TRX Wallet Address "
                                            value="{{$user->User_WalletTRX}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Wallet SKC</label>
                                    <div class="input-group">
                                        <input type="text" id="address_SKC" name="addressSKC" class="form-control"
                                            placeholder="SKC Wallet Address "
                                            value="{{$user->User_WalletSKC}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Currency Receive Interest</label>
                                    <div class="input-group">

                                        <div class="form-check">
                                            <input type="radio" id="exampleRadios1" value="2" name="coin"
                                                {{ $user->User_TypePay == 2 ? 'checked' : ''}}>
                                            <label class="form-check-label" for="exampleRadios1">
                                                ETH <span class="text-danger label-ETH" style="display:none;">Please select the exact method of profit you expect</span>
											</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" id="exampleRadios2" value="9" name="coin"
                                                {{ $user->User_TypePay == 9 ? 'checked' : ''}}>
                                            <label class="form-check-label" for="exampleRadios2">
                                                TRX <span class="text-danger label-TRX" style="display:none;">Please select the exact method of profit you expect</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputEmail_01"><i
                                            class="fa fa-google"></i> Google authenticator</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="otp" name="otp" required
                                            placeholder="Enter Google Authenticator">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="btn btn-stk btn-icon-anim"><i
                                        class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="VERIFICATION">
                        <form method="post" id="post-profile" action="{{route('system.user.PostKYC')}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-body overflow-hide">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="fa fa-google"></i> ID/Passport Number</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="passport" id="passport"
                                            placeholder="ID/Passport Number"
                                            value="{{ $kycProfile ? $kycProfile->Profile_Passport_ID : ''}}">
                                    </div>
                                </div>
                                <div class="form-group profile-box">
                                    <label class="control-label" for="exampleInputuname_01"><i class="fa fa-google"></i>
                                        ID/Passport Image</label>
                                    <br>
                                    <label class="control-label" for="exampleInputuname_01" style="color:#e09000!important"><i
                                            class="fa fa-caret-right"></i> Make sure the image is full and
                                        clear and the format is jpg, jpeg.</label><br>
                                    <label class="control-label text-danger" for="exampleInputuname_01" style="color:#e09000!important"><i
                                            class="fa fa-caret-right"></i> Please
                                        use image up to maximum 2MB size</label>
                                    <div class="profile-cover-pic">

                                        <div class="fileupload">
                                            <input onchange="readURL(this);" type="file" name="passport_image"
                                                id="passport-image" class="file-upload-input dropify bg-dark"
                                                data-default-file="{{ $kycProfile ? trans('message.url_upload').$kycProfile->Profile_Passport_Image : ''}}"
                                                accept="image/*" />
                                        </div>
                                        <div class="profile-image-overlay"></div>
                                    </div>
                                </div>
                                <div class="form-group profile-box">
                                    <label class="control-label" for="exampleInputuname_01"><i
                                            class="fa fa-picture-o"></i> ID/Passport Image With
                                        Selfie</label>
                                    <br>
                                    <label class="control-label  text-danger" for="exampleInputuname_01"><i
                                            class="fa fa-caret-right" style="color:#e09000!important"> Make sure the image is full and clear
                                            and the format is jpg, jpeg.</i></label><br>
                                    <label class="control-label text-danger" for="exampleInputuname_01" style="color:#e09000!important"><i
                                            class="fa fa-caret-right"></i> Your face</label>
                                    <br>
                                    <label class="control-label text-danger" for="exampleInputuname_01" style="color:#e09000!important"><i
                                            class="fa fa-caret-right"></i> Your ID/Passport</label><br>
                                    <label class="control-label text-danger" for="exampleInputuname_01" style="color:#e09000!important"><i
                                            class="fa fa-caret-right"></i> Please
                                        use image up to maximum 2MB size</label>
                                    <div class="profile-cover-pic">
                                        <div class="fileupload">
                                            <input onchange="readURL2(this);" type="file" name="passport_image_selfie"
                                                id="passport_image_selfie" class="file-upload-input dropify bg-dark"
                                                data-default-file="{{ $kycProfile ? trans('message.url_upload').$kycProfile->Profile_Passport_Image_Selfie : ''}}"
                                                accept="image/*" />
                                        </div>
                                        <div class="profile-image-overlay"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="btn btn-stk btn-icon-anim"><i
                                        class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="change-password">
    <div class="modal-dialog">
        <div class="modal-content border-info animated flipInX">
            <div class="modal-header">
                <h5 class="modal-title">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('postChangePassword')}}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label mb-10 text-white" for="exampleInputpwd_1">Current
                            Password</label>
                        <div class="input-group">
                            <input type="password" class="form-control" name="current_password"
                                placeholder="Enter current password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10 text-white" for="exampleInputpwd_1">New
                            Password</label>
                        <div class="input-group">

                            <input type="password" class="form-control" name="new_password"
                                placeholder="Enter new password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label mb-10 text-white" for="exampleInputpwd_1">Re
                            New
                            Password</label>
                        <div class="input-group">

                            <input type="password" class="form-control" name="password_confirm"
                                placeholder="Enter password confirm">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>
                        Close</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /.modal-dialog -->
<div id="ggAuth" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInX">
            <form action="{{route('postAuth')}}" method="post">
                {{csrf_field()}}
                <div class="modal-header bg-info">
                    <h5 class="modal-title">{{ ($Enable) ? 'Disable Authenticator' : 'Enable Authenticator' }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <!-- Row -->
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            @if(!$Enable)
                            <h5 class="text-center">
                                <code>Authenticator Secret Code:</code>
                                <code>{{ $secret }}</code></h5>
                            <div class="text-center">
                                <img
                                    src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={{ $inlineUrl }}&choe=UTF-8">
                            </div>
                            @endif
                            <p class="text-center">
                                <code>Enter the 2-step</code> verification code
                                provided by your authentication app</p>
                            <input type="text" name="verifyCode" class="form-control" id="exampleInputuname_01"
                                placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit"
                        class="btn btn-success waves-effect">{{ ($Enable) ? 'Disable' : 'Enable' }}</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('script')
<script src="dropify/dropify.min.js"></script>

<!-- Form Flie Upload Data JavaScript -->
<script src="dropify/form-file-upload-data.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    /*FileUpload Init*/
    $(document).ready(function() {
	    
	    $('#post_address').submit(function(e) {
		    _type_choose = $('input[name="coin"]:checked').val();
		    if(_type_choose == 2){
			    _address = $('#address_eth').val();
		    }else if(_type_choose == 9){
			    _address = $('#address_TRX').val();
		    }
		    if(!_address){
			    e.preventDefault();
			    NoticeAddress(9);
		    }else{
		        $(this).find("button[type='submit']").prop('disabled',true);
		        $('#btn-save-profile').text('Loading...');
		    }
	    });
	    
	    _type_current = {{$user->User_TypePay}};
	    /*
        $('input[name="coin"]').click(function(){
                _type_choose = $(this).val();
                NoticeAddress(_type_choose);
                });
        */
	    function NoticeAddress(_type){
		    if(_type == 2){
			    $('.label-ETH').show();
			    $('.label-TRX').hide();
		    }else if(_type == 9){
			    $('.label-ETH').hide();
			    $('.label-TRX').show();
		    }
	    }
        "use strict";
        /* Basic Init*/
        $('.dropify').dropify();

        /* Translated Init*/
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-dÃ©posez un fichier ici ou cliquez',
                replace: 'Glissez-dÃ©posez un fichier ou cliquez pour remplacer',
                remove:  'Supprimer',
                error:   'DÃ©solÃ©, le fichier trop volumineux'
            }
        });

        /* Used events */
        //
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element){
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element){
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element){
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e){
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });

    });
    $('#post-profile').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
        $('#btn-save-profile').text('Loading...');
    });
</script>
@endsection