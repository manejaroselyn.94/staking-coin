@extends('System.Layouts.Master')
@section('title', 'Member List')
@section('css')

<!--THIS PAGE LEVEL CSS-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<style>
    a:hover {
        cursor: pointer;
    }
</style>
@endsection

@section('content')

<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Member List</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header text-uppercase"><i class="fa fa-user"></i> Member</div>
            <div class="card-body">
                <label class="control-label mb-10 text-left"><i class="fa fa-user" aria-hidden="true"></i>
                    Referral Link</label>
                @include('shareview.linkref')
            </div>
        </div>
    </div>
    
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header text-uppercase"><i class="fa fa-user"></i> Search</div>
            <div class="card-body">
				<form method="GET" action="">
				    <div class="row">
				        <div class="col-sm-6">
				            <div class="form-group">
				                <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i>
				                    From</label>
				                <input id="datefrom" type="text" class="form-control" placeholder="yyyy/mm/dd" name="from"
				                    value="{{request()->input('from')}}">
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				                <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i>
				                    To</label>
				                <input id="dateto" type="text" class="form-control" placeholder="yyyy/mm/dd" name="to"
				                    value="{{request()->input('to')}}">
				            </div>
				        </div>
				        <div class="col-sm-6">
				            <div class="form-group">
				                <div class="form-actions mt-10">
				                    <button type="submit" class="btn-filler btn btn-stk  mr-10"><i class="fa fa-search"
				                            aria-hidden="true"></i>
				                        Search</button>
				                    <a href="{{ route('system.user.getList') }}"
				                        class="btn-filler btn btn-danger mr-10">Cancel</a>
				                </div>
				            </div>
				        </div>
				    </div>
				</form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Member
                <div class="card-action">
                    <div class="text-warning">TOTAL: ${{ number_format($total_invest_root, 2) }}</div>

                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="default-datatable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Level</th>
                                <th>
                                    User ID
                                </th>
                                <th>
                                    Email</th>
                                <th>
                                    Sponsor
                                </th>
                                <th>
                                    Investment</th>
                                <th>
                                    Investment SKC</th>
                                <!--<th>
                                    Sales
                                </th>-->
                                <th>
                                    Created Date
                                </th>
                                <th>
                                    Verification
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user_list as $v)
                            <tr>
                                <td style="text-align: center">F{{ (int)$v->f }}</td>
                                <th class="text-center"><b>{{ $v->User_ID }}</b></th>
                                <td>{{ $v->User_Email }}</td>
                                <td class="text-center">{{ $v->User_Parent }}</td>
                                <td>
                                    {{ number_format($v->aaa,2) }} USD
                                </td>
                                <td>
                                    {{ number_format($v->invest_skc,2) }} SKC
                                </td>
                                {{--<td>
                                    {{ number_format($v->total_invest_branch,2)}}
                                </td>--}}
                                <td>{{ $v->User_RegisteredDatetime }}</td>
                                <td>
                                    @if($v->Profile_Status == 1)
                                    <span class="badge badge-success r-3">Verified</span>

                                    @else
                                    <span class="badge badge-danger r-3">Unverify</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js">
</script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js">
</script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datefrom').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
        
      $('#dateto').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
</script>
<script>
    $('#list-member-table').DataTable({
        "bLengthChange": true,
        "paging": true
    });
    
</script>
<script>
    $('#dt-dashboard').DataTable({
            "bLengthChange": false,
        "searching": false,
            "paging": false,
            "order": [0, 'desc']
        });
</script>
<script>
    function copyToClipboard() {
        var copyText = document.getElementById("linkRef");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        var tooltip = document.getElementById("tooltiptext");
        // tooltip.innerText = "Copied";
        alert(copyText.value);
    }
    function hoverCopyTooltip() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy";
    }
</script>
@endsection