@extends('System.Layouts.Master')
@section('title', 'Member Tree')
@section('css')
<style>
    a:hover {
        cursor: pointer;
    }
</style>

<style>
    .copytooltip {
        position: relative;
        display: inline-block;
    }

    .copytooltip .tooltiptext {
        visibility: hidden;
        width: 100px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px;
        position: absolute;
        z-index: 1;
        bottom: 150%;
        left: 50%;
        margin-left: -75px;
        opacity: 0.5;
        transition: opacity 0.3s;
    }

    .copytooltip .tooltiptext::after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    .copytooltip:hover .tooltiptext {
        visibility: visible;
        opacity: 0.5;
    }

    .dt-buttons {
        margin-top: 15px;
    }

    .form-control:disabled,
    .form-control[readonly] {
        background-color: rgba(255, 255, 255, 0.2) !important;
    }
</style>
<!--Tree css-->
<link href="assets\css\tree\style.css?v={{time()}}" rel="stylesheet">
<link href="assets\css\tree\jquery.orgchart.css?v={{time()}}" rel="stylesheet">
<style>
    .node-empty {
        border: 0 !important;
        padding: 0 !important;
    }

    .orgchart {
        background-image: none;
    }

    .orgchart .node .title {
        min-width: 130px;
        width: auto;
    }
</style>
@endsection
@section('content')


<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Member List</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase"><i class="fa fa-user"></i> Referral Link</div>
            <div class="card-body">
                @include('shareview.linkref')
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase"><i class="fa fa-user"></i> Search</div>
            <div class="card-body">
                <div id="edit-panel" class="view-state card"
                    style="background:#fff0;box-shadow: 0 2px 0px rgba(0, 0, 0, 0);">
                    <div class="row">
                        <div class="col-xl-2 col-md-1 col-sm-0"></div>
                        <div class="col-xl-8 col-md-10 col-sm-12">
                            <div class="block">
                                <div class="block-content">
                                    <div class="form-group mb-3">
                                        <div class="input-group">
                                            <input type="text" id="selected-node" placeholder="Please enter User ID"
                                                class="form-control m-2">
                                            <div class="input-group-append m-2">
                                                <button type="button" id="btn-report-path"
                                                    class="btn btn-rounded btn-noborder btn-stk min-width-125 mr-2"><i
                                                        class="fa fa-search" aria-hidden="true"></i> Search</button>
                                                <button type="button" id="btn-reset"
                                                    class="btn btn-rounded btn-noborder btn-light min-width-125 text-white"><i
                                                        class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-md-12">
                <div id="chart-container"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="showInfo">
    <div class="modal-dialog">
        <div class="modal-content" id="info" style="background: #1f2429e6;
									border: 0.7px #e09000 solid;">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" style="padding: 5px;
								text-align: center;
								border-bottom: 1px #e09000 solid;
								border-radius: 0;">Information</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="">UserID</label>
                        <input type="text" id="userid" value="" class="form-control" readonly>
                    </div>
                    <div class="col-md-12 mt-3">
                        <label for="">UserName</label>
                        <input type="text" id="username" value="" class="form-control" readonly>
                    </div>
                    <div class="col-md-12 mt-3">
                        <label for="">Left Total Trade(Day)</label>
                        <input type="text" name="left" id="left" value="" class="form-control" readonly>
                    </div>
                    <div class="col-md-12 mt-3">
                        <label for="">Right Total Trade(Day)</label>
                        <input type="text" name="right" id="right" value="" class="form-control" readonly>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-rounded btn-noborder btn-light text-white min-width-125 mb-10"
                    style="margin:auto" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal" id="addChild">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #1f2429e6;
									border: 0.7px #e09000 solid;">

            <div class="modal-body">
                <form class="js-validation-signup" action="{{ route('system.user.addUserBinary') }}" method="POST"
                    id="sign-up-form">
                    @csrf
                    <input type="hidden" id="node_side" name="node_side">
                    <div class="block block-themed block-rounded block-shadow">
                        <div class="block-header bg-gd-emerald">
                            <h3 class="block-title " style="padding: 5px;
                                text-align: center;
                                color:#e09000;
								border-bottom: 1px #e09000 solid;
								border-radius: 0;">Please add your details</h3>
                        </div>
                        <div class="block-content">
                            {{-- <div class="form-group">
                                <div class="col-12">
                                    <label for="signup-username">Username</label>
                                    <input type="text" class="form-control" id="signup-username" name="UserName" value=""
                                        placeholder="User Name" required>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <div class="col-12">
                                    <label for="signup-email">Email</label>
                                    <input type="email" class="form-control" id="signup-email" placeholder="Email"
                                        value="" name="Email" required>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <div class="col-12">
                                    <label for="signup-password">Password</label>
                                    <input type="password" class="form-control" id="signup-password"
                                        placeholder="********" name="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-12">
                                    <label for="signup-password-confirm">Password
                                        Confirmation</label>
                                    <input type="password" class="form-control" id="signup-password-confirm"
                                        placeholder="********" name="Re-Password">
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <div class="col-12">
                                    <label for="">Sponsor</label>
                                    <input type="text" class="form-control" name="parent" id="parent" value=""
                                        autocomplete="off" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-12">
                                    <label for="">Presenter</label>
                                    <input type="text" name="brother" id="brother" class="form-control" value=""
                                        autocomplete="off" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-12">
                                    <div class="input-group mb-15">
                                        <input type="text" id="ref_link" onclick="copyLinkRefTree()"
                                            class="form-control" placeholder="Link Ref">
                                        <span class="input-group-btn">
                                            <button style="height: 100%;" type="button" id="tooltiptext"
                                                class="copytooltip btn btn-rounded btn-noborder btn-success btn-anim weight-600"
                                                onclick="copyLinkRefTree()">Copy</button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-0" style="border-top: 1px #e09000 solid;
												padding-top: 15px;">
                                <div class="col-sm-3 text-center">
                                </div>
                                <div class="form-group text-center">

                                    <button type="submit"
                                        class="btn btn-rounded btn-noborder btn-stk min-width-125 mb-10">
                                        <i class="fa fa-plus mr-10"></i> Add User
                                    </button>

                                    <button type="button"
                                        class="btn btn-rounded btn-noborder btn-light min-width-125 mb-10 text-white"
                                        data-dismiss="modal"><i class="fa fa-times"></i> Close</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

@endsection
@section('script')

<script src="orgchart/jquery.orgchart.js?v={{time()}}"></script>
<script>
    var sponsor = '{{ Session('user')->User_ID}}';
</script>
<script type="text/javascript">
    $(function() {
    var datascource = {!! $list !!};
    var oc = $('#chart-container').orgchart({
        'data' : datascource,
        'nodeContent': 'title',
        'visibleLevel': 2,
        'createNode': function($node, data) {
        	$node.on('click', '.edge', function (event) {
            if ($(event.target).is('.fa-chevron-down')) {
                showDescendents(this, 1);
            //   showDescendents(this, 2);
            }
          });
        }
    });
    var showDescendents = function(node, depth) {
      if (depth === 1) {
        return false;
      }
      $(node).closest('tr').siblings(':last').children().find('.node:first').each(function(index, node) {
        var $temp = $(node).closest('tr').siblings().removeClass('hidden');
        var $children = $temp.last().children().find('.node:first');
        if ($children.length) {
          $children[0].style.offsetWidth = $children[0].offsetWidth;
        }
        $children.removeClass('slide-up');
        showDescendents(node, depth--);
      });
    };


    // var showDescendents = function(node, visibleLevel) {
    //   if (visibleLevel === 1) {
    //     return false;
    //   }
    //   $(node).closest('tr').siblings(':last').children().find('.node:first').each(function(index, node) {
    //     var $temp = $(node).closest('tr').siblings().removeClass('hidden');
    //     var $children = $temp.last().children().find('.node:first');
    //     if ($children.length) {
    //     $children[0].style.offsetWidth = $children[0].offsetWidth;
    //   }
    //   $children.removeClass('slide-up');
    //   showDescendents(node, visibleLevel--);
    //   });
    // };

    oc.$chartContainer.on('click', '.node', function() {
    	
        if($(this).hasClass('node-tree')){
            console.log($(this).attr('id'), $('#datefrom').val(), $('#dateto').val());
            $.ajax({
                type: "GET",
                url: "{{ route('system.getAjaxSaleUser')}}",
                data: {
                    'User_ID': $(this).attr('id'),
                    'datefrom': $('#datefrom').val(),
                    'dateto': $('#dateto').val(),
                },
                success: function (data) {
                    console.log(',ttt',data);
                    if(data.status == 200){
                        console.log(data.infor);
                        console.log(data.trade);

                        $('#showInfo #userid').val(data.infor.User_ID);
                        $('#showInfo #username').val(data.infor.User_Email);
                        $('#left').val(data.trade.leftTrade ? '$ '+data.trade.leftTrade : '$ 0.00');
                        $('#right').val(data.trade.rightTrade ? '$ '+data.trade.rightTrade : '$ 0.00');
                    }
                    
                    console.log(data);
                }
            });
            $('#showInfo').modal('show');

        }
        else{
            
            if($(this).hasClass('left')){
                $node_side = 0;
            }
            if($(this).hasClass('right')){
                $node_side = 1;
            }
            presenter = $(this).attr('data-parent');
            $('#addChild').modal('show');
            $('#parent').val(sponsor);
            $('#brother').val($(this).attr('data-parent'));
            $('#node_side').val($node_side);
            $('#ref_link').val("{{route('getRegister')}}?ref="+sponsor+"&presenter="+presenter+"&node="+$node_side);
            //console.log("{{route('getRegister')}}?ref="+sponsor+"&presenter="+presenter+"&node="+$node_side);
        }


        var $this = $(this);
      
    });

    $('#btn-report-path').on('click', function() {
        $('#chart-container').find('.hidden').removeClass('hidden').end().find('.slide-up, .slide-right, .slide-left, .focused').removeClass('slide-up slide-right slide-left focused');
        var val_search = isNaN($('#selected-node').val());
        var get_val = $('#selected-node').val().toUpperCase();
        setTimeout(function(){

            if(val_search == true){
            $('.'+get_val).addClass('focused');
            console.log('string');
            console.log($('.'+get_val));
            }else{
                console.log(get_val);
                $('#'+get_val).addClass('focused');
                console.log('number');

            }
            
            var $selected = $('#chart-container').find('.node.focused');
            if ($selected.length) {
                $selected.parents('.nodes').children(':has(.focused)').find('.node:first').each(function(index, superior) {
                if (!$(superior).find('.horizontalEdge:first').closest('table').parent().siblings().is('.hidden')) {
                    $(superior).find('.horizontalEdge:first').trigger('click');
                }
                });
            } else {
                alert('Data does not exist');
            }
        }, 1);
        
    });
    $('#btn-reset').on('click', function() {
        oc.hideChildren(oc.$chart.find('.node:first'));
    //   $('#chart-container').find('.hidden').removeClass('hidden')
    //     .end().find('.slide-up, .slide-right, .slide-left, .focused').removeClass('slide-up slide-right slide-left focused');
      $('#selected-node').val('');
    });
  });
</script>
<script>
    $(document).ready(function () {
        $('.node-empty .title').css({
            'opacity' : 0,
            'height' : 0,
            'padding' : 0,
        });
        $('.node-empty i, .node-empty .content').hide();
        $('.node-empty').append('<img src="add-user-1.png" alt="" style="width: 50px;">');

    });
       
</script>
<script>
    function copyToClipboard() {
        var copyText = document.getElementById("linkRef");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        var tooltip = document.getElementById("tooltiptext");
        // tooltip.innerText = "Copied";
        alert(copyText.value);
    }
    function copyLinkRefTree() {
        var copyText = document.getElementById("ref_link");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        alert(copyText.value);
    }
    function hoverCopyTooltip() {
        var tooltip = document.getElementById("myTooltip");
        tooltip.innerHTML = "Copy";
    }
</script>


@endsection