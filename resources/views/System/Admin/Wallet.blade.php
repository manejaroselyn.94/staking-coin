@extends('System.Layouts.Master')
@section('title', 'Admin-Wallet')
@section('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!--THIS PAGE LEVEL CSS-->
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<!--REQUIRED THEME CSS -->
<link href="datetime/assets/css/themes/main_theme.css" rel="stylesheet" />
<link href="sweetalert/sweetalert.css" rel="stylesheet">
<style>
    .dtp-btn-cancel {
        background: #9E9E9E;
    }

    .dtp-btn-ok {
        background: #009688;
    }

    .dtp-btn-clear {
        color: black;
    }

    .btn-filler {
        margin-bottom: 10px;
    }

    .pagination {
        float: right;
    }

    .bg-section-2 {
        position: relative;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        border-radius: 0 !important;
        color: #fff !important;
        padding: 8px 10px !important;
        margin-bottom: 10px !important;
        margin-right: 5px !important;
        display: inline-block !important;
        text-align: center !important;
        vertical-align: baseline !important;
        white-space: nowrap !important;
        background: #4aa23c !important;
        border: none !important;
        line-height: 10px !important;
        font-size: 12px !important;

    }

    .page-title {
        color: #fff;
        padding: 15px 0;
    }

    .select2-container--default .select2-selection--multiple {
        background: #fff0;
        border: 0;
        border-bottom: 1px solid #10b5ea;
        border-radius: 0;
    }

    .select2-container--default .select2-results>.select2-results__options {
        background: #002c69;
    }

    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border: 1px solid #10b5ea;
        background: #fff0;
    }
</style>
@endsection
@section('content')
<div class="row">
	@if(Session('user')->User_Level == 1)
    <div class="col-md-4">
        <form method="POST" id="post-deposit" action="{{route('system.admin.postDepositAdmin')}}">
            @csrf
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><i class="fa fa-users"></i>
                                        User ID</label>
                                    <input type="text" class="form-control" name="user" id="exampleInputEmail1"
                                        placeholder="Enter User ID">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><i class="fa fa-money"></i>
                                        Amount USD</label>
                                    <input type="number" step="any" name="amount" class="form-control"
                                        placeholder="Enter Token">
                                </div>
                                <label><i class="fa fa-hand-o-down"></i> Currency</label>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <select class="form-control c-select" name="coin">
                                            <option value="5">USD</option>
                                            <option value="1">BTC</option>
                                            <option value="2">ETH</option>
                                            <option value="8">SKC</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><i class="fa fa-money"></i>
                                        Transaction Hash (Optional)</label>
                                    <input type="text" name="hash"
                                        class="form-control" placeholder="Enter Transaction Hash">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><i class="fa fa-money"></i>
                                        Rate (Optional)</label>
                                    <input type="number" step="any" name="rate"
                                        class="form-control" placeholder="Enter Rate">
                                </div>
                                <div class="m-t-43">
                                    <button type="submit" class="btn btn-stk" id="btn-deposit"><i
                                            class="fa fa-paper-plane" aria-hidden="true"></i>
                                        Deposit</button>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    @endif
    <div class="col-md-8">
        <form method="GET" action="{{route('system.admin.getWallet')}}">
            @csrf
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1">ID</label>
                                            <input type="text" name="id" class="form-control"
                                                placeholder="Enter ID" value="{{request()->input('id')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-user" aria-hidden="true"></i> User
                                                ID</label>
                                            <input type="text" name="user_id" class="form-control"
                                                placeholder="Enter User ID" value="{{request()->input('user_id')}}">
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left"><i class="fa fa-calendar"
                                                    aria-hidden="true"></i>
                                                From</label>
                                            <input type='text' name="datefrom" id="datefrom" class="form-control"
                                                value="{{request()->input('datefrom')}}" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i>
                                                Status</label>
                                            <select name="status" class="form-control">
                                                <option value="">--- Select ---</option>
                                                <option value="0"
                                                    {{request()->input('status') == 0 && request()->input('status') != '' ? 'selected' : ''}}>
                                                    Pending</option>
                                                <option value="2" {{request()->input('status') == 2 ? 'selected' : ''}}>
                                                    view</option>
                                                <option value="1" {{request()->input('status') == 1 ? 'selected' : ''}}>
                                                    Confirmed</option>
                                                <option value="-1"
                                                    {{request()->input('status') == -1 ? 'selected' : ''}}>
                                                    Canceled</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10 text-left"><i class="fa fa-calendar"
                                                    aria-hidden="true"></i>
                                                To</label>
                                            <input type='text' name="dateto" id="dateto" class="form-control"
                                                value="{{request()->input('dateto')}}" />
                                        </div>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputuname_1"><i
                                                    class="fa fa-chevron-down" aria-hidden="true"></i>
                                                Action</label>
                                            <div class="form-group">
                                                <select name="action[]" class="form-control select2-multi"
                                                    multiple="multiple">

                                                    <option value="">--- Select ---</option>
                                                    @foreach($action as $a)
                                                    <option value="{{$a->MoneyAction_ID}}"
                                                        {{(request()->input('action')) && array_search($a->MoneyAction_ID, request()->input('action'))  !== false ? 'selected' : ''}}>
                                                        {{$a->MoneyAction_Name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn-filler btn btn-lg1 btn-stk"><i
                                                        class="fa fa-search" aria-hidden="true"></i>
                                                    Search
                                                </button>
                                                <button type="submit" name="export" value="1"
                                                    class="btn-filler btn btn-lg1 btn-success  mr-10"><i
                                                        class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                    Export</button>
                                                <a href="{{ route('system.admin.getWallet') }}"
                                                    class="btn-filler btn btn-default mr-10">Cancel</a>
                                                @if(Session('user')->User_CanPayList == 1)
                                                <input type="hidden" name="password_user" id="password_user">
                                                <button type="button" id="btn-paymoney" class="btn-filler btn btn-lg1 btn-warning mr-10 float-right"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                    Pay Interset / Withdraw</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div>
                    <h3 class="panel-title txt-light"><i class="fa fa-table" aria-hidden="true"></i>
                        List Wallet Table</h3>
                </div>
            </div>
            <div class="panel-wrapper">
                <div class="panel-body">
                    <div class="table-wrap">
                        <div class="table-responsive">
                            <div style="clear:both"></div>
                            <table id="dttable-wallet" class="table table-striped table-bordered table-responsive"
                                cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            ID
                                        </th>
                                        <th data-hide="phone">
                                            LEVEL
                                        </th>
                                        <th data-hide="phone">
                                            USER ID
                                        </th>
                                        <th data-hide="phone">
                                            AMOUNT USD
                                        </th>
                                        <th data-hide="phone">
                                            AMOUNT TOKEN
                                        </th>
                                        <th data-hide="phone">
                                            FEE
                                        </th>
                                        <th data-hide="phone">
                                            RATE
                                        </th>
                                        <th data-hide="phone">
                                            CURRENCY
                                        </th>
                                        <th data-hide="phone">
                                            ACTION
                                        </th>
                                        <th data-hide="phone">
                                            COMMENT
                                        </th>
                                        <th data-hide="phone">
                                            TIME
                                        </th>
                                        <th data-hide="phone">
                                            STATUS
                                        </th>
                                        <th data-hide="phone">
                                            ACTION
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($walletList as $item)
                                    <tr>
                                        @if($item->User_Level == 0)
                                        <td>{{$item->Money_ID}}</td>
                                        @elseif($item->User_Level == 1)
                                        <td class="bg-success">{{$item->Money_ID}}</td>
                                        @elseif($item->User_Level == 2)
                                        <td class="bg-info">{{$item->Money_ID}}</td>
                                        @else
                                        <td class="bg-warning">{{$item->Money_ID}}</td>
                                        @endif
                                        <td>{{$level[$item->User_Level]}}</td>
                                        <td>{{$item->Money_User}}</td>
                                        <td>
                                            {{number_format($item->Money_Currency == 8 || $item->Money_Currency == 10 || $item->Money_Currency == 9 && ($item->Money_MoneyAction == 21 || $item->Money_MoneyAction == 19 || $item->Money_MoneyAction == 20) ? $item->Money_USDT*$item->Money_Rate : $item->Money_USDT, 2)}}
                                        </td>
                                        <td>
	                                        {{number_format($item->Money_Currency == 8 || $item->Money_Currency == 10 || $item->Money_Currency == 9 && ($item->Money_MoneyAction == 21 || $item->Money_MoneyAction == 19 || $item->Money_MoneyAction == 20) ? $item->Money_USDT : $item->Money_USDT/$item->Money_Rate, 2)}}
	                                        
                                            {{--number_format($item->Money_Currency == 8 || $item->Money_Currency == 10 || $item->Money_Currency == 9 && $item->Money_MoneyAction != 18 ? $item->Money_USDT : $item->Money_USDT / $item->Money_Rate, 4)--}}
                                        </td>
                                        <td>{{number_format($item->Money_USDTFee, 2)}}</td>
                                        <td>{{number_format($item->Money_Rate, 3)}}</td>
                                        <td>{{$item->Currency_Symbol}}</td>
                                        <td>{{$item->MoneyAction_Name}}</td>
                                        <td>{{$item->Money_Comment}}</td>
                                        <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                        <td>
                                            @if($item->Money_MoneyStatus == 1)
                                            @if(($item->Money_MoneyAction == 2 || $item->Money_MoneyAction == 18 || $item->Money_MoneyAction == 21) &&
                                            $item->Money_Confirm == 0)
                                            <span class="badge badge-warning">Pending</span>

                                            @else
                                            <span class="badge badge-success">Confirmed</span>
                                            @endif
                                            @elseif($item->Money_MoneyStatus == 2)
                                            <span class="badge badge-warning">View</span>
                                            @else

                                            <span class="badge badge-danger">Canceled</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-rounded btn-primary btn-xs"
                                                href="{{ route('system.admin.getWalletDetail', $item->Money_ID) }}">Detail</a>
                                                
			                                @if((Session('user')->User_Level == 1 || Session('user')->User_Level == 2) && $item->Money_MoneyStatus == 1)
			                                @if((($item->Money_MoneyAction == 2 || $item->Money_MoneyAction == 18 || $item->Money_MoneyAction == 21) && $item->Money_Confirm == 0 ))
			                                	@if($item->Money_Currency == 8)
					                                <button type="button" name="confirm" value="1" class="btn btn-rounded btn-info btn-xs btn-submit-confirm" data-address="{{ $item->Money_Address }}" data-id="{{ $item->Money_ID }}" data-amount="{{ $item->Money_CurrentAmount }}">Send Token</button>
			                                	@else
			                                		<button type="button" data-value="{{$item->Money_ID}}" class="btn btn-rounded btn-success btn-xs btn-confirm">Send {{$item->Money_Currency == 2 ? 'ETH' : 'TRX'}}</button>
				                                @endif
			                                @endif
			                                @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$walletList->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js">
</script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js">
</script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datefrom').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });

  $('#dateto').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
</script>
<script>
    var e=$("#demo-foo-col-exp");
    $("#demo-input-search2").on("input",function(o){o.preventDefault(),e.trigger("footable_filter",{filter:$(this).val()})})
</script>
<script>
    var today = new Date();
    var currentDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    $('#revenue-product').DataTable({
    dom: 'Bfrtip',
    "order": [[ 7, "desc" ]],
    buttons: [
    {
    extend: 'excelHtml5',
    title: "Wallet-"+currentDate
    }
    ]
    });
    $('#dttable-wallet').DataTable({
          "bLengthChange": false,
        "searching": false,
          "paging": false,
          "order": [0,'desc']
      });
    $('#post-deposit').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
    });
</script>
<script>
    $('#Money_Time').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD HH:mm:ss', time: true, clearButton: true });
    $('#Money_Confirm_Time').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD HH:mm:ss', time: true, clearButton: true });
    
</script>
<script src="sweetalert/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
	var base_url = window.location.origin + "/";
    $(".select2-multi").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    })
    
	@if(Session('user')->User_CanPayList == 1)
    $('#btn-paymoney').click(function(){
	    _buttonClick = $(this);
	    swal({
			title: 'Confirm Pay ALL',
			text: 'Type your password to confirm pay all!',
			type: 'input',
			inputType: 'password',
			inputPlaceholder: "Write something",
			name: 'txtOTP',
			showCancelButton: true,
			confirmButtonText: 'Submit',
			showLoaderOnConfirm: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-cancel'
			}, function (confirm) {
// 				console.log(confirm);
				_buttonClick.after("<input type='hidden' name='password_user' value='"+confirm+"'>");
				_buttonClick.after("<input type='hidden' name='paylist' value='1'>");
				_buttonClick.parents('form').submit();
			});
    });
    @endif
	$('.btn-confirm').click(function(){
		_id = $(this).attr("data-value");
		swal({
			title: 'Confirm Send Coin',
			text: 'Are you sure confirm this action!',
			showCancelButton: true,
			confirmButtonText: 'Submit',
			showLoaderOnConfirm: true,
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-cancel'
			}, function (confirm) {
				if(confirm === true){
					$.ajax({
						url: base_url+"system/admin/confirm/"+_id,
						type: 'GET',
						success: function (data) {
							console.log(data);
							if(data.status == true){
							    toastr.success(data.message, 'Success!', {timeOut: 3500})
								
								$('.label-status button[data-value = "'+_id+'"]').toggleClass('btn-warning btn-success');
								$('.label-status button[data-value = "'+_id+'"]').html('Success');
								$('.btn-confirm[data-value = "'+_id+'"]').hide();
							}else{
								swal({
									title: 'Error',
									text: data.message,
									type: 'error',
									confirmButtonClass: 'btn btn-confirm',
									allowOutsideClick: false
								});
							}
						}
					});
				}
	    	});
	});
</script>
@endsection