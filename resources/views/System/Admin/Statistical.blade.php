@extends('System.Layouts.Master')
@section('title', 'Admin Statistic')
@section('css')
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />

<style>
    a:hover {
        cursor: pointer;
    }
</style>

<!--THIS PAGE LEVEL CSS-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<!--REQUIRED THEME CSS -->
<link href="datetime/assets/css/themes/main_theme.css" rel="stylesheet" />
<style>
    .dtp-btn-cancel {
        background: #9E9E9E;
    }

    .dtp-btn-ok {
        background: #009688;
    }

    .dtp-btn-clear {
        color: black;
    }

    .btn-filler {
        margin-bottom: 10px;
    }

    .pagination {
        float: right;
    }
</style>
@endsection
@section('content')
<form method="GET" action="">
    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label mb-10" for="exampleInputpwd_1"><i class="fa fa-users"
                        aria-hidden="true"></i> User ID</label>
                <input type="number" class="form-control" placeholder="User ID" name="User_ID"
                    value="{{request()->input('User_ID')}}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i>
                    From</label>
                <input id="datefrom" type="text" class="form-control" placeholder="yyyy/mm/dd" name="from"
                    value="{{request()->input('from')}}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label mb-10" for="exampleInputuname_1"><i class="fa fa-chevron-down"
                        aria-hidden="true"></i>
                    Level</label>
                <select type="number" class="form-control" name="User_Level">
                    <option value="" selected>--- Select ---</option>
                    <option value="0" {{request()->input('User_Level') == '0' ? 'selected' : ''}}>
                        User</option>
                    <option value="1" {{request()->input('User_Level') == '1' ? 'selected' : ''}}>
                        Admin</option>
                    <option value="2" {{request()->input('User_Level') == '2' ? 'selected' : ''}}>
                        Finance</option>
                    <option value="4" {{request()->input('User_Level') == '4' ? 'selected' : ''}}>
                        Customer</option>
                    <option value="3" {{request()->input('User_Level') == '3' ? 'selected' : ''}}>
                        Support</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label mb-10 text-left"><i class="fa fa-calendar" aria-hidden="true"></i>
                    To</label>
                <input id="dateto" type="text" class="form-control" placeholder="yyyy/mm/dd" name="to"
                    value="{{request()->input('to')}}">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <div class="form-actions mt-10">
                    {{--                                                        <button type="submit" class="btn btn-lg1 btn-success  mr-10"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</button>--}}
                    <button type="submit" class="btn-filler btn btn-stk  mr-10"><i class="fa fa-search"
                            aria-hidden="true"></i>
                        Search</button>
                        
                    <button type="button" id="exportTest" class="btn-filler btn btn-success  mr-10"><i
                            class="fa fa-file-excel-o" aria-hidden="true"></i>
                        Export</button>

                    <a href="{{ route('system.admin.getStatistical') }}"
                        class="btn-filler btn btn-light mr-10 text-white">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="dt-responsive demo-foo-col-exp table table-striped table-bordered table-responsive"
                cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" class=""
                            style="text-align: center;vertical-align: middle; border: 1px solid;background: linear-gradient(to top, #f5b61a, #f58345)!important;">
                            User ID
                        </th>
                        <th rowspan="2" style="text-align: center;vertical-align: middle; border: 1px solid;">Level</th>
                        <th colspan="4" class="" style="text-align: center;vertical-align: middle; border: 1px solid;">
                            Balance
                        </th>
                        <th colspan="5" style="text-align:center; border: 1px solid;" class="border-right">
                            Deposit</th>
                        <th colspan="3" style="text-align:center; border: 1px solid;" class="border-right">
                            Withdraw</th>
                        <th colspan="2" class="border-right"
                            style="text-align: center;vertical-align: middle; border: 1px solid;">
                            Send Interest</th>
                        <th colspan="2" class="border-right"
                            style="text-align: center;vertical-align: middle; border: 1px solid;">
                            Investment</th>
                        <th colspan="3" class="border-right"
                            style="text-align: center;vertical-align: middle; border: 1px solid;">
                            Interest</th>
                        <th colspan="3" class="border-right"
                            style="text-align: center;vertical-align: middle; border: 1px solid;">
                            Commission</th>
                    </tr>
                    <tr>
                        <!-- balance -->
                        <th class="text-right border-right">USDT</th>
                        <th class="text-right border-right">TRX</th>
                        <th class="text-right border-right">SKC</th>
                        <th class="text-right border-right">SKC Profit</th>
                        <!-- deposit -->
                        <th class="text-right border-right">BTC</th>
                        <th class="text-right border-right">ETH</th>
                        <th class="text-right border-right">USDT</th>
                        <th class="text-right border-right">TRX</th>
                        <th class="text-right border-right">SKC</th>
                        <!-- withdraw -->
                        <th class="text-right border-right">ETH</th>
                        <th class="text-right border-right">TRX</th>
                        <th class="text-right border-right">SKC</th>
                        <!-- send interest -->
                        <th class="text-right border-right">ETH</th>
                        <th class="text-right border-right">TRX</th>
                        <!-- Investment -->
                        <th class="text-right">USDT</th>
                        <th class="text-right border-right">SKC</th>
                        <!-- Interest -->
                        <th class="text-right">USDT</th>
                        <th class="text-right">SKC Profit</th>
                        <th class="text-right border-right">SKC</th>
                        <!-- Commission -->
                        <th class="text-right">Direct</th>
                        <th class="text-right">Binary</th>
                        <th class="text-right border-right">Affiliate</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-weight: bold;">
                        <td colspan="2" class="text-center"><b> Total</b></td>
                        <td class="text-right">{{ $totalBalance['BalanceUSD'] }}</td>
                        <td class="text-right">{{ $totalBalance['BalanceTRX'] }}</td>
                        <td class="text-right">{{ $totalBalance['BalanceSKC'] }}</td>
                        <td class="text-right">{{ $totalBalance['BalanceSKCProfit'] }}</td>

                        <td class="text-right">{{ $Total->DepositBTC }}</td>
                        <td class="text-right">{{ $Total->DepositETH }}</td>
                        <td class="text-right">{{ $Total->DepositUSD }}</td>
                        <td class="text-right">{{ $Total->DepositTRX }}</td>
                        <td class="text-right">{{ $Total->DepositSKC }}</td>

                        <td class="text-right">{{ $Total->WithDrawETH }}</td>
                        <td class="text-right">{{ $Total->WithDrawTRX }}</td>
                        <td class="text-right">{{ $Total->WithDrawSKC }}</td>

                        <td class="text-right">{{ $Total->SendInterestETH }}</td>
                        <td class="text-right">{{ $Total->SendInterestTRX }}</td>
                        
                        <td class="text-right">{{ $Total->InvestmentUSD }}</td>
                        <td class="text-right">{{ $Total->InvestmentSKC }}</td>

                        <td class="text-right">{{ $Total->Interest }}</td>
                        <td class="text-right">{{ $Total->InterestSKCProfit }}</td>
                        <td class="text-right">{{ $Total->InterestSKC }}</td>

                        <td class="text-right">{{ $Total->DirectCommission }}</td>
                        <td class="text-right">{{ $Total->BinaryCommission }}</td>
                        <td class="text-right">{{ $Total->AffiliateCommission }}</td>
                    </tr>
                    @foreach($Statistic as $statistic)
                    <tr>
                        <td
                            style="{{App\Model\User::where('User_ID', $statistic->Money_User)->whereIn('User_Level', [1,2,3,4])->first() != null ? 'background-color:pink' : ''}}">
                            {{ $statistic->Money_User }}</td>
						<td>{{$level[$statistic->User_Level]}}</td>
                        <td class="text-right">{{ $statistic->BalanceUSD }}</td>
                        <td class="text-right">{{ $statistic->BalanceTRX }}</td>
                        <td class="text-right">{{ $statistic->BalanceSKC }}</td>
                        <td class="text-right">{{ $statistic->BalanceSKCProfit }}</td>

                        <td class="text-right">{{ $statistic->DepositBTC }}</td>
                        <td class="text-right">{{ $statistic->DepositETH }}</td>
                        <td class="text-right">{{ $statistic->DepositUSD }}</td>
                        <td class="text-right">{{ $statistic->DepositTRX }}</td>
                        <td class="text-right">{{ $statistic->DepositSKC }}</td>

                        <td class="text-right">{{ $statistic->WithDrawETH }}</td>
                        <td class="text-right">{{ $statistic->WithDrawTRX }}</td>
                        <td class="text-right">{{ $statistic->WithDrawSKC }}</td>

                        <td class="text-right">{{ $statistic->SendInterestETH }}</td>
                        <td class="text-right">{{ $statistic->SendInterestTRX }}</td>
                        
                        <td class="text-right">{{ $statistic->InvestmentUSD }}</td>
                        <td class="text-right">{{ $statistic->InvestmentSKC }}</td>

                        <td class="text-right">{{ $statistic->Interest }}</td>
                        <td class="text-right">{{ $statistic->InterestSKCProfit }}</td>
                        <td class="text-right">{{ $statistic->InterestSKC }}</td>

                        <td class="text-right">{{ $statistic->DirectCommission }}</td>
                        <td class="text-right">{{ $statistic->BinaryCommission }}</td>
                        <td class="text-right">{{ $statistic->AffiliateCommission }}</td>

                    </tr>
                    @endforeach

                </tbody>
            </table>
            {{--$Statistic->appends(request()->input())->links()--}}
        </div>
    </div>
</div>
@endsection
@section('script')


<script src="jquery-table2excel/dist/jquery.table2excel.min.js"></script>

<script>
    $(function() {
                $('#exportTest').click(function(){
                    $(".demo-foo-col-exp").table2excel({
                        exclude: ".noExl",
                        name: "Statistical",
                        filename: "Statistical" + new Date().toISOString().replace(/[\-\:\.]/g, "")+".xls",
                        fileext: ".xls",
                        exclude_img: true,
                        exclude_links: true,
                        exclude_inputs: true
                    });
                    
                });
            });
</script>


<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js">
</script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js">
</script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datefrom').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
        
      $('#dateto').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
</script>
@endsection