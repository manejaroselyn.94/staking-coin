@extends('System.Layouts.Master')
@section('title', 'Admin User')
@section('css')
<style>
    a:hover {
        cursor: pointer;
    }

    .btn-filler {
        margin-bottom: 10px;
    }
</style>

<!--THIS PAGE LEVEL CSS-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<!--REQUIRED THEME CSS -->
<link href="datetime/assets/css/themes/main_theme.css" rel="stylesheet" />
<style>
    .dtp-btn-cancel {
        background: #9E9E9E;
    }

    .dtp-btn-ok {
        background: #009688;
    }

    .dtp-btn-clear {
        color: black;
    }

    .edit-email-input {
        padding-left: 10px;
        border: 2px dotted #f5b61a;
        width: 79%;
    }.table-responsive {
      display: table;}
</style>
@endsection
@section('content')
<!-- /Title -->
<div class="row">
    <div class="col-md-12">
        <form method="GET" action="{{route('system.admin.getMemberListAdmin')}}">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper">
                    <div class="panel-body">
                        <div class="form-wrap">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-user" aria-hidden="true"></i> User
                                                ID</label>
                                            <input class="form-control" type="text" placeholder="User ID"
                                                value="{{request()->input('UserID')}}" name="UserID">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i>
                                                Email</label>
                                            <input class="form-control" type="text" placeholder="Email"
                                                value="{{request()->input('Email')}}" name="Email">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i> Created
                                                Date</label>
                                            <input type="text" class="form-control" placeholder="Registration Time"
                                                name="datetime" id="datetime"
                                                value="{{request()->input('datetime')}}" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i>
                                                Sponsor</label>
                                            <input class="form-control" type="text" placeholder="Sponsor"
                                                value="{{request()->input('sponsor')}}" name="sponsor">
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i> Binary Tree</label>
                                            <input class="form-control" type="text" placeholder="Tree"
                                                value="{{request()->input('tree')}}" name="tree">
                                        </div>
                                    </div>
                                    {{--<div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i> Sun Tree</label>
                                            <input class="form-control" type="text" placeholder="Tree"
                                                value="{{request()->input('suntree')}}" name="suntree">
                                        </div>
                                    </div>--}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i> Status
                                                Mail</label>
                                            <select id="inputState" class="form-control" name="status_email">
                                                <option selected value=""
                                                    {{request()->input('status_email') == '' ? 'selected' : ''}}>
                                                    --- Select ---</option>
                                                <option value="1"
                                                    {{request()->input('status_email') == '1' ? 'selected' : ''}}>
                                                    Active</option>
                                                <option value="0"
                                                    {{request()->input('status_email') == '0' ? 'selected' : ''}}>
                                                    Not Active</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i>
                                                Level</label>
                                            <select id="inputState" class="form-control" name="level">
                                                <option selected value=""
                                                    {{request()->input('level') == '' ? 'selected' : ''}}>
                                                    --- Select ---</option>
                                                @foreach ($user_level as $item)
                                                <option value="{{$item->User_Level_ID}}"
                                                    {{request()->input('level') == "$item->User_Level_ID" ? 'selected' : ''}}>
                                                    {{$item->User_Level_Name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                    class="fa fa-users" aria-hidden="true"></i> Agency
                                                Level</label>
                                            <select id="inputState" class="form-control" name="agency_level">
                                                <option selected value=""
                                                    {{request()->input('agency_level') == '' ? 'selected' : ''}}>
                                                    --- Select ---</option>
                                                @foreach ($user_agency_level as $item)
                                                <option value="{{$item->user_agency_level_ID}}"
                                                    {{request()->input('agency_level') == "$item->user_agency_level_ID" ? 'selected' : ''}}>
                                                    {{$item->user_agency_level_Name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-actions mt-10">
                                            <button type="submit"
                                                class="btn-filler btn btn-lg1 btn-stk waves-effect"><i
                                                    class="fa fa-search" aria-hidden="true"></i>
                                                Search
                                            </button>
                                            <a href="{{ route('system.admin.getMemberListAdmin') }}"
                                                class="btn-filler btn btn-default mr-10">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>

</div>

<!-- Row -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default card-view">
            <div class="panel-heading">
                <div class="">
                    <h6 class="panel-title txt-light"><i class="fa fa-table" aria-hidden="true"></i>
                        List User</h6>
                </div>
            </div>

            <div class="panel-wrapper">
                <div class="panel-body">
                    <div class="table-wrap">
                        <div class="table-responsive">

                            {{$user_list->appends(request()->input())->links()}}
                            <div style="clear:both"></div>
                            <table id="member-list-table"
                                class=" dt-responsive table table-striped table-bordered table-responsive"
                                cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th data-toggle="true">
                                            ID
                                        </th>
                                        <th>
                                            MAIL
                                        </th>
                                        <th>
                                            LEVEL
                                        </th>
                                        <th data-hide="phone">
                                            REGISTERED DATE
                                        </th>
                                        <th data-hide="phone">
                                            PARENT
                                        </th>
                                        <th data-hide="phone,tablet">
                                            BINARY TREE
                                        </th>
                                        <th data-hide="phone">
                                            STATUS
                                        </th>
                                        <th data-hide="phone,tablet">
                                            ACTION
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($user_list as $v)
                                    <tr>
                                        <td>
                                            <p>{{ $v->User_ID }}</p>
                                        </td>
                                        <td>
                                            <span id="">{{$v->User_Email}}</span>
                                            
                                        </td>
                                        <td>
                                                {{$level[$v->User_Level]}}
                                        </td>
                                        <td>{{ $v->User_RegisteredDatetime }}</td>
                                        <td>{{ $v->User_Parent }}</td>
                                        <td width="200px">
                                            <div style="overflow:auto;width:300px!important;height:60px">
                                                {{ str_replace(',',', ', $v->User_Tree) }}</div>
                                        </td>
                                        <td>
                                            @if($v->User_EmailActive == 0)
                                            <span class="badge badge-danger r-3 blink">Not
                                                Active</span>
                                            @else
                                            <span class="badge badge-success r-3">Active</span>
                                            @endif
                                            @php
                                            $enableKYC = App\Model\Profile::where('Profile_User',
                                            $v->User_ID)->where('Profile_Status', 1)->first();
                                            @endphp
                                            @if(isset($enableKYC))
                                            <span class="badge badge-success r-3">KYC ON</span>
                                            @else
                                            <span class="badge badge-danger r-3">KYC OFF</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($v->User_EmailActive == 0)
                                            <a href="{{ route('system.admin.getActiveMailMember', $v->User_ID) }}"
                                                class="bt-loginID btn btn-success btn-xs waves-effect waves-light"
                                                data-toggle="tooltip"><i class="fa fa-check"> Active mail</i></a>
                                            <a href="{{ route('system.admin.getResentMailActive', $v->User_ID) }}"
                                                class="bt-loginID btn btn-info btn-xs waves-effect waves-light"
                                                data-toggle="tooltip"><i class="fa fa-check"> Resent mail active</i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{$user_list->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<script>
	@if(Session('user')->User_Level != 1)
	$('.r-view').remove();
	@endif
    $('#member-list-table').DataTable({
        "bLengthChange": false,
        "searching": false,
        "paging": false
        });
</script>

<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js">
</script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js">
</script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datetime').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', clearButton: true, time: false });
</script>
@endsection