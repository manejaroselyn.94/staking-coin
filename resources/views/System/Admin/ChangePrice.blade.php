@extends('System.Layouts.Master')
@section('title', 'Admin Withdraw')
@section('css')
<meta name="_token" content="{!! csrf_token() !!}" />
<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
<link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />

<!--THIS PAGE LEVEL CSS-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<!--REQUIRED THEME CSS -->
<link href="datetime/assets/css/themes/main_theme.css" rel="stylesheet" />
<style>
    a:hover {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Change price token</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header text-uppercase"> Add price</div>
            <div class="card-body">
                <form method="post" action="{{route('system.admin.postAddPriceToken')}}">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1"><i class="fa fa-users"></i>
                          Price</label>
                        <input type="text" class="form-control" name="price"
                               id="exampleInputEmail1" placeholder="Price">
                    </div>
                    <div class="form-group">
                      <label class="control-label mb-10 text-left"><i
                                                                      class="fa fa-calendar" aria-hidden="true"></i>
                        Time</label>
                      <input type='text' name="datetime" id="dateprice"
                             class="form-control"
                             value="" />
                    </div>
                    <div class="m-t-43 form-group">
                      <button type="submit" class="btn btn-success"
                              id="btn-deposit"><i class="fa fa-paper-plane"
                                                  aria-hidden="true"></i>
                        Add Price</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
          <div class="card">
              <div class="card-header text-uppercase"> Search</div>
              <div class="card-body">
                  <form method="get" action="">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label class="control-label mb-10 text-left"><i
                                                                            class="fa fa-calendar" aria-hidden="true"></i>
                              From</label>
                            <input type='text' name="datefrom" id="datefrom"
                                   class="form-control"
                                   value="{{request()->input('datefrom')}}" />
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="form-group">
                            <label class="control-label mb-10 text-left"><i
                                                                            class="fa fa-calendar" aria-hidden="true"></i>
                              To</label>
                            <input type='text' name="dateto" id="dateto"
                                   class="form-control"
                                   value="{{request()->input('dateto')}}" />
                          </div>
                        </div>

                        <div class="col-sm-12">
                          <div class="form-group">
                            <div class="form-actions mt-10">
                              <button type="submit"
                                      class="btn-filler btn btn-lg1 btn-primary mr-10"><i
                                                                                          class="fa fa-search" aria-hidden="true"></i>
                                Search
                              </button>
                              <button type="submit" name="export" value="1"
                                      class="btn-filler btn btn-lg1 btn-success mr-10"><i
                                                                                          class="fa fa-file-excel-o"
                                                                                          aria-hidden="true"></i> Export</button>
                              <a href="#"
                                 class="btn-filler btn btn-danger mr-10">Cancel</a>
                            </div>
                          </div>
                        </div>
                    </div>
                  </form>
              </div>
          </div>
      </div>
  	<div class="col-md-12">
        <div class="card">
            <div class="card-header text-uppercase"> List price</div>
          	<div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    PRICE
                                </th>
                                <th>
                                  TIME
                              	</th>
                                <th>
                                  ACTION
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($PriceToken as $p)
                              <tr>
                                <td>{{$p->Changes_ID}}</td>
                                <td>{{$p->Changes_Price}}</td>
                                <td>{{$p->Changes_Time}}</td>
                                <td>
                                  <form action="{{ route('system.admin.postPriceToken') }}" method="post">@csrf
                                    <div class="table-responsive">
                                      <input type="number" step="any" name="Price" placeholder="Amount"
                                             class="form-control" value="" required min="0">
                                    </div>
                                    <br>
                                    <input type="hidden" name="ID" value="{{$p->Changes_ID}}">
                                    <input type="hidden" name="priceOld" value="{{$p->Changes_Price}}">
                                    <button class="btn btn-success btn-rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="btn-text">Edit</span></button>
                                  </form>
                                </td>
                              </tr>
                              @endforeach
                        </tbody>
                    </table>

                </div>
                {{$PriceToken->appends(request()->input())->links('System.Layouts.Pagination')}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js"></script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datefrom').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
    $('#dateprice').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD HH:mm:ss', time: true, clearButton: true });

  $('#dateto').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
</script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
