@extends('System.Layouts.Master')
@section('title', 'Admin-Investment')
@section('css')

<!--THIS PAGE LEVEL CSS-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="datetime/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
    rel="stylesheet" />
<link href="datetime/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" />
<link href="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
<link href="datetime/plugins/bootstrap-daterange/daterangepicker.css" rel="stylesheet" />
<link href="datetime/plugins/clockface/css/clockface.css" rel="stylesheet" />
<link href="datetime/plugins/clockpicker/clockpicker.css" rel="stylesheet" />
<!--REQUIRED THEME CSS -->
<link href="datetime/assets/css/themes/main_theme.css" rel="stylesheet" />
<style>
    .dtp-btn-cancel {
        background: #9E9E9E;
    }

    .dtp-btn-ok {
        background: #009688;
    }

    .dtp-btn-clear {
        color: black;
    }

    .btn-filler {
        margin-bottom: 10px;
    }

    .table-bordered tbody tr td,
    .table-bordered tbody tr th {
        padding: 10px;
        border: 1px solid #00a4df;
        border-bottom: none;
    }
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header text-uppercase">Investment</div>
            <div class="card-body">
                <form method="POST" id="post-deposit" action="{{route('system.admin.postInvestAdmin')}}">
                    @csrf
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><i class="fa fa-users"></i>
                                                User ID</label>
                                            <input type="text" class="form-control" name="user" id="exampleInputEmail1"
                                                placeholder="Enter User ID">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><i class="fa fa-money"></i>
                                                Amount USD</label>
                                            <input type="number" step="any" name="amount" class="form-control"
                                                placeholder="Enter USD">
                                        </div>
                                        <label><i class="fa fa-hand-o-down"></i> Currency</label>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <select class="form-control c-select" name="coin">
                                                    <option value="5">USD</option>
                                                </select>
                                            </div>
                                        </div>
                                        <label><i class="fa fa-hand-o-down"></i> Type Invest</label>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <select class="form-control c-select" name="type">
                                                    <option value="-1">Support</option>
                                                    <option value="1">Real</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="m-t-43">
                                            <button type="submit" class="btn btn-stk" id="btn-deposit"><i
                                                    class="fa fa-paper-plane" aria-hidden="true"></i>
                                                Deposit</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header text-uppercase">Deposit not Investment</div>
            <div class="card-body">
                <form method="POST" id="post-deposit" action="{{route('system.admin.postDepositAdmin')}}">
                    @csrf
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><i class="fa fa-users"></i>
                                                User ID</label>
                                            <input type="text" class="form-control" name="user" id="exampleInputEmail1"
                                                placeholder="Enter User ID">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><i class="fa fa-money"></i>
                                                Amount USD</label>
                                            <input type="number" step="any" name="amount" class="form-control"
                                                placeholder="Enter Token">
                                        </div>
                                        <label><i class="fa fa-hand-o-down"></i> Currency</label>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <select class="form-control c-select" name="coin">
                                                    <option value="5">USD</option>
                                                    <option value="1">BTC</option>
                                                    <option value="2">ETH</option>
                                                    <option value="8">IAM</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="m-t-43">
                                            <button type="submit" class="btn btn-stk" id="btn-deposit"><i
                                                    class="fa fa-paper-plane" aria-hidden="true"></i>
                                                Deposit</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase">Search</div>
            <div class="card-body">
                <form method="GET" action="{{route('system.admin.InvestmentList')}}">
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper">
                            <div class="panel-body">
                                <div class="form-wrap">
                                    <div class="form-body">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputpwd_1"><i
                                                            class="fa fa-users" aria-hidden="true"></i> User ID</label>
                                                    <input type="name" name="user_id" class="form-control"
                                                        id="exampleInputpwd_1" placeholder="Enter User ID"
                                                        value="{{request()->input('user_id')}}">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputuname_1"><i
                                                            class="fa fa-chevron-down" aria-hidden="true"></i>
                                                        Status</label>
                                                    <div class="form-group">
                                                        <select class="form-control" tabindex="1" name="status">
                                                            <option value="" selected>--- Select ---</option>
                                                            <option value="0"
                                                                {{request()->input('status') === 0 || request()->input('status') === "0" ? 'selected' : ''}}>
                                                                Waiting</option>
                                                            <option value="1"
                                                                {{request()->input('status') == 1 ? 'selected' : ''}}>
                                                                Active</option>
                                                            <option value="2"
                                                                {{request()->input('status') == 2 ? 'selected' : ''}}>
                                                                Refunded</option>
                                                            <option value="-1"
                                                                {{request()->input('status') == -1 ? 'selected' : ''}}>
                                                                Admin Cancel</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"><i
                                                            class="fa fa-calendar" aria-hidden="true"></i>
                                                        From</label>
                                                    <input type='text' name="datefrom" id="datefrom"
                                                        class="form-control" value="{{request()->input('datefrom')}}" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputuname_1"><i
                                                            class="fa fa-chevron-down" aria-hidden="true"></i>
                                                        Email</label>
                                                    <input type="email" name="email" class="form-control"
                                                        id="exampleInputpwd_1" placeholder="Enter User Email"
                                                        value="{{request()->input('email')}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 text-left"><i
                                                            class="fa fa-calendar" aria-hidden="true"></i>
                                                        To</label>
                                                    <input type='text' name="dateto" id="dateto" class="form-control"
                                                        value="{{request()->input('dateto')}}" />
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-actions mt-10">

                                                        <button type="submit"
                                                            class="btn-filler btn btn-lg1 btn-stk"><i
                                                                class="fa fa-search" aria-hidden="true"></i>
                                                            Search
                                                        </button>
                                                        <button type="submit" name="export" value="1"
                                                            class="btn-filler btn btn-lg1 btn-success  mr-10"><i
                                                                class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                            Export</button>
                                                        <a href="{{ route('system.admin.InvestmentList') }}"
                                                            class="btn-filler btn btn-default mr-10">Cancel</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> List Investment Table
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User ID</th>
                                <th>Level</th>
                                <th>Rate</th>
                                <th>Currency</th>
                                <th>Time</th>
                                <th>Status</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($investmentList as $item)
                            <tr>
                                <td>{{$item->investment_ID}}</td>
                                @if($item->User_Level == 0)
                                <td>{{$item->investment_User}}</td>
                                <th>User</th>
                                @elseif($item->User_Level == 1)
                                <td class="bg-success">{{$item->investment_User}}</td>
                                <th>Admin</th>
                                @elseif($item->User_Level == 2)
                                <td class="bg-info">{{$item->investment_User}}</td>
                                <th>Finance</th>
                                @else
                                <td class="bg-warning">{{$item->investment_User}}</td>
                                <th>Test</th>
                                @endif
                                <td>{{number_format($item->investment_Amount + 0, 4)}} USDT</td>
                                <td>{{number_format($item->investment_Rate, 3)}}</td>
                                <td>{{date('Y-m-d H:i:s', $item->investment_Time)}}</td>
                                @if($item->investment_Status == 1)
                                <td><span class="badge badge-success">Active</span></td>
                                @else
                                <td><span class="badge badge-danger">Cancel</span></td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$investmentList->appends(request()->input())->links()}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

<!-- THIS PAGE LEVEL JS -->
<script src="datetime/plugins/momentjs/moment.js"></script>
<script src="datetime/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js">
</script>
<script src="datetime/plugins/boootstrap-datepicker/bootstrap-datepicker.min.js">
</script>
<script src="datetime/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js">
</script>
<script src="datetime/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js">
</script>
<script src="datetime/plugins/bootstrap-daterange/daterangepicker.js"></script>
<script src="datetime/plugins/clockface/js/clockface.js"></script>
<script src="datetime/plugins/clockpicker/clockpicker.js"></script>

<script src="datetime/assets/js/pages/forms/date-time-picker-custom.js"></script>
<script>
    $('#datefrom').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });

  $('#dateto').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD', time: false, clearButton: true });
</script>
<script>
    var e=$("#demo-foo-col-exp");
    $("#demo-input-search2").on("input",function(o){o.preventDefault(),e.trigger("footable_filter",{filter:$(this).val()})})
</script>
<!-- Datatable init js -->
<script>
    var today = new Date();
        var currentDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        $('#dt-investment').DataTable({
          "bLengthChange": false,
        "searching": false,
          "paging": false,
          "order": [0,'desc']
      });
</script>
@endsection