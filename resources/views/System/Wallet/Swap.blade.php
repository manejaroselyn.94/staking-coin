@extends('System.Layouts.Master')
@section('title')
Swap Coin
@endsection
@section('css')

<style>
    span.message-custom.success {
        color: #009688;
    }

    span.message-custom.error {
        color: #F44336;
    }

    .icon-spin {
        position: absolute;
        top: 22%;
        right: 3%;
    }
</style>
<!-- switchery CSS -->
<link href="vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>

<!-- bootstrap-select CSS -->
<link href="vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>

<!-- bootstrap-tagsinput CSS -->
<link href="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" type="text/css"/>

<!-- bootstrap-touchspin CSS -->
<link href="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css"/>

<!-- multi-select CSS -->
<link href="vendors/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
<div class="container pt-30">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-header text-uppercase">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Wallet Swap</h6>
                    </div>
                    <div class="pull-right">
                        <h6 class="panel-title txt-danger m-0">Balance:
                            <span class="balance_coin"></span></h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                    <form action="{{ route('system.postSwap') }}" method="POST" id="form-Swap">
                        @csrf
                        <div class="form-group">
                            <label class="control-label">Rate SKC</label>
                            <input class="form-control" value="{{$rate['SKC']}}" readonly />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Select Coin From</label>
                            <select id="coin_from" name="coin_from" class="select form-control custom-select"
                                style="width: 100%; height:36px;">
                                <option value="5">USDT</option>
                                <option value="8">SKC</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount Coin From</label>
                            <input class="form-control" id="amount_from" type="text" name="amount_from"
                                placeholder="0" required />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Coin To</label>
                            <select id="coin_to" name="coin_to" class="select form-control custom-select"
                                style="width: 100%; height:36px;">
                                <option value="8">SKC</option>
                                <option value="5">USDT</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount Coin To</label>
                            <input class="form-control" id="amount_to" type="text" name="amount_to"
                                placeholder="0" required />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Amount Fee Coin To</label>
                            <input class="form-control" id="amount_fee_to" type="text"
                                placeholder="0" required />
                        </div>
                        <div class="form-group">
                            <label class="control-label">Authentication Code</label>
                            <input type="text" name="otp" class="form-control" placeholder="Google Authenticator"
                                required>
                        </div>
                        <div class="text-center m-t-15 mb-3">
                            <button type="submit" class="btn btn-primary waves-effect waves-light btnSubmit"><i
                                    class="fa fa-repeat" aria-hidden="true"></i> Swap</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Select2 JavaScript -->
<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- Bootstrap Select JavaScript -->
<script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

<!-- Bootstrap Tagsinput JavaScript -->
<script src="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- Bootstrap Touchspin JavaScript -->
<script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

<!-- Multiselect JavaScript -->
<script src="vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>


<!-- Bootstrap Switch JavaScript -->
<script src="vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<!-- Form Advance Init JavaScript -->
<script src="dist/js/form-advance-data.js"></script>
<script>
    $(document).ready(function () {
    _balance = { 5: '{{ $balance['USDT']*1 }}', 8: '{{ $balance['SKC']*1 }}' };
    _rate = {5: '{{ $rate['USD'] }}', 8: '{{ $rate['SKC'] }}'};
    _fee = {5: 0.01, 8: 0.1};
    _symbol_coin = { 5: 'USDT', 8: 'SKC' }
    _selected_balance =  $('#coin_from').val();
    _selected_to =  $('#coin_to').val();

    Balance(_selected_balance);
    function Balance(name_balance){
        $get_balance = _balance[name_balance];
        $symbol_balance = _symbol_coin[name_balance];
        $('.balance_coin').html($get_balance+' '+$symbol_balance);
        $('.symbol_coin').html($symbol_balance);
    }
    
    function optionSelect(){
        choose_formwallet = $('#coin_from').val();
        let option = '';
        $('#coin_to').empty();
        if(choose_formwallet == 5){
            option += `<option value="8">SKC</option>`;
        }
        else if(choose_formwallet == 8){
            option += `<option value="5">USDT</option>`;
            
        }
        $('#coin_to').append(option);
    }
    
    function updateAmountTo(){
	    _selected_to =  $('#coin_to').val();
	    _amount_from = $('#amount_from').val();
        _amount_to = _amount_from*_rate[_selected_balance]/_rate[_selected_to];
        _amount_fee_to = _amount_to*_fee[_selected_balance];
		$('#amount_to').val(_amount_to);
		$('#amount_fee_to').val(_amount_fee_to);
    }

    $('#coin_from').change(function(){
	    optionSelect();
        _selected_balance =  $(this).val();
        Balance(_selected_balance);
        updateAmountTo();
    });
    
    $('#amount_from').keyup(function(){
	    _amount_from = $(this).val();
	    if(Number(_amount_from) <= 0){
		    toastr.error('Amount <= 0!', 'Error!', {timeOut: 3500})
		    return false;
	    }
	    if(Number(_amount_from) > Number(_balance[_selected_balance])){
		    toastr.error('Your balance isn\'t enough!', 'Error!', {timeOut: 3500})
	    }
        updateAmountTo();

    });
    
});


</script>
<script>
    $('#form-Swap').submit(function(e){
        e.preventDefault();
        swal.fire({
            title: 'Swap to '+_symbol_coin[_selected_to],
            text: 'Are You Sure Swap From '+$('#amount_from').val()+' '+_symbol_coin[_selected_balance]+ ' To '+_symbol_coin[_selected_to]+' ?' ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                document.forms[0].submit();
            }
        });  
    });


</script>
@endsection