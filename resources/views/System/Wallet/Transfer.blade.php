@extends('System.Layouts.Master')
@section('title')
transfer
@endsection
@section('css')
<style>
    .text-red {
        color: #F44336;
    }

    .d--none {
        display: none;
    }
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: #444!important;
        opacity: 1; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: #444!important;
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
        color: #444!important;
    }
</style>
@endsection

@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Transfer</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card">
            <div class="card-header text-uppercase">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Wallet Transfer</h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <form id="form-transfer" method="post" action="{{route('system.postTransfer')}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label mb-10">Transfer</label>
                        <select id="coin_want_transfer" name="currency" class="form-control">
                            <option class="coin-2" value="5">USDT ( {{number_format($balance['Deposit'],2)}} USDT)</option>
                            <option class="coin-9" value="8">SKC ( {{number_format($balance['SKC'],4)}} SKC)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Transfer To <span class="text-success transferID"></span></label>
                        <input type="text" id="userID" name="userID" class="form-control" placeholder="Enter User ID Receive" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount Coin</label>
                        <input type="number" step="any" id="amount-input" name="amount" class="form-control"
                            placeholder="Amount" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fee <span class="txt-red">(Transfer fee of <span class="feetransfer"></span>%)</span></label>
                        <input type="text" step="any" id="amount-fee" readonly="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Authentication Code</label>
                        <input type="text" name="otp" class="form-control" placeholder="Google Authenticator" required>
                    </div>
                    <div class="text-center m-t-15 mb-3">
                        <button type="submit" class="btn btn-stk waves-effect waves-light"><i
                                class="fa fa-paper-plane" aria-hidden="true"></i>
                                Transfer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Transfer History
            </div>
            <div class="card-body">
                <div class="table-responsive">
					<table id="dt-dashboard" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    Amount
                                </th>
                                <th>Fee</th>
                                <th>Rate
                                </th>
                                <th>
                                    Currency</th>
                                <th>Action
                                </th>
                                <th>Comment
                                </th>
                                <th>Time
                                </th>
                                <th>Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($money as $item)
                            <tr>
                                <td>{{$item->Money_ID}}</td>
                                <td>{{ $item->Money_USDT+0}}</td>
                                <td>{{ $item->Money_USDTFee+0}}</td>
                                <td>{{number_format($item->Money_Rate, 2)}}</td>
                                <td>{{$item->Currency_Symbol}}</td>
                                <td>{{$item->MoneyAction_Name}}</td>
                                <td>{{$item->Money_Comment}}</td>
                                <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                <td>
                                    <span class="badge badge-success">Confirmed</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        _feetransfer = '{{ $feeTransfer }}';
        _balance = { 5: "{{ $balance['Deposit'] }}" };
        _rate = {5: 1, 9: "{{ $rate['TRX'] }}", 2: "{{ $rate['ETH'] }}"};
        _symbol_coin = {1: 'BTC', 2: 'ETH', 5: 'USD', 9: "TRX" }
        
        $('.feetransfer').html(_feetransfer * 100);
    });
    
    $('#userID').blur(function(){
        _userID = $(this).val();
        $.get("{{route('system.getAjaxUser')}}?User_ID="+_userID, function(data){
            $('.transferID').html(data.message);
        });
    });
    $('#amount-input').keyup(function(){
        $amount = $('#amount-input').val();
        $('#amount-fee').val($amount * _feetransfer);
    });
    
</script>

<script>
    
    $('#form-transfer').submit(function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Confirm transfer',
            text: 'Are You Sure transfer ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                document.forms[0].submit();
            }
        });  
    });
    
    @if(Session('flash_level') && Session('flash_level') == 'success')
    	Swal.fire({
            title: 'Success',
            text: '{{Session('flash_message')}}',
            type: 'success',
            confirmButtonText: 'OK',
            confirmButtonClass: 'btn btn-confirm',
            closeOnConfirm: true
        })
    @endif
</script>
@endsection