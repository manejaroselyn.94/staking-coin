@extends('System.Layouts.Master')

@section('title')
Deposit
@endsection
@section('css')
	<style>
		@media only screen and (max-width: 767px){
			img.image-qr{
				width:80%!important;
			}
		}
	</style>
@endsection

@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Deposit</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="card">
            <div class="card-header text-uppercase">Deposit</div>
            <div class="card-body">
                <form>
                    <div class="form-group">
                        <label><i class="fa fa-database"></i> Coin</label>
                        <select class="deposit select form-control custom-select" style="width: 100%; height:36px;">
                            <option value="1"> BTC</option>
                            <option value="8">SKC</option>
                            <option value="2">ETH</option>
                            <option value="5">USDT</option>
                            <option value="9">TRX</option>
                        </select>
                    </div>
                    <div class="form-group" style="display: flex;">
                        <input type="text" id="linkRef" name="example-input1-group2" class="form-control"
                            placeholder="Loading..." readonly>
                        <button type="button" id="tooltiptext" class="btn btn-stk" onclick="copyToClipboard()"><i
                                class="fa fa-clone" aria-hidden="true"></i>
                            Copy</button>

                    </div>
                    <div class="text-center">
                        <img class="image-qr" id="QR" src="" style="width: 40%;">
						<hr>
						<p class="text-warning">Please check the wallet address again. We are not responsible for any loss if you send the wrong wallet.</p>
                    </div>
					
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Deposit History
            </div>
            <div class="card-body">
                <div class="table-responsive">
					<table id="dt-dashboard" class="table table-bordered">
<thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    Amount
                                </th>
                                <th>Fee</th>
                                <th>Rate
                                </th>
                                <th>
                                    Currency</th>
                                <th>Action
                                </th>
                                <th>Comment
                                </th>
                                <th>Time
                                </th>
                                <th>Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($money as $item)
                            <tr>
                                <td>{{$item->Money_ID}}</td>
                                <td>{{ $item->Money_USDT+0}}</td>
                                <td>{{ $item->Money_USDTFee+0}}</td>
                                <td>{{number_format($item->Money_Rate, 2)}}</td>
                                <td>{{$item->Currency_Symbol}}</td>
                                <td>{{$item->MoneyAction_Name}}</td>
                                <td>{{$item->Money_Comment}}</td>
                                <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                <td>
                                    <span class="badge badge-success">Confirmed</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        getAddress($('.deposit').val());
        $('.deposit').change(function(){
            _coin = $(this).val();
            getAddress(_coin);
        });
        function getAddress(_coin){
            $.getJSON( "{{ route('system.json.getAddress') }}?coin="+_coin, function( data ) {
                $('#linkRef').val(data['address']);
                $('#QR').attr('src', data['Qr']);
            });
        }
        
    });
</script>
<script>
    function copyToClipboard() {
		var copyText = document.getElementById("linkRef");
		copyText.select();
		copyText.setSelectionRange(0, 99999);
		console.log(copyText.setSelectionRange(0, 99999));
		document.execCommand("copy");
		var tooltip = document.getElementById("tooltiptext");
		alert(copyText.value);
	}
	
</script>
@endsection