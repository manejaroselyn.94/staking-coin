@extends('System.Layouts.Master')
@section('title')
Withdraw
@endsection
@section('css')
<style>
    .text-red {
        color: #F44336;
    }

    .d--none {
        display: none;
    }
</style>
@endsection

@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Withdraw</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header text-uppercase">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Wallet Withdraw</h6>
                </div>
                <div class="pull-right">
                    <h6 class="panel-title txt-danger">{{ number_format($balance['Deposit'], 2)}} USDT
                    </h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <form id="form-withdraw" method="post" action="{{route('system.postWithdraw')}}">
                    @csrf
                    <div class="form-group">
                        <label class="control-label mb-10">Withdrawal</label>
                        <select id="coin_want_withdraw" name="coin_want_withdraw" class="form-control">
                            <option class="coin-2" value="2">ETH</option>
                            <option class="coin-9" value="9">TRX</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount USD</label>
                        <input type="number" step="any" id="amount-input" name="amount" class="form-control"
                            placeholder="Amount" required>

                    </div>
                    <div class="form-group">
                        <label class="control-label">Amount Withdraw <span
                                class="withdraw_coin_name">Coin</span></label>
                        <input type="number" step="any" id="amount-coin" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fee <span class="txt-red">(Withdrawal fee of
                                <span class="feeWithdraw"></span>%)</span></label>
                        <input type="text" step="any" id="amount-fee" readonly="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label"> Wallet Address</label>
                        <input type="text" class="form-control" name="address" placeholder="Wallet Address"
                            value="{{Session('user')->User_WalletAddress}}" required>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Authentication Code</label>
                        <input type="text" name="otp" class="form-control" placeholder="Google Authenticator" required>

                    </div>
                    <div class="text-center m-t-15 mb-3">
                        <button type="submit" class="btn btn-stk waves-effect waves-light"><i
                                class="fa fa-paper-plane" aria-hidden="true"></i>
                            Withdraw</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header text-uppercase">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Withdraw SKC</h6>
                </div>
                <div class="pull-right">
                    <h6 class="panel-title txt-danger">{{ number_format($balance['SKC'], 2)}} SKC
                    </h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <form id="form-withdraw-SKC" method="post" action="{{route('system.postWithdraw')}}">
                    @csrf
                    <input type="hidden" name="from_wallet" value="8" required>
                    <input type="hidden" name="coin_want_withdraw" value="8" required>
                    <div class="form-group">
                        <label class="control-label">Amount SKC</label>
                        <input type="number" step="any" id="amount-SKC" name="amount" class="form-control" placeholder="Amount" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fee <span class="txt-red">(Withdrawal fee of
                                <span class="feeWithdraw"></span>%)</span></label>
                        <input type="text" step="any" id="amount-fee-SKC" readonly="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label"> Wallet Address SKC</label>
                        <input type="text" class="form-control" name="address" placeholder="Wallet Address SKC"                          value="{{Session('user')->User_WalletSKC}}" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Authentication Code</label>
                        <input type="text" name="otp" class="form-control" placeholder="Google Authenticator" required>

                    </div>
                    <div class="text-center m-t-15 mb-3">
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                                class="fa fa-paper-plane" aria-hidden="true"></i>
                            Withdraw</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header text-uppercase">
                <div class="pull-left">
                    <h6 class="panel-title txt-dark">Withdraw TRX</h6>
                </div>
                <div class="pull-right">
                    <h6 class="panel-title txt-danger">{{ number_format($balance['TRX'], 2)}} TRX
                    </h6>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <form id="form-withdraw-TRX" method="post" action="{{route('system.postWithdraw')}}">
                    @csrf
                    <input type="hidden" name="from_wallet" value="9" required>
                    <input type="hidden" name="coin_want_withdraw" value="9" required>
                    <div class="form-group">
                        <label class="control-label">Amount TRX</label>
                        <input type="number" step="any" id="amount-TRX" name="amount" class="form-control" placeholder="Amount" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fee <span class="txt-red">(Withdrawal fee of
                                <span class="feeWithdraw"></span>%)</span></label>
                        <input type="text" step="any" id="amount-fee-TRX" readonly="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label"> Wallet Address TRX</label>
                        <input type="text" class="form-control" name="address" placeholder="Wallet Address TRX"                          value="{{Session('user')->User_WalletTRX}}" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Authentication Code</label>
                        <input type="text" name="otp" class="form-control" placeholder="Google Authenticator" required>

                    </div>
                    <div class="text-center m-t-15 mb-3">
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                                class="fa fa-paper-plane" aria-hidden="true"></i>
                            Withdraw</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Withdraw History
            </div>
            <div class="card-body">
                <div class="table-responsive">
					<table id="dt-dashboard" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    Amount
                                </th>
                                <th>Fee</th>
                                <th>Rate
                                </th>
                                <th>
                                    Currency</th>
                                <th>Action
                                </th>
                                <th>Comment
                                </th>
                                <th>Time
                                </th>
                                <th>Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($money as $item)
                            <tr>
                                <td>{{$item->Money_ID}}</td>
                                <td>{{ $item->Money_USDT+0}}</td>
                                <td>{{ $item->Money_USDTFee+0}}</td>
                                <td>{{number_format($item->Money_Rate, 2)}}</td>
                                <td>{{$item->Currency_Symbol}}</td>
                                <td>{{$item->MoneyAction_Name}}</td>
                                <td>{{$item->Money_Comment}}</td>
                                <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                <td>
                                    <span class="badge badge-success">Confirmed</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function () {

        _feeWithdraw = '{{ $feeWithdraw }}';
        _balance = { 5: "{{ $balance['Deposit'] }}" };
        _rate = {5: 1, 9: "{{ $rate['TRX'] }}", 2: "{{ $rate['ETH'] }}"};
        _symbol_coin = {1: 'BTC', 2: 'ETH', 5: 'USD', 9: "TRX" }
        _coin_want_withdraw = $('#coin_want_withdraw').val();
        _amount = $('#amount-input').val();
        $('#coin_want_withdraw').change(function(){
            $coin_withdraw = $(this).val();
            $amount = $('#amount-input').val();
            let result = ($amount * (1 - _feeWithdraw)) / (_rate[$coin_withdraw] * 1);
            $('#amount-coin').val(result);
            $('#amount-fee').val(($amount*_feeWithdraw)+' USDT');
            $('.withdraw_coin_name').html(_symbol_coin[$coin_withdraw]);
        });
        $('#amount-input').keyup(function(){
            $coin_withdraw = $('#coin_want_withdraw').val();
            $amount = $('#amount-input').val();
            let result = $amount / (_rate[$coin_withdraw] * 1);
            $('#amount-coin').val(result);
            $('#amount-fee').val($amount * _feeWithdraw+' USDT');
        });
        $('.feeWithdraw').html(_feeWithdraw * 100);
        $('.withdraw_coin_name').html(_symbol_coin[$('#coin_want_withdraw').val()]);
    });
    
    $('#amount-TRX').keyup(function(){
        $coin_withdraw = 9;
        $amount = $(this).val();
        let result = $amount / _rate[$coin_withdraw];
        $('#amount-fee-TRX').val($amount * _feeWithdraw+' TRX');
    });
    
    $('#amount-SKC').keyup(function(){
        $coin_withdraw = 8;
        $amount = $(this).val();
        let result = $amount / _rate[$coin_withdraw];
        $('#amount-fee-SKC').val($amount * _feeWithdraw+' SKC');
    });
    
</script>

<script>
    
    $('#form-withdraw').submit(function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Confirm Withdrawal',
            text: 'Are You Sure Withdrawal ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                document.forms[0].submit();
            }
        });  
    });
    $('#form-withdraw-SKC').submit(function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Confirm Withdrawal SKC',
            text: 'Are You Sure Withdraw '+$('#amount-SKC').val()+' SKC ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            if(confirm.value == true){
                document.forms[1].submit();
            }
        });
    });
    $('#form-withdraw-TRX').submit(function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Confirm Withdrawal TRX',
            text: 'Are You Sure Withdraw '+$('#amount-TRX').val()+' TRX ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            if(confirm.value == true){
                document.forms[1].submit();
            }
        });
    });
    @if(Session('flash_level') && Session('flash_level') == 'success')
    	Swal.fire({
            title: 'Success',
            text: '{{Session('flash_message')}}',
            type: 'success',
            confirmButtonText: 'OK',
            confirmButtonClass: 'btn btn-confirm',
            closeOnConfirm: true
        })
    @endif
</script>
@endsection