@extends('System.Layouts.Master')
@section('title', 'Investment')
@section('css')
<link href="sweetalert/sweetalert.css" rel="stylesheet">
<style>
    a:hover {
        cursor: pointer;
    }

    .text-red {
        color: #F44336;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Investment</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header text-uppercase"> INVEST</div>
            <div class="card-body">
                <form method="post" action="{{route('system.postInvestment')}}" id="form-investment">
                    @csrf
                    <div class="form-group">
                        <label><i class="fa fa-money"></i> From Wallet</label>
                        <div class="input-group">
                            <select name="currency" class="form-control coin-invest">
								<option value="5">USDT ({{ number_format($balance->User_BalanceDeposit+0, 2) }} USDT)</option>
								<option value="8">SKC ({{ number_format($balance->User_BalanceSKC+0, 2) }} SKC)</option>
							</select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><i class="fa fa-money"></i> Amount USDT</label>
                        <div class="input-group">
                            <input type="number" step="any" class="form-control" id="amount" name="amount">
                        </div>
                    </div>
                    <div class="form-group amount-skc">
                        <label><i class="fa fa-money"></i> Amount SKC</label>
                        <div class="input-group">
                            <input type="number" step="any" class="form-control" id="amount-skc" name="amount-skc">
                        </div>
                    </div>
                    @if($balance->User_TotalInvestment > 0 || $totalInvestSKC > 0)
                    <div class="form-group mb-0 text-center">
                        <h6 class="" style="color:red">Total Investment:
                            {{ number_format($totalInvest, 2)}} USD</h6>
                        <h6 class="text-warning" >Total Investment SKC:
                            {{ number_format($totalInvestSKC, 2)}} SKC</h6>
                    </div>
                    @endif
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-stk waves-effect waves-light m-1"> <i
                                class="fa fa-paper-plane"></i> <span>Invest</span> </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header text-uppercase"> INVESTMENT DATA</div>
            <div class="card-body">
                <div class="form-group">
                    <label><i class="fa fa-money"></i> Amount Coin </label>
                    <div class="input-group">
                        <input type="text" class="form-control" readonly id="total_invest" 
                            value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-money"></i> Profit </label>
                    <div class="input-group">
                        <input type="text" step="any" readonly class="form-control" id="interest_monthly"
                        	value="" >
                    </div>
                </div>
                <div class="form-group">
                    <label><i class="fa fa-money"></i> Expired Date</label>
                    <div class="input-group">
                        <input type="text" class="form-control" readonly id="expired_date" 
                            value="" >
                    </div>
                </div>
				<!--
                <div class="form-group">
                    <label><i class="fa fa-money"></i> Total Profit (USDT)</label>
                    <div class="input-group">
                        <input type="text" step="any" readonly class="form-control" id="total_profit"
                        	 value="" >
                    </div>
                </div>
				-->
                <div class="form-group">
                    <label><i class="fa fa-money"></i> Bonus (TRX)</label>
                    <div class="input-group">
                        <input type="text" step="any" readonly class="form-control" id="bonus_TRX"
                        	>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Investment
            </div>
            <div class="card-body">
                <div class="table-responsive">
					<table id="dt-dashboard" class="table table-bordered">

						<thead>
							<tr>
								<th>ID</th>
								<th>Amount</th>
								<th>Time</th>
								<th>Expired Date</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@php
								$package = DB::table('package_skc')
						    				->whereRaw($totalInvestSKC." BETWEEN package_Min AND package_Max")
						    				->select('package_Interest', 'package_Date', 'package_Name')
						    				->first();
								$timeExperied = time();
								if(isset($history_invest_skc[0])){
									$timeExperied = date('Y-m-d H:i:s', (strtotime('+'.$package->package_Date.' days', $history_invest_skc[0]->investment_Time)));
								}
							@endphp
							@foreach ($history_invest_skc as $item)
							<tr>
								<td>{{ $item->investment_ID}}</td>
								<td>{{ number_format($item->investment_Amount, 2) }} SKC</td>
								<td>{{ Date('Y-m-d H:i:s', $item->investment_Time)}}</td>
								<td>{{$timeExperied}}</td>
								<td>
									@if ($item->investment_Status == 1)
									<label class="label label-success">Success</label>
									@elseif($item->investment_Status == 2)
									<label class="label label-info">Expired</label>
									@else
									<label class="label label-warning">Cancel</label>
									@endif
								</td>
							</tr>
							@endforeach
							@php
								$package = DB::table('package')
						    				->whereRaw($totalInvest." BETWEEN package_Min AND package_Max")
						    				->select('package_Interest', 'package_Date', 'package_Name')
						    				->first();
								$timeExperied = time();
								if(isset($history_invest[0])){
									$timeExperied = date('Y-m-d H:i:s', (strtotime('+'.$package->package_Date.' days', $history_invest[0]->investment_Time)));
								}
							@endphp
							@foreach ($history_invest as $item)
							<tr>
								<td>{{ $item->investment_ID}}</td>
								<td>{{ number_format($item->investment_Amount, 2) }} USDT</td>
								<td>{{ Date('Y-m-d H:i:s', $item->investment_Time)}}</td>
								<td>{{$timeExperied}}</td>
								<td>
									@if ($item->investment_Status == 1)
									<label class="label label-success">Success</label>
									@elseif($item->investment_Status == 2)
									<label class="label label-info">Expired</label>
									@else
									<label class="label label-warning">Cancel</label>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="sweetalert/sweetalert.min.js"></script> --}}
<script>
	checkInvestSKC();
	function checkInvestSKC(){
		$coin = $('.coin-invest').val();
		if($coin == 8){
			$('.amount-skc').show();
		}else{
			$('.amount-skc').hide();
		}
	}
	$('.coin-invest').change(function(){
		checkInvestSKC();
	});
	$(document).ready(function(){
		_rate = {'SKC':'{{$rate['SKC']}}'};
		_total_invest = parseFloat({{$balance->User_TotalInvestment+0}});
		_total_invest_skc = parseFloat({{$totalInvestSKC+0}});
		_percent = 0.09;
		_total_bonus = {{$totalBonus+0}};
		_bonusArr = {5500:25000, 10500:60000, 30500:150000, 60500:400000};
		_bonus = 0;
		_day = 210;
		_current = '{{date('Y-m-d H:i:s')}}';
		console.log(new Date());
		$('#amount').blur(function(){
			_currency = $('.coin-invest').val();
			_amount = parseFloat($(this).val())+0;
			_amount_skc = _amount/_rate['SKC'];
			$('#amount-skc').val(_amount_skc);
			if(_currency == 5){
				_totalCheck = _total_invest+_amount;
			}else{
				_totalCheck = _total_invest_skc+_amount_skc;
			}
			console.log(_total_invest, _totalCheck);
			dataInvestment(_totalCheck, _amount, _currency);
		});
		$('#amount-skc').blur(function(){
			_currency = $('.coin-invest').val();
			_amount_skc = parseFloat($(this).val())+0;
			_amount = _amount_skc*_rate['SKC'];
			$('#amount').val(_amount);
			if(_currency == 5){
				_totalCheck = _total_invest+_amount;
			}else{
				_totalCheck = _total_invest_skc+_amount_skc;
			}
			console.log(_total_invest, _totalCheck);
			dataInvestment(_totalCheck, _amount, _currency);
		});
	});

	function dataInvestment(_totalCheck, _amount, _currency = 5){
		if(_currency == 5){
			if(_totalCheck >= 5500 && _totalCheck < 10500){
				_percent = 0.1;
				_bonus = _bonusArr[5500]-_total_bonus;
				_day = 210;
			}else if(_totalCheck >= 10500 && _totalCheck < 30500){
				_percent = 0.12;
				_bonus = _bonusArr[10500]-_total_bonus;
				_day = 180;
			}else if(_totalCheck >= 30500 && _totalCheck < 60500){
				_percent = 0.15;
				_bonus = _bonusArr[30500]-_total_bonus;
				_day = 180;
			}else if(_totalCheck >= 60500){
				_percent = 0.18;
				_bonus = _bonusArr[60500]-_total_bonus;
				_day = 150;
			}
			_symbol = 'USDT';
		}else{
			_percent = 0.0075;
			_percentMonth = 1.8;
			_day = 240;
			if(_totalCheck >= 50000 && _totalCheck < 200000){
				_percent = 0.0095;
				_percentMonth = 2;
				_day = 210;
			}else if(_totalCheck >= 200000 && _totalCheck < 600000){
				_percent = 0.0111;
				_percentMonth = 2;
				_day = 180;
			}else if(_totalCheck >= 600000 && _totalCheck < 1000000){
				_percent = 0.0133;
				_percentMonth = 2;
				_day = 150;
			}else if(_totalCheck >= 1000000){
				_percent = 0.0166;
				_percentMonth = 2;
				_day = 120;
			}
			_bonus = 0;
			_symbol = 'SKC';
			_percent = _percentMonth;
			_amount = _amount/_rate['SKC'];
		}
		_expired_date = getDate(_current, _day);
		_daily_profit = _amount*_percent;
		$('#total_invest').val(number_format(_amount,2)+' '+_symbol);
		$('#interest_monthly').val(number_format(_daily_profit,2)+' '+_symbol);
		// $('#total_profit').val(number_format(((_daily_profit*_day)+_amount),2)+' USDT');
		$('#bonus_TRX').val(number_format(_bonus,2)+' TRX');
	}
	
	function getDate(_datetime, _day){
		if(!_datetime){
			return false;
		}
		if(!_day){
			return false;
		}
		$.ajax({
			url: "{{route('getDate')}}",
			type: 'GET',
			data: {datetime:_datetime, day:_day},
			success: function (data) {
				$('#expired_date').val(data);
			}
		});
	}
	function strtotime(date, addTime){
	  let generatedTime=date.getTime();
	  if(addTime.seconds) generatedTime+=1000*addTime.seconds; //check for additional seconds 
	  if(addTime.minutes) generatedTime+=1000*60*addTime.minutes;//check for additional minutes 
	  if(addTime.hours) generatedTime+=1000*60*60*addTime.hours;//check for additional hours 
	  if(addTime.days) generatedTime+=1000*60*60*24*addTime.days;//check for additional hours 
	  return new Date(generatedTime);
	}
	@if(request()->input('token') && request()->input('id'))
		//confirm refund invest
		_id = {{request()->input('id')}};
		swal({
			title: 'Confirm Refund Investment',
			text: 'Choose action your refund investment!',
			showCancelButton: true,
			confirmButtonText: 'Submit',
			showLoaderOnConfirm: true,
			confirmButtonText: "ReInvest",
			cancelButtonText: "Withdrawl to address", 
			confirmButtonColor: '#e09000',
			cancelButtonColor: '#fff'
			}, function (confirm) 
			{
				if(confirm === true){
					_action = 3;
				}
				if(confirm === false){
					_action = 17;
				}
				$.ajax({
					url: "{{route('system.postReInvest')}}",
					type: 'POST',
					data: {_token:"{{csrf_token()}}", action:_action, id:_id},
					success: function (data) {
						if(data.status == true){
							swal({
								title: 'Success',
								text: data.message,
								type: 'success',
								confirmButtonClass: 'btn btn-confirm',
								allowOutsideClick: false
							}, function(confirm) {
								window.location.href = "{{route('Dashboard')}}";
							});
						}else{
							swal({
								title: 'Error',
								text: data.message,
								type: 'error',
								confirmButtonClass: 'btn btn-confirm',
								allowOutsideClick: false
							}, function(confirm) {
								window.location.href = "{{route('system.getInvestment')}}";
							});
						}
					}
				});
				
	    	});
	@endif


    $('#form-investment').submit(function(e){
        e.preventDefault();
		_currency = $('.coin-invest').val();
		_amount = $('#amount').val();
		if(_currency == 8){
			_symbol = 'SKC';
			_amount = _amount/_rate['SKC'];
		}else{
			_symbol = 'USD';
		}
        Swal.fire({
            title: 'Confirm Investment',
            text: 'Are you sure to invest '+ _amount +' '+_symbol,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                document.forms[0].submit();
            }
        });
        
    });
    
</script>
<script>
    $('#dt-dashboard').DataTable({
          "bLengthChange": false,
        "searching": false,
          "paging": false,
          "order": [0, 'desc']
      });
</script>

@endsection