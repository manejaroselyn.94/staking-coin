@extends('System.Layouts.Master')
@section('title', 'Interest History')
@section('css')

<style>
    a:hover {
        cursor: pointer;
    }

    .pagination {
        float: right;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">History Investment</h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> History Investment
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Amount
                                </th>
                                <th>Rate
                                </th>
                                <th>Comment
                                </th>
                                <th>Time
                                </th>
                                <th>Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($walletHistory as $item)
                            <tr>
                                <td>{{$item->Money_ID}}</td>
                                <td>{{ $item->Money_USDT+0}} USD</td>
                                <td>{{number_format($item->Money_Rate, 5)}}</td>
                                <td>{{$item->MoneyAction_Name}}</td>
                                <td>{{$item->Money_Comment}}</td>
                                <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                <td>
                                    <span class="badge badge-success">Confirmed</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$history_invest->appends(request()->input())->links('System.Layouts.Pagination')}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

@endsection