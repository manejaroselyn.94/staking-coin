@extends('System.Layouts.Master')
@section('title', 'Investment History')
@section('css')

<style>
    a:hover {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Investment History</h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Investment History
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Amount</th>
                                <th>Rate</th>
                                <th>Time</th>
                                <th>Expired time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($history_invest as $item)
                            <tr>
                                <td>{{ $item->investment_ID}}</td>
                                <td>{{ number_format($item->investment_Amount, 2) }} {{$item->Currency_Symbol}}</td>
                                <td>{{ $item->investment_Rate+0}}</td>
                                <td>{{ Date('Y-m-d H:i:s', $item->investment_Time) }}</td>
                                <td>
	                                <?php
		                                if($sum <= 5100){
			                                echo Date('Y-m-d H:i:s', $item->investment_Time+20736000);
		                                }else if($sum > 5100 && $sum < 20100){
			                                echo Date('Y-m-d H:i:s', $item->investment_Time+18144000);
		                                }else if($sum > 20100){
			                                echo Date('Y-m-d H:i:s', $item->investment_Time+15552000);
		                                }
	                                ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$history_invest->appends(request()->input())->links('System.Layouts.Pagination')}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')

<script>
    $(document).ready(function () {
    $('.btn-refund').click(function(){
        let invest_id = $(this).data('invest-id');
        console.log(invest_id);
        swal.fire({
            title: 'Confirm Refund Investment',
            text: 'Are You Sure Refund Investment',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                $('.refund-'+invest_id).submit();

            }
        });
    })


    $('.btn-reinvest').click(function(){
        let invest_id = $(this).data('invest-id');
        console.log(invest_id);
        swal.fire({
            title: 'Confirm Reinvestment',
            text: 'Are You Sure Reinvestment',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            confirmButtonClass: 'btn btn-confirm',
            cancelButtonClass: 'btn btn-cancel',
            closeOnConfirm: true
        }).then(function (confirm) {
            console.log(confirm);
            if(confirm.value == true){
                $('.reinvest-'+invest_id).submit();

            }
        });
    })
});

</script>
@endsection