@extends('System.Layouts.Master')
@section('title', 'Wallet History')
@section('css')

<style>
    a:hover {
        cursor: pointer;
    }

    .pagination {
        float: right;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Wallet History</h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> Wallet History
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>
                                    Amount
                                </th>
                                <th>Fee</th>
                                <th>Rate
                                </th>
                                <th>
                                    Currency</th>
                                <th>Action
                                </th>
                                <th>Comment
                                </th>
                                <th>Time
                                </th>
                                <th>Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($money as $item)
                            <tr>
                                <td>{{$item->Money_ID}}</td>
                                <td>{{ $item->Money_USDT+0}}</td>
                                <td>{{ $item->Money_USDTFee+0}}</td>
                                <td>{{number_format($item->Money_Rate, 2)}}</td>
                                <td>{{$item->Currency_Symbol}}</td>
                                <td>{{$item->MoneyAction_Name}}</td>
                                <td>{{$item->Money_Comment}}</td>
                                <td>{{date('Y-m-d H:i:s',$item->Money_Time)}}</td>
                                <td>
                                    <span class="badge badge-success">Confirmed</span>
                                </td>
<!--
                                <td>
	                                @if(($item->Money_MoneyAction == 17 || $item->Money_MoneyAction == 20) && $item->Money_Confirm == 0)
	                                	<a href="{{route('system.getResendMailConfirm', $item->Money_ID)}}"><button type="button" class="btn btn-rounded btn-primary btn-xs">Choose Payment</button></a>
	                                @endif
	                            </td>
-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                {{$money->appends(request()->input())->links('System.Layouts.Pagination')}}
            </div>
        </div>
    </div>
</div>



@endsection
@section('script')

@endsection