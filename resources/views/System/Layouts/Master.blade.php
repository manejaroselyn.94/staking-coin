<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta charset="UTF-8" />
    <title>{{ trans('message.title') }} | @yield('title')</title>
    <meta name="description" content="{{ trans('message.title') }}" />
    <meta name="keywords" content="{{ trans('message.title') }}" />
    <meta name="author" content="{{ trans('message.title') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="{{ asset('/staking').'/'}}">
    <!-- Favicon -->
    <link rel="icon" href="{{ trans('message.icon') }}" type="image">
    <!-- Vector CSS -->
    <link href="assets\plugins\vectormap\jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <!-- simplebar CSS-->
    <link href="assets\plugins\simplebar\css\simplebar.css" rel="stylesheet">
    <!-- Bootstrap core CSS-->
    <!--Data Tables -->
    <link href="assets\plugins\bootstrap-datatable\css\dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="assets\plugins\bootstrap-datatable\css\buttons.bootstrap4.min.css" rel="stylesheet" type="text/css">
    <link href="assets\plugins\dropzone\css\dropzone.css" rel="stylesheet" type="text/css">
    <link href="assets\css\bootstrap.min.css" rel="stylesheet">
    <!-- animate CSS-->
    <link href="assets\css\animate.css" rel="stylesheet" type="text/css">
    <!-- Icons CSS-->
    <link href="assets\css\icons.css" rel="stylesheet" type="text/css">
    <!-- Sidebar CSS-->
    <link href="assets\css\sidebar-menu.css?v={{time()}}" rel="stylesheet">
    <!-- Custom Style-->
    <link href="assets\css\app-style.css?v={{time()}}" rel="stylesheet">
    <!-- Toast CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/all.min.css">

    @yield('css')
    <link href="notify/toastr.min.css" rel="stylesheet">
    <link href="notify/sweetalert2.min.css" />
    <script src="assets\js\jquery.min.js"></script>
	<style>
		.border-danger {
			border-color: #ff0000!important;
		}
		.bg-danger {
			background-color: #ff0000!important;
		}
	</style>
</head>

<body class="bg-theme ">
    <div class="bg-img"></div>
    <div id="pageloader-overlay" class="visible incoming">
        <div class="loader-wrapper-outer">
            <div class="loader-wrapper-inner">
                <div class="loader"></div>
            </div>
        </div>
    </div>
    <div id="wrapper">

        @include('System.Layouts.Menu')
        <div class="content-wrapper">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    </div>

    <div class="modal fade" id="notifi-wallet">
        <div class="modal-dialog">
            <div class="modal-content border-danger animated flipInX" >
                <div class="modal-header bg-danger">
                    <h5 class="modal-title">USER INFORMATION</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="col-md-12" style="padding:15px">
                        <form method="post" action="{{route('postProfileV2')}}" data-toggle="validator" role="form"
                            novalidate="true">
                            @csrf
                            <div class="form-body overflow-hide">
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Wallet ETH</label>
                                    <div class="input-group">
                                        <input type="text" name="addressToken" class="form-control"
                                            placeholder="ETH Wallet Address"
                                            value="{{Session('user')->User_WalletETH}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Wallet TRX</label>
                                    <div class="input-group">
                                        <input type="text" name="addressTRX" class="form-control"
                                            placeholder="TRX Wallet Address "
                                            value="{{Session('user')->User_WalletTRX}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputuname_01"><i
                                            class="ti-wallet"></i> Currency Receive Interest</label>
                                    <div class="input-group">

                                        <div class="form-check">
                                            <input type="radio" value="2" name="coin"
                                                {{ Session('user')->User_TypePay == 2 ? 'checked' : ''}}>
                                            <label class="form-check-label" for="exampleRadios1">
                                                ETH <span class="text-danger label-ETH" style="display:none;">Please select the exact method of profit you expect</span>
											</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" value="9" name="coin"
                                                {{ Session('user')->User_TypePay == 9 ? 'checked' : ''}}>
                                            <label class="form-check-label" for="exampleRadios2">
                                                TRX <span class="text-danger label-TRX" style="display:none;">Please select the exact method of profit you expect</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-10" for="exampleInputEmail_01"><i
                                            class="fa fa-google"></i> Google authenticator</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="otp" required
                                            placeholder="Enter Google Authenticator">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions text-center">
                                <button type="submit" class="btn btn-stk btn-icon-anim"><i
                                        class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets\js\jquery.min.js"></script>
    <script src="assets\js\popper.min.js"></script>
    <script src="assets\js\bootstrap.min.js"></script>
    <!--Data Tables js-->
    <script src="assets\plugins\bootstrap-datatable\js\jquery.dataTables.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\dataTables.bootstrap4.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\dataTables.buttons.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\buttons.bootstrap4.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\jszip.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\pdfmake.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\vfs_fonts.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\buttons.html5.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\buttons.print.min.js"></script>
    <script src="assets\plugins\bootstrap-datatable\js\buttons.colVis.min.js"></script>
    <script>
        $(document).ready(function() {
                //Default data table
                $('#default-datatable').DataTable();
                    var table = $('#example').DataTable( {
                    lengthChange: false,
                    buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
                    } );

                    table.buttons().container()
                    .appendTo( '#example_wrapper .col-md-6:eq(0)' );

            } );
    </script>
    <!-- simplebar js -->
    <script src="assets\plugins\simplebar\js\simplebar.js"></script>
    <!-- sidebar-menu js -->
    <script src="assets\js\sidebar-menu.js"></script>
    <!-- loader scripts -->
    <!--  <script src="assets/js/jquery.loading-indicator.js"></script> -->
    <!-- Custom scripts -->
    <script src="assets\js\app-script.js?v={{time()}}"></script>
    <!-- Chart js -->

    <script src="assets\plugins\Chart.js\Chart.min.js"></script>
    <!-- Vector map JavaScript -->
    <script src="assets\plugins\vectormap\jquery-jvectormap-2.0.2.min.js"></script>
    <script src="assets\plugins\vectormap\jquery-jvectormap-world-mill-en.js"></script>
    <!-- Easy Pie Chart JS -->
    <script src="assets\plugins\jquery.easy-pie-chart\jquery.easypiechart.min.js"></script>
    <!-- Sparkline JS -->
    <script src="assets\plugins\sparkline-charts\jquery.sparkline.min.js"></script>
    <script src="assets\plugins\jquery-knob\excanvas.js"></script>
    <script src="assets\plugins\jquery-knob\jquery.knob.js"></script>

    <script>
        function number_format (number, decimals, dec_point, thousands_sep) {
		    var n = number, prec = decimals;
		
		    var toFixedFix = function (n,prec) {
		        var k = Math.pow(10,prec);
		        return (Math.round(n*k)/k).toString();
		    };
		
		    n = !isFinite(+n) ? 0 : +n;
		    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		    var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		    var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		    var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); 
		    //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		    var abs = toFixedFix(Math.abs(n), prec);
		    var _, i;
		
		    if (abs >= 1000) {
		        _ = abs.split(/\D/);
		        i = _[0].length % 3 || 3;
		
		        _[0] = s.slice(0,i + (n < 0)) +
		               _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		        s = _.join(dec);
		    } else {
		        s = s.replace('.', dec);
		    }
		
		    var decPos = s.indexOf(dec);
		    if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
		        s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		    }
		    else if (prec >= 1 && decPos === -1) {
		        s += dec+new Array(prec).join(0)+'0';
		    }
		    return s; 
		}
        $(function() {
                $(".knob").knob();
            });
    </script>

    <!-- Index js -->
    <script src="notify/sweetalert2.min.js?v={{time()}}"></script>

    <script src="notify/toastr.min.js"></script>
   
    <script>
        @if(Session::get('flash_level') == 'success')
        toastr.success('{{ Session::get('flash_message') }}', 'Success!', {timeOut: 3500})
        @elseif(Session::get('flash_level') == 'error')
            toastr.error('{{ Session::get('flash_message') }}', 'Error!', {timeOut: 3500})
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                toastr.error('{{$error}}', 'Error!', {timeOut: 3500})
            @endforeach
        @endif
        $(document).ready(function() {
            @php
            $check_wallet = DB::table('users')->where('User_ID', session('user')->User_ID)->first();
            @endphp
            @if(!$check_wallet->User_WalletETH && !$check_wallet->User_WalletADA)
                // $('#notifi-wallet').modal('show');
            @endif
        })
    </script>


    <script>
        $(document).ready(function () {
            @if(isset($RandomToken))
            $('form').append('<input type="hidden" name="CodeSpam" value="{{ $RandomToken }}">');
            @endif
        });
        
    </script>
    {{-- <script>
        $("a").click(function(){
            $("a").attr("target","_self");
        });
    </script>
    <script>
        $("a").click(function(){
            $("a").attr("target","_self");
        });
        $("button").click(function(){
            $("button").attr("target","_self");
        });
    </script> --}}
    <script>
        $(function(){
		$('.sidebar-menu li').click(function(){
			$(this).parent().addClass('active').siblings().removeClass('active')	
		})
	})
    
    </script>
    <script>
        $("#fullscreen").click(function () {
            $(this).toggleClass("nav-margin");
        });
        $(".btn-close-side").click(function(){
            $("#wrapper").toggleClass("toggled");
        })
    </script>
    @yield('script')
</body>

</html>