<?php
    $check_user = DB::table('profile')->where('Profile_User',Session('user')->User_ID)->first();
    $bg = "#F44336";
?>
@if($check_user)
@if($check_user->Profile_Status == 1)
<?php
            $bg = "#23bb3c";
        ?>
@endif
@endif

<nav class="navbar navbar-inverse ">
    <div class="mobile-only-brand pull-left">
        <div class="nav-header pull-left">
            <div class="logo-wrap">
                <a href="{{ route('Dashboard') }}">
                    <img class="brand-img" src="{{ trans('message.logo_not_text') }}" alt="brand" />
                    <img class="brand-img2" src="{{ trans('message.logo_text') }}" alt="brand" />
                </a>
            </div>
        </div>
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i
                class="zmdi zmdi-menu"></i></a>
        <div class="left-nav-btn">
            <a href="{{route('getProfile')}}" class="btn btn-success btn-icon-anim btn-square mr-10 hvr-pulse-shrink" style="line-height: 42px;"><i
                    class="fas fa-user-cog"></i></a>
            <a href="{{route('getLogout')}}" class="btn btn-danger btn-icon-anim btn-square hvr-buzz" style="line-height: 42px;"><i
                    class="fas fa-power-off"></i></a>

                    
        </div>

    </div>

</nav>