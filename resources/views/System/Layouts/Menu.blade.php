<?php
$name_img = "0";
$level = DB::table('users')->join('user_agency_level', 'user_agency_level_ID', 'User_Agency_Level')->where('User_ID',Session('user')->User_ID)->first();
$name_img = $level->User_Agency_Level;
$name_level = $level->user_agency_level_Name;
?>
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo position-relative">
        <a href="{{ route('Dashboard') }}" class="position-relative">
            @if(Session('user') && Session('user')->User_Level == 1)<h6 class="side-user-name mt-2 text-white">{{date('Y-m-d H:i:s')}}</h6>@endif
            <img src="{{ trans('message.logo') }}" class="logo-icon" alt="logo icon">
         
        </a>
        <span class="btn-close-side"><i class="fa fa-window-close"></i></span>
    </div>
    @if($name_img >= 1)
    <div class="user-details">
        <div class="media d-block align-items-center  collapsed" >
            <div class="media-body">
               <center><img src="https://stakingcoin.co/staking/level/{{$name_img}}.png" alt="" width="70%"></center>
                <h6 class="side-user-name mt-2">STAR {{$level->User_Agency_Level}}</h6>
            </div>
        </div>
    </div>
	@endif
    <div class="user-details user-box_id">
        <div class="media d-block align-items-center  collapsed" >
            <div class="media-body">
                <h6 class="side-user-name">User ID: #{{ Session('user')->User_ID }}</h6>
            </div>
        </div>
    </div>

    <ul class="sidebar-menu do-nicescrol">
        <li class="sidebar-header">MAIN</li>
        <li class="{{ (request()->is('Dashboard')) ? 'active' : '' }}">
            <a href="{{ route('Dashboard') }}" class="waves-effect">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-layers"></i>
                <span>Wallet</span> <i class="fa fa-angle-left pull-right"></i>
                <div class="test-eff"></div>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{route('system.getDeposit')}}"><i class="zmdi zmdi-long-arrow-right"></i>
                        Deposit</a></li>
                <li><a href="{{ route('system.getTransfer')}}"><i class="zmdi zmdi-long-arrow-right"></i>
                        Transfer</a></li>
                <li><a href="{{ route('system.getWithdraw')}}"><i class="zmdi zmdi-long-arrow-right"></i>
                        Withdrawal</a></li>
                <li><a href="{{ route('system.getSwap')}}"><i class="zmdi zmdi-long-arrow-right"></i>
                        Swap Coin</a></li>
                {{-- <li><a href="{{ route('system.getTransfer')}}"><i class="zmdi zmdi-long-arrow-right"></i>
                Transfer</a>
		        </li> --}}
		    </ul>
	    </li>
        <li>
            <a href="{{route('system.getInvestment')}}" class="waves-effect">
                <i class="zmdi zmdi-view-dashboard"></i> <span>Investment</span>
            </a>
        </li>
        <li>
            <a href="javaScript:void();" class="waves-effect">
                <i class="zmdi zmdi-layers"></i>
                <span>List & Binary Tree</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
                <li><a href="{{ route('system.user.getList') }}"><i class="zmdi zmdi-long-arrow-right"></i>
                        List</a></li>
                <li><a href="{{ route('system.user.getTree') }}"><i class="zmdi zmdi-long-arrow-right"></i>Binary
                        Tree</a></li>
            </ul>
        </li>
	    <li>
	        <a href="javaScript:void();" class="waves-effect">
	            <i class="zmdi zmdi-layers"></i>
	            <span>History</span> <i class="fa fa-angle-left pull-right"></i>
	        </a>
	        <ul class="sidebar-submenu">
	            <li><a href="{{ route('system.history.getHistoryWallet') }}"><i class="zmdi zmdi-long-arrow-right"></i>
	                    Wallet</a></li>
	            <li><a href="{{ route('system.history.getHistoryCommisson') }}"><i class="zmdi zmdi-long-arrow-right"></i>
	                    Commission</a></li>
	            <li><a href="{{ route('system.history.getHistoryInvestment') }}"><i class="zmdi zmdi-long-arrow-right"></i>
	                    Investment</a></li>
	        </ul>
	    </li>
	    <li>
	        <a href="{{ route('Ticket') }}" class="waves-effect">
	            <i class="zmdi zmdi-view-dashboard"></i> <span>Support</span>
	        </a>
	    </li>
	    @if(session('user')->User_Level == 1 || session('user')->User_Level == 2 || session('user')->User_Level == 3 || session('user')->User_Level == 15)
	    <li class="sidebar-header">ADMIN</li>
	    @if(session('user')->User_Level != 15)
      	<li>
	        <a href="{{ route('system.getfakedata') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> SKC Fund</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getPercentDepress') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Profit Depress</a>
	    </li>
      	 @if(session('user')->User_Level == 1)
       <li>
	        <a href="{{ route('system.admin.getPriceToken') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Price Token</a>
	    </li>
      	@endif
	    <li>
	        <a href="{{ route('system.admin.InvestmentList') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Investment</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getPercent') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Percent Profit</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getMemberListAdmin') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> User</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getWallet') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Wallet</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getStatistical') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Statistical</a>
	    </li>
	    @endif
      	<li>
	        <a href="{{ route('system.admin.getMemberSupport') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Member Support </a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getProfile') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> KYC</a>
	    </li>
	    <li>
	        <a href="{{ route('system.admin.getLogMail') }}" target="_self"><i class="fa fa-long-arrow-right"
	                aria-hidden="true"></i> Log Mail</a>
	    </li>
	    <li>
	        <a href="{{ route('getTicketAdmin') }}" target="_self"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>
	            Support</a>
	    </li>
	    @endif
	    
    </ul>
</div>

<header class="topbar-nav">
    <nav class="navbar navbar-expand ">
        <ul class="navbar-nav mr-auto align-items-center">
            <li class="nav-item nav-margin" id="fullscreen">
                <a class="nav-link toggle-menu" href="javascript:void();">
                    <i class="fa fa-cog menu-icon fa-spin " style="font-size:18px"></i>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav align-items-center right-nav-link">
            <li class="nav-item ">
                <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                    <span class="user-profile"><img src="assets\images\avatars\avatar.png" class="img-circle"
                            alt="user avatar"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item user-details">
                        <a href="javaScript:void();">
                            <div class="media">
                                <div class="avatar"><img class="align-self-start mr-3"
                                        src="assets\images\avatars\avatar.png" alt="user avatar"></div>
                                <div class="media-body">
                                    <h6 class="mt-2 user-title">{{ Session('user')->User_ID }}</h6>
                                    <p class="user-subtitle">{{ Session('user')->User_Email }}</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li class="dropdown-item"><a href="{{ route('getProfile') }}"><i class="icon-user mr-2"></i>
                            Profile</a></li>
                    <li class="dropdown-divider"></li>
                    <li class="dropdown-item"><a href="{{route('getLogout')}}"><i class="icon-power mr-2"></i>
                            Logout</a></li>
                    <li></li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
<div class="clearfix"></div>