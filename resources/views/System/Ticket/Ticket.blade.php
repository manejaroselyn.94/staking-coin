@extends('System.Layouts.Master')
@section('title', 'Ticket')
@section('css')

<style>
    a:hover {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Support</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 offset-lg-2">
        <div class="card">
            <div class="card-header text-uppercase">Ticket</div>
            <div class="card-body">
                <form action="{{route('postTicket')}}" method="post" class="placeholder-white">
                    @csrf
                    <div class="form-wrap">

                        <!-- form-group -->
                        <div class="form-group mb-0">
                            <label class="control-label mb-10  text-left txt-dark">Subject
                            </label>
                            <div class="input-group mb-15">
                                <span class="input-group">
                                    <select name="subject" class="form-control single-select" id="selectCoin">
                                        @foreach($subject as $s)
                                        <option value="{{$s->ticket_subject_id}}">
                                            {{$s->ticket_subject_name}}
                                        </option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label mb-10 text-left txt-dark">Text area</label>
                            <textarea name="content" id="commenttextarea" cols="30" rows="5" class="form-control"
                                rows="5"></textarea>
                        </div>

                    </div>

                    <!-- form-group -->


                    <div class="mb-20 text-center">

                        <button class="btn btn-stk btn-anim hvr-float btn-lg" type="submit">
                            <i class="fa fa-paper-plane font-20"></i>
                            <span class="btn-text">Send</span>
                        </button>


                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-table"></i> List Ticket
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr role="row">
                                <th>
                                    Ticket ID
                                </th>
                                <th>
                                    Subject
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Date Time
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                        </thead>

                        <tbody>


                            @foreach($ticket as $t)

                            @php
                            $findlastRep =
                            DB::table('ticket')->where('ticket_ReplyID',$t->ticket_ID)->orderBy('ticket_ID',
                            'DESC')->first();
                            if(!$findlastRep){
                            $status = 'Waiting';
                            $class = 'warning';
                            }else{
                            $getInfo = App\Model\User::whereIn('User_Level',
                            [1,2,3])->where('User_ID',
                            $findlastRep->ticket_User)->first();
                            if($getInfo){
                            $status = 'Replied';
                            $class = 'success';
                            }else{
                            $status = 'Waiting';
                            $class = 'warning';
                            }
                            }

                            @endphp
                            <tr>
                                <td>{{$t->ticket_ID}}</td>

                                <td>{{$t->ticket_subject_name}}</td>
                                <td>
                                    <button class="btn btn-rounded btn-{{$class}}">{{$status}}</button>
                                </td>
                                <td>{{$t->ticket_Time}}</td>

                                <td>
                                    <a class="btn btn-primary btn-rounded"
                                        href="{{route('getTicketDetail',$t->ticket_ID)}}">Details</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection