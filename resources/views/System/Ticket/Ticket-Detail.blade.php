@extends('System.Layouts.Master')
@section('title', 'Ticket detail')
@section('css')
<style>
    .media,
    .content-cmt {
        margin: 2%;
    }

    .img-cmt {
        border-radius: 50%;
        float: left;
        margin-right: 2%;
        background: #f5a32b;
        padding: 5px;
        border: 2px #f5a827 solid;
    }

    .content-cmt {
        background: linear-gradient(to top, #f5b61a, #f58345) !important;
        padding: 15px 25px;
        color: white;
        border-radius: 5px;
    }

    .info-user {
        background: #f3f3f3;
        padding: 10px 20px;
        font-size: 18px;
        border-radius: 5px;
    }

    .info-user span {
        color: #0a8e88;
        font-weight: 600;
    }

    .info-user small {
        color: #0a8e88;
    }

    textarea {
        resize: none;
    }
</style>
@endsection
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-12">
        <h4 class="page-title">Detail Ticket</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header text-uppercase">Ticket ID:<span class="text-warning"> {{$ticket[0]->ticket_ID}}</span>
                <div class="card-action">
                    <div class="text-warning">{{$ticket[0]->ticket_subject_name}}</div>
                </div>
            </div>
            <div class="card-body">
                @foreach($ticket as $t)

                <div class="deception">
                    <div class="numberID">
                        {{$t->ticket_User}}
                    </div>
                    <div class="content">
                        <h5 class="title">
                            {{$t->User_Level == 1 || $t->User_Level == 3 ? 'Admin' : $t->User_Email}}
                        </h5>
                        <p>
                            {!! $t->ticket_Content !!}
                        </p>
                        <span class="msg-time txt-light">{{ $t->ticket_Time }}</span>
                    </div>
                </div>
                @endforeach
                
                <form action="{{route('postTicket')}}" method="post" class="ticket-comment-form">
                    @csrf
                    <input type="hidden" name="subject" value="{{$ticket[0]->ticket_Subject}}">
                    <input type="hidden" name="replyID" value="{{$ticket[0]->ticket_ID}}">
                    <div class="form-group">
                        <label>REPLY</label>
                        <div class="input-group">
                            <textarea name="content" rows="3" class="form-control" id="basic-textarea"></textarea>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-light waves-effect waves-light m-1"> <i
                                class="fa fa-paper-plane"></i> <span>Send</span> </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
@endsection