@extends('System.Layouts.Master')
@section('title', 'Dashboard')
@section('css')

@endsection
@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card  bg-tranfer">
			<div class="card-content">
				<div class="row row-group m-0">
					<div class="col-12 col-lg-6 col-xl-6  ">
						<div class="card-body  hover-boxshawdow ">
							<h5 class=" mb-0 text-center">Balance</h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0 text-center small-font">{{ number_format($balance['Deposit'], 2) }} USDT</p>
							<p class="mb-0 text-center small-font">{{ number_format($balance['Bonus'], 2) }} TRX</p>
							<p class="mb-0 text-center small-font">{{ number_format($balance['Token'], 2) }} SKC</p>
							<p class="mb-0 text-center small-font">{{ number_format($balance['TokenProfit'], 2) }} SKC Profit</p>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-6  ">
						<div class="card-body  hover-boxshawdow">
							<h5 class="  mb-0"> Bonus TRX<span class="float-right">
								<!-- <img src="assets/images/coin/tron.png?v=1" width="30"> -->
							<img src="data:image/webp;base64,UklGRqIaAABXRUJQVlA4TJYaAAAv/8A/EFVhnbb/+d1G/837nqPj3nvvvffee++9p/fee++9995777333pNz3s/n2z7v+zN8RzMasWzfFVWgjM12gg+yqZztvYkeYf0LGjGPYahNt50ZjZnGML1RYRkGm27XwUHbhTxhFtXMdv0LogldloLOGUPPQRI2rcj4YDFNoKhwCrJoet0maYJMrTHbvqvQmAqZbRcTTEUaQ3k0Iy7mGKVhUeETtL1+2ScyS2Hbdw8Osw0VKqR/QGgb1RhpcpBH0JogmW7vXVCT3pugJrQ3UacLm5oq1KKeoSDJtmnbsm3j2LZt69q2bdvW3muu8/w78bsmuZEkR5LDnZxG1h6R4r4nVRQAAnBsG9m2fTc227YR7e0XzOZqr9m2be9JgiRJbptqPBimDwoGgZ1zd7+h79TzzRfp/P6T7+Dr7WKMcxEWuoB1LmCji7DPBZxy4F114O46+D124L1y4D44+H134P3doOVrayz637m/7wYN04tWy/rgjmHkUl//fyExxBZGoxniAlY6cLsd/M65cC8cOMq6UYHsO+QqXjnC0Bf6Ipzkr+YowHwnedsceBccuJ+sVT9eesMnrnL/yEDXxKY6P376uOg2uOjOuoBfbEjJBetscv1gp9uP/6Eu/ulw8VvvorvCxtXk2354o4DG/fM3K4q8yS7CfhfhCxNRscxHLRG4dPY96U7ypjvwTjr4/jMp1W26acMAJebrkh7lAo46+P4xQb1ki0dVCkai0E+7C7fLgfeDyeotd/+twKOLH5vmwlvkwt1m4nrsKz8vyIghp8xFb7sL+M0+UMKz1w0qovD1dPA7xL7R178vgBj+vy84msAoF3CZfaWfKbNGsDDX3hcUhW+oC3eLfad/W/bhwcG1Ur7AKNj+udmXyt0uMSCIRte9NPtWpe8UBESjy3HhDrKvtUiG3+nx4Yurk8w+V3Kd//A1Lr6xNTgAVOOd/iXm67K34IDQFn/hUz/aK1tyYKjlKyv7kCy4Kxx45zlQ9MQn+A4XYU4CB4wS1vcXnf96iZkcQMr8Rx+Rhbg6F+EhB5K++p2+wcE/8xI4oJTwMJ941RbhANMilXxAFL4GF+ERB5qyPp68iVnURfeHA04liibSDqA5+O3hANQH70+Ybj9+xTvwTnAg6hM/hizRP/5muoBLHJB6/i2pehhcuAccmLp9Bknc5Kt24F5zgOrm9yJINLndinGgqtifkCNXMgesknMRIwrdzLocuKr7NFI4+JZwIOvfKL3mgBadl7nqBjV1c1H5h5I5sJVM49+5VzEOcBWjMP7IUB5uRRMnijwBNwDLMG9rKA62u3uBfbrCxYvItVI+V0/7XLCMHFFp5ulOgx+NIBuCm7ZBPkbR1MpCwKlowgJ95Ipz96TAeuv5H2PWz6BqaO8yFiyuS+eTY4jVjtrTsLsjJZDMcJN+iEQHv72syMWKiBXr8jnkeHy1i3gaebPFfEPwOGISzfFKViXVyunKmZ8Ts9XuQmHMldbFJwTLUdSci7WEMlfMIlZeV00np6SLvfjCeIutmzcIEF/k40052DV4mI+kMFZN10j9nL5Z7MvpJpltPT6toPBGVzLzm3TEFyaeLVZd10z5nLWeHcDqpppovd2tQIhHGCEKMJ81kONS+cTq0bmr2MGMZoaR0vf18v3xRBOxRT3BtQOZndWnC5aRw2jMbP1lwLPl75DcO/VHljWFVkfebFavPrL9PN1pHRcd/wZl/1E32gLr56tgWhAOznbclvw6OMvUjIN/5rIu9hgIAi5RyA6pM+etJHre2jxtl7GQM+b7+PBYXyvR5KtM0MZAZ8sKtNtw4N7uYZrx0vbsdULPW1VOwhK7DwXfDc4SdKbmVNaZU3T6ZiF6zE423ThWPT3C5FNlv3Z3pkZxby1vZZyKJnw2OHtiZZ3jbY0s0BGsQFfMIkMcLmiRTzTX9uF2p6UIMCzXMDjr4Rl8c7xSXz5hS530er+EJ/cC5awQI+r6DcZebnsIhpOyRBUtb9279f5tbqqJUsYf6Xl/oQvN6ZTH5ylw/cCTeAqGyCEamo201XYeD8cRqMvlEqtF56pmBzW66UdLX9fLBwmamqjGemkArp0jKA08gYfQGxYrijyVnn5MsbpsLtHz1jaH0plZB1L/F4v4oSdF9z9qaKardwDGZ5FjiRQ8eztqTyylpCEP1vyd6ahSla7nrYsWk6Xm7k6DHi/KOcr/oYM6rFuHUxsvZ6tjXb+fNHgrHX+hsbQGOVlz9qbDq9SFS5mety5ZQI4mNWsNvT+aEz3q6PAzJ2tnY+vVZv1cQT7r1U3ULpx+L9fMg2XN81YyPW9dLudzbKGqMML2yEIAxUhEhjoGsupSLlFIvOzb6Xi09XbVdGK9qq3sbuj9DrQNzv79hFy1m0i4Q+dfx28W+J3YgPZnc17+pjnfBp6wh+q4Ynp8gq3243BnbGLv0fLWVTLIyRlqzz437lJrTOq4k6qzpbQJhtsd1qvPYn4TPFuFWY+6fYUJ5tvePO7UVHG19I+Wt1JP1yL26QoTzxY6g7PSiWq0YyP6Wy+HVRvm5p3gqdpRe2Bt6vIDxlpp2gZnZ2lgt63ZgcbRTq1+MLcZ8ni5QjZ5OXOjc9YweKl8FutUzPcZeXMU6XT5HKLNg9XP1SJQrvdwFZZlM/R/saxX048UZh7gfF0mj8DnxkBnS/vte9i98fSmL9Dz1oVKydaz96fCho9lVYpnjVWPVvRyPL7iWI210qAhcp4qZuJOkTT40ZqnOx1JYT5Sz1vVjyJT83alIQ6XQQ/Wv60hTzSBUWyK3wfftZ4fAf0Q1p6YrUwNGwY8W7c5jMasqOetsnft82axKZWRp4kx6oGHx1ocpwObDTwzBzEZNqe+r1eDEueuYjpeXCnzM19nMla0LV84z+aU18vpm4U3Gn1itoInRuq8aLXoa962tbr35oRcdTs2o/fJ8vUGmXq8WK8GOh0cp8Wg9zF1nKXGprXYJDPlGX+TqujGrWmob4Fs1wg2SKnNvezJ79ijgU/X5XI+1qvL5pJyTEBdv5/P2rM/PG9plReH1hozg9J15Xg2m9ShdAYYHOzVZ70HDhS6fwGmoc6/zuhrLc+JOKq49HH+Cjby5jDBs+VaBSUYZezlZr2C5+CGyJ2BUJAh5htG3Bo7j4f6aZKuyz37g4EHXEJDOWerUZov5+XAZgM/yCH0Bp6VA1ock1LzoffHWkeVmatkfGRefG85Ei5Y0w1z9uV0Xi6VT3LAq+TkDAVPytzdiYmpiNyzqLj+H+6xMu2S2DArW68mmW3wgxRKBbQYc3XQYlvpx1DeXh6ANeu3xLhwu9i0gCPlTQw1+NGAllTZ8pQYc6Uhv3ZEpXmBcCg+8WzRzN3FSSY/rgM9OWjxjbCsDPiXn7OGNaLDcLsDsU7343Dc2z2cgCusSAc1uq7eQW8Lr0oiXHhT2Di/AO4Hsw4URrSzRIBnpFNzKrRB8jw+zLHaCvoroKW3JevUeuJYi3mdnKGAy40xHVpr4Bm5HhF+9yIlzAKapnjHd2doZkL/1B6CIcVYDCYGdcZLCDBHXwL+oT5uAfWnIHmps/cnEpQ5byVBLNrOvg57VWE7oevytBShMbnlJZ+KsykToPsXAF7nnScSY+r5EaAF3aIthZ+rNXJyszGikbZabaGreq6exNq0KY4D7yRT0NElCgRgGNWNrpBNwPlo0O/lMk6501AEPLVpAodOtoMZjbEncFO8R2hdEkwxVWAAhlFNMtvgMOnstcy08zzm+5yAp+B5/eJniIci6aIj/fd1VaUsgfEoJsEvA6tgH+7AuHYRT8j1YPYvb35UmUHikjIX8Mp5rUi7jgTWo0fhWVY0VMJLM5FHcGt4NvbpCkY5lNYgjobBjofkPf/fRcd2ujOxNipGhDFWm/VqrJUmuhyRG8Ysg8kgb7IW+PaH3xksqQHO10ICLlxC9Py7xTaCuPing4mQVNFLVRaolwe4UCmBIbTFjZHHwk+uYMO8PLvgWIm16Ich38ZUtDePA3FnYax0qIMBvxXpOQ0xb1dCxlWTzjT5oMWsA2kbK2D6kaKHb0P8zWQY/GhYr6YeLyzS5FMF/uUlsxlh+tECP/W9Zxoq0tf7dKMl3gJp/oo3ga3RmY5KgkcMC7WbSEAGzEkGmHi2tLAWuPWln/edrISmnCysR9f3sgkhdtoawCizHgt1eJWBJ2Sv3qCfOyCW9posx4Tz7UyNzMroNFSRoolNTGTaKCdUADNgnu4k4Tg9LUXAU9JAN8PsDSRpbf82x1IUPg1FSAZkrqs5C6cgE9KJOMrLQixWh68AxOs1jnmgs5VuLYzqyzDKxjgZU1hJzd2TWBcFvcV1yZR4hfVKJn1u2+IW5KdoDRf2db3aw1N5LJFKkWDYvRGb5CBJJ41ZSPePYxWmROMqXnYXCiyhKScLfHycqZG9XB+FzlXNrAXeDIn0ozZHlRmFWFRv98D6tIq3wIaUjqg0IN6iNno6qlRp49bIWOpuYqMt//ICh0Nq1a/AWFx7Oc6GtJhorlmvtlT5P7F2j4GgibYnYyr4Oc9Ry+qxgGfcMUOQklOr/K4jwSLUYp1sGEdpWmQhIJ/KU6KLT0CS9KeaaHp+grxIzVIft8A4PT6tfbid19+I3JJuxK0yEL5Cb6SvdBiGlX+caekAVgeG2BXkHsvN4INxWx1GzOFVCvGoX1dwZ8sjSuM+CUusxPzLZ6tjFkFzqPvHK4dhDLGFTIwRtof1alWW0pJV4NVRRsf/H0xgwv04bXfclvKJ/IxjLzeO1T5dwSI0Zc0qHoa5mJqAd3QpltM04wUZGf6yekETvERRY7XCHH1JlLZ8tjr2Td6hKRaKO1c1K6WbXGHoAlaS46ngfMmaWHvyO2QcpaucJeOzUPNKVNVVu0GS95t4/gqG/bPD7wzWzQ5h6MDtJkd565W0kXh3a4F/WokZhwu8kt4z1UTD3IutBa++Ys7eBO9sBzcYi7Amayd7GBZgcjoNRYCMA2kHCbST7r2yiqmJBFf/FjEwflWUmb+TttRWqvj5bhX0UyAMXbgXtEAT794jPfzr82Z9JCw/HOx4SDP6WkNMzZ3HA7yZn5itRDn5ebJhOW8fiRDfhvVzo7Aj06MQcBS8iWW1Ujz0oK+tbGruzePYw8bHFCuRVflNeOABHX6/iU2o44OZoPJ6OVOjUEidUEzPH+h0pIvyFssfXqUEmRYZ+7e5MqKfAeFkTGUmE/XBDr7eChDIwmuj7J+09iAmJ/W1RJJOl3LRifsYBzW6VIGBcUidySn6lGkIJfq5AjZC1mrymM3CU/YxueggX6R5u5KYesvAj3QcgaoQG1HZj8PFCwyMIypNORk/h+nQbrWiTFFVgb3A8spWEl5HohgFN0Yi6SdnqFL83Xv2uRKiiOJmLNT8ncki1DfWk8UFrCNJYetVW5UsDXhzKVue8WE14kg6YxO7dVfvsKtoENUqLj/SVpPxSlXBnA0dTHGdGWBJktTMy8GMhhU0ysbIgDZ1I9zURMbPfd6staqLDAwpeyrldC0CdzaYomEmk9Q+3AGk3SoZF9tbC6oVosiDmprQN7cnv2sveNV60pkmPcJHsrDYGJnfR5PNrFdTTBUdmYrW3lWmi0kLUT7RdKMliaUYdne0sEgwejFzfJ+D3zWmqdZeji5RaqH0+sIg0Z79wUqq4gqyo+nOv84yFmH8hWayCum/ibLzeACDpHXU3Ipnr2VIPNYbs5HjEoVsR+3hl1Vi0djwxSD/7eD3hCh93iwwVpyzN7GS8ueDnqWczLexUuywm0gox/LGyb0RzlfBspmkWQ0mquVBFwJWElaoe+YGUW7KyfIeqbLbrVUu28bbYKOxUTeG2Ur011Llh61XaoNh9GI7FU1kyBTcXk8ygUrG2bAkG+W1Laly66tkfDSlAN1VodT23/u6XqrufYROv2y4F8cGbEraetIto0oUefJKcu8VVSOna/8N9ogeZm+wWTagh9gdVIkV1bi1XBJ9bPcaRY4sV3irI9O0NITOnnRvUi8xrC7mbzUkDU07VizC6VpEimlavpbJ6o5IOwJV3T1ewK/GWqLKPKMs1r7KdAky82tr0OXuSDsCZW2FUnKSmcI6lGYtlopjnBqLMl3VBgEGDZ17EIq/UY/rbFXUFmfzLPrfhFkT+MPKySCZmdv6jZoyc/OXQLhJTgL8d27CXBfmAKpHGDVnZCX9vkXYngmQ+/uYsECuVZoyxxArLzV1IDTPjilWTIHvyzSJthtqqnIhAHA1ttVEm3gs9rQtCW7QkDI9P0KqpodDdS+DnCw9ZCEATS+efKowCRq6gHVMWcCzcvpmoTpAu7eXetr7fB5aa5gG6UWNom98WFiNXwDfziQ9dYepmDvqu4lQtBppXl4CtCPQ0rv3K1iABlfb2MuNiVAtK5NWBnAMqVHTy6mpQgv7tTtrsSxcKmR9sAZotCNQYkftwctdtdS13Bvh3FXs5WR4cEd1iLQj0BGj2LfT6XDlYK621JWYDB1j8y7ICs0Ie0EOFWqBbpUaOKTOWIQ9+xyT4UZhWIA2rWA7AhVOxlTaKiUmmSkW4ZE56FAgLu+KtjqBdgQa8vSkM8bk8muWpjTFTfbYvDvi7DyRYDsCBap4wbLx5fPVEHaWCEyIHcIwF3H6ul6wHYFCayXrJYc+54Q9JV2QasmaKy7vlrhOyhKgHYE8gx0PUJfNivRxC5fNJVg0pr/LxZQoHpd3TZynAk96kjTjLjXgdFHFU3UNQ3mq6M+7DksTp1G8aprp9l5OwBUKIL38oBeDFKW9dRfE9SbQjkCaUl4OpTNqDHC+sJ5bZXUU1eivu3gidSaZbbAdgQhhssXePE5t9FrRIkwzXpgWT/TUXVEnxwsUw6NNgW9G1/Ru7ZkYq4C6O+JqC9sRqE1cUVOFIQ4XFpP+m7U1obvuLixInZywHYEky3nJqdJxo6RFmGC+MTEKmq+71Tw5T1U5Ut7j5RdYKUkX62ZNjToU6q61Ogs3l6uJ2dJ6ya9w1b4Hza8hxyag7p48A50OmBupUqZ1iUKm0G6ntqn8Gr1192ET8gfID80rxULAkpbnICZnLL9Gb9+F8NvIszBIESkkw47bEvgvUZjUASHtl+nxbaK+K34IwMzTnRRcqfWljdLlEFoMvT+YHj8s6rtDOwAjakcgUzaTR8PElo9gzWjuuxNmUkKx3LK8/LBr1oGknl9zWopIoT0T3aPIU6oEaEcgH3T8LPX8moFOBxPkUYK+a9S1NMggl4gkWS/ryE5phbAwE6TuEsK+e/4IwGwpnZuxmBRzdyeLcCKOYoo0Ffdd9EcAZv82J9uqrIqcs+HfsTpLmjOSbSpouv0SZvZFACZfBckbcry0swEpYabISxqK+q6Sp5VsO4IbKnhli1iEo0uUL+agexSTF2gNsZSIfbidl8OpjUzlTgZC9UI0eZSw7/JbmP0RgKnS9fuRa8q4d4+TcDa0tgiTzjQmyVsqyfTd9kkAppSA50h73Ra2CFWZJneX6rvukwDMc+TCklNMFXFV4nuwaYZuTZTfkuu7Tz0AI9eOoKyXkbaairNhzNXBNHms5LwL5AMwMt0Bftl6EebPHsRkLMJ+7Y6J8kqpJYMmMPkAjEw7gpySXrfPsmgfGaIkfJ7kvCt+CcAMdjyUvW7dv0Az7X1kqMy7Eq5LnhwvkEiw/ia5DhW1rMUalFFlXel5l3wSgGmfJDat6gvz8JDi+wpU+Xr5ebfoB2DEGdbbycQTymBrWYgf7HgwVd4XyupnyB+1xe0IqnrZfSjgTVvwPjJU+RnleffoV6JcLufzy0g6hUTLkNtYhJMzVBJZnOSNVJl30S8BmCkni3CymlWxqTWw6TevkE36vVxMlX9bQ3neTR9UohxDrCBpXlohRTUnYyqLsAKTZVn1eVd9UImS0ePTAuNr6wUpIHmTlj4ytOddDdv5JQAzd3cSeB1bJMkU1bygA13aqc677JMAzKloQuB1vHAJQYtqVDqT+mTe5fBOhFCMpOT3MP1IATEV+FjC+8jQ5U465l33RSVKHrw7SVPQlDQV7SNDeAUyWubd90Ulyt/iM7Yf2GwU+sj4ct79sA5xxllq1qvhdgeaS51H3ILiEUyXOnrW3UCbKPKAjhH7szu0Q+cKwj4ytbP5e90N4TuZtvbldGhf5opeJp4touax7xllY/hx3RV6YzBUJqi8EXMSuNEu6ZmzCGFh36+7I/yLljQPoa1RK9bEtl5i24qviufX+H/dJeEraVME6cs8zN4AruUKgvyaVpTX3aJz3TWk6ft6gRvsDCMFNg9KZ14RnTMsENZdEz6Btg/6i2GlwZy9CTTd4sXxGWMCYd09Ybg+aVYAj5cyYJx9PL7q8AKE4gOdDr+vu8gfmVjrgFnUd9QeQO+wqvUtwlaBse6m8B+/ms6hkGuXPQ34odH8GtLrrjKw7i7CTDbdYDqNFSqtz5sVIOvuCsOHESbm+5S1sppxuDBdHhZKyccW+EFMTpajSxTldbeZWncdXUbdGJKk9/wIlNddZ27dfWS1jRzfFDjr7gvDohRRabF7KJ0JoHUXhokfJMsgJ0uG9o0or7vR7LorqSq7mHuPtdIor7vS8Lo7qbKj9iRmf3ZHed2dxtddSpQO74nDB/k1zLe/JYV1t9KUaNEIqbcjvO7WwFp37QwjRcCe/I7wumuDa9292fBlpDyS7FJNk/8kyNZdfHiVwrhKBvkw3XUXB9q6mzdG+8jQXXdzsK27OqU65Hlk82sceIsDa93dwijz5XMI1fyauk8LvHWXN/aWHLaYcrKQXXd5AK67vfGq56tg9grZJO8I24PsutsDct31v0x43fURf939EX/fBRF/3w0LXvddEYaJrwy+fXe8MjHS77tkwe++W8IwMQowP7j2XfPExEi/757Iv+8iX+m5wbPvpudG/H1XRfx9d0X8fZf5VtUCYt9tEX/fdZF+332Rft+FEX/fjZF+35VBs+/OJv6iSZk1Ivy+SyP+vlsDTA39sO/ahgG4794fp8uP3/23AnLfxetR3XfxepXC4NSnUtx386dG+n1XB7Ni/vsXRmbf3ZF93+WRfd/tC473Xe/bffdnfl/u/3bwewL33d/sv3N/X6Zf990fAg==" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0   small-font">{{ number_format($balance['Point'], 4) }} <br>~ ${{ number_format($balance['Point']*$rate['TRX'], 2) }}<span
									class="float-right">Rate: ${{ number_format($rate['TRX']*$percentDeposit, 4) }} <i class="zmdi zmdi-long-arrow-up"></i></span></p>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-6  ">
						<div class="card-body  hover-boxshawdow">
							<h5 class="  mb-0">Total Profit <span class="float-right"><img
										src="assets/images/coin/dollar.png" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0   small-font">{{ number_format($total['Profit'], 2) }} USDT</p>
							<p class="mb-0   small-font">{{ number_format($total['ProfitSKC'], 2) }} SKC</p>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-6  ">
						<div class="card-body  hover-boxshawdow">
							<h5 class="  mb-0">Total Commission <span class="float-right"><img
										src="assets/images/coin/dollar.png" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0   small-font">{{ number_format($total['Commission'], 2) }} USDT</p>
							<p class="mb-0   small-font">{{ number_format($total['CommissionSKC'], 2) }} SKC</p>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-6  ">
						<div class="card-body  hover-boxshawdow">
							<h5 class="  mb-0">Left Sales <span class="float-right"><img
										src="assets/images/coin/binary-tree.png" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0   small-font">{{ number_format($branch['left'], 2) }} USDT</p>
							<p class="mb-0   small-font">{{ number_format($branchSKC['left'], 2) }} SKC</p>
						</div>
					</div>
					<div class="col-12 col-lg-6 col-xl-6">
						<div class="card-body    hover-boxshawdow">
							<h5 class="  mb-0">Right Sales <span class="float-right"><img
										src="assets/images/coin/binary-tree.png" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<p class="mb-0   small-font">{{ number_format($branch['right'], 2) }} USDT</p>
							<p class="mb-0   small-font">{{ number_format($branchSKC['right'], 2) }} SKC</p>
						</div>
					</div>
					<div class="col-12 col-lg-12 col-xl-12">
						<div class="card-body  hover-boxshawdow">
							<h5 class="  mb-0">SKC Fund <span class="float-right"><img
										src="assets/images/coin/binary-tree.png" width="30"></span></h5>
							<div class="progress my-3" style="height:3px;">
								<div class="progress-bar" style="width:100%"></div>
							</div>
							<h5 class="mb-0 text-center small-font text-info">{{number_format($data_set->Value,4)}} SKC</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-border">
					<div class="card-header">Maxout Data
						<div class="card-action">
							@if($total_invest > 0)
							@if(($chartMaxout[0]['value'] + $chartMaxout[1]['value']) >= ($chartMaxout[2]['value']*3.8))
							<div class="text-warning">  
								<div id="calltrap-btn" style="cursor: pointer;" class="row b-calltrap-btn calltrap_offline hidden-phone visible-tablet">
									<div id="calltrap-ico"><p><i class="fa fa-bell" aria-hidden="true"></i></p></div>
								</div>
							</div>
							@endif
							@endif
						</div>
					</div>
					<div class="card-body">
						<canvas id="chart-area"></canvas>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="card card-border">
					<div class="card-header">Maxout Data SKC
						<div class="card-action">
						</div>
					</div>
					<div class="card-body">
						<canvas id="chart-area-skc"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row mt-5">
	<div class="col-lg-12">
		<div class="card card-border">
			<div class="card-header">
				<i class="fa fa-table"></i> Investment
				<div class="card-action">
					<div class="text-warning">TOTAL: {{ number_format($sum_invest, 2)}} USDT</div>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table id="dt-dashboard" class="table table-bordered">

						<thead>
							<tr>
								<th>ID</th>
								<th>Amount</th>
								<th>Time</th>
								<th>Expired Date</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@php
								$package = DB::table('package_skc')
						    				->whereRaw($totalInvestSKC." BETWEEN package_Min AND package_Max")
						    				->select('package_Interest', 'package_Date', 'package_Name')
						    				->first();
								$timeExperied = time();
								if(isset($history_invest_skc[0])){
									$timeExperied = date('Y-m-d H:i:s', (strtotime('+'.$package->package_Date.' days', $history_invest_skc[0]->investment_Time)));
								}
							@endphp
							@foreach ($history_invest_skc as $item)
							<tr>
								<td>{{ $item->investment_ID}}</td>
								<td>{{ number_format($item->investment_Amount, 2) }} SKC</td>
								<td>{{ Date('Y-m-d H:i:s', $item->investment_Time)}}</td>
								<td>{{$timeExperied}}</td>
								<td>
									@if ($item->investment_Status == 1)
									<label class="label label-success">Success</label>
									@elseif($item->investment_Status == 2)
									<label class="label label-info">Expired</label>
									@else
									<label class="label label-warning">Cancel</label>
									@endif
								</td>
							</tr>
							@endforeach
							@php
								$package = DB::table('package')
						    				->whereRaw($sum_invest." BETWEEN package_Min AND package_Max")
						    				->select('package_Interest', 'package_Date', 'package_Name')
											->first();
								$timeExperied = time();
								if(isset($history_invest[0])){
									$timeExperied = date('Y-m-d H:i:s', (strtotime('+'.$package->package_Date.' days', $history_invest[0]->investment_Time)));
								}
							@endphp
							@foreach ($history_invest as $item)
							<tr>
								<td>{{ $item->investment_ID}}</td>
								<td>{{ number_format($item->investment_Amount, 2) }} USDT</td>
								<td>{{ Date('Y-m-d H:i:s', $item->investment_Time)}}</td>
								<td>{{$timeExperied}}</td>
								<td>
									@if ($item->investment_Status == 1)
									<label class="label label-success">Success</label>
									@elseif($item->investment_Status == 2)
									<label class="label label-info">Expired</label>
									@else
									<label class="label label-warning">Cancel</label>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-animation-11">
  <div class="modal-dialog modal-dialog-centered">
	<div class="modal-content animated tTRX" style="background:#1f2429">
	  <div class="modal-header">
		<h5 class="modal-title">MAXOUT INCOME</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
			<div class="text-warning" style="margin-top:20px">  
				<div id="calltrap-btn" class=" row b-calltrap-btn calltrap_offline hidden-phone visible-tablet">
					<div id="calltrap-ico"><p><i class="fa fa-bell" aria-hidden="true"></i></p></div>
				</div>
			</div>
		<p style="margin-top: 4.5rem;text-align: center;color: #ffe500;">You almost reach MAXOUT. Please invest more.</p>
	  </div>
	  <div class="modal-footer" style="margin:auto; text-align:center">
		<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	  </div>
	</div>
  </div>
</div>
				

@endsection
@section('script')
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<script>
	$('#dt-dashboard').DataTable({
          "bLengthChange": false,
        "searching": false,
          "paging": false,
          "order": [0, 'desc']
      });
</script>
<script>
	$(document).ready(function () {
		$('#calltrap-btn').click(function(){
			$('#modal-animation-11').modal('show');
		});
		
		$('.deposit').click(function(){
			_coin = $(this).attr('data-id');
			getAddress(_coin);
		});
	
	
	function getAddress(_coin){
		$.getJSON( "{{ route('system.json.getAddress') }}?coin="+_coin, function( data ) {
			Swal.fire({
				titleText: "Deposit "+data['name'],
				html: '<div class="pay--attention">PLEASE COPY WALLET ADDRESS OR SCAN QR CODE AND THEN WAITING THE SYSTEM PROCESS YOUR REQUEST...</div>'+
						'<div class="send--to">Send to address:</div>'+
						
						'<div class="the--address"><b style="font-weight: 900;">'+data['address']+'</b></div>'+
						'<br>'+
						'<img src="'+data['Qr']+'" style="width: 90%;"></img>'
			})
				
			$('#inputAddress').val(data['address']);
			$('#ImageQr').html('<img src="'+data['Qr']+'" style="margin: 0 auto;"/>');
		});
	}
	
	
	
	});
</script>

<script>
	$(document).ready(function () {
		var config2 = {
		type: 'pie',
		data: {
			datasets: [{
				data: [
					{{ $chartMaxout[0]['value'] }},
					{{ $chartMaxout[1]['value'] }},
					{{ $chartMaxout[2]['value'] }},
				],
				backgroundColor: [
					window.chartColors.green,
					window.chartColors.yellow,
					window.chartColors.red,
					window.chartColors.white,
					window.chartColors.orange,
				],
				label: 'Dataset 1'
			}],
			labels: [
				'Interest',
				'Commission',
				'Max Out'
			]
			},
			options: {
				responsive: true
			}
		};

		var ctx2 = document.getElementById('chart-area').getContext('2d');
		window.myPie = new Chart(ctx2, config2);
		Chart.defaults.global.defaultFontColor = "#ffff";

		var config3 = {
		type: 'pie',
		data: {
			datasets: [{
				data: [
					{{ $chartMaxoutSKC[0]['value'] }},
					{{ $chartMaxoutSKC[1]['value'] }},
					{{ $chartMaxoutSKC[2]['value'] }},
				],
				backgroundColor: [
					window.chartColors.green,
					window.chartColors.yellow,
					window.chartColors.red,
					window.chartColors.white,
					window.chartColors.orange,
				],
				label: 'Dataset 1'
			}],
			labels: [
				'Interest',
				'Commission',
				'Max Out'
			]
			},
			options: {
				responsive: true
			}
		};

		var ctx3 = document.getElementById('chart-area-skc').getContext('2d');
		window.myPie = new Chart(ctx3, config3);
		Chart.defaults.global.defaultFontColor = "#ffff";
	});
	
</script>
@endsection