<?php


return [
    'title' => 'Staking',
    'icon' => 'assets\images\favicon.png',
    'logo' => 'assets\images\staking_logo.png',
    'logo_not_text' => '',
    'logo_text' => '',
    'logo_wide' => '',
    'bg-form' => '',
    'bg-cover' => '',
    'style-image' => 'width: 100%;',
    'description' => '',
    'name_auth' => 'StakingCoin',
    'url_upload' => 'http://media.stakingcoin.co/',
    'url_web' => 'https://stakingcoin.co/',
    'name_coin' => 'ADA'
];
